(function ($) {
    'use strict';

    var sposWindow = $(window);
    var sideNavWrapper = $("#sidenavWrapper");
    var sideNavUserWrapper = $("#sidenavUserWrapper");
    var blackOverlay = $(".sidenav-black-overlay");

    // :: Preloader
    sposWindow.on('load', function () {
        $('#preloader').fadeOut('1000', function () {
            $(this).remove();
        });
    });

    // :: Navbar
    $("#sposNavbarToggler").on("click", function () {
        sideNavWrapper.addClass("nav-active");
        blackOverlay.addClass("active");
    });

    $("#goHomeBtn").on("click", function () {
        sideNavWrapper.removeClass("nav-active");
        blackOverlay.removeClass("active");
    });

    blackOverlay.on("click", function () {
        $(this).removeClass("active");
        sideNavWrapper.removeClass("nav-active");
    })

    $("#userNavbarToggler").on("click", function () {
      sideNavUserWrapper.addClass("nav-active");
      blackOverlay.addClass("active");
    });
    $("#goHomeBtnUser").on("click", function () {
        sideNavUserWrapper.removeClass("nav-active");
        blackOverlay.removeClass("active");
    });

    blackOverlay.on("click", function () {
        $(this).removeClass("active");
        sideNavUserWrapper.removeClass("nav-active");
    }) 

    // :: Comment Reply Form
    $(".reply-comment-btn").on("click", function () {
        $(".reply-comment-form").toggleClass("show");
    });

    // :: Hero Slides
    if ($.fn.owlCarousel) {
        var welcomeSlider = $('.hero-slides');
        welcomeSlider.owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            dots: false,
            margin: 0,
            nav: false,
            smartSpeed: 1000,
            autoplayTimeout: 5000
        })
    }

    // :: Catagory Slides
    if ($.fn.owlCarousel) {
        var opiniSlide = $('.opini-slides');
        opiniSlide.owlCarousel({
            items: 3,
            margin: 15,
            loop: true,
            autoplay: true,
            smartSpeed: 1000,
            autoplayTimeout: 3000,
            dots: false,
            nav: false
        })
    }

    // :: Catagory Slides
    if ($.fn.owlCarousel) {
        var videoSlide = $('.spos-video-slides');
        videoSlide.owlCarousel({
            items: 1,
            margin: 10,
            loop: true,
            autoplay: false,
            smartSpeed: 1000,
            autoplayTimeout: 3000,
            dots: true,
            nav: false
        })
    }    

    // :: Catagory Slides
    if ($.fn.owlCarousel) {
        var itemSlide = $('.item-slides');
        itemSlide.owlCarousel({
            items: 2,
            margin: 10,
            loop: true,
            autoplay: true,
            smartSpeed: 1000,
            autoplayTimeout: 3000,
            dots: false,
            nav: false
        })
    }    
    // :: Editorial Slides
    if ($.fn.owlCarousel) {
        var editorialslides = $('.editorial-choice-news-slide');
        editorialslides.owlCarousel({
            items: 1,
            margin: 0,
            loop: true,
            autoplay: true,
            smartSpeed: 1000,
            autoplayTimeout: 5000,
            dots: true,
            nav: false,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut'
        })
    }

    // :: Owl Carousel Slides
    if ($.fn.owlCarousel) {
        var newstenowlslides = $('.newsten-owl-carousel-slides');
        newstenowlslides.owlCarousel({
            items: 1,
            margin: 0,
            loop: true,
            autoplay: true,
            smartSpeed: 1000,
            autoplayTimeout: 5000,
            dots: true,
            nav: false,
        })
    }

    // :: Tooltip
    if ($.fn.tooltip) {
        $('[data-toggle="tooltip"]').tooltip();
    }

    // :: Jarallax
    if ($.fn.jarallax) {
        $('.jarallax').jarallax({
            speed: 0.5
        });
    }

    // :: Counter Up
    if ($.fn.counterUp) {
        $('.counter').counterUp({
            delay: 150,
            time: 3000
        });
    }

    // :: Prevent Default 'a' Click
    $('a[href="#"]').on('click', function ($) {
        $.preventDefault();
    });

    // :: Password Strength Active Code
    if ($.fn.passwordStrength) {
        $('#registerPassword').passwordStrength({
            minimumChars: 8
        });
    }

    // :: Animated Headline Active Code
    if ($.fn.animatedHeadline) {
        $('.built-with-selector').animatedHeadline();
    }
    

  var $window = $(window);
  var $videoWrap = $('.video-wrap');
  var $video = $('.video');
  var videoHeight = $video.outerHeight();

  $window.on('scroll',  function() {
    var windowScrollTop = $window.scrollTop();
    var videoBottom = videoHeight + $videoWrap.offset();
    
    if (windowScrollTop > videoBottom) {
      $videoWrap.height(videoHeight);
      $video.addClass('stuck');
      $video.attr('id','floatingvideo');
    } else {
      $videoWrap.height('auto');
      $video.removeClass('stuck');
      $video.attr('id','video');
    }
  });

    $(".loadmore-frame .content-box").slice(0, 10).show();
    $("body").on('click touchstart', '.loadmore-frame .load-more', function (e) {
        e.preventDefault();
        if(e.type == "click") {
            $(".loadmore-frame .content-box:hidden").slice(0, 5).slideDown();
            if ($(".loadmore-frame .content-box:hidden").length == 0) {
                $(".loadmore-frame .load-more").css('display', 'none');
                $(".loadmore-frame .load-more-arsip").css('display', 'inline-block');
            }
        }
    });
                
    $('#button').on('click', function() {
      $('#promosi').remove();
      $('#konten-premium').show();
    });

    $(window).on('scroll', function () {

        /**Fixed header**/
        if ($(window).scrollTop() > 250) {
          $('.is-ts-sticky').addClass('sticky fade_down_effect');
        } else {
          $('.is-ts-sticky').removeClass('sticky fade_down_effect');
        }
    });
    document.addEventListener("DOMContentLoaded", function() {
        var lazyloadImages;    
      
        if ("IntersectionObserver" in window) {
          lazyloadImages = document.querySelectorAll(".lazy");
          var imageObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
              if (entry.isIntersecting) {
                var image = entry.target;
                image.classList.remove("lazy");
                imageObserver.unobserve(image);
              }
            });
          });
      
          lazyloadImages.forEach(function(image) {
            imageObserver.observe(image);
          });
        } else {  
          var lazyloadThrottleTimeout;
          lazyloadImages = document.querySelectorAll(".lazy");
          
          function lazyload () {
            if(lazyloadThrottleTimeout) {
              clearTimeout(lazyloadThrottleTimeout);
            }    
      
            lazyloadThrottleTimeout = setTimeout(function() {
              var scrollTop = window.pageYOffset;
              lazyloadImages.forEach(function(img) {
                  if(img.offsetTop < (window.innerHeight + scrollTop)) {
                    img.src = img.dataset.src;
                    img.classList.remove('lazy');
                  }
              });
              if(lazyloadImages.length == 0) { 
                document.removeEventListener("scroll", lazyload);
                window.removeEventListener("resize", lazyload);
                window.removeEventListener("orientationChange", lazyload);
              }
            }, 20);
          }
      
          document.addEventListener("scroll", lazyload);
          window.addEventListener("resize", lazyload);
          window.addEventListener("orientationChange", lazyload);
        }
    })
    
})(jQuery);