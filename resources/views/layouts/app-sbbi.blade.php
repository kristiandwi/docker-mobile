@php header('Access-Control-Allow-Origin: *');
@endphp
    <!-- Google Tag Manager (noscript) -->
    <!-- End Google Tag Manager (noscript) -->
    <!-- ads frame top -->
    @include('includes.sbbi.header')
    @yield('content')
    @include('includes.sbbi.footer')
    
