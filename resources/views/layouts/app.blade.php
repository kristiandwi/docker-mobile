@php header('Access-Control-Allow-Origin: *');
@endphp
<!DOCTYPE html>
<html lang="id-ID">
    @include('includes.header')
    <body>
    @include('includes.header-area')
    @yield('content')
    @include('includes.footer')
    </body>
</html>