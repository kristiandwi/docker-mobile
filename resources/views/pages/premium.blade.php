@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper">

      <div class="container">
        @if(Cookie::get('is_login') == 'true')
        <div id="konten-premium" class="premium">
            <!-- User Meta Data-->
            <div class="alert alert-primary alert-dismissable mb-30" style="padding: 5px 10px;"> 
              <h6 style="margin:0 0 10px 0;font-size:13px;font-weight:400; display:block;">
                <a href="https://id.solopos.com/profile">{{ Helper::greetings() }}, <strong>{{ Cookie::get('is_name') }} !</strong></a>
              </h6> 
              <div class="user-meta-data d-flex">          
                <div class="user-content" style="padding-left:0;">
                  <p style="font-size:12px; line-height:15px;padding-top:10px;">Selamat datang di ekosistem SoloposID. Silakan kelola akun Anda melalui Dashboard SoloposID.</p>
                </div>
                <div class="align-items-center" style="padding-left:5px;">
                  <img loading="lazy" src="https://id.solopos.com/images/logo.png" style="width:120px;" alt="Solopos.id"> 
                  <div style="text-align:center;">
                    <button class="btn btn-success btn-sm" style="padding:.25rem 1rem;"><a href="https://id.solopos.com" style="color:#fff !important;">Akun Saya</a></button>
                  </div>										
                </div>
              </div>                  
            </div>
              @if(Cookie::get('is_membership') == 'free')
              <div id="promosi" style="padding-top: 20px;">
                <div class="subscribe mx-auto">
                  <div class="row justify-content-center">
                    <div class="col-md-12">
                      <div class="header-box">
                        <img loading="lazy" class="logo" src="{{ url('images/plus.png') }}" alt="logo">
                        <div class="login">
                          <a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="Langganan Espos Plus">Aktifkan Paket <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                      </div>
                      
                      <div class="top-box">
                        <div class="lead">Langganan Espos Plus</div>
                        <div class="sublead">
                          Hi {{ Cookie::get('is_name') }}, silakan <a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="Langganan Espos Plus">Aktifkan Paket <i class="fa fa-arrow-circle-o-right"></i></a> untuk mendapatkan akses tak terbatas ke artikel, foto, info grafis, dan lainnya.
                        </div>
                      </div>
                      <div class="button-container">
                        <a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="langganan" class="checkout-button" style="padding:8px 10px !important; font-size:12px;color:#fff !important;">Langganan Sekarang, Dapat Mobil !</a>
                        <a href="https://m.solopos.com/page/help" target="_blank">APA ITU ESPOS PLUS?</a>
                      </div>
                      <div class="footer-box">
                        <span class="callout">*Syarat & Kententuan berlaku</span> 
                      </div>																						
                                            
                    </div>
                    
                  </div>
                </div>
              </div>
              <div>
                <img loading="lazy" src="https://cdn.solopos.com/plus/espos-plus-mobile.jpg" width="100%">
              </div>
              <div class="mb-4"></div>							
              @endif
        </div>
        @else
        <div id="promosi" style="padding-top: 20px;">             
          <div class="subscribe mx-auto">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <div class="header-box">
                  <img loading="lazy" class="logo" src="{{ url('images/plus.png') }}" alt="logo">
                  <div class="login">
                    Sudah Langganan ?  <a href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}" title="login">Login <i class="fa fa-arrow-circle-o-right"></i></a>
                  </div>
                </div>
                
                <div class="top-box">
                  <div class="lead">Langganan Espos Plus</div>
                  <div class="sublead">Silakan berlangganan untuk mendapatkan akses tak terbatas ke artikel, foto, info grafis, dan lainnya.</div>
                </div>
                <div class="button-container">
                  <a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="langganan" class="checkout-button" style="padding:8px 10px !important; font-size:12px;color:#fff !important;">Langganan Sekarang, Dapat Mobil !</a>
                  <a href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}" title="Login" class="checkout-button inverse">LOGIN | DAFTAR</a>
                  <a href="https://m.solopos.com/page/help" target="_blank">APA ITU ESPOS PLUS?</a>
                </div>
                <div class="footer-box">
                  <span class="callout">*Syarat & Kententuan berlaku</span> 
                  </div>																						
                                      
              </div>
              
            </div>
          </div>              
        </div>
        <div>
          <img loading="lazy" src="https://cdn.solopos.com/plus/espos-plus-mobile.jpg" width="100%">
        </div>
        <div class="mb-4"></div>            
        @endif
      </div>

      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        {{-- <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Espospedia Terkini</h5><a class="btn btn-primary btn-sm" href="/arsip">Indeks Berita</a>
          </div>
        </div> --}}
        <div class="container">
          @foreach ($premium as $item)
              
            <!-- Terkini Post-->
            <div class="card mb-3 content-box">
              <a class="premium-post" style="font-size: 0.9rem;padding: 2px 7px;" href="https://m.solopos.com/plus"><i class="lni lni-circle-plus"></i> Espos Plus</a>
              <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">
              <img loading="lazy" class="card-img-top" src="{{ $item['images']['thumbnail'] }}" alt="{{ json_encode($item['images']['caption']) }}">
              </a>
              <div class="card-body">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">{{ html_entity_decode($item['title']) }}</a>
              </div>
            </div>
            @endforeach
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                + PLUS Lainnya...
              </a>
              <a href="https://m.solopos.com/arsip-plus" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>                               
        </div>
      </div>

      <div class="container">
        <div class="border-top"></div>
      </div>

    </div>

    <style>
      .subscribe {
        background-color: #fff;
        margin-top: 20px;
        margin-bottom: 50px;
        position: relative;
        border: 1px solid #00437d;
        border-radius: 10px;
      }	
      .header-box {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
        padding: 20px 15px;
      }																
      .top-box {
        display: flex;
        flex-direction: column;
        justify-content: center;
        padding: 0 15px;
      }									
      .header-box .logo {
        width: 120px;
      }
      .header-box .login {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 13px;
        font-weight: 600;
        text-align: right;
        letter-spacing: 0.02em;
        color: #f29b27;
      }
      .subscribe .lead {
        font-size: 21px;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        line-height: 29px;
        font-weight: 700;
        text-align: center;
        letter-spacing: -0.01em;
        color: #2E2E2E;
        margin: 5px auto 10px auto;
      }
      .subscribe .sublead {
        text-align: center;
        margin: 0 auto 15px auto;
        font-size: 12px;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        line-height: 19px;
      }	
      .button-container {
        display: flex;
        flex-direction: column;
        justify-content: center;
        margin: 20px auto;
      }										
      .checkout-button {
        display: inline-block;
        width: 325px;
        height: 40px;
        padding: 8px 32px;
        border: 1px solid #00437d;
        border-radius: 4px;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-weight: bold;
        font-size: 14px;
        letter-spacing: 0.05em;
        text-transform: uppercase;
        text-align: center;
        color: #FFFFFF !important;
        background: #00437d;
        margin: 0 auto 15px auto;
      }	
      .inverse {
        color: #00437d !important;
        background: #FFFFFF;
        border: 1px solid #00437d;
      }										
      .button-container a {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        font-size: 14px;
        font-weight: bold;
        text-align: center;
        letter-spacing: 0.02em;
        text-decoration: none;
      }		
      .footer-box {
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
        padding: 15px;
      }	
      .callout {
        display: block;
        font-family: 'Roboto', sans-serif;
        font-weight: 500;
        font-size: 9px;
        line-height: 9px;
        text-transform: uppercase;
        letter-spacing: 0.05em;
        opacity: 0.7;
      }																											
    </style>

@endsection