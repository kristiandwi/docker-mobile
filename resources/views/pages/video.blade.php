@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
    <div class="page-content-wrapper">
      @include('includes.ads.top-swipe')
      <!-- Solopos TV-->
      <div class="video-wrapper">
        <div class="container ">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Solopos TV</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/video">View All</a>
          </div>
        </div>
        <div class="container loadmore-frame">
          @foreach($breakingcat as $vid)
 
            <div class="spos-v-card mb-5 content-box">
              <a class="video-icon" href="{{ url("/{$vid['slug']}-{$vid['id']}") }}" title="{{ $vid['title'] }}" style="background: #fb072e; width: 70px; border-radius: 17%;opacity: 0.8;">
                <i class="lni lni-play" style="background-color: #fb072e;width: 70px; border-radius: 17%;opacity: 0.8;"></i>
              </a>
              
              <img class="card-img-top" src="{{ $vid['images']['thumbnail'] }}" alt="{{ $vid['title'] }}">
              
              <div class="post-content text-center" style="padding: 7px; background: linear-gradient(to bottom, rgba(255,0,0,0), #0c153b)">
                <a class="post-title" href="{{ url("/{$vid['slug']}-{$vid['id']}") }}" title="{{ $vid['title'] }}"">{{ $vid['title'] }}</a>
              </div>
            </div>
          @endforeach
          <div class="text-center mb-3">
            <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
              Berita Video Lainnya
            </a>
            <a href="https://www.youtube.com/user/SoloposTV" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
              Arsip Berita
            </a>
          </div>           
        </div>
      </div>

      <div class="container">
        <div class="border-top"></div>
      </div>

    </div>

@endsection