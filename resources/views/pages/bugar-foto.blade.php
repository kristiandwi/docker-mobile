@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper">

    <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.youtube.com/c/RSJIHSolo" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-1.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
          window.googletag = window.googletag || {cmd: []};
          googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-1', [[336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1640666517438-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
          });
        </script>
        <!-- /54058497/RSJIH-MOBILE-1 -->
        <div id='div-gpt-ad-1640666517438-0' style='min-width: 300px; min-height: 250px;'>
          <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666517438-0'); });
          </script>
        </div>
  	  </div>

      <!-- Galeri Foto -->
      <div class="video-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between">
            <h5 class="mb-0 pl-1 spos-title">GALERI FOTO BUGAR</h5><a class="btn btn-primary btn-sm" href="{{ url('/rsjihsolo/foto') }}">Indeks Foto</a>
          </div>
        </div>

      <!--div class="mt-3 mb-3" align="center">
        <a href="https://www.instagram.com/rs.jihsolo/" target="_blank">
            <img src="{{ url('images/bugar/mobile-banner-2.gif') }}">
          </a>
      </div-->

        <div class="container">
            @php $f_loop = 1; @endphp
            @foreach($foto as $item)

            <div class="card mb-3">
              <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
              <img loading="lazy" class="card-img-top" src="{{ $item['featured_image']['thumbnail'] }}" alt="{{ $item['title'] }}">
              </a>
              <div class="card-body">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}"  title="{{ $item['title'] }}"> {{ $item['title'] }}</a>
              </div>
            </div> 

            @php $f_loop++ @endphp
            @endforeach              
          </div>
        </div>
      </div>

      <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.rs-jih.co.id/rsjihsolo/medsos" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-3.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-3', [[300, 250], [336, 280], [300, 300]], 'div-gpt-ad-1640666720305-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
        </script>
        <!-- /54058497/RSJIH-MOBILE-3 -->
        <div id='div-gpt-ad-1640666720305-0' style='min-width: 300px; min-height: 250px;'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666720305-0'); });
        </script>
        </div>
    </div>

      <div class="container">
        <div class="border-top"></div>
      </div>

    </div>
@endsection