@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper">

      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Artikel Terkini</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/wisata-joglosemar/artikel">Indeks</a>
          </div>
        </div>
        <div class="container">
          @php $b_loop = 1; @endphp
          @foreach ($artikel as $item)

            @if ($b_loop == 1)
            <div class="card mb-3 content-box">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                <img class="card-img-top" src="{{ $item['images']['thumbnail'] }}" alt="{{ $item['title'] }}">
                </a>
                <div class="card-body">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
                </div>
            </div>

            <div class="mt-3 mb-3" align="center">
                <a target="_blank" href="https://bob.kemenparekraf.go.id/" target="_blank">
                <img src="{{ asset('banner/bob-banner-mr.gif') }}">
                </a>
            </div>

            @else          
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  <img src="{{ $item['images']['thumbnail'] }}" alt="" style="object-fit: cover; height: 100px; width: 100px;">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $item['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            </div>
            @endif
            @php $b_loop++ @endphp
            @endforeach
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Berita Lainnya
              </a>
              <a href="https://m.solopos.com/wisata-joglosemar/artikel" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>                               
        </div>
      </div>

      <div class="mt-3 mb-3" align="center">
        <a target="_blank" href="https://youtube.com/channel/UCfdq2dv6zodZqqWpvc3U-MA" target="_blank">
          <img src="{{ asset('banner/bob-banner-mr.gif') }}">
        </a>
  	  </div>      
    
      <div class="container">
        <div class="border-top"></div>
      </div>

    </div>
@endsection