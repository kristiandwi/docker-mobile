@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper">

    <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.youtube.com/c/RSJIHSolo" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-1.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
          window.googletag = window.googletag || {cmd: []};
          googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-1', [[336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1640666517438-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
          });
        </script>
        <!-- /54058497/RSJIH-MOBILE-1 -->
        <div id='div-gpt-ad-1640666517438-0' style='min-width: 300px; min-height: 250px;'>
          <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666517438-0'); });
          </script>
        </div>
  	</div>

      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Artikel Bugar Terkini</h5><a class="btn btn-primary btn-sm" href="{{ url('/bugar/artikel') }}">Indeks</a>
          </div>
        </div>

        <div class="mt-3">
        </div>

        <div class="container">
          @php $b_loop = 1; @endphp
          @foreach ($artikel as $item)
          @if($b_loop <=50)          
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  <img loading="lazy" src="{{ $item['featured_image']['thumbnail'] }}" alt="" style="object-fit: cover; height: 100px; width: 100px;">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $item['catsname'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            </div>
            @endif
            @php $b_loop++ @endphp
            @endforeach
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Artikel Lainnya
              </a>
              <a href="{{ url('/rsjihsolo/artikel') }}" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>                               
        </div>
      </div>


      <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.rs-jih.co.id/rsjihsolo/medsos" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-3.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-3', [[300, 250], [336, 280], [300, 300]], 'div-gpt-ad-1640666720305-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
        </script>
        <!-- /54058497/RSJIH-MOBILE-3 -->
        <div id='div-gpt-ad-1640666720305-0' style='min-width: 300px; min-height: 250px;'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666720305-0'); });
        </script>
        </div>
    </div>

    
      <div class="container">
        <div class="border-top"></div>
      </div>

      <!--<style type="text/css">
      .spos-v-card .post-thumbnail::after {opacity: 0 !important; }
      .blog-description a {color: #b7212c !important; }
      .btn-primary, #uksw .bg-primary, #uksw .badge-primary {background: #ffc107 !important; border-color: #ffc107; color:#00437c;/*linear-gradient(to right, #00437c, #b7212c) !important;*/}
      .spos-title {color:#00437c; border-bottom: 1px solid #b7212c;}
      .single-hero-slide::after {background:#ffc107;/*background: linear-gradient(to right, #00437c, #b7212c);*/}
      .single-hero-slide .post-title {color: #00437c; line-height: 25px;}
      .single-hero-slide .post-meta a {color:#b7212c; }
      .single-hero-slide::after {background:#ffc107;/*background: linear-gradient(to right, #00437c, #b7212c);*/}
      </style>-->

    </div>
@endsection