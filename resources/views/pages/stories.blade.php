<!DOCTYPE html>
<html lang="id-ID">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Solopos Stories</title>
  <link rel="stylesheet" href="{{ asset('css/stories.css') }}">
</head>

<body>

  <div data-slide="slide" class="slide">
    <div class="slide-items">
        @php $no = 1; @endphp
        @foreach($data as $item) 
        @if($no <= 5)  
        <div class="item">
        <img loading="lazy" src="{{ $item['images']['thumbnail'] }}" alt="{{ $item['title'] }}" style="object-position: center !important;">
          <div class="caption">
            <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                {{ $item['title'] }}
            </a> 
          </div> 
        </div>
        @endif
        @php $no++; @endphp
        @endforeach
    </div>
    <nav class="slide-nav">
      <div class="slide-thumb"></div>
      <button class="slide-prev">Prev</button>
      <button class="slide-next">Next</button>
    </nav>
  </div>

  <script src="{{ asset('js/slide-stories.js') }}"></script>
</body>

</html>