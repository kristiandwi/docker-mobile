@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
    <div class="page-content-wrapper">
    <div class="profile-content-wrapper">
        <!-- Settings Option-->
        <div class="profile-settings-option"><a href="#" style="top:20px;"><i class="lni lni-cog"></i></a></div>
        <div class="container mt-3">
          <div class="user-meta-data d-flex">
            <div class="user-thumbnail"><img loading="lazy" src="{{ $header['avatar'] }}" alt="{{ $header['name'] }}"></div>
            <div class="user-content">
              <h6>{{ $header['name'] }}</h6>
              <p>{{ $header['website'] }}</p>
              <div class="d-flex justify-content-between">
                  <p style="font-size: 12px;">{{ $header['description'] }}</p>              
              </div>
              <div class="author-social">
                <span>Follow Me: </span>
                <a href="https://www.twitter.com/soloposdotcom" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="https://www.facebook.com/soloposcom" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://www.instagram.com/koransolopos" target="_blank"><i class="fa fa-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UC3jhKGRRBnIi5pfNIE8HOEw" target="_blank"><i class="fa fa-youtube"></i></a>
              </div>              
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="border-top"></div>
      </div>

      <div class="container">
        <div class="border-top"></div>
      </div>    
      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          @php $b_loop = 1; @endphp
          @foreach ($breaking as $item)
            @php           
            $image = $item['featured_image']['thumbnail'] ?? 'https://m.solopos.com/images/no-thumb.jpg'; 
            $title = html_entity_decode($item['title']);
            @endphp          
            @if ($b_loop == 1)
            <div class="card mb-3 content-box">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                <img loading="lazy" class="card-img-top" src="{{ $image }}" alt="{{ $title }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                </a>
                <div class="card-body">
                  {{-- @if($item['is_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								  @endif --}}
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                </div>
            </div>

            <div class="mt-3 mb-3" align="center">
              <!-- /54058497/soloposmobile/mobile-home-1 -->
              <div id='div-gpt-ad-1625738305324-0' style='min-width: 300px; min-height: 50px;'>
                <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1625738305324-0'); });
                </script>
              </div>
            </div>

            @else          
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                    <img loading="lazy" src="{{ $image }}" alt="{{ $title }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  {{-- @if($item['is_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								  @endif --}}
                  <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                  <div class="post-meta d-flex align-items-center">
                  <a href="https://m.solopos.com/">@if($item['catparent'] ==''){{ $item['catsname'] }} @else {{ $item['catparent'] }} @endif</a>|<a href="#" style="padding-left:7px;">{{ Carbon\Carbon::parse($item['date'])->translatedFormat('j M Y - H:i') }}</a>
                  </div>
                </div>
              </div>
            </div>
            @endif
            @php $b_loop++ @endphp
            @endforeach
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Berita Lainnya
              </a>
              <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>                               
        </div>
      </div>

      <!-- Ads Breaking -->
      <div class="iklan mt-3">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-ad-format="auto"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>  
      </div>

      <!-- widget Berit Video -->
      @include('includes.widget-video')      
   
      <div class="container">
        <div class="border-top"></div>
      </div>

    </div>
@endsection