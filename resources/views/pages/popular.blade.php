@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
<div class="page-content-wrapper">
      <!-- Editors Choice Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Terpopuler</h5><a class="btn btn-primary btn-sm" href="{{ url('/arsip') }}">Indeks Berita</a>
          </div>
        </div>
        <div class="container">  
          @php $pop_loop = 1; @endphp
          @foreach($popular as $pop)  
                              
          <!-- Terkini Post--> 
          @if ($pop_loop == 1)
          <div class="card mb-3 content-box">
            <a href="{{ url("/{$pop['post_slug']}-{$pop['post_id']}") }}" title="{{ html_entity_decode($pop['post_title']) }}">
            <img loading="lazy" class="card-img-top" src="{{ $pop['post_thumb'] }}" alt="{{ html_entity_decode($pop['post_title']) }}">
            </a>
            <div class="card-body">
              <a class="post-title" href="{{ url("/{$pop['post_slug']}-{$pop['post_id']}") }}" title="{{ html_entity_decode($pop['post_title']) }}">
                {{-- @if($pop['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
                {{ html_entity_decode($pop['post_title']) }}</a>
            </div>
          </div>
          @else    
          <div class="terkini-post content-box">
            <div class="d-flex">
            <div class="post-thumbnail">
              <a href="{{ url("/{$pop['post_slug']}-{$pop['post_id']}") }}" title="{{ html_entity_decode($pop['post_title']) }}">
                <img loading="lazy" src="{{ $pop['post_thumb'] }}" alt="{{ html_entity_decode($pop['post_title']) }}" style="object-fit: none; object-position: center; height: 100px; width: 100px;">
              </a>
            </div>
            <div class="post-content">
              {{-- @if($pop['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
							@endif --}}
              <a class="post-title" href="{{ url("/{$pop['post_slug']}-{$pop['post_id']}") }}" title="{{ html_entity_decode($pop['post_title']) }}">{{ html_entity_decode($pop['post_title']) }}</a>
              <div class="post-meta d-flex align-items-center">
                <a href="https://m.solopos.com/{{ $pop['post_category'] }}">{{ $pop['post_category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($pop['post_date']) }}</a>
              </div>
            </div>
            </div>
          </div>
          @endif
          @php $pop_loop++; @endphp
          @endforeach   
          {{-- <div id="arsipPost"></div> --}}
          {{-- <p style="font-size:12px">Mohon tunggu beberapa saat, kami memerlukan waktu untuk menampilkan semua data berita yang ada inginkan.</p> --}}
          <div class="text-center mt-3">
            <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
              Cek Berita Lainnya
            </a>
            <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
              Arsip Berita
            </a>
          </div>                                   
        </div>
      </div>
      {{-- @push('custom-scripts')
      <script>
        $(document).ready(function() {
            $.ajax({ //create an ajax request related post
            type: "GET",
            url: "https://tf.solopos.com/api/v1/stats/popular/all", 
            dataType: "JSON",
            success: function(data) {
              console.log(data);
                var arsipPosts = $("#arsipPost");
  
                $.each(data, function(i, item) {
                  arsipPosts.append("<div class=\"terkini-post content-box d-flex\"><div class=\"post-thumbnail\"><a href=\"" + data[i]['post_slug'] + "-" + data[i]['post_id'] + "\" title=\"" + data[i]['post_title'] + "\"><img src=\"" + data[i]['post_thumb'] +"\" loading=\"lazy\" alt=\"" + data[i]['post_title'] + "\" style=\"object-fit: cover; height: 100px; width: 100px;\"></a></div><div class=\"post-content\"><a class=\"post-title\" href=\"" + data[i]['post_slug'] + "-" + data[i]['post_id'] + "\" title=\"" + data[i]['post_title'] + "\">" + data[i]['post_title'] + "</a><div class=\"post-meta d-flex align-items-center\"><a href=\"#\">" + data[i]['post_category'] + "</a><a href=\"#\" style=\"padding-left:7px;\"></a></div></div></div>");
                });
            }
          });
        });
      </script>
      @endpush --}}
    <!-- ads footer -->
</div>   

@endsection