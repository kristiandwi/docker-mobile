@extends('layouts.app')
@section('content')
<div id="uksw" class="page-content-wrapper"> 
	  <!-- Blok 1 Terkini Wrapper -->
    <div class="terkini-wrapper">
        <div class="container">
          <img style="max-width: 100%; margin-bottom: 30px;" src="https://www.uksw.edu/public/upload/2019/11/30/USER120191130112455.jpg">
          <div class="d-flex align-items-center mb-3">
            <h5 class="mb-0 pl-1 spos-title">TANYA JAWAB UKSW SALATIGA</h5>
          </div>
        </div>
        <!-- Tanya Jawab Wrapper-->
        <div class="element-wrapper" style="padding-top: 5px;">
          <div class="container">
            <!-- Default Accordian 1-->
            <div class="accordion" id="faq-premium">
              <!-- Single Accordian-->
              <div class="card border-0">
                <div class="card-header bg-primary p-0" id="heading1">
                  <h2 class="mb-0">
                    <button class="btn" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">1. Apa yang dimaksud dengan Espos Premium?</button>
                  </h2>
                </div>
                <div class="collapse show" id="collapse1" aria-labelledby="heading1" data-parent="#faq-premium">
                  <div class="card-body">
                    <p class="mb-0">Espos Premium adalah artikel yang menyajikan informasi dari fakta dan peristiwa yang diolah serta dipaparkan dengan cara kreatif dan lebih visual. Artikel ditulis dengan bahasa yang lebih sederhana dan lugas serta artikel yang lebih mendalam. Espos Premium juga dapat diakses dengan sistem berlangganan pada situs dalam jaringan (online).</p>
                  </div>
                </div>
              </div>
              <!-- Single Accordian 2-->
              <div class="card border-0">
                <div class="card-header bg-primary p-0" id="heading2">
                  <h2 class="mb-0">
                    <button class="btn" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">2. Apa perbedaan Espos Premium dengan Web-paper Solopos?</button>
                  </h2>
                </div>
                <div class="collapse" id="collapse2" aria-labelledby="heading2" data-parent="#faq-premium">
                  <div class="card-body">
                    <p class="mb-0">Web-Paper Solopos merupakan replika digital edisi cetak Harian Umum Solopos. Dan bisa mengakses Web-Paper Solopos melalui alat-alat digital seperti telepon pintar (smartphone), komputer genggam tablet, laptop, atau komputer meja (desktop). Untuk memperoleh informasi lebih detail tentang berlanganan E-Paper Solopos, kunjungi https://koran.solopos.com/. Espos Premium adalah konten yang dapat diakses dengan sistem berlangganan pada situs dalam jaringan (online). Espos Premium disajikan dengan artikel yang lebih mendalam.</p>
                  </div>
                </div>
              </div>
              <!-- Single Accordian 3-->
              <div class="card border-0">
                <div class="card-header bg-primary p-0" id="heading3">
                  <h2 class="mb-0">
                    <button class="btn" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">3. Bagaimana cara berlangganan Espos Premium?
                </div>
                <div class="collapse" id="collapse3" aria-labelledby="heading3" data-parent="#faq-premium">
                  <div class="card-body">
                    <p class="mb-0">Untuk pembaca setia Solopos.com, selama pengembangan Espos Premium Solopos sepenuhnya Gratis.</p>
                  </div>
                </div>
              </div>
              <!-- Single Accordian 4-->
              <div class="card border-0">
                <div class="card-header bg-primary p-0" id="heading4">
                  <h2 class="mb-0">
                    <button class="btn" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">4. Apa keunggulan berlangganan Espos Premium?</button>
                  </h2>
                </div>
                <div class="collapse" id="collapse4" aria-labelledby="heading4" data-parent="#faq-premium">
                  <div class="card-body">
                    <div class="container">
                    <ol>
                      <li>Dengan berlangganan Espos Premium, Anda mendapatkan berita ekonomi, bisnis, dan market terpilih dengan artikel yang lebih mendalam.</li>
                      <li>
                        Sebuah karya jurnalistik adalah sumber informasi sahih. Untuk mendapatkan informasi tersebut, butuh biaya tinggi. Dengan berlangganan Espos Premium, berarti Anda mendukung keberlangsungan informasi yang akan menjadi sumber kepercayaan diri ketika membuat keputusan.
                      </li>
                      <li>
                        Untuk benefit-benefit sebesar itu, harga berlangganan Espos Premium tergolong murah dan sangat bermanfaat.
                      </li>
                      <li>
                        Espos Premium Bisnis Indonesia membuat koleksi berlangganan produk Bisnis Indonesia Group yang sudah Anda miliki menjadi lengkap.
                      </li>
                    </ol>
                  </div>
                  </div>
                </div>
              </div>             
            </div>
          </div>
        </div>

      </div>

      <div align="center">
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
          window.googletag = window.googletag || {cmd: []};
          googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/UKSW-MR', [300, 250], 'div-gpt-ad-1634121385191-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
          });
        </script>
        <!-- /54058497/UKSW-MR -->
        <div id='div-gpt-ad-1634121385191-0' style='min-width: 300px; min-height: 250px;'>
          <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1634121385191-0'); });
          </script>
        </div>
		  </div>

      <div class="container">
        <div class="border-top"></div>
      </div>

      <style type="text/css">
        #uksw .btn-primary, #uksw .bg-primary, #uksw .badge-primary {
            background: #ffc107 !important;
            border-color: #ffc107;
            color: #00437c;
        } 
        .spos-title {color:#00437c; border-bottom: 1px solid #b7212c;}
      </style>     	  
</div>
@endsection