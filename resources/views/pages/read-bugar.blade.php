@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
      <!-- Caption Modal-->
      {{-- <div class="modal fade" id="fotocaption" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button class="close close" type="button" data-dismiss="modal" aria-label="Close"><i class="lni lni-close"></i></button>
              <!-- Heading-->
              <h6 class="mb-3 pl-2">Photo Caption</h6>
              @if(!empty($content['caption']))
                <p>SOLOPOS.COM - {{ htmlentities($content['caption']) }}</p>
              @else
                <p>SOLOPOS.COM - Panduan Informasi dan Inspirasi</p>
              @endif
            </div>
          </div>
        </div>
      </div>--}}

    <div class="page-content-wrapper">
        <div class="container">
          <nav aria-label="breadcrumb" style="text-align: center;">
            <ol class="breadcrumb" style="text-transform: capitalize;font-size:13px;font-weight:600;">
              <li class="breadcrumb-item"><a href="{{ url('/') }}" title="Solopos.com">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ url('/') }}/{{ $content['category_parent'] }}" title="Archive">Bugar</a></li>
              @if($header['category_child'] != '')
              <li class="breadcrumb-item active" aria-current="page">
                <a href="{{ url('/') }}/{{ $header['category_parent'] }}/{{ $header['category_child'] }}" title="Archive">{{ $header['category_child'] }}</a></li>
              @endif
            </ol>
          </nav>
        </div>
      <!-- Scroll Indicator-->
      <div id="scrollIndicator"></div>
      <!-- Single Blog Info-->
      <div class="single-blog-info">
        <div class="d-flex align-items-center">
          <div class="post-content-wrap mt-1">
            <h1 class="mt-2 mb-2">{{ $header['title'] }}</h1>
            <div class="mb-4" style="font-size: 12px;line-height:16px;font-family:roboto">
              {{ $header['ringkasan'] ?? ''}}
            </div>
            <div class="post-meta">
              <div class="post-date">{{ Carbon\Carbon::parse($content['date'])->translatedFormat('l, j F Y - H:i') }} WIB</div>
              <div class="post-date penulis">
                <div class="multi-reporter">
                  @if($header['author'] != $header['editor'] )
                      Penulis:
                      @foreach ($author_slug as $author)
                      <a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $header['editor_url'] }} @else {{ url('/') }}/penulis/{{$author}} @endif">{{ ucwords(str_replace('_', ' ', $author)) }}</a>
                      @endforeach
                  @endif
                  </div>
                  Editor : <a href="@if(!empty($header['editor_url'])){{ url('/')}}/author/{{ $header['editor_url'] }} @else https://m.solopos.com/arsip @endif" style="text-transform: none;"> {{ $header['editor'] }}</a>
                </div>
              <div class="post-date"><!-- reading time --></div>
            </div>
          </div>
        </div>
      </div>
      <!-- Single Blog Thumbnail-->
      @if(!empty($video))
      <div class="video-wrap">
        <div class="video">
          <iframe id="ytplayer" type="text/html" width="100%" height="200" src="https://www.youtube.com/embed/{{ $video }}?autoplay=1&origin=https://m.solopos.com" frameborder="0"></iframe>
        </div>
      </div>
      @else
      <div class="single-blog-thumbnail">
        <img loading="lazy" class="w-100 single-blog-image" src="{{ $content['image'] }}" alt="{{ html_entity_decode($content['title']) }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
      </div>
      <div class="foto-caption mb-3" style="font-size: 11px;padding: 5px 10px;line-height: 13px;color:#797494;">
        @if(!empty($content['caption']))
          SOLOPOS.COM - {{ html_entity_decode($content['caption']) }}
        @else
          SOLOPOS.COM - Panduan Informasi dan Inspirasi
        @endif
      </div>
      @endif

      <!-- Blog Description-->
      <div class="blog-description mt-3">
        <div class="container">

          <div class="mt-3 mb-3" align="center">
              <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
              <script>
                window.googletag = window.googletag || {cmd: []};
                googletag.cmd.push(function() {
                  googletag.defineSlot('/54058497/RSJIH-MOBILE-1', [[336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1640666517438-0').addService(googletag.pubads());
                  googletag.pubads().enableSingleRequest();
                  googletag.enableServices();
                });
              </script>
            <!-- /54058497/RSJIH-MOBILE-1 -->
            <div id='div-gpt-ad-1640666517438-0' style='min-width: 300px; min-height: 250px;'>
               <script>
                 googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666517438-0'); });
               </script>
            </div>
          </div>

          @php
            $konten = Helper::konten(htmlspecialchars_decode($content['content'])) ;
            $contents = explode('</p>', $konten);
            $total_p = count(array_filter($contents));
            // many paragraph
            if($total_p > 15 && $total_p < 25 ):
							$p_iklan_1  = array_slice($contents, 0, 2);
							$p_iklan_2  = array_slice($contents, 2, 4);
							$p_iklan_3  = array_slice($contents, 6, 5);
							$last_p  = array_slice($contents, 11);

							elseif ($total_p > 25 && $total_p < 40 ) :
							$p_iklan_1  = array_slice($contents, 0, 3);
							$p_iklan_2  = array_slice($contents, 3, 6);
							$p_iklan_3  = array_slice($contents, 9, 7);
							$p_iklan_4  = array_slice($contents, 16, 7);
							$last_p  = array_slice($contents, 23);

							elseif ($total_p > 40 ) :
							$p_iklan_1  = array_slice($contents, 0, 3);
							$p_iklan_2  = array_slice($contents, 3, 7);
							$p_iklan_3  = array_slice($contents, 10, 8);
							$p_iklan_4  = array_slice($contents, 18, 9);
							$p_iklan_5  = array_slice($contents, 27, 9);
							$p_iklan_6  = array_slice($contents, 36, 9);
							$p_iklan_7  = array_slice($contents, 45, 9);
							$last_p  = array_slice($contents, 54);

							else :
							$p_iklan_1  = array_slice($contents, 0, 2);
							$p_iklan_2  = array_slice($contents, 2, 3);
							$p_iklan_3  = array_slice($contents, 5, 3);
							$last_p  = array_slice($contents, 8);
						endif;
          @endphp
          <!-- ads top -->
          <style>
          .blog-description p em strong {
            display:inline-block;padding:10px;font-size:15px;line-height:23px;background:#f1f1f1;font-weight:700;border-radius:5px;color:#44474a;
          }
          .blog-description p em strong a {
            color:#0b6097 !important;font-weight:500!important;padding-left:5px;font-style:normal !important;
          }
          </style>
          <!-- ads parallax -->

          {!! implode('</p>', $p_iklan_1) !!}
          @include('includes.ads.inside-article-1')

          {!! implode('</p>', $p_iklan_2) !!}
          @include('includes.ads.inside-article-2')

          {!! implode('</p>', $p_iklan_3) !!}
          @include('includes.ads.inside-article-3')

          @if($total_p > 25 && $total_p < 40 )
            {!! implode('</p>', $p_iklan_4) !!}
            @include('includes.ads.inside-article-4')

          @elseif($total_p > 40  )
            {!! implode('</p>', $p_iklan_4) !!}
            @include('includes.ads.inside-article-4')

            {!! implode('</p>', $p_iklan_5) !!}
            @include('includes.ads.inside-article-5')

            {!! implode('</p>', $p_iklan_6) !!}
            @include('includes.ads.inside-article-6')

            {!! implode('</p>', $p_iklan_7) !!}
            @include('includes.ads.inside-article-7')

          @endif

          {!! implode('</p>', $last_p) !!}
          @include('includes.ads.inside-article-last')
          @include('includes.ads.internal-promotion')
        </div>

        <div class="container social-share-btn d-flex align-items-center flex-wrap">
          SHARE :
          <a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="lni lni-facebook"></i></a>
          <a class="btn-twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="lni lni-twitter-original"></i></a>
          <a class="btn-instagram" href="https://www.instagram.com/koransolopos" title="social"><i class="lni lni-instagram"></i></a>
          <a class="btn-whatsapp" href="whatsapp://send?text=*{{ $header['title'] }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="lni lni-whatsapp"></i></a>
          <a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini {{ $header['link'] }}" title="social"><i class="lni lni-envelope"></i></a>
        </div>
      </div>

      <div class="container mt-3">
        <div class="tag-lists">
        <span>Kata Kunci :</span>
        @if(isset($content['tag']))
            @foreach($content['tag'] as $tag)
            @php
                $tag_name = ucwords($tag);
                $tag_slug = str_replace(' ', '-',$tag);
                $tag_slug = strtolower($tag_slug)
            @endphp
            <a href="{{ url("/tag/{$tag_slug}") }}">{{ $tag_name }}</a>
            @endforeach
        @endif
        </div>
      </div>

      <!-- ads under article -->
      <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.instagram.com/rs.jihsolo/" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-2.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
          window.googletag = window.googletag || {cmd: []};
          googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-2', [[336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1640666650907-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
          });
        </script>
        <!-- /54058497/RSJIH-MOBILE-2 -->
        <div id='div-gpt-ad-1640666650907-0' style='min-width: 300px; min-height: 250px;'>
          <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666650907-0'); });
          </script>
        </div>
      </div>

      {{--<div class="iklan mt-3">
      @include('includes.ads.under-article')
      </div>--}}

      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title"> Berita Terkait </h5>
          </div>
        </div>
        <div class="container">
          <ul id="relposts"></ul>
        </div>
      </div>

      <!-- Ads Mobile Single 2 -->
      {{--@include('includes.ads.article-single-1') --}}
      <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.rs-jih.co.id/rsjihsolo/medsos" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-3.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-3', [[300, 250], [336, 280], [300, 300]], 'div-gpt-ad-1640666720305-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
        </script>
        <!-- /54058497/RSJIH-MOBILE-3 -->
        <div id='div-gpt-ad-1640666720305-0' style='min-width: 300px; min-height: 250px;'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666720305-0'); });
        </script>
        </div>
      </div>

      <!-- widget promosi -->
      @include('includes.widget-promosi')

      <!-- Terpopuler Wrapper-->
      <div class="mt-3 mb-3">
        @include('includes.widget-popular')
      </div>

      <!-- Ads Mobile Single 2 -->
      @include('includes.ads.article-single-2')

      <div class="mt-3 mb-3">
        @include('includes.widget')
      </div>

      <!-- Facebook Komentar -->
      {{-- <div class="container">
        <div class="d-flex align-items-center justify-content-between mb-3">
          <h5 class="mb-0 pl-1 spos-title">Komentar Anda</h5>
        </div>

        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v11.0&appId=1438927622798153&autoLogAppEvents=1" nonce="uCAeYQ2X"></script>
        <div class="fb-comments" data-href="https://m.solopos.com" data-width="320" data-numposts="5"></div>
      </div> --}}
      <!-- Konten Premium Wrapper -->
      <div class="container">
        <div class="editorial-choice-news-wrapper mt-3">
          <div class="bg-shape1"><img loading="lazy" src="{{ asset('images/edito.png') }}"></div>
          <div class="bg-shape2" style="background-image: url('{{ asset('images/edito2.png') }}')"></div>
          <div class="container" align="center">
            <img loading="lazy" src="{{ url('images/plus-putih.png') }}" height="50%" width="50%" style="margin-bottom: 30px;">
          </div>
          <div class="container">
            <div class="editorial-choice-news-slide owl-carousel">
              @php $pc_loop = 1; @endphp
              @foreach($premium as $bp)
                @if($pc_loop <=6)

              <div class="single-editorial-slide d-flex">
                <a class="bookmark-post" href="#"><i class="lni lni-star"></i></a>
                <div class="post-thumbnail">
                  <img loading="lazy" src="{{ $bp['images']['url_thumb'] }}" alt="{{ json_encode($bp['images']['caption']) }}" style="object-fit: cover; height: 177px; width: 132px;">
                </div>
                <div class="post-content">
                  <!--<a class="post-catagory" href="https://m.solopos.com/premium">Premium {{ ucwords($bp['category']) }}</a>-->
									<span class="espos-plus">+ PLUS</span>
                  <a class="post-title d-block" href="{{ url("/{$bp['slug']}-{$bp['id']}") }}" title="{{ html_entity_decode($bp['title']) }}" >{{ html_entity_decode($bp['title']) }}</a>
                </div>
              </div>
              @endif
              @php $pc_loop++; @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>

      <!-- Ads Top Terkini 2 -->
      <div class="iklan mt-3">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-ad-format="auto"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>

      <!-- Terkini Wrapper-->

      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Terkini</h5><a class="btn btn-primary btn-sm" href="/arsip">Indeks Berita</a>
          </div>
        </div>
        <div class="container">
          @php $loop_br = 1; @endphp
          @foreach ($breakingcat as $item)
          @if( $loop_br <= 15 )

            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">
                  <img loading="lazy" src="{{ $item['images']['url_thumb'] }}" alt="{{ html_entity_decode($item['title']) }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                @if($item['konten_premium'] == 'premium')
									<span class="espos-plus">+ PLUS</span>
								@endif
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">{{ html_entity_decode($item['title']) }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $item['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            </div>
            @endif
            @php $loop_br++; @endphp
            @endforeach
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Berita Lainnya
              </a>
              <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>
        </div>
      </div>

      <div class="iklan mt-3">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>

      {{-- <div class="container">
          <!-- Composite Start -->
          <div id="M628318ScriptRootC990864">
          </div>
          <script src="https://jsc.mgid.com/s/o/solopos.com.990864.js" async></script>
          <!-- Composite End -->
      </div> --}}

      <div class="container">
        <div class="border-top"></div>
      </div>

      {{-- <img src="https://cms.solopos.com/set-view?id={{ $content['id'] }}" style="display: none;"> --}}
    </div>
    {{-- <iframe src="https://api.solopos.com/set-view?id={{ $content['id'] }}" style="position: absolute;width:0;height:0;border:0;bottom:0;"></iframe> --}}
    @push('custom-scripts')
    <script>
      $(document).ready(function() {
          $.ajax({ //create an ajax request related post
          type: "GET",
          url: "https://api.solopos.com/api/wp/v2/posts?tags={{ $relatedTags }}&exclude={{ $content['id'] }}&per_page=6", //72325
          dataType: "JSON",
          success: function(data) {
            // console.log(data);
              var relatedPosts = $("#relposts");

              $.each(data, function(i, item) {
                //alert(data[i].item.id);
                // console.log(data[i]['title']['rendered']);
                // relatedPosts.append("<li><a href='" + data[i]['slug'] + "-" + data[i]['id'] + "'>" + data[i]['title']['rendered'] + "</a></li>");

                relatedPosts.append("<div class=\"terkini-post d-flex\"><div class=\"post-thumbnail\"><a href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"\"><img src=\"" + data[i]['one_call']['featured_list']['source_url'] +"\" loading=\"lazy\" alt=\"" + data[i]['title']['rendered'] + "\" style=\"object-fit: cover; height: 100px; width: 100px;\"></a></div><div class=\"post-content\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"" + data[i]['title']['rendered'] + "\">" + data[i]['title']['rendered'] + "</a><div class=\"post-meta d-flex align-items-center\"><a href=\"#\">" + data[i]['one_call']['categories_list'][0]['name'] + "</a><a href=\"#\" style=\"padding-left:7px;\"></a></div></div></div>");
              });
				  }
			  });
		  });

    //   $(window).load(function() {
    //     $.ajax({
    //         type: "GET",
    //         url: 'https://api.solopos.com/set-view?id={{ $content['id'] }}',
    //     });
    // });
    </script>
    @endpush
    @push('tracking-scripts')
        <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        url:'https://tf.solopos.com/api/v1/stats/store',
        method:'POST',
        data:{
                post_id:'{{ $content['id'] }}',
                post_title:'{{ $header['title'] }}',
                post_slug:'{{ $content['slug'] }}',
                post_date:'{{ $content['date'] }}',
                post_author:'{{ $header['author'] }}',
                post_editor:'{{ $header['editor'] }}',
                post_category:'{{ $header['category_parent'] }}',
                post_subcategory:'{{ $header['category_child'] }}',
                post_tag:'{!! serialize($content['tag']) !!}',
                post_thumb:'{{ $header['image'] }}',
                post_view_date:'{{ date('Y-m-d') }}',
                domain:'{{ 'm.solopos.com' }}'
            },
        success:function(response){
            if(response.success){
                console.log(response.message)
            }else{
                console.log(error)
            }
        },
        error:function(error){
            console.log(error)
        }
        });
        </script>
    @endpush
@endsection
