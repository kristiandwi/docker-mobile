@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
      <!-- Caption Modal-->
      {{-- <div class="modal fade" id="fotocaption" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button class="close close" type="button" data-dismiss="modal" aria-label="Close"><i class="lni lni-close"></i></button>
              <!-- Heading-->
              <h6 class="mb-3 pl-2">Photo Caption</h6>
              @if(!empty($content['caption']))
                <p>SOLOPOS.COM - {{ htmlentities($content['caption']) }}</p>
              @else
                <p>SOLOPOS.COM - Panduan Informasi dan Inspirasi</p>
              @endif
            </div>
          </div>
        </div>
      </div>--}}
    <div class="page-content-wrapper">
        <div class="container">
          <nav aria-label="breadcrumb" style="text-align: center;">
            <ol class="breadcrumb" style="text-transform: capitalize;font-size:13px;font-weight:600;">
              <li class="breadcrumb-item"><a href="{{ url('/') }}" title="Solopos.com">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ url('/') }}/{{ $content['category_parent'] }}" title="Archive">{{ $header['category_parent'] }}</a></li>
              @if($header['category_child'] != '')
              <li class="breadcrumb-item active" aria-current="page">
                <a href="{{ url('/') }}/{{ $content['category_parent'] }}/{{ $header['category_child'] }}" title="Archive">{{ $header['category_child'] }}</a></li>
              @endif
            </ol>
          </nav>
        </div>
      <!-- Scroll Indicator-->
      <div id="scrollIndicator"></div>
      <!-- Single Blog Info-->
      <div class="single-blog-info">
        <div class="d-flex align-items-center">
          <div class="post-content-wrap mt-1">
            {{-- <a class="kategori" href="https://m.solopos.com" style="margin-bottom: 20px;">Solopos.com</a>|<a class="kategori-2 mb-5" href="https://m.solopos.com/{{ $content['category_parent'] }}">{{ $content['category_parent'] }}</a> --}}
            <h1 class="mt-2 mb-2 title-only">{{ $header['title'] }}</h1>
            <div class="mb-4" style="font-size: 12px;line-height:16px;font-family:roboto">
              {{ $header['ringkasan'] ?? ''}}
            </div>
            <div class="post-meta">
              <div class="post-date tgl-terbit" style="margin-bottom: 5px;">{{ Carbon\Carbon::parse($content['date'])->translatedFormat('l, j F Y - H:i') }} WIB</div>
              <div class="post-date">
                <div class="multi-reporter">
                @if($header['author'] != $header['editor'] )
										Penulis:
										@foreach ($author_slug as $author)
										<a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $header['editor_url'] }} @else {{ url('/') }}/penulis/{{$author}} @endif">{{ ucwords(str_replace('_', ' ', $author)) }}</a>
										@endforeach
								@endif
                </div>
                Editor : <a href="@if(!empty($header['editor_url'])){{ url('/')}}/author/{{ $header['editor_url'] }} @else https://m.solopos.com/arsip @endif" style="text-transform: none;"> {{ $header['editor'] }}</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container social-share-btn d-flex align-items-center flex-wrap">
        SHARE
        <a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="lni lni-facebook"></i></a>
        <a class="btn-twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="lni lni-twitter-original"></i></a>
        <a class="btn-instagram" href="https://www.instagram.com/koransolopos" title="social"><i class="lni lni-instagram"></i></a>
        <a class="btn-whatsapp" href="whatsapp://send?text=*{{ urlencode($header['title']) }}* | {{ urlencode($header['ringkasan']) }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="lni lni-whatsapp"></i></a>
        <a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini {{ $header['link'] }}" title="social"><i class="lni lni-envelope"></i></a>
      </div>
      <!-- Single Blog Thumbnail-->
      @if(!empty($video))
      <div class="video-wrap">
        <div class="video">
          <iframe loading="lazy" id="ytplayer" type="text/html" width="100%" height="200" src="https://www.youtube.com/embed/{{ $video }}?autoplay=1&origin=https://m.solopos.com" frameborder="0"></iframe>
        </div>
      </div>
      @else
      <div class="single-blog-thumbnail">
        <img loading="lazy" class="w-100 single-blog-image" src="{{ $content['image'] }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'" alt="{{ html_entity_decode($content['title']) }}">
        {{-- <a class="post-bookmark" href="#" data-toggle="modal" data-target="#fotocaption"><i class="lni lni-text-align-justify"></i></a> --}}
      </div>
      <div class="foto-caption mb-3" style="font-size: 11px;padding: 5px 10px;line-height: 13px;color:#797494;">
        @if(!empty($content['caption']))
          SOLOPOS.COM - {{ html_entity_decode($content['caption']) }}
        @else
          SOLOPOS.COM - Panduan Informasi dan Inspirasi
        @endif
      </div>
      @endif

      <!-- Blog Description-->
      <div class="blog-description mt-3">
        <div class="container">
          @include('includes.ads.top-single')

          @php
            $konten = Helper::konten(htmlspecialchars_decode($content['content'])) ;
            $contents = explode('</p>', $konten);
            $total_p = count(array_filter($contents));
            // many paragraph
            if($total_p > 15 && $total_p < 25 ):
              $p_promosi  = array_slice($contents, 0, 2);
							$p_iklan_1  = array_slice($contents, 2, 1);
							$p_iklan_2  = array_slice($contents, 3, 4);
							$p_iklan_3  = array_slice($contents, 7, 5);
							$last_p  = array_slice($contents, 12);

							elseif ($total_p > 25 && $total_p < 40 ) :
              $p_promosi  = array_slice($contents, 0, 2);
							$p_iklan_1  = array_slice($contents, 2, 1);
							$p_iklan_2  = array_slice($contents, 3, 6);
							$p_iklan_3  = array_slice($contents, 9, 7);
							$p_iklan_4  = array_slice($contents, 16, 7);
							$last_p  = array_slice($contents, 23);

							elseif ($total_p > 40 ) :
              $p_promosi  = array_slice($contents, 0, 2);
							$p_iklan_1  = array_slice($contents, 2, 2);
							$p_iklan_2  = array_slice($contents, 4, 7);
							$p_iklan_3  = array_slice($contents, 11, 8);
							$p_iklan_4  = array_slice($contents, 19, 9);
							$p_iklan_5  = array_slice($contents, 28, 9);
							$p_iklan_6  = array_slice($contents, 37, 9);
							$p_iklan_7  = array_slice($contents, 46, 9);
							$last_p  = array_slice($contents, 55);

							else :
              $p_promosi  = array_slice($contents, 0, 2);
							$p_iklan_1  = array_slice($contents, 2, 1);
							$p_iklan_2  = array_slice($contents, 3, 3);
							$p_iklan_3  = array_slice($contents, 6, 3);
							$last_p  = array_slice($contents, 9);
						endif;
          @endphp
          <!-- ads top -->

          <!-- ads parallax -->
          {!! implode('</p>', $p_promosi) !!}
          <p><em><strong><span>Promosi</span></span><a href="https://m.solopos.com/{{ $promosi['slug'] }}-{{ $promosi['id'] }}?utm_source=read_promote" title="{{ $promosi['title'] }}" target="_blank" rel="noopener">{{ $promosi['title'] }}</a></strong></em>
          </p>

          {!! implode('</p>', $p_iklan_1) !!}
          @include('includes.ads.inside-article-1')

          {!! implode('</p>', $p_iklan_2) !!}
          @include('includes.ads.inside-article-2')

          {!! implode('</p>', $p_iklan_3) !!}
          @include('includes.ads.inside-article-3')

          @if($total_p > 25 && $total_p < 40 )
            {!! implode('</p>', $p_iklan_4) !!}
            @include('includes.ads.inside-article-4')

          @elseif($total_p > 40  )
            {!! implode('</p>', $p_iklan_4) !!}
            @include('includes.ads.inside-article-4')

            {!! implode('</p>', $p_iklan_5) !!}
            @include('includes.ads.inside-article-5')

            {!! implode('</p>', $p_iklan_6) !!}
            @include('includes.ads.inside-article-6')

            {!! implode('</p>', $p_iklan_7) !!}
            @include('includes.ads.inside-article-7')

          @endif

          {!! implode('</p>', $last_p) !!}
          @include('includes.ads.inside-article-last')

          @include('includes.ads.internal-promotion')

          {{-- <script src="https://geo.dailymotion.com/player/x9ykf.js" data-playlist="x6ttrz"></script> --}}
        </div>
        {{-- <div class="container mb-3" style="font-size: 15px;">
          Editor : <a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://m.solopos.com/arsip @endif" class="editor-only">{{ $content['editor'] }}</a>
        </div> --}}
        <div class="container social-share-btn d-flex align-items-center flex-wrap">
          SHARE :
          <a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="lni lni-facebook"></i></a>
          <a class="btn-twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="lni lni-twitter-original"></i></a>
          <a class="btn-instagram" href="https://www.instagram.com/koransolopos" title="social"><i class="lni lni-instagram"></i></a>
          <a class="btn-whatsapp" href="whatsapp://send?text=*{{ urlencode($header['title']) }}* | {{ urlencode($header['ringkasan']) }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="lni lni-whatsapp"></i></a>
          <a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini {{ $header['link'] }}" title="social"><i class="lni lni-envelope"></i></a>
        </div>
      </div>

      {{-- <div class="profile-content-wrapper">
        <div class="container">
          <!-- User Meta Data-->
          <div class="user-meta-data d-flex">
            <!-- User Thumbnail-->
            <div class="user-thumbnail">
                  @if(!empty($content['avatar']))
                  <img src="{{ htmlentities($content['avatar']) }}" alt="Profile">
                  @else
                  <img src="https://images.solopos.com/2019/10/avatar-solopos-370x370.jpg" alt="Profile">
                  @endif
            </div>
            <!-- User Content-->
            <div class="user-content">
              <h6>
                <a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://m.solopos.com/arsip @endif" class="editor-only">{{ $content['editor'] }}</a>
              </h6>

              <p>Jurnalis di Solopos Group. Menulis konten di Solopos Group yaitu Harian Umum Solopos, Koran Solo, Solopos.com.</p>

              <div class="post-list">
                <a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://index.solopos.com @endif">Lihat Artikel Saya Lainnya</a>
              </div>
              <div class="author-social">
                <span>Follow Me: </span>
                <a href="https://www.twitter.com/soloposdotcom" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="https://www.facebook.com/soloposcom" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://www.instagram.com/koransolopos" target="_blank"><i class="fa fa-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UC3jhKGRRBnIi5pfNIE8HOEw" target="_blank"><i class="fa fa-youtube"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div> --}}

      <div class="container mt-3">
        <div class="tag-lists">
        <span>Kata Kunci :</span>
        @if(isset($header['arrayTag']))
            @foreach($header['arrayTag'] as $tag)
            @php
                $tag_name = ucwords($tag);
                $tag_slug = str_replace(' ', '-',$tag);
                $tag_slug = strtolower($tag_slug)
            @endphp
            <a href="{{ url("/tag/{$tag_slug}") }}">{{ $tag_name }}</a>
            @endforeach
        @endif
        </div>
      </div>

      <!-- ads under article -->
      <div class="iklan mt-3">
      @include('includes.ads.under-article')
      </div>

      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Terkait</h5><a class="btn btn-primary btn-sm" href="{{ url('/') }}/arsip">Indeks Berita</a>
          </div>
        </div>
        <div class="container">
          <ul id="relposts">
            <div class="terkini-post d-flex"><div class="post-thumbnail"><a href="b20-indonesia-angkat-prinsip-bisnis-dan-investasi-berkesinambungan-1427824" title=""><img loading="lazy" src="https://images.solopos.com/2022/09/rsz_1wismilak-100x100.jpg" alt="B20 Indonesia Angkat Prinsip Bisnis dan Investasi Berkesinambungan" style="object-fit: cover; height: 100px; width: 100px;"></a></div><div class="post-content"><a class="post-title" href="b20-indonesia-angkat-prinsip-bisnis-dan-investasi-berkesinambungan-1427824" title="B20 Indonesia Angkat Prinsip Bisnis dan Investasi Berkesinambungan">B20 Indonesia Angkat Prinsip Bisnis dan Investasi Berkesinambungan</a><div class="post-meta d-flex align-items-center"><a href="#">Bisnis</a><a href="#" style="padding-left:7px;"></a></div></div></div>

            <div class="terkini-post d-flex"><div class="post-thumbnail"><a href="https://www.semarangpos.com/dseason-premiere-hotel-segudang-fasilitas-dan-paling-recomended-di-jepara-1052538" title=""><img src="https://www.semarangpos.com/files/2022/06/season-premier.jpg" loading="lazy" alt="D’Season Premiere, Hotel Segudang Fasilitas dan Paling Recomended di Jepara" style="object-fit: cover; height: 100px; width: 100px;"></a></div><div class="post-content"><a class="post-title" href="https://www.semarangpos.com/dseason-premiere-hotel-segudang-fasilitas-dan-paling-recomended-di-jepara-1052538" title="D’Season Premiere, Hotel Segudang Fasilitas dan Paling Recomended di Jepara">D’Season Premiere, Hotel Segudang Fasilitas dan Paling Recomended di Jepara</a><div class="post-meta d-flex align-items-center"><a href="#">News</a><a href="#" style="padding-left:7px;"></a></div></div></div>

          </ul>
        </div>
      </div>

      <!-- Terpopuler Wrapper-->
      <div class="mt-5">
        @include('includes.widget-popular') 
      </div>

      {{--<!-- Ads Mobile Single 0 -->
      @include('includes.ads.article-single-0')
      --}}

      {{--<div class="mt-1 mb-5">
        @include('includes.widget-solopos-stories')
      </div>--}}

      <!-- Ads Mobile Single 1 -->
      @include('includes.ads.article-single-1')

      <div class="mt-1">
        @include('includes.widget-slides')
      </div>

      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Lainnya</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/arsip" target="_blank">Indeks Berita</a>
        {{--<h2 class="block-title">
          <span class="title-angle-shap"> Rekomendasi </span>
        </h2>--}}
          </div>
        </div>
        <div class="container d-flex">
          <ul id="widgetbisnis" class="harjo-widget"></ul>
        </div>
        <div class="container d-flex">
          <ul id="rekomendasi" class="harjo-widget"></ul>
        </div>
        @if( date('Y-m-d H:i:s') >= '2022-06-08 00:00:01' && date('Y-m-d H:i:s') <= '2022-09-02 23:59:59')
					<div class="container d-flex"><ul id="lock" class="harjo-widget">
					<li><a href="https://jogjapolitan.harianjogja.com/read/2022/08/26/510/1109984/tinjau-sumber-mata-air-pdam-tirtamarta-di-lereng-merapi-penjabat-wali-kota-jogja-jamin-ketersediaan-air-bersih" title="Tinjau Sumber Mata Air PDAM Tirtamarta di Lereng Merapi, Penjabat Wali Kota Jogja Jamin Ketersediaan Air Bersih" target="_blank" rel="noopener noreferrer">Tinjau Sumber Mata Air PDAM Tirtamarta di Lereng Merapi, Penjabat Wali Kota Jogja Jamin Ketersediaan Air Bersih</a></li>
					<li><a href="https://jogjapolitan.harianjogja.com/read/2022/08/11/510/1108649/penataan-pedestrian-senopati-dukung-wisata-malioboro" title="Penataan Pedestrian Senopati Dukung Wisata Malioboro" target="_blank" rel="noopener noreferrer">Penataan Pedestrian Senopati Dukung Wisata Malioboro</a></li>
					</ul>
        </div>
					@endif
      </div>

      <!-- Start Related Wrapper -->
      {{-- @if(!empty($related))
      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">{{$relatedtitle}}</h5>
          </div>
        </div>
        <div class="container">
          @php $rel_loop = 1; @endphp
          @foreach($related as $rel) @if($rel_loop <= 5)

          @php
          $image = $rel['one_call']['featured_list']['source_url'] ?? 'https://m.solopos.com/images/solopos.jpg';
          $title = html_entity_decode($rel['title']['rendered']);
          @endphp
          <!-- related Post-->
          @if ($rel_loop==1)
          <div class="card mb-3">
            <a href="{{ url("/{$rel['slug']}-{$rel['id']}") }}" title="{{ $title }}">
            <img class="card-img-top" src="{{ $image }}" loading="lazy" alt="{{ $title }}">
            </a>
            <div class="card-body">
              <a class="post-title" href="{{ url("/{$rel['slug']}-{$rel['id']}") }}" title="{{ $title }}">{{ $title }}</a>
            </div>
          </div>
          @else
          <div class="terkini-post d-flex">
            <div class="post-thumbnail">
              <a href="{{ url("/{$rel['slug']}-{$rel['id']}") }}" title="{{ $title }}">
                <img src="{{$image }}" loading="lazy" alt="{{ $rel['title']['rendered'] }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;">
              </a>
            </div>
            <div class="post-content">
              <a class="post-title" href="{{ url("/{$rel['slug']}-{$rel['id']}") }}" title="{{ $title }}">{{  $title }}</a>
              <div class="post-meta d-flex align-items-center">
                <a href="#">{{ $rel['one_call']['categories_list'][0]['name'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($rel['date']) }}</a>
              </div>
            </div>
          </div>
          @endif
          @endif
          @php $rel_loop++; @endphp
          @endforeach
        </div>
      </div>
      @endif    --}}

      <!-- widget promosi -->
      @include('includes.widget-promosi')
      <!-- Ads Mobile Single 2 -->
      @include('includes.ads.article-single-2')

      <div class="mt-3 mb-3">
        @include('includes.widget')
      </div>

      <!-- Facebook Komentar -->
      {{-- <div class="container">
        <div class="d-flex align-items-center justify-content-between mb-3">
          <h5 class="mb-0 pl-1 spos-title">Komentar Anda</h5>
        </div>

        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v11.0&appId=1438927622798153&autoLogAppEvents=1" nonce="uCAeYQ2X"></script>
        <div class="fb-comments" data-href="https://m.solopos.com" data-width="320" data-numposts="5"></div>
      </div> --}}
      <!-- Konten Premium Wrapper -->
      <div class="container">
        <div class="editorial-choice-news-wrapper mt-3">
          <div class="bg-shape1"><img loading="lazy" src="{{ asset('images/edito.png') }}"></div>
          <div class="bg-shape2" style="background-image: url('{{ asset('images/edito2.png') }}')"></div>
          <div class="container" align="center">
            <img loading="lazy" src="{{ url('images/plus-putih.png') }}" height="50%" width="50%" style="margin-bottom: 30px;">
          </div>
          <div class="container">
            <div class="editorial-choice-news-slide owl-carousel">
              @php $pc_loop = 1; @endphp
              @foreach($premium as $bp)
                @if($pc_loop <=6)

              <div class="single-editorial-slide d-flex">
                <a class="bookmark-post" href="#"><i class="lni lni-star"></i></a>
                <div class="post-thumbnail">
                  <img loading="lazy" src="{{ $bp['images']['thumbnail'] }}" alt="{{ json_encode($bp['images']['caption']) }}" style="object-fit: cover; height: 177px; width: 132px;">
                </div>
                <div class="post-content">
                  <!--<a class="post-catagory" href="https://m.solopos.com/premium">Premium {{ ucwords($bp['category']) }}</a>-->
									{{-- <span class="espos-plus">+ PLUS</span> --}}
                  <a class="post-title d-block" href="{{ url("/{$bp['slug']}-{$bp['id']}") }}" title="{{ html_entity_decode($bp['title']) }}" >{{ html_entity_decode($bp['title']) }}</a>
                </div>
              </div>
              @endif
              @php $pc_loop++; @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>

      {{-- <div class="container mt-3">
        <!-- Composite Start -->
        <div id="M628318ScriptRootC990860">
        </div>
        <script src="https://jsc.mgid.com/s/o/solopos.com.990860.js" async></script>
        <!-- Composite End -->
      </div> --}}

      {{-- <div class="container mt-3">
        <!-- Begin Dable bottom_new / For inquiries, visit http://dable.io -->
        <div id="dablewidget_Pl1OVjoE" data-widget_id="Pl1OVjoE">
        <script>
        (function(d,a,b,l,e,_) {
        if(d[b]&&d[b].q)return;d[b]=function(){(d[b].q=d[b].q||[]).push(arguments)};e=a.createElement(l);
        e.async=1;e.charset='utf-8';e.src='//static.dable.io/dist/plugin.min.js';
        _=a.getElementsByTagName(l)[0];_.parentNode.insertBefore(e,_);
        })(window,document,'dable','script');
        dable('setService', 'm.solopos.com');
        dable('sendLogOnce');
        dable('renderWidget', 'dablewidget_Pl1OVjoE');
        </script>
        </div>
        <!-- End bottom_new / For inquiries, visit http://dable.io -->
      </div> --}}

      <!-- Ads Mobile Single 3 -->
      @include('includes.ads.article-single-3')

      <!-- Terkini Wrapper-->

      <div class="terkini-wrapper"> <!--loadmore-frame-->
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Terkini</h5><a class="btn btn-primary btn-sm" href="{{ url('/') }}/arsip">Indeks Berita</a>
          </div>
        </div>
        <div class="container">
          @php $loop_br = 1; @endphp
          @foreach ($breakingcat as $item)
          @if( $loop_br <= 20 )
            <!-- Terkini Post-->
            <div class="terkini-post"> <!-- content-box -->
              <div class="d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">
                    <img loading="lazy" src="{{ $item['images']['thumbnail'] }}" alt="{{ html_entity_decode($item['title']) }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  {{-- @if($item['konten_premium'] == 'premium')
                    <span class="espos-plus">+ PLUS</span>
                  @endif --}}
                  <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">{{ html_entity_decode($item['title']) }}</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="">{{ $item['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                  </div>
                </div>
              </div>
            </div>
            @if( $loop_br == 5 )
            @include('includes.ads.article-single-terkini-1')
            @elseif ($loop_br == 10)
            @include('includes.ads.article-single-terkini-2')
            @elseif ($loop_br == 15)
            @include('includes.ads.article-single-terkini-3')
            @endif
            @endif

            @php $loop_br++; @endphp
            @endforeach
            <div class="text-center mt-3">
              <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more" title="Kumpulan Berita">
                Arsip Berita
              </a>
              {{--<a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Berita Lainnya
              </a>
              <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>--}}
            </div>
        </div>
      </div>

      {{-- <div class="container">
          <!-- Composite Start -->
          <div id="M628318ScriptRootC990864">
          </div>
          <script src="https://jsc.mgid.com/s/o/solopos.com.990864.js" async></script>
          <!-- Composite End -->
      </div> --}}

      <div class="container">
        <div class="border-top"></div>
      </div>

      {{-- <img src="https://cms.solopos.com/set-view?id={{ $content['id'] }}" style="display: none;"> --}}
    </div>

    {{-- <iframe src="https://api.solopos.com/set-view?id={{ $content['id'] }}" style="position: absolute;width:0;height:0;border:0;bottom:0;"></iframe> --}}

    @push('custom-scripts')
    <script>

      $(document).ready(function() {
          $.ajax({ //create an ajax request related post
          type: "GET",
          url: "https://api.solopos.com/api/wp/v2/posts?tags={{ $relatedTags }}&exclude={{ $content['id'] }}&per_page=4", //72325
          dataType: "JSON",
          success: function(data) {
            // console.log(data);
              var relatedPosts = $("#relposts");

              $.each(data, function(i, item) {
                //alert(data[i].item.id);
                // console.log(data[i]['title']['rendered']);
                // relatedPosts.append("<li><a href='" + data[i]['slug'] + "-" + data[i]['id'] + "'>" + data[i]['title']['rendered'] + "</a></li>");

                relatedPosts.append("<div class=\"terkini-post d-flex\"><div class=\"post-thumbnail\"><a href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"\"><img src=\"" + data[i]['one_call']['featured_list']['media_details']['sizes']['thumbnail']['source_url'] +"\" loading=\"lazy\" alt=\"" + data[i]['title']['rendered'] + "\" style=\"object-fit: cover; height: 100px; width: 100px;\"></a></div><div class=\"post-content\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"" + data[i]['title']['rendered'] + "\">" + data[i]['title']['rendered'] + "</a><div class=\"post-meta d-flex align-items-center\"><a href=\"#\">" + data[i]['one_call']['categories_list'][0]['name'] + "</a><a href=\"#\" style=\"padding-left:7px;\"></a></div></div></div>");
              });
				  }
			  });
		  });

      $(document).ready(function () {
            $.ajax({
                url: 'https://www.harianjogja.com/rss',
                type: 'GET',
                dataType: "xml"
            }).done(function(xml) {

                $.each($("item", xml), function(i, e) {

                    var postNumber = i + 1 + ". ";

                    var itemURL = ($(e).find("link"));
                    var postURL = "<a href='" + itemURL.text() + "'>" + itemURL.text() +"</a>";

                    // var itemImg = ($(e).find("enclosure"));
                    // var imgUrl = itemImg.getAttribute('url');

                    var itemTitle = ($(e).find("title"));
                    var postTitle = "<a href='" + itemURL.text() + "'>" + itemTitle.text() + "</a>";

                    if(postNumber <= 3) {

                    $("#rekomendasi").append("<li><a href=\"" + itemURL.text() + "\" title=\""+ itemTitle.text() +"\" target=\"_blank\">" + itemTitle.text() +"</a></li>");

                    // $("#rekomendasi").append("<li><div class=\"post-block-style media\"><!--<div class=\"post-thumb\"><a href=\"" + itemURL.text() + "\" title=\"\"><img class=\"img-fluid\" src=\"\" style=\"object-fit: cover; object-position: center; height: 85px; width: 85px;\"></a></div>--><div class=\"post-content media-body\"><h2 class=\"post-title\"><a href=\"" + itemURL.text() + "\" title=\""+ itemTitle.text() +"\" target=\"_blank\">" + itemTitle.text() +"</a></h2><!--<div class=\"post-meta mb-7\"><span class=\"post-date\"><i class=\"fa fa-clock-o\"></i> </span></div>--></div></div></li>");

                    }
                });
            });
        });

        $(document).ready(function () {
             $.ajax({
                 url: 'https://rss.bisnis.com',
                 type: 'GET',
                 dataType: "xml"
             }).done(function(xml) {

                 $.each($("item", xml), function(i, e) {

                     var postNumber = i + 1 + ". ";

                     var itemURL = ($(e).find("link"));
                     var postURL = "<a href='" + itemURL.text() + "'>" + itemURL.text() +"</a>";

                     // var itemImg = ($(e).find("enclosure"));
                     // var imgUrl = itemImg.getAttribute('url');

                     var itemTitle = ($(e).find("title"));
                     var postTitle = "<a href='" + itemURL.text() + "'>" + itemTitle.text() + "</a>";

                     if(postNumber <= 4) {

                     $("#widgetbisnis").append("<li><a href=\"" + itemURL.text() + "\" title=\""+ itemTitle.text() +"\" target=\"_blank\" rel=\"noopener noreferrer\">" + itemTitle.text() +"</a></li>");

                     }
                 });
             });
         });

        // function addCredit(){var t=$(".title-only").text(),e=document.location.href,a=$(".penulis").text(),i=$(".editor-only").text(),dt=$(".tgl-terbit").text(),n="",o="";a&&(n="Author : "+a),i&&(o="<br />"+i);var r,l=n.concat(o),d=document.getElementsByTagName("body")[0],p=(r=window.getSelection())+'<br /><br />-------------------------------------------------<br /> Artikel ini telah tayang di <a href="https://m.solopos.com/">Solopos.com</a> dengan judul "'+t+'", Klik selengkapnya di sini: <a href="'+e+'">'+e+"</a> <br />"+'Author: '+a+'<br />Editor: '+i+'<br />Tayang: '+dt+'<br/><br/>  Belanja Fashion Etnik? ya di <a href="https://pisalin.com/">Pisalin by Toko Solopos</a><br/>"Inspirasi Wastra Nusantara"<br/>Grosir - Reseller - Dropship - Batik - Tenun - Lurik<br/><br/>Solopos.com - Panduan Insformasi & Inspirasi',s=document.createElement("div");s.style.position="absolute",s.style.left="-99999px",d.appendChild(s),s.innerHTML=p,r.selectAllChildren(s),window.setTimeout(function(){d.removeChild(s)},0);}
        // document.addEventListener('copy',addCredit);

        document.oncopy = function () {
        var _title = $(".title-only").text(),
            _editor = $(".editor-only").text(),
            _author = $(".penulis").text(),
            _publish = $(".tgl-terbit").text(),
            _href = document.location.href,
                _result,
                _body = document.getElementsByTagName("body")[0];
                _text =
                    (nl2br(_result = window.getSelection())) +
                    '<br /><br /><br /> Baca artikel <b>"' + _title.trim() +
                    '"</b> selengkapnya di sini: <a href="' +
                    _href +
                    '">' +
                    _href +
                    '</a>.'+
                    '<br />Editor  : '+_editor+'<br />Penulis: '+_author+'<br />Publish: '+_publish+
                    '<br/><br/> Mau Mobil SUV Idaman hanya dengan Rp.328/hari? <a href="https://id.solopos.com/register">Langganan Espos Plus Sekarang</a><br/>Silakan berlangganan dan dapatkan berbagai konten menarik di Espos Plus.<br/><br/>Solopos.com - Panduan Insformasi & Inspirasi';

        if (window.clipboardData && window.clipboardData.setData) {
            // IE specific code path to prevent textarea being shown while dialog is visible.
            return clipboardData.setData(p);

        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {

            var ss = document.createElement("div");

                (ss.style.position = "absolute"),
                (ss.style.left = "-99999px"),
                _body.appendChild(ss),
                (ss.innerHTML = _text),
                _result.selectAllChildren(ss);
                window.setTimeout(function () {
                    _body.removeChild(ss);
                }, 0);
        }
        };


        function nl2br (str, is_xhtml) {
        if (typeof str === 'undefined' || str === null) {
        return '';
        }
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        }


        // $(window).load(function() {
        //     $.ajax({
        //         type: "GET",
        //         url: 'https://api.solopos.com/set-view?id={{ $content['id'] }}',
        //     });
        // });

    </script>
    @endpush
    @push('tracking-scripts')
        <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        url:'https://tf.solopos.com/api/v1/stats/store',
        method:'POST',
        data:{
                post_id:'{{ $content['id'] }}',
                post_title:'{{ $header['title'] }}',
                post_slug:'{{ $content['slug'] }}',
                post_date:'{{ $content['date'] }}',
                post_author:'{{ $header['author'] }}',
                post_editor:'{{ $header['editor'] }}',
                post_category:'{{ $header['category_parent'] }}',
                post_subcategory:'{{ $header['category_child'] }}',
                post_tag:'{!! serialize($content['tag']) !!}',
                post_thumb:'{{ $header['image'] }}',
                post_view_date:'{{ date('Y-m-d') }}',
                domain:'{{ 'm.solopos.com' }}'
            },
        success:function(response){
            if(response.success){
                console.log(response.message)
            }else{
                console.log(error)
            }
        },
        error:function(error){
            console.log(error)
        }
        });
        </script>
    @endpush
@endsection
