@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper">

      <!-- Headline Wrapper-->
      <div class="headline-wrapper">
        <div class="container">
          <div class="slides-wrapper">
            <!-- Hero Slides-->
            <div class="hero-slides owl-carousel">           
              @php $hl_loop = 1; @endphp
              @foreach($headline as $hl)
              @if($hl_loop <=5)           
              <div class="single-hero-slide">
                <div class="headline-image d-flex align-items-start">
                  <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ $hl['title'] }}">
                  <img src="{{ $hl['images']['thumbnail'] }}" loading="lazy" alt="{{ json_encode($hl['images']['caption']) }}" loading="lazy">
                  </a>
                </div>
                <div class="d-flex align-items-end">
                  <div class="container-fluid mb-3">
                    <div class="post-meta d-flex align-items-center">
                      <a href="">{{ $hl['category'] }}</a> | <span style="margin-left: 5px;">{{ Helper::time_ago($hl['date']) }}</span>
                    </div>                  
                    <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ $hl['title'] }}">{{ $hl['title'] }}</a>
                  </div>
                </div>
              </div>
              @endif
              @php $hl_loop++ @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>

      <div class="mt-3 mb-3" align="center">
        <a target="_blank" href="https://bob.kemenparekraf.go.id/" target="_blank">
          <img src="{{ asset('banner/bob-banner-mr.gif') }}">
        </a>
  	  </div>

      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Artikel Terkini</h5><a class="btn btn-primary btn-sm" href="/arsip">Indeks</a>
          </div>
        </div>
        <div class="container">
          @foreach ($artikel as $item)
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  <img src="{{ $item['images']['thumbnail'] }}" alt="" style="object-fit: cover; height: 100px; width: 100px;">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $item['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            </div>
            @endforeach
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Berita Lainnya
              </a>
              <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>                               
        </div>
      </div>

      <!-- Ads Breaking -->
      <div class="mt-3 mb-3" align="center">
        <a target="_blank" href="https://instagram.com/boborobudur" target="_blank">
          <img src="{{ asset('banner/bob-banner-mr.gif') }}">
        </a>
  	  </div>

      <!-- Info Grfis Wrapper -->
      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">INFO GRAFIS</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/wisata-joglosemar/grafis">Indeks Grafis</a>
          </div>
        </div>
        <div class="container">  
          @php $g_loop = 1; @endphp
          @foreach($grafis as $item)
          @if($g_loop <=5)                  
          <div class="card mb-3">
            <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
            <img class="card-img-top" src="{{ $item['images']['thumbnail'] }}" alt="{{ $item['title'] }}">
            </a>
            <div class="card-body">
            <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
            </div>
          </div>
          @endif
          @php $g_loop++ @endphp
          @endforeach                            
        </div>
      </div>

      <div class="mt-3 mb-3" align="center">
        <a target="_blank" href="https://youtube.com/channel/UCfdq2dv6zodZqqWpvc3U-MA" target="_blank">
          <img src="{{ asset('banner/bob-banner-mr.gif') }}">
        </a>
      </div>
      
      <!-- Solopos TV-->
      <div class="video-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between">
            <h5 class="mb-0 pl-1 spos-title">GALERI FOTO</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/wisata-joglosemar/foto">Indeks Foto</a>
          </div>
        </div>
        <div class="container">
          <div class="row spos-video-slides owl-carousel">
            @php $f_loop = 1; @endphp
            @foreach($foto as $item)
            @if($f_loop <=5) 
            <div class="col-md-4 mt-3">
              <div class="spos-v-card">
                <a class="video-icon" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}"><i class="lni lni-image"></i></a>
                <div class="post-thumbnail">
                  <img src="{{ $item['images']['thumbnail'] }}" alt="{{ $item['title'] }}" style=" height: 220px;">
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  {{ $item['title'] }}
                  </a>
                </div>
              </div>
            </div>
            @endif
            @php $f_loop++ @endphp
            @endforeach              
          </div>
        </div>
      </div>

      <div class="container">
        <div class="border-top"></div>
      </div>

      <style type="text/css">
      .spos-v-card .post-thumbnail::after {opacity: 0 !important; }
      .single-hero-slide::after {background:linear-gradient(182deg, rgb(0 196 255) 5%, rgb(9 21 49) 100%);}
      </style>

    </div>
@endsection