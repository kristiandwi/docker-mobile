@extends('layouts.app-sbbi')
@section('content')	
    <div class="uk-cover-container uk-light uk-flex uk-flex-middle" style="height: 23vw">				
        <!-- TOP CONTAINER -->
        <div class="uk-container uk-flex-auto top-container uk-position-relative uk-margin-medium-top" data-uk-parallax="y: 0,50; easing:0; opacity:0.9">
            <div class="uk-width-1-1@s" data-uk-scrollspy="cls: uk-animation-slide-right-medium; target: > *; delay: 150">

            </div>
        </div>
        <!-- /TOP CONTAINER -->
        <!-- TOP IMAGE -->

        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-srcset="https://cdn.solopos.com/sbbi/2022/top-banner.jpg"
        data-sizes="12vw"
        data-src="https://picsum.photos/1200/900/?image=816" alt="" data-uk-cover data-uk-img data-uk-parallax="opacity: 1,0.1; easing:0"
        >
        <!-- /TOP IMAGE -->
    </div>
</div>
<section class="uk-section uk-article">
    <div class="uk-container uk-container-small">
        <h1 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">{{html_entity_decode($content['title'])}}</h1><br>
        {{--<p class="uk-article-meta" style="padding-bottom: 30px;">{{ Carbon\Carbon::parse($content['date'])->translatedFormat('l, j F Y - H:i') }} WIB | Posted in <a href="{{ url('/') }}/sbbi">SBBI</a></p>
         {{-- {{ Helper::indo_datetime($content['date']) }} WIB --}}
    </div>
    
    <!-- large image -->
    <!--<div class="uk-container uk-section">
        <div class="uk-position-relative uk-visible-toggle uk-light" data-uk-slideshow="ratio: 7:3; animation: push; min-height: 270; velocity: 3">
            <ul class="uk-slideshow-items">
                <li>
                    <img data-src="https://www.solopos.com/images/no-medium.jpg" data-uk-img="target: !.uk-slideshow-items" alt="Tentang Solopos Digital Award (SDA) 2021" data-uk-cover>
                    <div class="uk-position-bottom uk-position-medium uk-text-center uk-light">
                        <p class="uk-margin-remove uk-visible@s"></p>
                    </div>
                </li>
            </ul>
        </div>
    </div>-->
    <!-- /large image -->
    <!-- text -->
    <div class="uk-container uk-container-small">
        @php
        $konten = Helper::konten(htmlspecialchars_decode($content['content'])) ;
        $contents = explode('</p>', $konten);
        $total_p = count(array_filter($contents));
        @endphp
        {!! $konten !!}

        {{-- <p><a href="https://images.solopos.com/2021/05/thumbnail-microsite-SDA-02.png"><img loading="lazy" class="size-full wp-image-1124733 aligncenter" src="https://images.solopos.com/2021/05/thumbnail-microsite-SDA-02.png" alt="" width="100%" height="100%" srcset="https://images.solopos.com/2021/05/thumbnail-microsite-SDA-02-560x343.png 560w, https://images.solopos.com/2021/05/thumbnail-microsite-SDA-02.png 750w" sizes="(max-width: 750px) 100vw, 750px" /></a></p>
        <p>Di era serbadigital, Solopos Group memberi sentuhan baru terhadap program penghargaan pada 2021. Bersamaan dengan pelaksanaan SBBI 2021, Solopos juga menggelar Solopos Digital Award atau SDA 2021.</p>
        <p>Sama-sama mengukur dinamika merek, SDA dan SBBI berbeda. SDA 2021 mengukur popularitas sebuah brand dengan riset berbasis data online alias digital. Pada tahun ini, SDA hanya menyasar satu kategori yakni pemerintahan.</p>
        <p>Penghargaan SDA bakal diberikan kepada kepala daerah, humas pemerintah daerah, dan pemerintah daerah paling populer di beberapa kabupaten/kota di Jawa Tengah dan Jawa Timur yang paling populer.</p>
        <p>Popularitas sejumlah kalangan itu diukur dari performanya di kancah digital. Solopos menggunakan parameter yang bisa dipertanggungjawabkan untuk mengukur popularitas tokoh publik atau pemerintahan tersebut.</p>
        <p>&nbsp;</p>
        <p><strong>Makin Dekat Makin Cepat</strong></p>
        <p>Riset berbasis digital seperti SDA 2021 menjadi penting di Indonesia, mengingat semakin banyak masyarakat Tanah Air melek digital, terutama media sosial. Laporan berjudul <em>Digital 2021: The Latest Insights Into The State of Digital</em> mengungkap dari total 274,9 juta penduduk di Indonesia, 170 juta di antaranya menggunakan media sosial (61,8%).</p>
        <p>Dengan kata lain, lebih dari separuh masyarakat akan melihat “profil digital” pemimpin atau pemerintahan melalui kanal digital. Segala informasi yang disebar melalui kanal digital berpeluang besar diketahui atau memengaruhi sebagain besar masyarakat tersebut.</p>
        <p>Fakta-fakta itu menguatkan Solopos untuk menghadirkan SDA 2021. Pada tahun ini, SDA mengangkat tema <em>“Makin Dekat Makin Cepat”</em>. Tema ini untuk mengambarkan bagaimana pejabat publik maupun instansi pemerintahan saat ini harus bisa dekat dengan <em>stakeholders</em>/masyarakat.</p>
        <p>Mereka juga harus bisa cepat dalam merespons keluhan dan problematika di masyarakat. Media <em>online</em> dan media sosial menjadi sarana mereka “makin dekat” dengan masyarakat.</p>
        <p>Lewat SDA 2021, Solopos ingin mendukungan kalangan pemerintahan agar memberikan layanan tercepat kepada masyarakat.</p>
        <p>&nbsp;</p>
        <p><strong>Tujuan SDA 2021</strong></p>
        <p>SDA 2021 digelar dengan tujuan:</p>
        <ul>
        <li>Memberi apresiasi kepada kepala daerah/pemerintah daerah yang telah merawat “profil digital” mereka dengan baik.</li>
        <li>Memberi masukan/saran kepada kalangan pemerintahan untuk memperbaiki kinerja khsusunya dalam hal memberikan layanan cepat untuk masyarakat.</li>
        </ul>
        <p>&nbsp;</p>
        <p><strong>Kategori SDA 2021</strong></p>
        <p>SDA 2021 hanya memberikan penghargaan untuk satu kalangan yakni pemerintahan. Dengan perincian penghargaan meliputi kepala daerah terpopuler, humas pemerintah daerah terpopuler, dan pemerintah daerah terpopuler.</p>
        <p>&nbsp;</p>
        <hr />
        <p><span style="color: #00ccff;"><strong>Baca Juga</strong></span></p>
        <ul>
        <li><a href="https://www.solopos.com/sbbi/kilas-balik">Kilas Balik SBBI Awards</a></li>
        <li><a href="https://www.solopos.com/sbbi/pemenang-2020">Pemenang SBBI 2020</a></li>
        </ul> --}}
    </div>
    <!-- text -->
		<!-- LOGOS -->
		{{--
			<div id="partner" class="uk-container uk-container-small uk-section uk-section-small uk-section-muted" tabindex="-1" uk-slider>
			<div>
			    <ul class="uk-slider-items uk-child-width-1-4 uk-child-width-1-3@s uk-child-width-1-4@m logos-grid">
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/mitsubishi.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/mandiri.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/mandiri-syariah.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/bank-bri.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/kiat-motor.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/sun-motor.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/prodia.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/sharp.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/indofood.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/pku-sejati.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/djarum-super.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/djarum-76.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/djarum-mld.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/yamaha.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/candi-elektronik.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/best-western.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/telkom.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/solo-paragon.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/ums.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/superindo.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/astra.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/lorin-hotel.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/syariah-hotel.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/raya-seluler.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/ilufa.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/ella-skincare.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/nasmoco.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/daihatsu.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/kopi-luwak.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://cms.solopos.com/assets/ads/sbbi/tolak-angin.jpg" data-uk-img alt="SBBI 2020">
					</li>					
			    </ul>
				
		    </div>
		</div>
		--}}
		<!-- /LOGOS -->    
</section>
@endsection