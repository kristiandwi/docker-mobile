@extends('layouts.app')
@section('content')
<div class="page-content-wrapper">
	  <!-- Blok 1 Terkini Wrapper -->
      <div class="terkini-wrapper">
        <div class="container">
			<img style="max-width: 100%; margin-bottom: 30px;" src="https://bob.kemenparekraf.go.id/wp-content/uploads/2021/06/Whats_App_Image_2021_06_23_at_13_25_34_1_f49c2f749f.jpeg">
			<h3>Kontak Kami</h3>
			<p>Badan Otorita Borobudur bertugas melakukan koordinasi, sinkronisasi, dan fasilitasi  perencanaan, pengembangan, pembangunan, dan pengendalian di Kawasan Pariwisata Borobudur; dan melakukan perencanaan, pengembangan, pembangunan, pengelolaan, dan pengendalian di zona otorita Kawasan Pariwisata Borobudur.<a href="https://bob.kemenparekraf.go.id/" target="_blank">Selengkapnya...</a></p>
			<div class="widget contact-info">
				<div class="contact-info-box">
					<div class="contact-info-box-content">
						<h4>Alamat</h4>
						<p>Jl. Faridan M Noto No.19, Kotabaru, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta.<</p>
					</div>
				</div>

				<div class="contact-info-box">
					<div class="contact-info-box-content">
						<h4>Alamat Eamil</h4>
						<p>bob@kemenparekraf.go.id</p>
					</div>
				</div>

				<div class="contact-info-box">
					<div class="contact-info-box-content">
						<h4>Nomor Telepon</h4>
						<p>(0274) 581769</p>
					</div>
				</div>

				<div class="contact-info-box">
					<div class="contact-info-box-content">
						<h4>Website</h4>
						<p>bob.kemenparekraf.go.id</p>
					</div>
				</div>

			</div><!-- Widget end -->

			<h3>Kontak Form</h3>
			<form id="contact-form" action="#" method="post">
				<div class="error-container"></div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Nama</label>
						<input class="form-control form-control-name" name="name" id="name" placeholder="" type="text" required>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Email</label>
							<input class="form-control form-control-email" name="email" id="email" 
							placeholder="" type="email" required>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Tentang</label>
							<input class="form-control form-control-subject" name="subject" id="subject" 
							placeholder="" required>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Pesan</label>
					<textarea class="form-control form-control-message" name="message" id="message" placeholder="" rows="10" required></textarea>
				</div>
				<div>
					<button class="btn btn-submit" type="submit">Kirim Pesan</button> 
				</div>
			</form>
        </div>
      </div> 
</div>
@endsection