@extends('layouts.app')
@section('content')

@include('includes.ads.popup-banner')
    <div class="page-content-wrapper">
      <!-- Headline Wrapper-->
      <div class="headline-wrapper">
        <div class="container">
          <!-- Hero Slides-->
          <div class="slides-wrapper">
            <div class="hero-slides owl-carousel">
              @include('includes.ads.hl-feedblock')           
              @php $hl_loop = 1; @endphp
              @foreach($headline as $hl)
              @if($hl_loop <=5)           
              <div class="single-hero-slide">
                <div class="headline-image d-flex align-items-start">
                  <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">
                  <img src="{{ $hl['images']['thumbnail'] }}" loading="lazy" alt="{{ json_encode($hl['images']['caption']) }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                  </a>
                </div>
                <div class="d-flex align-items-end">
                  <div class="container-fluid mb-3">
                    <div class="post-meta d-flex align-items-center">
                      <a href="">{{ $hl['category'] }}</a> | <span style="margin-left: 5px;">{{ Helper::time_ago($hl['date']) }}</span>
                    </div>                  
                    {{-- @if($hl['konten_premium'] == 'premium') 
                    <span class="espos-plus">+ PLUS</span>
                    @endif --}}
                    <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">{{ html_entity_decode($hl['title']) }}</a>
                  </div>
                </div>
              </div>
              @endif
              @php $hl_loop++ @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>

      <!-- Konten Premium Wrapper -->
      <div class="container">
        <div class="editorial-choice-news-wrapper mt-3">
          <div class="bg-shape1"><img loading="lazy" src="{{ asset('images/edito.png') }}"></div>
          <div class="bg-shape2" style="background-image: url('{{ asset('images/edito2.png') }}')"></div>
          <div class="container" align="center">
            <img loading="lazy" src="{{ url('images/plus-putih.png') }}" height="50%" width="50%" style="margin-bottom: 30px;">
          </div>
          <div class="container">
            <div class="editorial-choice-news-slide owl-carousel">
              @php $pc_loop = 1; @endphp
              @foreach($premium as $bp)
                @if($pc_loop <=6)
                                   
              <div class="single-editorial-slide d-flex">
                <a class="bookmark-post" href="https://m.solopos.com/plus"><i class="lni lni-circle-plus"></i></a>
                <div class="post-thumbnail">
                  <img loading="lazy" src="{{ $bp['images']['thumbnail'] }}" alt="{{ json_encode($bp['images']['caption']) }}" style="object-fit: cover; height: 177px; width: 132px;">
                </div>
                <div class="post-content">
                  <!--<a class="post-catagory" href="https://m.solopos.com/premium">Premium {{ ucwords($bp['category']) }}</a>-->
									{{-- <span class="espos-plus">+ PLUS</span> --}}
                  <a class="post-title d-block" href="{{ url("/{$bp['slug']}-{$bp['id']}") }}" title="{{ html_entity_decode($bp['title']) }}" title="{{ html_entity_decode($bp['title']) }}">{{ html_entity_decode($bp['title']) }}</a>
                </div>
              </div>
              @endif
              @php $pc_loop++; @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>

      @include('includes.ads.home-1-banner')
      {{-- @include('includes.ads.top-banner')--}}
      {{-- @include('includes.widget-soloposfm') --}}

      @include('includes.widget-popular-all')

      @include('includes.widget-promosi')
      <!-- Blok 1 Terkini Wrapper -->
      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Terkini</h5><a class="btn btn-primary btn-sm" href="/arsip">Indeks Berita</a>
          </div>
        </div>
        <div class="container">
          @php $br_loop = 1; @endphp
          @foreach ($breaking as $item) @if($br_loop <= 3)
              
            <!-- Terkini Post-->
            <div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">
                  <img src="{{ $item['images']['url_thumb'] }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                {{-- @if($item['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">{{ html_entity_decode($item['title']) }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $item['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            @endif
            @php $br_loop++ @endphp
            @endforeach    
            @include('includes.feedblock')                                
        </div>
      </div>
      <!-- ads parallax -->
      <div class="mt-1 mb-5">
        @include('includes.widget-solopos-stories')
      </div>
      <!-- Mobile Home 2 Banner -->
      @include('includes.ads.home-2-banner')

      <!-- Editors Choice Wrapper -->
      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Fokus</h5><a class="btn btn-primary btn-sm" href="/arsip">Indeks Berita</a>
          </div>
        </div>
        <div class="container">  
          @php $ec_loop = 1; @endphp
          @foreach($editorchoice as $ec) @if($ec_loop <= 5)  
                              
          <!-- Terkini Post--> 
          @if ($ec_loop==1)
          <div class="card mb-3">
            <a href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ html_entity_decode($ec['title']) }}">
            <img class="card-img-top" src="{{ $ec['images']['thumbnail'] }}" loading="lazy" alt="{{ html_entity_decode($ec['title']) }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
            </a>
            <div class="card-body">
              {{-- @if($ec['konten_premium'] == 'premium') 
								<span class="espos-plus">+ PLUS</span>
							@endif --}}
              <a class="post-title" href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ html_entity_decode($ec['title']) }}">{{ html_entity_decode($ec['title']) }}</a>
            </div>
          </div>
          @else     
          <div class="terkini-post d-flex">
            <div class="post-thumbnail">
              <a href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ html_entity_decode($ec['title']) }}">
                <img src="{{ $ec['images']['url_thumb'] }}" loading="lazy" alt="{{ html_entity_decode($ec['title']) }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
              </a>
            </div>
            <div class="post-content">
              {{-- @if($ec['konten_premium'] == 'premium') 
								<span class="espos-plus">+ PLUS</span>
							@endif --}}
              <a class="post-title" href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ html_entity_decode($ec['title']) }}">{{ html_entity_decode($ec['title']) }}</a>
              <div class="post-meta d-flex align-items-center">
                <a href="https://m.solopos.com/">{{ $ec['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($ec['date']) }}</a>
              </div>
            </div>
          </div>
          @endif
          @endif
          @php $ec_loop++; @endphp
          @endforeach                             
        </div>
      </div>

      <!-- widget topik -->
      <div class="mt-3">
        @include('includes.widget-slides')		
      </div>

      <!-- Mobile Home 3 Banner -->
      @include('includes.ads.home-3-banner')

      <!-- Tematik Wrapper-->
      <div class="mt-3 mb-1">
        @include('includes.widget')		
      </div>

      <!-- Blok 2 Terkini Wrapper-->
      <div class="terkini-wrapper">
        <div class="container">
          @php $br_loop2 = 1; @endphp
          @foreach($breaking as $post) @if($br_loop2 > 3 && $br_loop2 <= 9)
            <!-- Terkini Post-->
            <div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$post['slug']}-{$post['id']}") }}" title="{{ html_entity_decode($post['title']) }}">
                  <img src="{{ $post['images']['url_thumb'] }}" loading="lazy" alt="{{ html_entity_decode($post['title']) }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                {{-- @if($post['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
                <a class="post-title" href="{{ url("/{$post['slug']}-{$post['id']}") }}" title="{{ html_entity_decode($post['title']) }}">{{ html_entity_decode($post['title']) }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="https://m.solopos.com/">{{ $post['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($post['date']) }}</a></div>
              </div>
            </div>
          @endif
          @php $br_loop2++; @endphp
          @endforeach                            
        </div>
      </div>

      <!-- Opini Wrapper-->
      <div class="opini-wrapper mb-3">
        <div class="bg-shapes">
          <div class="shape1"></div>
          <div class="shape2"></div>
          <div class="shape3"></div>
          <div class="shape4"></div>
          <div class="shape5"></div>
        </div>
        <h6 class="mb-3 catagory-title">Kolom</h6>
        <div class="container">
          <!-- Catagory Slides-->
          <div class="opini-slides owl-carousel">
          @php $kl_loop = 1; @endphp
          @foreach($kolom as $kl) @if($kl_loop <=5)            
            <div class="card catagory-card">
              <a class="post-title d-block" href="{{ url("/{$kl['slug']}-{$kl['id']}") }}" title="{{ html_entity_decode($kl['title']) }}">
                <img src="{{ $kl['images']['thumbnail'] }}" loading="lazy" alt="{{ html_entity_decode($kl['title']) }}" style="object-fit: cover; height: 150px; width: 134px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                <h6>@if(!empty($kl['author'])) {{ $kl['author'] }} @endif</h6>
              </a>
            </div>
            @endif
            @php $kl_loop++; @endphp
            @endforeach
          </div>
        </div>
      </div>

      <!-- Mobile Home 4 Banner -->
      @include('includes.ads.home-4-banner')
	  
      <!-- Blok 3 Terkini Wrapper-->
      <div class="terkini-wrapper">
        <div class="container">
          @php $no = 1; @endphp
          @foreach($breaking as $br3)
            @if($no > 9 && $no <= 19)
            <!-- Terkini Post-->
            <div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$br3['slug']}-{$br3['id']}") }}" title="{{ html_entity_decode($br3['title']) }}" title="{{ html_entity_decode($br3['title']) }}">
                  <img src="{{ $br3['images']['url_thumb'] }}" loading="lazy" alt="{{ html_entity_decode($br3['title']) }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                {{-- @if($br3['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
                <a class="post-title" href="{{ url("/{$br3['slug']}-{$br3['id']}") }}" title="{{ html_entity_decode($br3['title']) }}">{{ html_entity_decode($br3['title']) }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="https://m.solopos.com">{{ $br3['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($br3['date']) }}</a></div>
              </div>
            </div>
            @endif
          @php $no++; @endphp
          @endforeach                            
        </div>
      </div>

      <!-- Spos V Card Wrapper-->
      <div class="info-grafis-wrapper mt-3 mb-3">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between">
            <h5 class="mb-0 pl-1 spos-title">#Espospedia</h5>
          </div>
        </div>
        <div class="container">
          <div class="row">
          @php $ep_loop = 1; @endphp
          @foreach($espospedia as $ep)
            @if($ep_loop <= 4)
            <div class="col-6 col-md-4">
              <div class="spos-v-card mt-3"><a class="bookmark-post" href="#"><i class="lni lni-bookmark"></i></a>
                <div class="post-thumbnail">
                  <img src="{{ $ep['images']['thumbnail'] }}" loading="lazy" alt="{{ json_encode($ep['images']['caption']) }}" style="object-fit: cover; height: 210px; width: 188px;">
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/{$ep['slug']}-{$ep['id']}") }}" title="{{ html_entity_decode($ep['title']) }}">#infoGrafis #esposPedia</a>
                </div>
              </div>
            </div>
            @endif
            @php $ep_loop++; @endphp
            @endforeach
          </div>
        </div>
      </div>

      <!-- Mobile Home 5 Banner -->
      @include('includes.ads.home-5-banner')

      <!-- Blok 4 Terkini Wrapper-->
      <div class="terkini-wrapper">
        <div class="container">
          @php $no=1; @endphp
          @foreach($breaking as $br4)
            @if($no > 19 && $no <= 30 )
                    
            @if ($no == 6)
              <!-- ads breaking -->
            <div class="iklan mt-3">
              <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
              <!-- Iklan Responsif -->
              <ins class="adsbygoogle"
                  style="display:block"
                  data-ad-client="ca-pub-4969077794908710"
                  data-ad-slot="2921244965"
                  data-ad-format="auto"
                  data-full-width-responsive="true"></ins>
              <script>
                  (adsbygoogle = window.adsbygoogle || []).push({});
              </script>  
            </div>
          @else
            <!-- Terkini Post-->
            <div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$br4['slug']}-{$br4['id']}") }}" title="{{ html_entity_decode(html_entity_decode($br4['title'])) }}">
                  <img src="{{ $br4['images']['url_thumb'] }}" loading="lazy" alt="{{ html_entity_decode($br4['title']) }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                {{-- @if($br4['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
                <a class="post-title" href="{{ url("/{$br4['slug']}-{$br4['id']}") }}" title="{{ html_entity_decode($br4['title']) }}">{{ html_entity_decode($br4['title']) }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="https://m.solopos.com">{{ $br4['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($br4['date']) }}</a></div>
              </div>
            </div>
            @endif
            @endif
            @php $no++; @endphp
          @endforeach                              
        </div>
      </div>

      <!-- widget Berita Video -->
      @include('includes.widget-video')

      <!-- Mobile Home 6 Banner -->
      @include('includes.ads.home-6-banner')
      
      <!-- Blok 5 Terkini Wrapper-->
      <div class="terkini-wrapper">
        <div class="container">
          @php $no=1; @endphp
          @foreach($breaking as $br5)
            @if($no > 30 )
                    
            <!-- Terkini Post-->
            <div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$br5['slug']}-{$br5['id']}") }}" title="{{ html_entity_decode($br5['title']) }}">
                  <img src="{{ $br5['images']['url_thumb'] }}" loading="lazy" alt="{{ html_entity_decode($br5['title']) }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                {{-- @if($br5['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
                <a class="post-title" href="{{ url("/{$br5['slug']}-{$br5['id']}") }}" title="{{ html_entity_decode($br5['title']) }}">{{ html_entity_decode($br5['title']) }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="https://m.solopos.com">{{ $br5['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($br5['date']) }}</a></div>
              </div>
            </div>
          @endif
          @php $no++; @endphp
        @endforeach                              
        </div>
      </div>

      <!-- Tabs News Wrapper-->
      <div class="tabs-news-wrapper bg-gray">
        <div class="container">
          <nav>
            <!-- Nav Tabs-->
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-jateng-tab" href="#nav-jateng" data-toggle="tab" role="tab" aria-controls="nav-jateng" aria-selected="true">Jateng</a>
              <a class="nav-item nav-link" id="nav-jatim-tab" href="#nav-jatim" data-toggle="tab" role="tab" aria-controls="nav-jatim" aria-selected="false">Jatim</a>
              <a class="nav-item nav-link" id="nav-jogja-tab" href="#nav-jogja" data-toggle="tab" role="tab" aria-controls="nav-jogja" aria-selected="false">Jogja</a>
            </div>
          </nav>
          <!-- Tabs Content-->
          <div class="tab-content" id="nav-tabContent">

            <!-- Single Tab Pane-->
            <div class="tab-pane fade show active" id="nav-jateng" role="tabpanel" aria-labelledby="nav-jateng-tab">
            @php $no=1; @endphp
            @foreach($jateng as $jte)
              @if($no <= 5 )            
              <!-- Single News Post-->
              <div class="single-news-post d-flex align-items-center">
                <div class="post-thumbnail">
                  <img src="{{ $jte['images']['url_thumb'] }}" loading="lazy" alt="{{ html_entity_decode($jte['title']) }}" style="object-fit: cover; height: 70px; width: 70px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </div>
                <div class="post-content">
                  {{-- @if($jte['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								  @endif --}}
                  <a class="post-title" href="{{ url("/{$jte['slug']}-{$jte['id']}") }}" title="{{ html_entity_decode($jte['title']) }}">{{ html_entity_decode($jte['title']) }}</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="#">{{ Helper::time_ago($jte['date']) }}</a>
                  </div>
                </div>
              </div>
              @endif
              @php $no++; @endphp
            @endforeach
            </div>

            <!-- Single Tab Pane-->
            <div class="tab-pane fade" id="nav-jatim" role="tabpanel" aria-labelledby="nav-jatim-tab">
            @php $no=1; @endphp
            @foreach($jatim as $jti)
              @if($no <= 5 )             
              <!-- Single News Post-->
              <div class="single-news-post d-flex align-items-center">
                <div class="post-thumbnail">
                  <img src="{{ $jti['images']['url_thumb'] }}" loading="lazy" alt="{{ html_entity_decode($jti['title']) }}" style="object-fit: cover; height: 70px; width: 70px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </div>
                <div class="post-content">
                  {{-- @if($jti['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								  @endif --}}
                  <a class="post-title" href="{{ url("/{$jti['slug']}-{$jti['id']}") }}" title="{{ html_entity_decode($jti['title']) }}">{{ html_entity_decode($jti['title']) }}</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="#">{{ Helper::time_ago($jti['date']) }}</a>
                  </div>
                </div>
              </div>
              @endif
              @php $no++; @endphp
            @endforeach
            </div>

            <!-- Single Tab Pane-->
            <div class="tab-pane fade" id="nav-jogja" role="tabpanel" aria-labelledby="nav-jogja-tab">
            @php $no=1; @endphp
            @foreach($jogja as $jog)
              @if( $no <= 5 )           
              <!-- Single News Post-->
              <div class="single-news-post d-flex align-items-center">
                <div class="post-thumbnail">
                  <img src="{{ $jog['images']['url_thumb'] }}" loading="lazy" alt="{{ html_entity_decode($jog['title']) }}" style="object-fit: cover; height: 70px; width: 70px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </div>
                <div class="post-content">
                  {{-- @if($jog['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								  @endif --}}
                  <a class="post-title" href="{{ url("/{$jog['slug']}-{$jog['id']}") }}" title="{{ html_entity_decode($jog['title']) }}">{{ html_entity_decode($jog['title']) }}</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="#">{{ Helper::time_ago($jog['date']) }}</a>
                  </div>
                </div>
              </div>
              @endif
              @php $no++; @endphp
            @endforeach
            </div>
          </div>
        </div>
      </div>

    </div>
@endsection