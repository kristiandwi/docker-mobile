@extends('layouts.app')
@section('content')  

    <div class="page-content-wrapper">
      {{-- @include('includes.ads.top-swipe') --}}
      <!-- Scroll Indicator-->
      <div id="scrollIndicator"></div>

      <!-- Single Blog Thumbnail-->
      <div class="single-blog-thumbnail">
        <img loading="lazy" class="w-100 single-blog-image" src="{{ asset('images/background-espos-plus.jpg') }}" alt="Solopos Digital Media">
      </div>

      <!-- Single Blog Info-->
      <div class="single-blog-info">
          <div class="d-flex align-items-center">
            <!-- Post Content Wrap-->
            <div class="post-content-wrap">
              <h1>Pusat Bantuan</h1>
            </div>
          </div>
      </div>     

      <!-- Element FAQ-->
      <div class="element-wrapper pt-0">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between mb-3">
                <h5 class="mb-0 pl-1 spos-title">Frequently Asked Questions</h5><a class="btn btn-danger btn-sm" href="#">.</a>
            </div>
        </div>
        <div class="container">
          <!-- Default Accordian 1-->
          <div class="accordion" id="faq-premium">
            <!-- Single Accordian-->
            <div class="card border-0">
              <div class="card-header bg-success p-0" id="heading1">
                <h4 class="mb-0">
                  <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">Apa itu Espos Plus?</button>
                </h4>
              </div>
              <div class="collapse show" id="collapse1" aria-labelledby="heading1" data-parent="#faq-premium">
                <div class="card-body">
                  <p class="mb-0">
                    Espos Plus adalah artikel yang menyajikan informasi dari fakta dan peristiwa yang diolah serta dipaparkan dengan cara kreatif dan lebih visual. Artikel ditulis dengan bahasa yang lebih sederhana, lugas, tetapi lebih mendalam. Espos Plus dapat diakses dengan sistem berlangganan pada situs dalam jaringan (online).<br><br>

                    Konten Espos Plus di Solopos.com berbeda dengan free content Solopos.com. Espos Plus menyajikan informasi dengan insight dan perspektif yang berbeda dan lebih mendalam. Pembaca akan lebih nyaman membaca Espos Plus tanpa harus terganggu dengan iklan yang muncul.
                  </p>
                </div>
              </div>
            </div>
            <!-- Single Accordian 2-->
            <div class="card border-0">
              <div class="card-header bg-success p-0" id="heading2">
                <h4 class="mb-0">
                  <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">Bagaimana cara mengakses Espos Plus?</button>
                </h4>
              </div>
              <div class="collapse" id="collapse2" aria-labelledby="heading2" data-parent="#faq-premium">
                <div class="card-body">
                    <p class="mb-0">
                        Espos Plus adalah konten yang bisa diakses melalui smartphone, tablet, laptop, maupun desktop melalui <a href="https://www.solopos.com/plus">https://www.solopos.com/plus</a>.
                    </p>
                </div>
              </div>
            </div>
            <!-- Single Accordian 3-->
            <div class="card border-0">
              <div class="card-header bg-success p-0" id="heading3">
                <h4 class="mb-0">
                  <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">Bagaimanacara berlangganan Espos Plus?</button>
                </h4>
              </div>
              <div class="collapse" id="collapse3" aria-labelledby="heading3" data-parent="#faq-premium">
                <div class="card-body">
                    <p class="mb-0">
                        Untuk pembaca setia Solopos.com, Anda bisa mendaftar terlebih dahulu di <a href="https://id.solopos.com/register">https://id.solopos.com/register</a>. Setelah Anda mendaftar atau sudah memiliki akun, Anda bisa Login terlebih dahulu di <a href="https://id.solopos.com/login">https://id.solopos.com/login</a>. Kemudian Anda dapat memilih paket langganan yang tersedia.<br><br>
                        
                        Mulai 1 Januari 2022, pembaca bisa menikmati Espos Plus dengan memilih paket langganan yang beragam yaitu langganan durasi 7 hari, langganan durasi 1 bulan, langganan durasi 6 bulan, dan langganan durasi 1 tahun.<br><br>
                        
                        Selama 2022, Espos Plus menawarkan program khusus Espos Vaganza bagi pelanggan Espos Plus dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya.
                    </p>
                </div>
              </div>
            </div>
            <!-- Single Accordian 4-->
            <div class="card border-0">
              <div class="card-header bg-success p-0" id="heading4">
                <h4 class="mb-0">
                  <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">Apa keunggulan berlangganan Espos Plus?</button>
                </h4>
              </div>
              <div class="collapse" id="collapse4" aria-labelledby="heading4" data-parent="#faq-premium">
                <div class="card-body">
                    <p class="mb-0">
                    Dengan berlangganan Espos Plus, Anda mendapatkan beritayang lebih mendalam dan bebas dari iklan.Sebuah karya jurnalistik adalah sumber informasi sahih. Untuk mendapatkan informasi tersebut, butuh biaya tinggi.<br><br>

                    Dengan berlangganan Espos Plus, berarti Anda mendukung keberlangsungan informasi yang akan menjadi sumber kepercayaan diri ketika membuat keputusan. Anda juga bisa mengikuti program Espos Vaganza dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya.
                    </p>
                </div>
              </div>
            </div>  
            <!-- Single Accordian 5-->
            <div class="card border-0">
                <div class="card-header bg-success p-0" id="heading5">
                  <h4 class="mb-0">
                    <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">Apa itu Espos Vaganza?</button>
                  </h4>
                </div>
                <div class="collapse" id="collapse5" aria-labelledby="heading5" data-parent="#faq-premium">
                  <div class="card-body">
                      <p class="mb-0">
                        Espos Vaganza adalah program khusus bagi pelanggan Espos Plus selama 2022. Program khusus ini menawarkan berbagai promo khusus untuk pelanggan Espos Plus seperti Espos Plus E-Money dengan hadiah e-money yang diundi tiap bulan, Espos Plus Emas dengan  hadiah emas batangan yang diundi tiap bulan, Super Espos Plus yang memberikan hadiah emas batangan untuk pemenang tiap bulan.<br><br>

                        Kemudian Espos Vaganza dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya khusus bagi pelanggan Espos Plus yang memilih paket langganan 1 tahun.<br><br>
                        
                        Lebih lengkap mengenai syarat dan ketentuan tentang promo Espos Vaganza bisa baca bagian syarat & ketentuan. 
                        
                      </p>
                  </div>
                </div>
            </div> 
            <!-- Single Accordian 6-->
            <div class="card border-0">
                <div class="card-header bg-success p-0" id="heading6">
                  <h4 class="mb-0">
                    <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">Siapa yang bisa ikut Espos Vaganza?</button>
                  </h4>
                </div>
                <div class="collapse" id="collapse6" aria-labelledby="heading6" data-parent="#faq-premium">
                  <div class="card-body">
                      <p class="mb-0">
                        Pelanggan yang mendaftar dan berlangganan Espos Plus di periode 1 Januari 2022-31 Desember 2022 bisa mengikuti program Espos Vaganza sesuai syarat dan ketentuan yang berlaku.<br><br>

                        Promo Espos Vaganza dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya hanya berlaku untuk pelanggan yang memilih paket langganan 1 tahun.<br><br>
                        
                        Lebih lengkap mengenai syarat dan ketentuan tentang promo Espos Vaganza bisa baca bagian syarat & ketentuan.
                        
                      </p>
                  </div>
                </div>
            </div>
            <!-- Single Accordian 7-->
            <div class="card border-0">
                <div class="card-header bg-success p-0" id="heading7">
                  <h4 class="mb-0">
                    <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">Metode Pembayaran ?</button>
                  </h4>
                </div>
                <div class="collapse" id="collapse7" aria-labelledby="heading7" data-parent="#faq-premium">
                  <div class="card-body">
                      <p class="mb-0">
                      - QRIS<br>
                      - Gopay<br>
                      - ShopeePay*<br>
                      - Transfer Bank (BRI, BNI, BCA*, Mandiri, Permata)<br>
                      - Indomaret*<br>
                      - Alfamart*<br>
                      - BCA Klikpay*<br><br>
                      <strong>* Dalam Proses</strong>
                      </p>
                  </div>
                </div>
            </div>  
            <!-- Single Accordian 7-->
            {{-- <div class="card border-0">
                <div class="card-header bg-primary p-0" id="heading7">
                  <h4 class="mb-0">
                    <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">Apa keunggulan berlangganan Espos Plus?</button>
                  </h4>
                </div>
                <div class="collapse" id="collapse7" aria-labelledby="heading7" data-parent="#faq-premium">
                  <div class="card-body">
                      <p class="mb-0">
                      Dengan berlangganan Espos Plus, Anda mendapatkan beritayang lebih mendalam dan bebas dari iklan.Sebuah karya jurnalistik adalah sumber informasi sahih. Untuk mendapatkan informasi tersebut, butuh biaya tinggi.<br><br>
  
                      Dengan berlangganan Espos Plus, berarti Anda mendukung keberlangsungan informasi yang akan menjadi sumber kepercayaan diri ketika membuat keputusan. Anda juga bisa mengikuti program Espos Vaganza dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya.
                      </p>
                  </div>
                </div>
            </div> 
            <!-- Single Accordian 8-->
            <div class="card border-0">
                <div class="card-header bg-primary p-0" id="heading8">
                  <h4 class="mb-0">
                    <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">Apa keunggulan berlangganan Espos Plus?</button>
                  </h4>
                </div>
                <div class="collapse" id="collapse8" aria-labelledby="heading8" data-parent="#faq-premium">
                  <div class="card-body">
                      <p class="mb-0">
                      Dengan berlangganan Espos Plus, Anda mendapatkan beritayang lebih mendalam dan bebas dari iklan.Sebuah karya jurnalistik adalah sumber informasi sahih. Untuk mendapatkan informasi tersebut, butuh biaya tinggi.<br><br>
  
                      Dengan berlangganan Espos Plus, berarti Anda mendukung keberlangsungan informasi yang akan menjadi sumber kepercayaan diri ketika membuat keputusan. Anda juga bisa mengikuti program Espos Vaganza dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya.
                      </p>
                  </div>
                </div>
            </div>                                                                   --}}
          </div>
        </div>
      </div>      
      <!-- Blog Description-->
      <div class="blog-description">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between mb-3">
                <h5 class="mb-0 pl-1 spos-title" style="font-size:16px;">Syarat & Ketentuan Espos Vaganza</h5>
            </div>
            <ol>
              <li>Promo Espos Vaganza bagi pelanggan Espos Plus dilaksanakan selama satu tahun mulai 1 Januari 2022-31 Desember 2022.</li>
              <li>Undian Tahunan Espos Vaganza dengan hadiah utama 1 unit mobil Daihatsu Rocky, sepeda motor Yamaha NMax, emas batangan, sepeda, dan beragam alat elektronik hanya berlaku bagi <strong>pelanggan Espos Plus yang memilih paket 1 tahun</strong> yang mendaftar pada periode 1 Januari 2022-31 Desember 2022.</li>
              <li>Undian Tahunan Espos Vaganza dan pengumuman pemenang akan dilakukan dalam Puncak Espos Vaganza yang disiarkan secara langsung di Youtube SoloposTV, Facebook Solopos.com, dan Instagram <a href="https://instagram.com/koransolopos" target="_blank"><i>@koransolopos</i></a> setelah periode promosi berakhir.</li>
              <li>Promo Espos Vaganza juga menghadirkan Undian Bulanan dengan hadiah e-money dan emas batangan.</li>
              <li>Undian Bulanan Espos Vaganza hanya berlaku untuk <strong>pelanggan Espos Plus yang memilih paket 1 bulan, 6 bulan, dan 1 tahun</strong> pada periode pendaftaran di bulan tersebut.</li>
              <li>Hadiah Undian Bulanan Espos Vaganza yaitu satu e-money Rp30.000 untuk pelanggan 1 bulan, satu e-money Rp150.000 untuk pelanggan 6 bulan, dan satu e-money Rp240.000, satu emas 0,2 gram, satu emas 0,5 gram untuk pelanggan 1 tahun.</li>
              <li>Pelanggan 1 tahun yang sudah menang dalam Undian Bulanan Espos Vaganza tetap diikutkan dalam Undian Tahunan Espos Vaganza.</li>
              <li>Undian Bulanan Espos Vaganza dan pengumuman pemenang disiarkan secara langsung melalui  Instagram <a href="https://instagram.com/koransolopos" target="_blank"><i>@koransolopos</i></a> pada awal bulan berikutnya.</li>
              <li>Promo Espos Vaganza terbuka untuk masyarakat umum yang menjadi pelanggan Espos Plus dan tidak berlaku bagi karyawan dan keluarga karyawan Solopos Media Group.</li>
              <li>Pajak hadiah ditanggung pemenang.</li>
              <li>Pengundian dan pengumuman pemenang dilakukan secara transparan dan hasilnya tidak dapat diganggu gugat.</li>
            </ol>		
        </div>	
      </div>
            
      <style>
      .about-us p, .blog-description ol li {font-size:15px;}
      .blog-description ol li {margin-left:15px;margin-bottom:15px;}
      .btn {padding: 3px 10px; font-size: 14px;text-align:left;font-weight: 600;}
      .accordion>.card>.card-header {margin-bottom: 1px;}
      .btn-heading {padding: 10px 15px !important;font-size: 15px; font-weight: normal;}
      </style>      
      <!-- Post Author-->
      <div class="profile-content-wrapper">
        <!-- Settings Option-->
        <div class="container">
          <!-- User Meta Data-->
          <div class="user-meta-data d-flex">
            <!-- User Thumbnail-->
            <div class="user-thumbnail">
                  <img loading="lazy" src="{{ asset('images/icon.png') }}" alt="editor">
            </div>
            <!-- User Content-->
            <div class="user-content">
              <h6><a href="{{ url('/') }}" title="">Redaksi Solopos</a></h6>
            </div>
          </div>
        </div>
      </div>
@endsection