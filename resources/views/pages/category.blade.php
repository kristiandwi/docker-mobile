@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
    <div class="page-content-wrapper">
      
      <!-- Headline Wrapper-->
      <div class="headline-wrapper">
        <div class="container">
          <!-- Hero Slides-->
          <div class="slides-wrapper">
            <div class="hero-slides owl-carousel">           
              @php $hl_loop = 1; @endphp
              @foreach($headline as $hl)
              @if($hl['category'] == $category)
              @if($hl_loop <=5)    
              <div class="single-hero-slide">
                <div class="headline-image d-flex align-items-start">
                  <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">
                  <img loading="lazy" src="{{ $hl['images']['thumbnail'] }}" alt="{{ html_entity_decode($hl['title']) }}">
                  </a>
                </div>
                <div class="d-flex align-items-end">
                  <div class="container-fluid mb-3">
                    <div class="post-meta d-flex align-items-center">
                      <a href="">{{ $category }}</a> | <span style="margin-left: 5px;">{{ Helper::time_ago($hl['date']) }}</span>
                    </div>                  
                    <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">
                      {{-- @if($hl['konten_premium'] == 'premium') 
                        <span class="espos-plus">+ PLUS</span>
                      @endif --}}
                      {{ html_entity_decode($hl['title']) }}</a>
                  </div>
                </div>
              </div>
              @endif
              @php $hl_loop++ @endphp
              @endif
              @endforeach
            </div>
          </div>
        </div>
      </div>
      <!-- Ads Headline-Slider -->
      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Terkini</h5><a class="btn btn-primary btn-sm" href="/arsip">Indeks Berita</a>
          </div>
        </div>
        <div class="container">
          @php $br_loop = 1; @endphp
          @foreach ($breakingcat as $item)
            @php           
            $title = html_entity_decode($item['title']);
            @endphp
          @if($br_loop <=3) 
            <!-- Terkini Post-->
            <div class="terkini-post">
              <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                  <img loading="lazy" src="{{ $item['images']['url_thumb'] }}" alt="{{ $title }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                {{-- @if($item['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $category }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            </div>
            @endif
            @php $br_loop++ @endphp            
            @endforeach                             
        </div>
      </div>
      <div class="iklan">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script> 
      </div>      
      <!-- Terpopuler Wrapper-->
      <div class="mt-5">
        @include('includes.widget-popular')
  	  </div>

      <!-- ads parallax -->

      <!-- Editors Choice Wrapper -->
      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Fokus</h5><a class="btn btn-primary btn-sm" href="/arsip">Indeks Berita</a>
          </div>
        </div>
        <div class="container">  
          @php $ec_loop = 1; @endphp
          @foreach($editorchoice as $ec) 
          @php           
            $title = html_entity_decode($ec['title']);
            @endphp
          @if($ec['category'] == $category) 
          @if($ec_loop <= 3)                   
          <!-- Terkini Post--> 
          @if ($ec_loop==1)
          <div class="card mb-3">
            <a href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ $title }}">
            <img loading="lazy" class="card-img-top" src="{{ $ec['images']['thumbnail'] }}" alt="{{ $title }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
            </a>
            <div class="card-body">
              <a class="post-title" href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ $title }}">
                {{-- @if($ec['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
                {{ $title }}</a>
            </div>
          </div>          
          @else     
          <div class="terkini-post d-flex">
            <div class="post-thumbnail">
              <a href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ $title }}">
                <img loading="lazy" src="{{ $ec['images']['url_thumb'] }}" alt="{{ $title }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
              </a>
            </div>
            <div class="post-content">
              {{-- @if($ec['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
              <a class="post-title" href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ $title }}">{{ $title }}</a>
              <div class="post-meta d-flex align-items-center">
                <a href="https://m.solopos.com/">{{ $category }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($ec['date']) }}</a>
              </div>
            </div>
          </div>
          @endif
          @endif
          @php $ec_loop++; @endphp
          @endif
          @endforeach                             
        </div>

      </div>

      <!-- ads breaking -->
      <div class="iklan mb-3">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script> 
      </div>

      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          @php $br2_loop = 1; @endphp
          @foreach ($breakingcat as $item)
            @php           
              $title = html_entity_decode($item['title']);
            @endphp
          @if($br2_loop > 3) 
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                  <img loading="lazy" src="{{ $item['images']['url_thumb'] }}" alt="{{ $title }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                {{-- @if($item['konten_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif --}}
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $category }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            </div>
            @endif
            @php $br2_loop++ @endphp             
            @endforeach
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Berita Lainnya
              </a>
              <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>                               
        </div>
      </div>

      <!-- Ads Breaking -->
      <div class="iklan">
      <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-ad-format="auto"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
      </div>

      <!-- widget Berit Video -->
      @include('includes.widget-video')      

      <div class="container">
        <div class="border-top"></div>
      </div>

    </div>
@endsection