@extends('layouts.app')
@section('content')  
@include('includes.ads.popup-banner')  
<div class="page-content-wrapper">
  @include('includes.ads.top-swipe')
      <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Foto</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/foto">Indeks Berita</a>
          </div>
        </div>
        <div class="container loadmore-frame">  
        @foreach($breakingcat as $foto)             
          <!-- Terkini Post--> 
          <div class="card mb-3 content-box">
            <a href="{{ url("/{$foto['slug']}-{$foto['id']}") }}" title="{{ html_entity_decode($foto['title']) }}">
            <img class="card-img-top" src="{{ $foto['images']['thumbnail'] }}" alt="{{ html_entity_decode($foto['title']) }}">
            </a>
            <div class="card-body">
                <a class="post-title" href="{{ url("/{$foto['slug']}-{$foto['id']}") }}" title="{{ html_entity_decode($foto['title']) }}">
                    {{ html_entity_decode($foto['title']) }}
                </a>
            </div>
          </div>
        @endforeach 
            
          <div class="text-center mb-3">
            <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
              Berita Foto Lainnya
            </a>
            <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
              Arsip Berita
            </a>
          </div>            
                                      
        </div>
      </div>
    </div>
@endsection