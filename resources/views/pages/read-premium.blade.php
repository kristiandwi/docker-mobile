@extends('layouts.app')
@section('content')
      <!-- Caption Modal-->
      <div class="modal fade" id="fotocaption" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button class="close close" type="button" data-dismiss="modal" aria-label="Close"><i class="lni lni-close"></i></button>
              <!-- Heading-->
              <h6 class="mb-3 pl-2">Photo Caption</h6>
              @if(!empty($data['images']['caption']))
              <p>SOLOPOS.COM - {{ htmlentities($data['images']['caption']) }}</p>
              @else
              <p>SOLOPOS.COM - Panduan Informasi dan Inspirasi</p>
              @endif
            </div>
          </div>
        </div>
      </div>

      <div class="page-content-wrapper">
        <!-- Scroll Indicator-->
        <div id="scrollIndicator"></div>

        <!-- Single Blog Thumbnail-->
        @if(!empty($video))
          <div class="video-wrap">
            <div class="video">
              <iframe id="ytplayer" type="text/html" width="100%" height="200" src="https://www.youtube.com/embed/{{ $video }}?autoplay=1&origin=https://m.solopos.com" frameborder="0"></iframe>
            </div>
          </div>
        @else
        <div class="single-blog-thumbnail">
          <img loading="lazy" class="w-100 single-blog-image" src="{{ $data['images']['content']}}" alt="@if(!empty($data['images']['caption'])) {{ htmlentities($data['images']['caption']) }} @endif">
          <a class="post-bookmark" href="#" data-toggle="modal" data-target="#fotocaption"><i class="lni lni-text-align-justify"></i></a>
        </div>
        @endif

        <!-- Single Blog Info-->
        <div class="single-blog-info">
            <div class="d-flex align-items-center">
              <!-- Post Content Wrap-->
              <div class="post-content-wrap">
                <img loading="lazy" style="width: auto; height:17px;" src="{{ asset('images/plus.png') }}" alt="Espos Plus">
                {{-- <a class="kategori" href="{{ url('/') }}" style="margin-bottom: 20px;">Solopos.com</a> --}}
                <a class="kategori-2 mb-5" href="{{ url('/') }}/{{ $content['category_parent'] }}">{{ $header['category_parent'] }}</a>
                <h1 class="mt-2 mb-2">{{ $header['title'] }}</h1>
                <div class="mb-3" style="font-size: 13px;">
                  {{ $header['ringkasan'] }}
                </div>
                <div class="post-meta">
                  <div class="post-date">{{ Carbon\Carbon::parse($data['created'])->translatedFormat('l, j F Y - H:i') }} WIB</div>
                  <div class="post-date">
                    <div class="multi-reporter">
                      @if($header['author'] != $header['editor'] )
                          Penulis:
                          @foreach ($author_slug as $author)
                          <a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $header['editor_url'] }} @else {{ url('/') }}/penulis/{{$author}} @endif">{{ ucwords(str_replace('_', ' ', $author)) }}</a>
                          @endforeach
                      @endif
                      </div>
                      Editor : <a href="@if(!empty($header['editor_url'])){{ url('/')}}/author/{{ $header['editor_url'] }} @else https://m.solopos.com/arsip @endif" style="text-transform: none;"> {{ $header['editor'] }}</a>
                  </div>
                  {{-- <div class="post-date"><!-- reading time --></div> --}}
                </div>
              </div>
            </div>
        </div>
        <!-- Blog Description-->
        <div class="blog-description">
          <div class="container">
            @php
            $konten = Helper::konten(htmlspecialchars_decode($data['content']['content'])) ;
            $contents = explode('</p>', $konten);
            $total_p = count(array_filter($contents));
            $intro  = array_slice($contents, 0, 2);
            @endphp

            @if(Cookie::get('is_login') == 'true')
            <div id="konten-premium" class="premium">
                <!-- User Meta Data-->
                <div class="alert alert-primary alert-dismissable mb-30" style="padding: 5px 10px;">
                  <h6 style="margin:0 0 10px 0;font-size:13px;font-weight:400; display:block;">
                    <a href="https://id.solopos.com/profile">{{ Helper::greetings() }}, <strong>{{ Cookie::get('is_name') }} !</strong></a>
                  </h6>
                  <div class="user-meta-data d-flex">
                    <div class="user-content" style="padding-left:0;">
                      <p style="font-size:12px; line-height:15px;padding-top:10px;">Selamat datang di ekosistem SoloposID. Silakan kelola akun Anda melalui Dashboard SoloposID.</p>
                    </div>
                    <div class="align-items-center" style="padding-left:5px;">
                      <img loading="lazy" src="https://id.solopos.com/images/logo.png" style="width:120px;" alt="Solopos.id">
                      <div style="text-align:center;">
                        <button class="btn btn-success btn-sm" style="padding:.25rem 1rem;"><a href="https://id.solopos.com" style="color:#fff !important;">Akun Saya</a></button>
                      </div>
                    </div>
                  </div>
                </div>
                <style>
                  .blog-description p em strong {
                    display:inline-block;padding:10px;font-size:15px;line-height:23px;background:#f1f1f1;font-weight:700;border-radius:5px;color:#44474a;
                  }
                  .blog-description p em strong a {
                    color:#0b6097 !important;font-weight:500!important;padding-left:5px;font-style:normal !important;
                  }
                </style>
									@if(Cookie::get('is_membership') == 'free')
									<div id="promosi">
										{!! implode('</p>', $intro) !!}
										<div style="background-image: linear-gradient(
										180deg,hsla(0,0%,100%,0),#fff);height:5rem;width:100%;position:relative;margin-top:-90px;"></div>
										<div class="subscribe mx-auto">
											<div class="row justify-content-center">
												<div class="col-md-12">
													<div class="header-box">
														<img loading="lazy" class="logo" src="{{ url('images/plus.png') }}" alt="logo">
														<div class="login">
															<a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="Langganan Espos Plus">Aktifkan Paket <i class="fa fa-arrow-circle-o-right"></i></a>
														</div>
													</div>

													<div class="top-box">
														<div class="lead">Mau baca selengkapnya?</div>
                            <div class="sublead">
                              Hi {{ Cookie::get('is_name') }}, silakan <a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="Langganan Espos Plus">Aktifkan Paket <i class="fa fa-arrow-circle-o-right"></i></a> untuk mendapatkan akses tak terbatas ke artikel, foto, info grafis, dan lainnya.
                            </div>
													</div>
													<div class="button-container">
														<a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="langganan" class="checkout-button" style="padding:8px 10px !important; font-size:12px;color:#fff !important;">Langganan Sekarang, Dapat Mobil !</a>
														<a href="https://m.solopos.com/page/help" target="_blank">APA ITU ESPOS PLUS?</a>
													</div>
													<div class="footer-box">
														<span class="callout">*Syarat & Kententuan berlaku</span>
													</div>

												</div>

											</div>
										</div>
									</div>
                  <div>
                    <a href="https://id.solopos.com/login/"><img loading="lazy" src="https://cdn.solopos.com/plus/espos-plus-mobile.jpg" width="100%"></a>
                  </div>
                  <div class="mb-4"></div>
									@else
									{!! $konten !!}
									@endif
            </div>
            @else
            <div id="promosi">
              {!! implode('</p>', $intro) !!}
              <div class="intro-content"></div>
              <div class="subscribe mx-auto">
                <div class="row justify-content-center">
                  <div class="col-md-12">
                    <div class="header-box">
                      <img loading="lazy" class="logo" src="{{ url('images/plus.png') }}" alt="logo">
                      <div class="login">
                        Sudah Langganan ?  <a href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}" title="login">Login <i class="fa fa-arrow-circle-o-right"></i></a>
                      </div>
                    </div>

                    <div class="top-box">
                      <div class="lead">Mau baca selengkapnya?</div>
                      <div class="sublead">Silakan berlangganan untuk mendapatkan akses tak terbatas ke artikel, foto, info grafis, dan lainnya.</div>
                    </div>
                    <div class="button-container">
                      <a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="langganan" class="checkout-button" style="padding:8px 10px !important; font-size:12px;color:#fff !important;">Langganan Sekarang, Dapat Mobil !</a>
                      <a href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}" title="Login" class="checkout-button inverse">LOGIN | DAFTAR</a>
                      <a href="https://m.solopos.com/page/help" target="_blank">APA ITU ESPOS PLUS?</a>
                    </div>
                    <div class="footer-box">
                      <span class="callout">*Syarat & Kententuan berlaku</span>
                      </div>

                  </div>

                </div>
              </div>
            </div>
            <div>
              <a href="https://id.solopos.com/login/"><img loading="lazy" src="https://cdn.solopos.com/plus/espos-plus-mobile.jpg" width="100%"></a>
            </div>
            <div class="mb-4"></div>
            @endif
          </div>

          <div class="container social-share-btn d-flex align-items-center flex-wrap">
            SHARE :
            <a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ url("/{$data['content']['slug']}-{$data['properties']['post_id']}") }}" target="_blank" rel="noopener"><i class="lni lni-facebook"></i></a>
            <a class="btn-twitter" href="https://twitter.com/home?status={{ url("/{$data['content']['slug']}-{$data['properties']['post_id']}") }}" target="_blank" rel="noopener"><i class="lni lni-twitter-original"></i></a>
            <a class="btn-instagram" href="#"><i class="lni lni-instagram"></i></a>
            <a class="btn-whatsapp" href="whatsapp://send?text=*{{ urlencode($header['title']) }}* | {{ urlencode($header['ringkasan']) }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" target="_blank" rel="noopener"><i class="lni lni-whatsapp"></i></a>
            <a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini {{ url("/{$data['content']['slug']}-{$data['properties']['post_id']}") }}"><i class="lni lni-envelope"></i></a>
          </div>
        </div>

      {{-- <div class="profile-content-wrapper">
        <div class="container">
          <!-- User Meta Data-->
          <div class="user-meta-data d-flex">
            <!-- User Thumbnail-->
            <div class="user-thumbnail">
              @if(!empty($data['authors']['avatar']))
                <img src="{{ htmlentities($data['authors']['avatar']) }}" alt="Profile">
              @else
                <img src="{{ asset('img/bg-img/3.jpg') }}" alt="Profile">
              @endif
            </div>
            <!-- User Content-->
            <div class="user-content">
              <h6>
                <a href="#">{{ $data['authors']['editor'] }}</a>
              </h6>

              <p>Jurnalis di Solopos Group. Menulis konten di Solopos Group yaitu Harian Umum Solopos, Koran Solo, Solopos.com.</p>

              <div class="post-list">
                <a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://index.solopos.com @endif">Lihat Artikel Saya Lainnya</a>
              </div>
              <div class="author-social">
                <span>Follow Me: </span>
                <a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ url("/{$data['content']['slug']}-{$data['properties']['post_id']}") }}" target="_blank" rel="noopener"><i class="lni lni-facebook"></i></a>
                <a class="btn-twitter" href="https://twitter.com/home?status={{ url("/{$data['content']['slug']}-{$data['properties']['post_id']}") }}" target="_blank" rel="noopener"><i class="lni lni-twitter-original"></i></a>
                <a class="btn-instagram" href="https://www.instagram.com/koransolopos"><i class="lni lni-instagram"></i></a>
                <a class="btn-whatsapp" href="whatsapp://send?text=*{{ $data['content']['title'] }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ url("/{$data['content']['slug']}-{$data['properties']['post_id']}") }}"><i class="lni lni-whatsapp"></i></a>
                <a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini {{ url("/{$data['content']['slug']}-{$data['properties']['post_id']}") }}"><i class="lni lni-envelope"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div> --}}

      <div class="container mt-3">
        <div class="tag-lists">
        <span>Kata Kunci :</span>
        @if(isset($data['tags']['tag']))
            @foreach($data['tags']['tag'] as $tag)
            @php
                $tag_name = $tag;
                $tag_slug = str_replace(' ', '-',$tag)
            @endphp
                <a href="{{ url("tag/{$tag_slug}") }}">{{ $tag_name }}</a>
            @endforeach
        @endif
        </div>
      </div>

      <!-- widget promosi -->

      <!-- Konten Premium Wrapper -->
      <div class="container">
        <div class="editorial-choice-news-wrapper mt-3">
          <div class="bg-shape1"><img loading="lazy" src="{{ asset('images/edito.png') }}" alt=""></div>
          <div class="bg-shape2" style="background-image: url('{{ asset('images/edito2.png') }}')"></div>
          <div class="container" align="center">
            <img loading="lazy" src="{{ asset('images/plus-putih.png') }}" height="50%" width="50%" style="margin-bottom: 30px;">
          </div>
          <div class="container">
            <div class="editorial-choice-news-slide owl-carousel">
              @php $pc_loop = 1; @endphp
              @foreach($premium as $pc)
              @if($pc_loop <= 6)
              <div class="single-editorial-slide d-flex">
                <a class="bookmark-post" href="https://m.solopos.com/plus"><i class="lni lni-circle-plus"></i></a>
                <div class="post-thumbnail">
                  <img loading="lazy" src="{{ $pc['images']['thumbnail'] }}" alt="{{ html_entity_decode($pc['title']) }}" style="object-fit: cover; object-position: center; height: 177px; width: 132px;">
                </div>
                <div class="post-content">
                  <span class="espos-plus">+ PLUS</span>
                  <a class="post-title d-block" href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ html_entity_decode($pc['title']) }}">{{ html_entity_decode($pc['title']) }}</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="https://m.solopos.com/{{ $pc['category'] }}">{{ $pc['category'] }}</a>
                  </div>
                </div>
              </div>
              @endif
              @php $pc_loop++; @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>

      <?php //include (TEMPLATEPATH . '/widget/widget-jelajahtol.php'); ?>

      <!-- Editors Choice Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          @php $ec_loop = 1; @endphp
          @foreach($premium as $ec) @if($ec_loop > 6 && $ec_loop <=50)
          <div class="terkini-post content-box">
            <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ html_entity_decode($ec['title']) }}">
                  <img loading="lazy" src="{{ $ec['images']['thumbnail'] }}" alt="{{ html_entity_decode($ec['title']) }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                <span class="espos-plus">+ PLUS</span>
                <a class="post-title" href="{{ url("/{$ec['slug']}-{$ec['id']}") }}" title="{{ html_entity_decode($ec['title']) }}">{{ html_entity_decode($ec['title']) }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="https://m.solopos.com/{{ $ec['category']}}">{{ $ec['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($ec['date']) }}</a>
                </div>
              </div>
            </div>
          </div>
          @endif
          @php $ec_loop++; @endphp
          @endforeach
          <div class="text-center mt-3">
            <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
              + PLUS Lainnya...
            </a>
            <a href="https://m.solopos.com/arsip-plus" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
              Arsip Berita
            </a>
          </div>
        </div>
      </div>

      <!-- FAQ Wrapper-->
      {{-- <div class="element-wrapper">
        <div class="container">
          <h6 class="mb-3 mt-4 newsten-title">Frequently Asked Questions</h6>
        </div>
        <div class="container">
          <!-- Default Accordian 1-->
          <div class="accordion" id="faq-premium">
            <!-- Single Accordian-->
            <div class="card border-0">
              <div class="card-header bg-primary p-0" id="heading1">
                <h2 class="mb-0">
                  <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">1. Apa yang dimaksud dengan Espos Premium?</button>
                </h2>
              </div>
              <div class="collapse show" id="collapse1" aria-labelledby="heading1" data-parent="#faq-premium">
                <div class="card-body">
                  <p class="mb-0">Espos Premium adalah artikel yang menyajikan informasi dari fakta dan peristiwa yang diolah serta dipaparkan dengan cara kreatif dan lebih visual. Artikel ditulis dengan bahasa yang lebih sederhana dan lugas serta artikel yang lebih mendalam. Espos Premium juga dapat diakses dengan sistem berlangganan pada situs dalam jaringan (online).</p>
                </div>
              </div>
            </div>
            <!-- Single Accordian 2-->
            <div class="card border-0">
              <div class="card-header bg-primary p-0" id="heading2">
                <h2 class="mb-0">
                  <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">2. Apa perbedaan Espos Premium dengan Web-paper Solopos?</button>
                </h2>
              </div>
              <div class="collapse" id="collapse2" aria-labelledby="heading2" data-parent="#faq-premium">
                <div class="card-body">
                  <p class="mb-0">Web-Paper Solopos merupakan replika digital edisi cetak Harian Umum Solopos. Dan bisa mengakses Web-Paper Solopos melalui alat-alat digital seperti telepon pintar (smartphone), komputer genggam tablet, laptop, atau komputer meja (desktop). Untuk memperoleh informasi lebih detail tentang berlanganan E-Paper Solopos, kunjungi https://koran.solopos.com/. Espos Premium adalah konten yang dapat diakses dengan sistem berlangganan pada situs dalam jaringan (online). Espos Premium disajikan dengan artikel yang lebih mendalam.</p>
                </div>
              </div>
            </div>
            <!-- Single Accordian 3-->
            <div class="card border-0">
              <div class="card-header bg-primary p-0" id="heading3">
                <h2 class="mb-0">
                  <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">3. Bagaimana cara berlangganan Espos Premium?
              </div>
              <div class="collapse" id="collapse3" aria-labelledby="heading3" data-parent="#faq-premium">
                <div class="card-body">
                  <p class="mb-0">Untuk pembaca setia Solopos.com, selama pengembangan Espos Premium Solopos sepenuhnya Gratis.</p>
                </div>
              </div>
            </div>
            <!-- Single Accordian 4-->
            <div class="card border-0">
              <div class="card-header bg-primary p-0" id="heading4">
                <h2 class="mb-0">
                  <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">4. Apa keunggulan berlangganan Espos Premium?</button>
                </h2>
              </div>
              <div class="collapse" id="collapse4" aria-labelledby="heading4" data-parent="#faq-premium">
                <div class="card-body">
                  <div class="container">
                  <ol>
                    <li>Dengan berlangganan Espos Premium, Anda mendapatkan berita ekonomi, bisnis, dan market terpilih dengan artikel yang lebih mendalam.</li>
                    <li>
                      Sebuah karya jurnalistik adalah sumber informasi sahih. Untuk mendapatkan informasi tersebut, butuh biaya tinggi. Dengan berlangganan Espos Premium, berarti Anda mendukung keberlangsungan informasi yang akan menjadi sumber kepercayaan diri ketika membuat keputusan.
                    </li>
                    <li>
                      Untuk benefit-benefit sebesar itu, harga berlangganan Espos Premium tergolong murah dan sangat bermanfaat.
                    </li>
                    <li>
                      Espos Premium Bisnis Indonesia membuat koleksi berlangganan produk Bisnis Indonesia Group yang sudah Anda miliki menjadi lengkap.
                    </li>
                  </ol>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> --}}

      <!-- Terpopuler Wrapper-->
      <div class="mt-3">
        @include('includes.widget-popular')
      </div>

      <div class="container">
        <div class="border-top"></div>
      </div>
    </div>
    {{-- <img src="https://cm+sx.solopos.com/set-view?id={{ $content['id'] }}" style="display: none;"> --}}
      {{-- <iframe src="https://api.solopos.com/set-view?id={{ $content['id'] }}" style="position: absolute;width:0;height:0;border:0;bottom:0;"></iframe> --}}
      @push('custom-scripts')
      {{-- <script>
        $(window).load(function() {
          $.ajax({
              type: "GET",
              url: 'https://api.solopos.com/set-view?id={{ $content['id'] }}',
          });
      });
      </script> --}}
      @endpush
      @push('tracking-scripts')
        <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        url:'https://tf.solopos.com/api/v1/stats/store',
        method:'POST',
        data:{
                post_id:'{{ $content['id'] }}',
                post_title:'{{ $header['title'] }}',
                post_slug:'{{ $content['slug'] }}',
                post_date:'{{ $content['date'] }}',
                post_author:'{{ $header['author'] }}',
                post_editor:'{{ $header['editor'] }}',
                post_category:'{{ $header['category_parent'] }}',
                post_subcategory:'{{ $header['category_child'] }}',
                post_tag:'{!! serialize($content['tag']) !!}',
                post_thumb:'{{ $header['image'] }}',
                post_view_date:'{{ date('Y-m-d') }}',
                domain:'{{ 'm.solopos.com' }}'
            },
        success:function(response){
            if(response.success){
                console.log(response.message)
            }else{
                console.log(error)
            }
        },
        error:function(error){
            console.log(error)
        }
        });
        </script>
    @endpush

      <style>
        .subscribe {
          background-color: #fff;
          margin-top: 20px;
          margin-bottom: 50px;
          position: relative;
          border: 1px solid #00437d;
          border-radius: 10px;
        }
        .header-box {
          display: flex;
          flex-direction: row;
          justify-content: space-between;
          align-items: center;
          padding: 20px 15px;
        }
        .top-box {
          display: flex;
          flex-direction: column;
          justify-content: center;
          padding: 0 15px;
        }
        .header-box .logo {
          width: 120px;
        }
        .header-box .login {
          font-family: Verdana, Geneva, Tahoma, sans-serif;
          font-size: 13px;
          font-weight: 600;
          text-align: right;
          letter-spacing: 0.02em;
          color: #f29b27;
        }
        .subscribe .lead {
          font-size: 21px;
          font-family: Verdana, Geneva, Tahoma, sans-serif;
          line-height: 29px;
          font-weight: 700;
          text-align: center;
          letter-spacing: -0.01em;
          color: #2E2E2E;
          margin: 5px auto 10px auto;
        }
        .subscribe .sublead {
          text-align: center;
          margin: 0 auto 15px auto;
          font-size: 12px;
          font-family: Verdana, Geneva, Tahoma, sans-serif;
          line-height: 19px;
        }
        .button-container {
          display: flex;
          flex-direction: column;
          justify-content: center;
          margin: 20px auto;
        }
        .checkout-button {
          display: inline-block;
          width: 325px;
          height: 40px;
          padding: 8px 32px;
          border: 1px solid #00437d;
          border-radius: 4px;
          font-family: Verdana, Geneva, Tahoma, sans-serif;
          font-weight: bold;
          font-size: 14px;
          letter-spacing: 0.05em;
          text-transform: uppercase;
          text-align: center;
          color: #FFFFFF !important;
          background: #00437d;
          margin: 0 auto 15px auto;
        }
        .inverse {
          color: #00437d !important;
          background: #FFFFFF;
          border: 1px solid #00437d;
        }
        .button-container a {
          font-family: Verdana, Geneva, Tahoma, sans-serif;
          font-size: 14px;
          font-weight: bold;
          text-align: center;
          letter-spacing: 0.02em;
          text-decoration: none;
        }
        .footer-box {
          display: flex;
          flex-direction: row;
          justify-content: flex-start;
          padding: 15px;
        }
        .callout {
          display: block;
          font-family: 'Roboto', sans-serif;
          font-weight: 500;
          font-size: 9px;
          line-height: 9px;
          text-transform: uppercase;
          letter-spacing: 0.05em;
          opacity: 0.7;
        }
        .intro-content {background-image: linear-gradient(180deg,hsla(0,0%,100%,0),#fff);height:5rem;width:100%;position:relative;margin-top:-90px;}
        [data-theme="dark"] .intro-content {background-image: linear-gradient(180deg,hsla(0,0%,100%,0),#111);}
      </style>

@endsection
