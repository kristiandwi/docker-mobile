<html>
<head>
    <script type="text/javascript">
      if (screen.width > 640) {
        var pathname = window.location.pathname;
        window.location.href = "https://www.solopos.com"+pathname;
      }
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-MBVNS9P');</script>
    <!-- End Google Tag Manager -->
    <title>Ads Inventory Solopos.com Mobile Single Page</title>
    <meta content="Ads Inventory Solopos.com Mobile Single Page" itemprop="headline" />
    <meta name="title" content="Ads Inventory Solopos.com Mobile Single Page" />
    <meta name="description" content="Ads Inventory Solopos.com Mobile Single Page" itemprop="description" />
    <meta name="keywords" content="Ads Inventory Solopos.com Mobile Single Page" itemprop="keywords" />
    <meta name="news_keywords" content="Ads Inventory Solopos.com Mobile Single Page" itemprop="keywords" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta name="copyright" content="Solopos Media Group" /> 
    <meta name="googlebot-news" content="index,follow" />    
    <meta name="googlebot" content="index,follow,all" />
    <meta name="Bingbot" content="index,follow">
    <meta name="robots" content="index,follow,max-image-preview:large" />
    <meta name="google-site-verification" content="AA4UjbZywyFmhoSKLELl4RA451drENKllt5Sbq9uINw" />
    <meta name="yandex-verification" content="7966bb082003f8ae" />
    <meta name="msvalidate.01" content="F2320220951CEFB78E7527ECC232031C" />
    <meta name='dailymotion-domain-verification' content='dm0w2nbrkrxkno2q2' />
    <meta name="language" content="id" />
    <meta name="geo.country" content="id" />
    <meta http-equiv="content-language" content="In-Id" />
    <meta name="geo.placename" content="Indonesia" />
    <meta name="showus-verification" content="pub-2627" />

    <!-- Favicon-->
    <link rel="icon" href="{{ url('images/favicon.ico') }}">
  
    <!-- Stylesheet-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" />    
    <link rel="stylesheet" href="https://cdn.lineicons.com/2.0/LineIcons.css">
    <link rel="stylesheet" href="{{ url('style.css')}}?v={{time()}}">    
  </head>
<!-- Header Area-->
<div class="header-area" id="headerArea">
    <div class="container h-100 d-flex align-items-center justify-content-between">

      <div class="navbar--toggler" id="sposNavbarToggler">
        <a href="#">
          <i class="lni lni-grid-alt" style="font-size: 1.45rem; font-weight: bold; display: block; color: #00437c; margin-top: 2px; margin-left: 3px;"></i>
        </a>
      </div>
     
      <!-- Logo-->
      <div class="logo-wrapper">                       
        <a href="{{ url('/mediakit') }}"> 
          <img class="logo-image" src="{{ asset('images/logo.png') }}" alt="Solopos.com">
        </a>
      </div>

      <div class="spos-story">
        <div class="spos-icon">
          <a href="https://m.solopos.com/" title="Solopos Story">
            <img src="{{ asset('images/icon.png') }}">
          </a>
        </div>
      </div>                           

    </div>
  </div>
  <div class="page-content-wrapper">
    <div class="container">
      <nav aria-label="breadcrumb" style="text-align: center;">
        <ol class="breadcrumb" style="text-transform: capitalize;font-size:13px;font-weight:600;">
          <li class="breadcrumb-item"><a href="{{ url('/') }}" title="Solopos.com">Home</a></li>
          <li class="breadcrumb-item"><a href="#" title="Archive">soloraya</a></li>       
        </ol>
      </nav>
    </div>        
  <!-- Scroll Indicator-->
  <div id="scrollIndicator"></div>
  <!-- Single Blog Info-->
  <div class="single-blog-info">
    <div class="d-flex align-items-center">
      <div class="post-content-wrap mt-1">
        <h1 class="mt-2 mb-2">5 Kuliner Gerobakan yang Lagi Viral di Solo, Sudah Pernah Coba?</h1>
        <div class="mb-4" style="font-size: 12px;line-height:16px;font-family:roboto">
          Ini dia lima kuliner viral dijual di gerobak di Solo, Jawa Tengah. Kira-kira kamu suka yang mana dari kelima jajanan viral tersebut?
        </div>
        <div class="post-meta">
          <div class="post-date penulis">
            Nugroho Meidinata - Solopos.com
          </div>
          <div class="post-date">Senin, 3 Mei 2021 - 14:06 WIB</div>
          <div class="post-date"><!-- reading time --></div>
        </div>
      </div>
    </div>
  </div>
      
   <!-- Single Blog Thumbnail-->
   <div class="single-blog-thumbnail">
     <img class="w-100 single-blog-image" src="{{ url('images/iklan-img/office-solopos.jpg') }}" alt="">
   </div>
   <div class="foto-caption mb-3" style="font-size: 11px;padding: 5px 10px;line-height: 13px;color:#797494;">
    SOLOPOS.COM - Panduan Informasi dan Inspirasi
  </div> 
  
  <!-- Blog Description-->
  <div class="blog-description">
    <div class="container">
      
      <p><strong>Solopos.com, SOLO</strong> -- Di bawah ini ada kuliner gerobakan yang lagi viral di Solo, Jawa Tengah.</p>
      <p>Berbicara Solo tak bisa lepas dengan beraneka macam kulinernya. Berbagai penganan lezat nan menarik banyak ditemukan di <strong><a href="">Solo</a></strong>.</p>

      <div class="iklan mb-3" align="center" >
        <a href="" data-toggle="modal" data-target="#ModalMobileArtikel1">
        <img class="img-fluid" src="{{ url('images/iklan-img/mobile-artikel-1.png') }}" style="object-fit: cover; object-position: center; ">
        </a>
      </div>                    
            
      <p><em><strong>Baca Juga: <a href=""> Mudik Dilarang, Enaknya Ngapain Ya Pas Libur Lebaran?</a></strong></em></p>
      <p>Mulai dari aneka sate, soto, tengkleng, nasi liwet, gudeg, selat dan aneka kuliner lainnya sangat mudah ditemukan di Solo.</p>
      <p>Selain <strong><a href="">kuliner</a> </strong>di atas, ternyata ada kuliner lain yang lagi viral di Solo dan dijual di gerobak.</p>
      <p><em><strong>Baca Juga: <a href=""> Pertama Kali, Paguyuban Lintas Selatan Bagikan Takjil di Gembongan Sukoharjo</a></strong></em></p>
      <p>Apa saja kuliner viral yang dijual di gerobakan di Solo itu? Berikut ini di antaranya.</p>
      <h3>5 Kuliner Viral di Solo</h3>
      <h4>1. Martabak Korea</h4>

      <div align='center' class="iklan mb-3">
        <a href="" data-toggle="modal" data-target="#ModalMobileArtikel2">
        <img class="img-fluid" src="{{ url('images/iklan-img/mobile-artikel-2.png') }}" style="object-fit: cover; object-position: center; ">
        </a>				                
      </div>

      <p>Jika biasanya masyarakat mengenal martabak Tegal, di pinggiran Solo, tepatnya di Cemani, Sukoharjo terdapat jajanan yang dijual di gerobak, yakni martabak Korea.</p>
      <p>Berbeda dengan martabak lainnya, martabak Korea ini berukuran lebih kecil dan memiliki tekstur lebih renyah.</p>
      <p><em><strong>Baca Juga: <a href=""> Kapan Malam Lailatul Qadar? Kenali Tanda-tandanya di Sini</a></strong></em></p>
      <p>Isian dari kuliner viral di Solo ini ada daun bawang dan juga telur.</p>
      <p>Uniknya martabak telur Korea ini dinikmati dengan cocolan sambal yang rasanya pedas manis dan gurih. Karena rasanya yang lezat dan harganya yang murah cuma Rp1.000 per biji, kuliner ini banyak diminati masyarakat.</p>
      <p><em><strong>Baca Juga: <a href=""> 4 Kuliner Khas Angkringan Solo, Bisa Jadi Menu Buka Puasa Hlo!</a></strong></em></p>
      <h4>2. Sate Kenyil</h4>
      <p>Kuliner viral di Solo berikutnya ada sate kenyil. Ada banyak penjual yang menjajakn sate kenyil di Solo.</p>
      <p>Beberapa di antaranya ada di belakang <strong><a href="">Stadion Manahan</a></strong> dan ada pula di Gentan, Baki, Sukoharjo.</p>
      <p><em><strong>Baca Juga: <a href=""> Ini Menu Sahur Bersejarah Soekarno-Hatta Saat Malam Penyusunan Teks Proklamasi</a></strong></em></p>
      <p>Sate yang memiliki tekstur kenyal ini sangat murah <em>hlo</em>, cuma Rp500 per tusuk. Bagaimana, mau mencoba?</p>
      <h4>3. Bakso Gongso</h4>
      <p>Kuliner viral di Solo selanjutnya ada bakso gongso. Jika biasanya penganan ini terbuat dari jerohan sapi maupun ayam, kali ini berbeda karena berbahan dasar bakso.</p>
      <p>Penjual bakso gongso di Solo mudah ditemukan, salah satunya di Jl Mr Sartono No. 30, Nusukan atau di depan SMAN 6 Solo.</p>
      <p><em><strong>Baca Juga: <a href=""> Pakai Inhaler Saat Puasa, Bagaimana Hukumnya?</a></strong></em></p>
      <p>Dengan harga seporsi Rp5.000, kamu bakal dapat bakso gonso lengkap berisi bakso ayam, bakso sapi, tahu bakso dan juga telur. Selain itu, ada tambahan pangsit di atasnya.</p>
      <h4>4. Takoyaki Hiroshi</h4>
      <p>Beberapa waktu lalu sempat viral di Solo kuliner takoyaki yang dimasak langsung oleh orang Jepang langsung, yakni Hiroshi.</p>
      <p>Takoyaki buatan Hiroshi dijual seharga Rp10.000 per porsi dengan isi tujuh bulatan. Meski harganya murah, resep takoyaki itu asli dari Jepang. Isiannya adalah gurita, daun bawang, serta ikan. Sausnya juga diracik sendiri dengan resep otentik.</p>
      <p><em><strong>Baca Juga: <a href=""> Tsunami Covid-19 India, Ahli: Bisa Jadi karena Euforia Vaksinasi</a></strong></em></p>
      <p>Selain takoyaki, ada juga ramen yang dijual seharga Rp12.00-Rp13.000 per porsi. Serta gyoza seharga Rp5.000 per porsi isi lima.</p>
      <p>Untuk lokasinya sendiri, warung kuliner viral ini berlokasi di dekat SPBU Pucangsawit, Jebres.</p>
      <p><em><strong>Baca Juga: <a href=""> Yayasan Pendidikan Warga, Hadirkan Sekolah dengan Biaya Terjangkau dan Berkualitas di Solo</a></strong></em></p>
      <h4>5. Dimsum Pasar Gede</h4>

      <p>Kuliner viral di Solo terakhir adalah dimsum yang dijual di Pasar Gede bagian barat.</p>
      <p>Dimsum di sini terkenal murah sehingga ramai oleh pembeli. Untuk satu buah dimsum cuma dihargai Rp3.000 saja. Variannya pun beraneka ragam sehingga cocok untuk segala kalangan.</p>
      <p><em><strong>Baca Juga: <a href=""> Kenangan Netizen di Terminal Kartasura Lama: Tempat Main Gim - Lokasi Bolos Sekolah</a></strong></em></p>
      <p>Tapi kalau ke sini harus sabar antre ya!</p>

    <!-- Ads Bottom Articles  -->
    <div class="iklan mb-3" align="center">
      <a href="" data-toggle="modal" data-target="#ModalMobileArtikel3">
        <img class="img-fluid" src="{{ url('images/iklan-img/mobile-artikel-3.png') }}" style="object-fit: cover; object-position: center; ">
      </a>
    </div> 
    
    </div>
  </div>


  <div class="profile-content-wrapper">
    <!-- Settings Option-->
    <div class="profile-settings-option"><a class="post-share" href="#" data-toggle="modal" data-target="#postShareModal"><i class="fa fa-share-alt"></i></a></div>
    <div class="container">
      <!-- User Meta Data-->
      <div class="user-meta-data d-flex">
        <!-- User Thumbnail-->
        <div class="user-thumbnail">
            <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Profile">
        </div>
        <!-- User Content-->
                      
        <div class="user-content">
          <h6>
            <a href="" target="_blank">Nugroho Meidinata</a>
          </h6>
             <p><p>Describe your mind with words.</p></p>
          <div class="post-list">
            <a href="" target="_blank">Lihat Artikel Saya Lainnya</a>
          </div>
          <div class="author-social">
            <span>Follow Me: </span>
            <a href=""><i class="fa fa-twitter"></i></a>
            <a href=""><i class="fa fa-facebook"></i></a>
            <a href=""><i class="fa fa-instagram"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
          </div>              
        </div>
      </div>
    </div>
  </div>      

  <div class="container mt-3">
    <div class="tag-lists">
    <span>Kata Kunci :</span>
      <a href="">Wisata</a> <a href="">Kuliner</a> <a href="">wisata solo</a> <a href="">kuliner solo</a>                                          
    </div>
  </div> 

  <div class="iklan mt-3" align="center">
    <a href="" data-toggle="modal" data-target="#ModalMobileUnderArtikel">
    <img class="img-fluid" src="{{ url('images/iklan-img/mobile-single-underartikel.png') }}" style="object-fit: cover; object-position: center;">
    </a>
  </div>
    


    <div class="terkait-wrapper">
      <div class="container">
        <div class="d-flex align-items-center justify-content-between mb-3">
          <h5 class="mb-0 pl-1 spos-title">Berita Terkait</h5>
        </div>
      </div>
      <div class="container">
          <div class="terkini-post d-flex">
            <div class="post-thumbnail">
              <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
              </a>
            </div>
            <div class="post-content">
              <a class="post-title" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
              <div class="post-meta d-flex align-items-center">
                <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 2 Jam yang lalu</a>
              </div>
            </div>
          </div>

          <div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                  <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                <div class="post-meta d-flex align-items-center">
                  <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 2 Jam yang lalu</a>
                </div>
              </div>
            </div>

            <div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                  <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                <div class="post-meta d-flex align-items-center">
                  <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 2 Jam yang lalu</a>
                </div>
              </div>
            </div>
      </div>
    </div>


    
    <!-- Konten Promosi Wrapper-->
      <div class="container mt-3">
        <div class="d-flex align-items-center justify-content-between mb-3">
          <div class="badge badge-danger">Promo & Events</div>
        </div>
        <!-- Default Carousel-->
        <div class="carousel slide promosi" id="carouselExampleIndicators" data-ride="carousel">
          <!-- Indicators-->
          <ol class="carousel-indicators">
            <li class="active" data-target="#carouselExampleIndicators" data-slide-to="0"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          </ol>
          <!-- Carousel Inner-->
          <div class="carousel-inner">		  
              <div class="carousel-item active">
                  <a href="" data-toggle="modal" data-target="#ModalMobilePromo" title="Pisalin" target="_blank">
                  <img class="d-block w-100" src="{{ url('./banner/pisalin_oct.png') }}">
                  </a>
              </div>
              <div class="carousel-item">
                  <a href="" data-toggle="modal" data-target="#ModalMobilePromo" title="Promo dan Event">
                  <img class="d-block w-100" src="{{ url('banner/ayam-silaturahmi.png') }}" alt="Promo dan Event">
                  </a>
              </div>	  
          </div>
        </div>         
    </div>

    <!-- Konten Terpopuler Wrapper -->
    <div class="mt-3">
      <!-- Item Slides Wrapper-->
      <div class="container">
        <div class="d-flex align-items-center justify-content-between mb-3 mt-3">
          <a class="btn btn-primary btn-sm" href="/arsip">Berita Terpopular</a>
        </div>
      </div>       
      <div class="item-wrapper mb-3">       
        <div class="container">
        <!-- Catagory Slides-->
          <div class="item-slides owl-carousel">                        
            <div class="item">
              <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet}">
                <div class="item-thumbnail">
                  <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; width: 195px; height: 115px;" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                </div> 
                <div class="item-caption">
                  <span class="link-title">Lorem ipsum dolor sit amet</span>
                </div>
              </a>
            </div>                                                        
          </div>
        </div>
      </div>
    </div> 

     <!-- Ads Mobile Single 1 -->
     <div class="iklan mt-3">
        <a href="" data-toggle="modal" data-target="#ModalMobileSingle1">
            <img class="img-fluid" src="{{ url('images/iklan-img/mobile-single-1.png') }}" style="object-fit: cover; object-position: center;">
        </a>
    </div>

    <!-- Konten Premium Wrapper -->
    <div class="container">
      <div class="editorial-choice-news-wrapper mt-3">
        <div class="bg-shape1"><img src="{{ asset('images/edito.png') }}"></div>
        <div class="bg-shape2" style="background-image: url('{{ asset('images/edito2.png') }}')"></div>
        <div class="container" align="center">
          <img src="{{ asset('images/premium.png') }}" height="50%" width="50%" style="margin-bottom: 30px;">
        </div>
        <div class="container">
          <div class="editorial-choice-news-slide owl-carousel">
   
            <div class="single-editorial-slide d-flex">
              <a class="bookmark-post" href="#"><i class="lni lni-star"></i></a>
              <div class="post-thumbnail">
                <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" style="object-fit: cover; height: 177px; width: 132px;" loading="lazy" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
              </div>
              <div class="post-content">
                <a class="post-catagory" href="{{ url("/premium") }}">Premium</a>
                <a class="post-title d-block" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet" >Lorem ipsum dolor sit amet</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

     <!-- Ads Mobile Single 2 -->
     <div class="iklan mt-3">
      <a href="" data-toggle="modal" data-target="#ModalMobileSingle2">
          <img class="img-fluid" src="{{ url('images/iklan-img/mobile-single-2.png') }}" style="object-fit: cover; object-position: center;">
      </a>
    </div>

    <!-- Blok 1 Terkini Wrapper -->
    <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Terkini</h5><a class="btn btn-primary btn-sm" href="/arsip">Indeks Berita</a>
          </div>
        </div>
        <div class="container">

            <div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                  <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                <div class="post-meta d-flex align-items-center">
                  <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 2 Jam yang lalu</a>
                </div>
              </div>
            </div>

            <div class="terkini-post d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                    <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 2 Jam yang lalu</a>
                  </div>
                </div>
              </div>

              <div class="terkini-post d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                    <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 2 Jam yang lalu</a>
                  </div>
                </div>
              </div>

            </div>
          </div> 
        </div>
         <!-- Ads Mobile Single 3 -->
  <div class="iklan mt-3 mb-3">
      <a href="" data-toggle="modal" data-target="#ModalMobileSingle3">
          <img class="img-fluid" src="{{ url('images/iklan-img/mobile-single-3.png') }}" style="object-fit: cover; object-position: center;">
      </a>
  </div>      
  <div class="container">
    <div class="border-top"></div>
  </div>

<!-- Footer Nav-->
<div class="footer-nav-area" id="footerNav">
  <div class="spos-footer-nav h-100">
    <ul class="h-100 d-flex align-items-center justify-content-between">
      <li>
          <a href="/videos">
              <img src="{{ asset('images/icon_live.png') }}" width="32px" style="margin-top:10px;">
          </a>
      </li>
      <li>
          <a href="/terpopuler">
              <img src="{{ asset('images/icon_trending.png') }}"><!--i class="lni lni-bolt-alt"></i--><span>TRENDING</span>
          </a>
      </li>
      <li>
          <a href="/premium">
              <img src="{{ asset('images/icon_premium.png') }}"><!--i class="lni lni-star-filled"></i--><span>PREMIUM</span>
          </a>
      </li>
      <li>
          <a href="/soloraya">
              <img src="{{ asset('images/icon_soloraya.png') }}" width="32px"><!--i class="lni lni-shortcode"></i--><span>SOLORAYA</span> 
          </a>
      </li>          
      <li>
          <a href="/arsip">
              <img src="{{ asset('images/icon_index.png') }}" width="32px"><!--i class="lni lni-archive"></i--><span>INDEKS</span>
          </a>
      </li>
    </ul>
  </div>
</div>

<div id="footer">
  <div class="footer" data-component="mobile:footer" data-component-name="mobile:footer">
    <div class="footer__nav-wrapper">
      <div class="footer__logo">
        <img class="footer-logo-image" style="filter: grayscale(100%);" src="{{ asset('images/logo-solopos.png') }}" alt="Solopos.com">
      </div>
      
      <ul class="footer--nav-list js-footer-nav-list" data-component-name="mobile:footer:nav-list">
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/about-us" title="Tentang Kami">Tentang Kami</a>
        </li>   
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/copyright" title="Copyright">Copyright</a>
        </li>                     
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/kontak" title="Kontak">Kontak</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/mediakit" title="Media Kit">Media Kit</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/about-us" title="Redaksi">Redaksi</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/code-of-conduct" title="Pedoman Media Siber">Pedoman Media Siber</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/loker" title="Karir">Karir</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/privacy-policy" title="Privacy Policy">Privacy Policy</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/cekfakta" title="Metode Cek Fakta">Cek Fakta</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/info-iklan" title="Info Iklan">Info Iklan</a>
        </li>
      </ul>

      <div class="footer__copyright">
        <p> Copyright © 2021 <a href="https://www.solopos.com" class="footer__copyright-link">solopos.com</a><br>Media Informasi & Inspirasi. All Rights Reserved </p>
      </div>
    </div>
  </div>
</div>

@include('includes.navbar-section')

  <!-- Modal Mobile Artikel 1-->
  <div class="modal fade" id="ModalMobileArtikel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Artikel 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single/Konten (Kecuali Premium) Didalam artikel Setelah Paragraf Ke-2<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Artikel 2-->
  <div class="modal fade" id="ModalMobileArtikel2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Artikel 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single/Konten (Kecuali Premium) Didalam artikel Setelah Paragraf Ke-7<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Artikel 3-->
  <div class="modal fade" id="ModalMobileArtikel3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Artikel 3 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single/Konten Diakhir artikel/Setelah Paragrah Terakhir<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  
  <!-- Modal Mobile Under Artikel-->
  <div class="modal fade" id="ModalMobileUnderArtikel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Under Artikel Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single/Konten (Kecuali Premium) Dibawah Artikel/Konten<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Single 1-->
  <div class="modal fade" id="ModalMobileSingle1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Single 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single ( Dibawah Bagian Berita Terpopuler )<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Single 2-->
  <div class="modal fade" id="ModalMobileSingle2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Single 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single ( Diatas Bagian Berita Terkini Single, Dibawah Widget Espos Premium)<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Single 3-->
  <div class="modal fade" id="ModalMobileSingle3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Single 3  Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single ( Dibawah Berita Terkini )<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Single 4-->
  <div class="modal fade" id="ModalMobileSingle4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Single 4 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan (Dibawah Berita Terpopuler)<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Widget-->
  <div class="modal fade" id="ModalMobileWidget" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Widget Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Dibawah Konten / Artikel (Dalam Bentuk feed/list berita)
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Promo -->
  <div class="modal fade" id="ModalMobilePromo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Promo & Event Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan & Artikel, Banner Slide<br>
          <b>Rekomendasi Ukuran : </b>340 x 100 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>

<!-- All JavaScript Files-->
<script src="{{ url('js/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="{{ asset('js/waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/jquery.animatedheadline.min.js') }}"></script>
<script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/default/date-clock.js') }}"></script>
<script src="{{ asset('js/default/dark-mode-switch.js') }}"></script>
<script src="{{ asset('js/default/active.js') }}"></script>

</body>
</html>