    @extends('layouts.app')
    @section('content')
    @include('includes.ads.popup-banner')

        <div class="page-content-wrapper">
            <div class="container">
            <nav aria-label="breadcrumb" style="text-align: center;">
                <ol class="breadcrumb" style="text-transform: capitalize;font-size:13px;font-weight:600;">
                <li class="breadcrumb-item"><a href="{{ url('/') }}" title="Solopos.com">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/') }}/foto" title="Archive">Foto</a></li>
                </ol>
            </nav>
            </div>
        <!-- Scroll Indicator-->
        <div id="scrollIndicator"></div>
        <!-- Single Blog Info-->
        <div class="single-blog-info">
            <div class="d-flex align-items-center">
            <div class="post-content-wrap mt-1">
                <h1 class="mt-2 mb-2 title-only">{{ $header['title'] }}</h1>
                <div class="mb-4" style="font-size: 12px;line-height:16px;font-family:roboto">
                {{ $header['ringkasan'] ?? ''}}
                </div>
                <div class="post-meta">
                <div class="post-date tgl-terbit" style="margin-bottom: 5px;">{{ Carbon\Carbon::parse($content['date'])->translatedFormat('l, j F Y - H:i') }} WIB</div>
                <div class="post-date">
                    <div class="multi-reporter">
                    @if($header['author'] != $header['editor'] )
                        Penulis:
                        @foreach ($author_slug as $author)
                        <a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else {{ url('/') }}/penulis/{{$author}} @endif">{{ ucwords(str_replace('_', ' ', $author)) }}</a>
                        @endforeach
                    @endif
                    </div>
                    Editor : <a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://m.solopos.com/arsip @endif" style="text-transform: none;"> {{ $header['editor'] }}</a>
                </div>
                </div>
            </div>
            </div>
        </div>
        <style>
            figure, .wp-caption {display: none !important;}
            .carousel-control-next, .carousel-control-prev {top: 20px !important; align-items:start !important;}
            .carousel-control-prev {left: auto; right: 30px !important;}
        </style>
        <!-- Single Blog Thumbnail-->
            @php
            $image = Helper::konten(htmlspecialchars_decode($content['content'])) ;
            $images = explode('</figure>', $image);
            $total_img = count(array_filter($images));

            $text = Helper::konten(htmlspecialchars_decode($content['content']));
            preg_match_all( '@src="([^"]+)"@' , $text, $img );
            preg_match_all( '/<figcaption (.*?)>(.*?)<\/figcaption>/s' , $text, $capt );
            $src = array_pop($img);
            $captions = array_pop($capt);
            @endphp

            @if($total_img == 1 )
            <div class="single-blog-thumbnail">
                <img loading="lazy" class="w-100 single-blog-image" src="{{ $content['image'] }}" alt="{{ html_entity_decode($content['title']) }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                {{-- <a class="post-bookmark" href="#" data-toggle="modal" data-target="#fotocaption"><i class="lni lni-text-align-justify"></i></a> --}}
              </div>
              <div class="foto-caption mb-3" style="font-size: 11px;padding: 5px 10px;line-height: 13px;color:#797494;">
                @if(!empty($content['caption']))
                  SOLOPOS.COM - {{ html_entity_decode($content['caption']) }}
                @else
                  SOLOPOS.COM - Panduan Informasi dan Inspirasi
                @endif
              </div>
            @else
            <!-- Slide with Captions-->
            <div class="carousel slide" id="beritaFoto" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="single-blog-thumbnail">
                            <img loading="lazy" class="w-100 single-blog-image" src="{{ $content['image'] }}" alt="{{ html_entity_decode($content['title']) }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                        </div>
                        <div class="foto-caption mb-3" style="font-size: 11px;padding: 5px 10px;line-height: 13px;color:#797494;">
                            @if(!empty($content['caption']))
                            SOLOPOS.COM - {{ html_entity_decode($content['caption']) }}
                            @else
                            SOLOPOS.COM - Panduan Informasi dan Inspirasi
                            @endif
                        </div>
                    </div>
                    @foreach (array_combine($src, $captions) as $src => $captions)
                    <div class="carousel-item">
                        <div class="single-blog-thumbnail">
                            <img loading="lazy" class="w-100 single-blog-image" src="{{ $src }}" alt="{{ html_entity_decode($content['title']) }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                        </div>
                        <div class="foto-caption mb-3" style="font-size: 11px;padding: 5px 10px;line-height: 13px;color:#797494;">
                            SOLOPOS.COM - {{$captions}}
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- Controls-->
                <a class="carousel-control-prev" href="#beritaFoto" role="button" data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#beritaFoto" role="button" data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a>
            </div>
            @endif


        {{-- <style>
            figure {display: none !important;}
        </style>
        <div class="single-blog-thumbnail">
            <img class="w-100 single-blog-image" src="{{ $content['image'] }}" alt="{{ html_entity_decode($content['title']) }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
        </div>
        <div class="foto-caption mb-3" style="font-size: 11px;padding: 5px 10px;line-height: 13px;color:#797494;">
            @if(!empty($content['caption']))
            SOLOPOS.COM - {{ html_entity_decode($content['caption']) }}
            @else
            SOLOPOS.COM - Panduan Informasi dan Inspirasi
            @endif
        </div> --}}
        <!-- Blog Description-->
        <div class="blog-description mt-3">
            <div class="container">
                @include('includes.ads.top-single')

                @php
                    $konten = Helper::konten(htmlspecialchars_decode($content['content'])) ;
                    $contents = explode('</p>', $konten);
                    $total_p = count(array_filter($contents));
                    // many paragraph
                    $p_promosi  = array_slice($contents, 0, 2);
                    $p_iklan_1  = array_slice($contents, 2, 2);
                    $last_p  = array_slice($contents, 9);
                @endphp
                <!-- ads top -->

                <!-- ads parallax -->
                {!! implode('</p>', $p_promosi) !!}
                <p><em><strong><span>Promosi</span></span><a href="https://m.solopos.com/{{ $promosi['slug'] }}-{{ $promosi['id'] }}?utm_source=read_promote" title="{{ $promosi['title'] }}" target="_blank" rel="noopener">{{ $promosi['title'] }}</a></strong></em>
                </p>

                {!! implode('</p>', $p_iklan_1) !!}
                @include('includes.ads.inside-article-1')

                {!! implode('</p>', $last_p) !!}
                @include('includes.ads.inside-article-last')

                @include('includes.ads.internal-promotion')
            </div>

            <div class="container social-share-btn d-flex align-items-center flex-wrap">
            SHARE :
            <a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="lni lni-facebook"></i></a>
            <a class="btn-twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="lni lni-twitter-original"></i></a>
            <a class="btn-instagram" href="https://www.instagram.com/koransolopos" title="social"><i class="lni lni-instagram"></i></a>
            <a class="btn-whatsapp" href="whatsapp://send?text=*{{ urlencode($header['title']) }}* | {{ urlencode($header['ringkasan']) }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="lni lni-whatsapp"></i></a>
            <a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini {{ $header['link'] }}" title="social"><i class="lni lni-envelope"></i></a>
            </div>
        </div>

        <div class="container mt-3">
            <div class="tag-lists">
            <span>Kata Kunci :</span>
            @if(isset($header['arrayTag']))
                @foreach($header['arrayTag'] as $tag)
                @php
                    $tag_name = ucwords($tag);
                    $tag_slug = str_replace(' ', '-',$tag);
                    $tag_slug = strtolower($tag_slug)
                @endphp
                <a href="{{ url("/tag/{$tag_slug}") }}">{{ $tag_name }}</a>
                @endforeach
            @endif
            </div>
        </div>

        <!-- ads under article -->
        <div class="iklan mt-3">
        @include('includes.ads.under-article')
        </div>

        <div class="terkini-wrapper">
            <div class="container">
            <div class="d-flex align-items-center justify-content-between mb-3">
                <h5 class="mb-0 pl-1 spos-title">Berita Terkait</h5><a class="btn btn-primary btn-sm" href="{{ url('/') }}/arsip">Indeks Berita</a>
            </div>
            </div>
            <div class="container">
            <ul id="relposts">
                <div class="terkini-post d-flex"><div class="post-thumbnail"><a href="tips-bangun-bisnis-yang-cocok-buat-kaum-rebahan-di-2022-anti-ribet-1336534" title=""><img src="https://images.solopos.com/2022/06/Dilayani-Tokopedia-2-1-100x100.jpg" loading="lazy" alt="Tips Bangun Bisnis yang Cocok Buat Kaum Rebahan di 2022, Anti Ribet!" style="object-fit: cover; height: 100px; width: 100px;"></a></div><div class="post-content"><a class="post-title" href="tips-bangun-bisnis-yang-cocok-buat-kaum-rebahan-di-2022-anti-ribet-1336534" title="Tips Bangun Bisnis yang Cocok Buat Kaum Rebahan di 2022, Anti Ribet!">Tips Bangun Bisnis yang Cocok Buat Kaum Rebahan di 2022, Anti Ribet!</a><div class="post-meta d-flex align-items-center"><a href="#">Bisnis</a><a href="#" style="padding-left:7px;"></a></div></div></div>

                <div class="terkini-post d-flex"><div class="post-thumbnail"><a href="mengupas-hipospadia-kelainan-bawaan-yang-bisa-dialami-anak-laki-laki-1336362" title=""><img src="https://images.solopos.com/2022/06/hispos-100x100.jpg" loading="lazy" alt="Mengupas Hipospadia, Kelainan Bawaan yang Bisa Dialami Anak Laki-Laki" style="object-fit: cover; height: 100px; width: 100px;"></a></div><div class="post-content"><a class="post-title" href="mengupas-hipospadia-kelainan-bawaan-yang-bisa-dialami-anak-laki-laki-1336362" title="Mengupas Hipospadia, Kelainan Bawaan yang Bisa Dialami Anak Laki-Laki">Mengupas Hipospadia, Kelainan Bawaan yang Bisa Dialami Anak Laki-Laki</a><div class="post-meta d-flex align-items-center"><a href="#">Bugar</a><a href="#" style="padding-left:7px;"></a></div></div></div>

            </ul>
            </div>
        </div>

        <!-- Terpopuler Wrapper-->
        <div class="mt-5">
            @include('includes.widget-popular')
        </div>

        <!-- Ads Mobile Single 0 -->
        @include('includes.ads.article-single-0')

        <div class="mt-1 mb-5">
            @include('includes.widget-solopos-stories')
        </div>

        <!-- Ads Mobile Single 1 -->
        @include('includes.ads.article-single-1')

        <div class="mt-1">
            @include('includes.widget-slides')
        </div>

        <div class="terkini-wrapper">
            <div class="container">
            <div class="d-flex align-items-center justify-content-between mb-3">
                <h5 class="mb-0 pl-1 spos-title">Berita Lainnya</h5><a class="btn btn-primary btn-sm" href="https://www.harianjogja.com/index" target="_blank">Indeks Berita</a>
            </div>
            </div>
            <div class="container d-flex">
            <ol id="rekomendasi" class="harjo-widget">

            </ol>
            </div>
        </div>

        <!-- widget promosi -->
        @include('includes.widget-promosi')
        <!-- Ads Mobile Single 2 -->
        @include('includes.ads.article-single-2')

        <div class="mt-3 mb-3">
            @include('includes.widget')
        </div>

        <!-- Facebook Komentar -->
        {{-- <div class="container">
            <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Komentar Anda</h5>
            </div>

            <div id="fb-root"></div>
            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v11.0&appId=1438927622798153&autoLogAppEvents=1" nonce="uCAeYQ2X"></script>
            <div class="fb-comments" data-href="https://m.solopos.com" data-width="320" data-numposts="5"></div>
        </div> --}}
        <!-- Konten Premium Wrapper -->
        <div class="container">
            <div class="editorial-choice-news-wrapper mt-3">
            <div class="bg-shape1"><img loading="lazy" src="{{ asset('images/edito.png') }}"></div>
            <div class="bg-shape2" style="background-image: url('{{ asset('images/edito2.png') }}')"></div>
            <div class="container" align="center">
                <img loading="lazy" src="{{ url('images/plus-putih.png') }}" height="50%" width="50%" style="margin-bottom: 30px;">
            </div>
            <div class="container">
                <div class="editorial-choice-news-slide owl-carousel">
                @php $pc_loop = 1; @endphp
                @foreach($premium as $bp)
                    @if($pc_loop <=6)

                <div class="single-editorial-slide d-flex">
                    <a class="bookmark-post" href="#"><i class="lni lni-star"></i></a>
                    <div class="post-thumbnail">
                    <img loading="lazy" src="{{ $bp['images']['thumbnail'] }}" alt="{{ json_encode($bp['images']['caption']) }}" style="object-fit: cover; height: 177px; width: 132px;">
                    </div>
                    <div class="post-content">
                    <!--<a class="post-catagory" href="https://m.solopos.com/premium">Premium {{ ucwords($bp['category']) }}</a>-->
                                        <span class="espos-plus">+ PLUS</span>
                    <a class="post-title d-block" href="{{ url("/{$bp['slug']}-{$bp['id']}") }}" title="{{ html_entity_decode($bp['title']) }}" >{{ html_entity_decode($bp['title']) }}</a>
                    </div>
                </div>
                @endif
                @php $pc_loop++; @endphp
                @endforeach
                </div>
            </div>
            </div>
        </div>

        <!-- Ads Mobile Single 3 -->
        @include('includes.ads.article-single-3')

        <!-- Terkini Wrapper-->

        <div class="terkini-wrapper"> <!--loadmore-frame-->
            <div class="container">
            <div class="d-flex align-items-center justify-content-between mb-3">
                <h5 class="mb-0 pl-1 spos-title">Berita Terkini</h5><a class="btn btn-primary btn-sm" href="{{ url('/') }}/arsip">Indeks Berita</a>
            </div>
            </div>
            <div class="container">
            @php $loop_br = 1; @endphp
            @foreach ($breakingcat as $item)
            @if( $loop_br <= 20 )
                <!-- Terkini Post-->
                <div class="terkini-post"> <!-- content-box -->
                <div class="d-flex">
                    <div class="post-thumbnail">
                    <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">
                        <img loading="lazy" src="{{ $item['images']['thumbnail'] }}" alt="{{ html_entity_decode($item['title']) }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                    </a>
                    </div>
                    <div class="post-content">
                    {{-- @if($item['konten_premium'] == 'premium')
                        <span class="espos-plus">+ PLUS</span>
                    @endif --}}
                    <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">{{ html_entity_decode($item['title']) }}</a>
                    <div class="post-meta d-flex align-items-center">
                        <a href="">{{ $item['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                    </div>
                    </div>
                </div>
                </div>
                @if( $loop_br == 5 )
                @include('includes.ads.article-single-terkini-1')
                @elseif ($loop_br == 10)
                @include('includes.ads.article-single-terkini-2')
                @elseif ($loop_br == 15)
                @include('includes.ads.article-single-terkini-3')
                @endif
                @endif

                @php $loop_br++; @endphp
                @endforeach
                <div class="text-center mt-3">
                <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more" title="Kumpulan Berita">
                    Arsip Berita
                </a>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="border-top"></div>
        </div>
        </div>

        @push('custom-scripts')
        <script>

        $(document).ready(function() {
            $.ajax({ //create an ajax request related post
            type: "GET",
            url: "https://api.solopos.com/api/wp/v2/posts?tags={{ $relatedTags }}&exclude={{ $content['id'] }}&per_page=4", //72325
            dataType: "JSON",
            success: function(data) {
                // console.log(data);
                var relatedPosts = $("#relposts");

                $.each(data, function(i, item) {
                    relatedPosts.append("<div class=\"terkini-post d-flex\"><div class=\"post-thumbnail\"><a href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"\"><img src=\"" + data[i]['one_call']['featured_list']['media_details']['sizes']['thumbnail']['source_url'] +"\" loading=\"lazy\" alt=\"" + data[i]['title']['rendered'] + "\" style=\"object-fit: cover; height: 100px; width: 100px;\"></a></div><div class=\"post-content\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"" + data[i]['title']['rendered'] + "\">" + data[i]['title']['rendered'] + "</a><div class=\"post-meta d-flex align-items-center\"><a href=\"#\">" + data[i]['one_call']['categories_list'][0]['name'] + "</a><a href=\"#\" style=\"padding-left:7px;\"></a></div></div></div>");
                });
                    }
                });
            });

        $(document).ready(function () {
                $.ajax({
                    url: 'https://www.harianjogja.com/rss',
                    type: 'GET',
                    dataType: "xml"
                }).done(function(xml) {

                    $.each($("item", xml), function(i, e) {

                        var postNumber = i + 1 + ". ";

                        var itemURL = ($(e).find("link"));
                        var postURL = "<a href='" + itemURL.text() + "'>" + itemURL.text() +"</a>";
                        var itemTitle = ($(e).find("title"));
                        var postTitle = "<a href='" + itemURL.text() + "'>" + itemTitle.text() + "</a>";

                        if(postNumber <= 5) {

                        $("#rekomendasi").append("<li><a href=\"" + itemURL.text() + "\" title=\""+ itemTitle.text() +"\" target=\"_blank\">" + itemTitle.text() +"</a></li>");
                        }
                    });
                });
            });

            document.oncopy = function () {
            var _title = $(".title-only").text(),
                _editor = $(".editor-only").text(),
                _author = $(".penulis").text(),
                _publish = $(".tgl-terbit").text(),
                _href = document.location.href,
                    _result,
                    _body = document.getElementsByTagName("body")[0];
                    _text =
                        (nl2br(_result = window.getSelection())) +
                        '<br /><br /><br /> Baca artikel <b>"' + _title.trim() +
                        '"</b> selengkapnya di sini: <a href="' +
                        _href +
                        '">' +
                        _href +
                        '</a>.'+
                        '<br />Editor  : '+_editor+'<br />Penulis: '+_author+'<br />Publish: '+_publish+
                        '<br/><br/> Mau Mobil SUV Idaman hanya dengan Rp.328/hari? <a href="https://id.solopos.com/register">Langganan Espos Plus Sekarang</a><br/>Silakan berlangganan dan dapatkan berbagai konten menarik di Espos Plus.<br/><br/>Solopos.com - Panduan Insformasi & Inspirasi';

            if (window.clipboardData && window.clipboardData.setData) {
                // IE specific code path to prevent textarea being shown while dialog is visible.
                return clipboardData.setData(p);

            } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {

                var ss = document.createElement("div");

                    (ss.style.position = "absolute"),
                    (ss.style.left = "-99999px"),
                    _body.appendChild(ss),
                    (ss.innerHTML = _text),
                    _result.selectAllChildren(ss);
                    window.setTimeout(function () {
                        _body.removeChild(ss);
                    }, 0);
            }
            };


            function nl2br (str, is_xhtml) {
            if (typeof str === 'undefined' || str === null) {
            return '';
            }
            var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
            }

        </script>
        @endpush
        @push('tracking-scripts')
            <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
            url:'https://tf.solopos.com/api/v1/stats/store',
            method:'POST',
            data:{
                    post_id:'{{ $content['id'] }}',
                    post_title:'{{ $header['title'] }}',
                    post_slug:'{{ $content['slug'] }}',
                    post_date:'{{ $content['date'] }}',
                    post_author:'{{ $header['author'] }}',
                    post_editor:'{{ $header['editor'] }}',
                    post_category:'{{ $header['category_parent'] }}',
                    post_subcategory:'{{ $header['category_child'] }}',
                    post_tag:'{!! serialize($content['tag']) !!}',
                    post_thumb:'{{ $header['image'] }}',
                    post_view_date:'{{ date('Y-m-d') }}',
                    domain:'{{ 'm.solopos.com' }}'
                },
            success:function(response){
                if(response.success){
                    console.log(response.message)
                }else{
                    console.log(error)
                }
            },
            error:function(error){
                console.log(error)
            }
            });
            </script>
        @endpush
    @endsection
