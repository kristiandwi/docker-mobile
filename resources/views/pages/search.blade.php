@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
    <div class="page-content-wrapper">
    <div class="container">
        <nav aria-label="breadcrumb" style="text-align: center;">
          <ol class="breadcrumb" style="text-transform: capitalize;font-size:13px;font-weight:600;">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item">Search</li>
            <li class="breadcrumb-item active" aria-current="page">{{ucwords($s)}}</li>
          </ol>
        </nav>
      </div>
      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <!-- <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Indek Berita Solopos.com</h5>
          </div>
        </div> -->
        <div class="container mb-5">
            <div class="search-post-wrapper">
                <div class="container">
                  <div class="search-page-form">
                    <!-- Search via Voice-->
                    <a class="search-via-voice" href="#"><i class="lni lni-mic"></i></a>
                    <form action="{{ route('search') }}" method="post">
                        {{ csrf_field() }}
                        <input type="search" class="form-control" name="s" placeholder="Search" value="">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                  </div>
                </div>
            </div> 
        </div>  

        <div class="container">
          @php $b_loop = 1; @endphp
          @foreach ($breaking as $item)
            @php           
            $image = $item['thumbnail'] ?? 'https://m.solopos.com/images/no-thumb.jpg'; 
            $title = html_entity_decode($item['title']);
            @endphp          
            @if ($b_loop == 1)
            <div class="card mb-3 content-box">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                <img loading="lazy" class="card-img-top" src="{{ $image }}" alt="{{ $title }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                </a>
                <div class="card-body">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                </div>
            </div>

            <div class="mt-3 mb-3" align="center">
              <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
              <!-- Iklan Responsif -->
              <ins class="adsbygoogle"
                  style="display:block"
                  data-ad-client="ca-pub-4969077794908710"
                  data-ad-slot="2921244965"
                  data-ad-format="auto"
                  data-full-width-responsive="true"></ins>
              <script>
                  (adsbygoogle = window.adsbygoogle || []).push({});
              </script>  
            </div>

            @else          
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                    <img loading="lazy" src="{{ $image }}" alt="" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="https://m.solopos.com/">@if($item['catparent'] ==''){{ $item['catsname'] }} @else {{ $item['catparent'] ?? 'News' }} @endif</a>|<a href="#" style="padding-left:7px;">{{ Carbon\Carbon::parse($item['date'])->translatedFormat('j M Y - H:i') }}</a>
                  </div>
                </div>
              </div>
            </div>
            @endif
            @php $b_loop++ @endphp
            @endforeach
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Berita Lainnya
              </a>
              <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>                               
        </div>
      </div>

      <!-- Ads Breaking -->
      <div class="iklan mt-3">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-ad-format="auto"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>  
      </div>

      <!-- widget Berit Video -->
      @include('includes.widget-video')      
   
      <div class="container">
        <div class="border-top"></div>
      </div>
    <style>
        select,input{display:block;width:100%;border:1px solid #cecece;padding:3px;}
    </style>
    </div>
@endsection