@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
    <div class="page-content-wrapper">
      @include('includes.ads.top-swipe')
      <div class="container">
        <nav aria-label="breadcrumb" style="text-align: center;">
          <ol class="breadcrumb" style="text-transform: capitalize;font-size:13px;font-weight:600;">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/cekfakta')}}">Cek Fakta</a></li>
          </ol>
        </nav>
      </div>
      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame" style="padding-top:0;">
        <div class="container">
          @php $b_loop = 1; @endphp
          @foreach ($cekfakta as $item)
            @php           
            $thumb = $item['featured_image']['thumbnail'] ?? 'https://m.solopos.com/images/no-thumb.jpg'; 
            $medium = $item['featured_image']['medium'] ?? 'https://m.solopos.com/images/no-medium.jpg';
            $title = html_entity_decode($item['title']);
            @endphp          
            @if ($b_loop == 1)
            <div class="card mb-3 content-box">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                <img class="card-img-top" src="{{ $medium }}" alt="{{ $title }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                </a>
                <div class="card-body">
                  @if($item['is_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								  @endif
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                </div>
            </div>

            <!--div class="mt-3 mb-3" align="center"-->
              <!-- /54058497/soloposmobile/mobile-home-1 -->
              <!--div id='div-gpt-ad-1625738305324-0' style='min-width: 300px; min-height: 50px;'>
                <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1625738305324-0'); });
                </script>
              </div>
            </div-->
            <div class="iklan">
              <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
              <!-- Iklan Responsif -->
              <ins class="adsbygoogle"
                style="display:block"
                data-ad-client="ca-pub-2449643854165381"
                data-ad-slot="3798913759"
                data-ad-format="auto"
                data-full-width-responsive="true"></ins>
              <script>
              (adsbygoogle = window.adsbygoogle || []).push({});
              </script>
            </div>

            @else          
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                    <img src="{{ $thumb }}" alt="{{ $title }}" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  @if($item['is_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								  @endif
                  <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="#">{{ $item['catsname'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                  </div>
                </div>
              </div>
            </div>
            @endif
            @php $b_loop++ @endphp
            @endforeach
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Berita Lainnya
              </a>
              <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>                               
        </div>
      </div>

      <div class="mt-3 mb-3" align="center">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-ad-format="auto"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
  	  </div>   
    
      <div class="container">
        <div class="border-top"></div>
      </div>

    </div>
@endsection