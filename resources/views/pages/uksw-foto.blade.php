@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper">
      <br>
      <div class="mb-3" align="center">
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
          window.googletag = window.googletag || {cmd: []};
          googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/UKSW-MR', [300, 250], 'div-gpt-ad-1634121385191-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
          });
        </script>
        <!-- /54058497/UKSW-MR -->
        <div id='div-gpt-ad-1634121385191-0' style='min-width: 300px; min-height: 250px;'>
          <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1634121385191-0'); });
          </script>
        </div>
  	  </div>

      <!-- Galeri Foto -->
      <div class="video-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between">
            <h5 class="mb-0 pl-1 spos-title">GALERI FOTO</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/uksw/foto">Indeks Foto</a>
          </div>
        </div>
        <div class="container">
            @php $f_loop = 1; @endphp
            @foreach($foto as $item)

            <div class="card mb-3">
              <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
              <img class="card-img-top" src="{{ $item['images']['thumbnail'] }}" alt="{{ $item['title'] }}">
              </a>
              <div class="card-body">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}"  title="{{ $item['title'] }}"> {{ $item['title'] }}</a>
              </div>
            </div> 

            @php $f_loop++ @endphp
            @endforeach              
          </div>
        </div>
      </div>

      <div class="container">
        <div class="border-top"></div>
      </div>

      <style type="text/css">
      .spos-v-card .post-thumbnail::after {opacity: 0 !important; }
      .blog-description a {color: #b7212c !important; }
      .btn-primary, #uksw .bg-primary, #uksw .badge-primary {background: #ffc107 !important; border-color: #ffc107; color:#00437c;/*linear-gradient(to right, #00437c, #b7212c) !important;*/}
      .spos-title {color:#00437c; border-bottom: 1px solid #b7212c;}
      .single-hero-slide::after {background:#ffc107;/*background: linear-gradient(to right, #00437c, #b7212c);*/}
      .single-hero-slide .post-title {color: #00437c; line-height: 25px;}
      .single-hero-slide .post-meta a {color:#b7212c; }
      .single-hero-slide::after {background:#ffc107;/*background: linear-gradient(to right, #00437c, #b7212c);*/}
      </style>

    </div>
@endsection