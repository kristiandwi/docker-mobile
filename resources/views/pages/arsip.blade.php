@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
    <div class="page-content-wrapper">
    {{-- @include('includes.ads.top-swipe') --}}
    <div class="container">
        <nav aria-label="breadcrumb" style="text-align: center;">
          <ol class="breadcrumb" style="text-transform: capitalize;font-size:13px;font-weight:600;">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Index Berita</li>
          </ol>
        </nav>
      </div>
      <style>
        select,input{display:block;width:100%;border:1px solid #cecece;padding:3px;}
        .hideMe {
          -moz-animation: cssAnimation 0s ease-in 12s forwards;
          /* Firefox */
          -webkit-animation: cssAnimation 0s ease-in 12s forwards;
          /* Safari and Chrome */
          -o-animation: cssAnimation 0s ease-in 12s forwards;
          /* Opera */
          animation: cssAnimation 0s ease-in 12s forwards;
          -webkit-animation-fill-mode: forwards;
          animation-fill-mode: forwards;
        }          
    </style>      
      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <!-- <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Indek Berita Solopos.com</h5>
          </div>
        </div> -->
        <div class="container">
            <div class="row col-sm-12" align="center">
                <form class="form-inline" action="{{ route('arsip') }}" method="post" name="sortindeks"> {{ csrf_field() }}
                    <div class="form-group mb-3">{!! Form::select('tgl', array('01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31'), $tgl ); !!}</div>
                    <div class="form-group mb-3">{!! Form::select('bln', array('1' => 'Januari', '2' => 'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember'), $bln); !!}</div>
                    <div class="form-group mb-3">{!! Form::selectRange('thn', 2009, 2022, $thn); !!}</div>
                    <!-- {!! Form::select('cat', array('664067' => 'News', '94686' => 'Soloraya','669739'=>'Ekonomi Bisnis','83'=>'Sport','8467'=>'Lifestyle','152076'=>'Entertainment','76212'=>'Otomotif','15783'=>'Teknologi','636'=>'Jawa Tengah','4175'=>'Jawa Timur','47665'=>'Jogjakarta','74227'=>'Video','54'=>'Foto','670832'=>'Cek Fakta','734344'=>'Writing Contest','669740'=>'Fiksi','18499'=>'Jagad Jawa','15420'=>'Kolom','11091'=>'Lowongan Kerja','655246'=>'Espospedia'), '2021'); !!}                                       -->
                    <div class="form-group mb-2"><input name="submit" type="submit" id="submit" value="GO" class="btn btn-primary btn-sm" style="padding: 7px;margin-left: 7px;margin-bottom: 7px;"/></div>
                </form>
            </div>
            
        </div>  

        <div class="container">
          {{-- @php $b_loop = 1; @endphp
          @foreach ($breaking as $item)
            @php           
            $image = $item['featured_image']['thumbnail'] ?? 'https://m.solopos.com/images/no-thumb.jpg'; 
            $title = html_entity_decode($item['title']);
            @endphp          
            @if ($b_loop == 1)
            <div class="card mb-3 content-box">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                <img class="card-img-top" src="{{ $image }}" alt="{{ $title }}" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                </a>
                <div class="card-body">
                  @if($item['is_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								  @endif
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                </div>
            </div>

            <div class="mt-3 mb-3" align="center">
              <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
              <!-- Iklan Responsif -->
              <ins class="adsbygoogle"
                  style="display:block"
                  data-ad-client="ca-pub-4969077794908710"
                  data-ad-slot="2921244965"
                  data-ad-format="auto"
                  data-full-width-responsive="true"></ins>
              <script>
                  (adsbygoogle = window.adsbygoogle || []).push({});
              </script>  
            </div>

            @else          
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">
                    <img src="{{ $image }}" alt="" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  @if($item['is_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								  @endif
                  <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $title }}">{{ $title }}</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="https://m.solopos.com/">@if($item['catparent'] ==''){{ $item['catsname'] }} @else {{ $item['catparent'] ?? 'News' }} @endif</a>|<a href="#" style="padding-left:7px;">{{ Carbon\Carbon::parse($item['date'])->translatedFormat('j M Y - H:i') }}</a>
                  </div>
                </div>
              </div>
            </div>
            @endif
            @php $b_loop++ @endphp
            @endforeach --}}

            <div id="arsipPost"></div>
            <p class="hideMe" style="font-size:12px">Mohon tunggu beberapa saat, kami memerlukan waktu untuk menampilkan semua data.</p>
            <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Berita Lainnya
              </a>
              <a href="https://m.solopos.com/arsip" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Berita
              </a>
            </div>                               
        </div>
      </div>

      <!-- Ads Breaking -->
      <div class="iklan mt-3">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-ad-format="auto"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>  
      </div>

      <!-- widget Berit Video -->
      @include('includes.widget-video')      
   
      <div class="container">
        <div class="border-top"></div>
      </div>
    @push('custom-scripts')
    <script>
      function timeSince(date) {
          var seconds = Math.floor((new Date() - date) / 1000);

          var interval = seconds / 31536000;

          if (interval > 1) {
          return Math.floor(interval) + " tahun lalu";
          }
          interval = seconds / 2592000;
          if (interval > 1) {
          return Math.floor(interval) + " bulan lalu";
          }
          interval = seconds / 86400;
          if (interval > 1) {
          return Math.floor(interval) + " hari lalu";
          }
          interval = seconds / 3600;
          if (interval > 1) {
          return Math.floor(interval) + " jam lalu";
          }
          interval = seconds / 60;
          if (interval > 1) {
          return Math.floor(interval) + " menit lalu";
          }
          return Math.floor(seconds) + " detik lalu";
      }      
      $(document).ready(function() {
          $.ajax({ //create an ajax request related post
          type: "GET",
          url: "https://api.solopos.com/api/breaking/arsip/posts?year={{ $thn }}&month={{ $bln }}&day={{ $tgl }}", 
          dataType: "JSON",
          success: function(data) {
              var arsipPosts = $("#arsipPost");

              $.each(data, function(i, item) {
                var str = data[i]['is_premium'];
                if (str == 'premium'){
                  plus =  '<!--<span class="espos-plus">+ PLUS</span>-->'
                } else {
                  plus = ''
                }                    
                arsipPosts.append("<div class=\"terkini-post content-box d-flex\"><div class=\"post-thumbnail\"><a href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"" + data[i]['title'] + "\"><img src=\"" + data[i]['featured_image']['thumbnail'] +"\" loading=\"lazy\" alt=\"" + data[i]['title'] + "\" style=\"object-fit: cover; height: 100px; width: 100px;\" onerror=\"javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'\"></a></div><div class=\"post-content\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"" + data[i]['title'] + "\">" + plus + "  " + data[i]['title'] + "</a><div class=\"post-meta d-flex align-items-center\"><a href=\"#\" style=\"padding-right:5px;\">" + data[i]['catsname'] + "</a>-<a href=\"#\" style=\"padding-left:5px;\">" + timeSince(new Date(data[i]['date'])) + "</a></div></div></div>");
              });
				  }
			  });
		  });
    </script>
    @endpush
    </div>
@endsection