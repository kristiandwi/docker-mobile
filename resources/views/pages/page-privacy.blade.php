@extends('layouts.app')
@section('content')
      <!-- Share Modal-->
      <div class="modal fade post-share-modal" id="postShareModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button class="close close" type="button" data-dismiss="modal" aria-label="Close"><i class="lni lni-close"></i></button>
              <!-- Heading-->
              <h6 class="mb-3 pl-2">Share this post</h6>
              <div class="social-share-btn d-flex align-items-center flex-wrap"><a class="btn-facebook" href="#"><i class="lni lni-facebook"></i></a><a class="btn-twitter" href="#"><i class="lni lni-twitter-original"></i></a><a class="btn-instagram" href="#"><i class="lni lni-instagram"></i></a><a class="btn-whatsapp" href="#"><i class="lni lni-whatsapp"></i></a><a class="btn-linkedin" href="#"><i class="lni lni-linkedin"></i></a><a class="btn-tumblr" href="#"><i class="lni lni-tumblr"></i></a><a class="btn-quora" href="#"><i class="lni lni-quora"></i></a></div>
            </div>
          </div>
        </div>
      </div>      

    <div class="page-content-wrapper">
      <!-- Scroll Indicator-->
      <div id="scrollIndicator"></div>

      <!-- Single Blog Thumbnail-->
      <div class="single-blog-thumbnail">
        <img loading="lazy" class="w-100 single-blog-image" src="{{ asset('images/solopos.jpg') }}" alt="Solopos Digital Media">
      </div>

      <!-- Single Blog Info-->
      <div class="single-blog-info">
          <div class="d-flex align-items-center">
            <!-- Post Content Wrap-->
            <div class="post-content-wrap">
              <h1 class="mt-2 mb-3">Privacy Policy</h1>
            </div>
          </div>
      </div>     
    <!-- Blog Description-->
    <div class="blog-description">
        <div class="container">
            <p>Seluruh konten di <strong>Solopos.com, Solopos.com, Madiunpos.com</strong> dan <strong>Semarangpos.com</strong> baik berupa artikel, berita, teks, foto dan video dilindungi oleh UU Hak Cipta. Informasi yang disajikan Solopos.com Group bisa dimanfaatkan individu untuk referensi dan bersifat nonkomersial.</p>
            <p>Pengguna tidak diperkenankan menyalin, memproduksi ulang, mempublikasikan secara utuh maupun sebagian materi di Solopos.com Group dengan cara apapun dan atau melalui media apapun, kecuali untuk kepentingan pribadi dan tidak bersifat komersial.</p>
            <p>Penggunaan di luar kepentingan tersebut harus mendapat persetujuan tertulis dan kerja sama dengan <strong>Solopos.com Group.</strong></p>
            <p><strong>Solopos.com Group</strong> menjalin kerja sama dengan beberapa sindikasi berita dan kantor berita, sehingga ada beberapa materi berupa berita, foto, maupun video bersumber dari situs-situs sindikasi dan kantor berita.</p>
            <h1 class="entry_title">Kebijakan Privasi</h1>
            <p>Solopos.com memperlakukan privasi Anda dengan sungguh-sungguh. Bacalah uraian berikut untuk mengetahui lebih jauh mengenai kebijakan privasi kami.</p>
            <p><strong>Yang Dicakup oleh Kebijakan Privasi ini</strong></p>
            <ul>
            <li>Kebijakan Privasi ini mencakup perlakuan kami atas informasi yang dapat mengidentifikasi diri (data pribadi) Anda saat sedang berada di situs bisnis.com, dan saat Anda menggunakan layanan kami. Kebijakan ini juga mencakup perlakukan kami atas data pribadi yang digunakan bersama oleh kami.</li>
            <li>Kebijakan ini tidak berlaku pada praktik perusahaan yang bukan grup unit usaha milik kami atau di luar kendali kami atau pada orang-orang yang bukan karyawan kami atau di luar pengelolaan kami.</li>
            </ul>
            <p><strong>Pengumpulan dan Penggunaan Informasi</strong></p>
            <ul>
            <li>Saat Anda mendaftar pada kami, kami menanyakan nama, alamat email, alamat tempat tinggal, tanggal lahir, kode pos, dan nomor telepon. Setelah Anda mendaftar dan masuk ke layanan kami, Anda bukanlah orang yang anonim bagi kami.</li>
            <li>Kami menggunakan informasi untuk memenuhi permintaan Anda akan produk dan layanan tertentu, dan untuk menghubungi Anda mengenai tawaran khusus dan produk baru.</li>
            </ul>
            <p><strong>Perubahan terhadap Kebijakan Privasi ini</strong></p>
            <ul>
            <li>Kami dapat mengubah kebijakan ini dari waktu ke waktu. Jika kami melakukan perubahan besar dalam cara penggunaan data pribadi Anda, kami akan memberi tahu Anda dengan memasang pengumuman yang tampak jelas di halaman kami. Anda setuju bahwa pemasangan pengumuman itu sudah cukup dan memadai untuk memberi tahu Anda mengenai perubahan tersebut.</li>
            <li>Anda setuju bahwa dengan terus menggunakan produk atau layanan kami setelah dilakukannya perubahan tersebut, Anda menerima dan setuju untuk terikat dengan ketentuan dalam kebijakan yang baru.</li>
            </ul>
            <p><strong>Pengungkapan informasi anda</strong></p>
            <p>Kami dapat mengungkapkan informasi pribadi anda kepada pihak ketiga:</p>
            <ul>
            <li>Apabila kami menjual atau membeli suatu bisnis atau aset, dimana, dalam hal ini, kami dapat mengungkapkan data pribadi anda kepada calon penjual atau pembeli dari bisnis atau aset tersebut.</li>
            <li>Apabila kami berkewajiban mengungkapkan atau berbagi data pribadi anda dalam upaya mematuhi kewajiban hukum atau dalam upaya memberlakukan atau menerapkan syarat-syarat penggunaan kami dan perjanjian-perjanjian lain; atau untuk melindungi hak, properti, atau keselamatan bisnis.com, pelanggan kami, atau pihak lain. Di sini termasuk pertukaran informasi dengan perusahaan dan organisasi lain untuk tujuan perlindungan terhadap penipuan dan pengurangan resiko kredit.</li>
            </ul>
            <p><strong>Penggunaan informasi</strong></p>
            <p>Kami menggunakan informasi yang dimiliki mengenai anda dengan cara-cara sebagai berikut:</p>
            <ul>
            <li>Untuk memastikan bahwa isi dari situs kami disajikan dengan cara yang paling efektif untuk anda dan untuk komputer anda.</li>
            <li>Untuk memberikan kepada anda informasi, produk atau layanan yang anda minta dari kami atau yang kami rasa mungkin penting bagi anda, dimana anda setuju untuk dihubungi untuk tujuan tersebut.</li>
            <li>Untuk melaksanakan kewajiban kami yang timbul dari suatu kontrak yang diadakan antara anda dan kami.</li>
            <li>Untuk memungkinkan anda ambil bagian dalam fitur interaktif layanan kami, sewaktu anda memilih demikian.</li>
            <li>Untuk memberitahu anda mengenai perubahan terhadap layanan kami.</li>
            </ul>
            <p><strong>Hak anda</strong></p>
            <p>Anda berhak meminta kami untuk tidak memproses data pribadi anda untuk tujuan pemasaran. Kami biasanya akan memberitahu anda (sebelum mengumpulkan data anda) apabila kami bermaksud menggunakan data anda untuk tujuan tersebut atau apabila kami bermaksud mengungkapkan informasi anda kepada pihak ketiga untuk tujuan tersebut. Anda dapat menggunakan hak anda untuk mencegah pemrosesan tersebut dengan menghubungi kami di <strong>digital@solopos.com</strong>.</p>
            <p>Situs kami, dari waktu ke waktu, dapat berisi link ke dan/atau dari situs web jaringan mitra, pengiklan dan afiliasi kami. Apabila anda mengikuti link ke salah satu dari situs-situs web ini, perlu diperhatikan bahwa situs-situs web ini memiliki kebijakan hak-pribadinya sendiri dan kami tidak memiliki tanggung jawab atau menanggung kewajiban atas kebijakan tersebut. Silakan periksa kebijakan ini sebelum anda menyerahkan data pribadi ke situs-situs web ini.</p>
            <p><strong>Perubahan atas kebijakan hak-pribadi kami</strong></p>
            <p>Perubahan yang dapat kami lakukan atas kebijakan hak-pribadi kami di masa mendatang akan ditempatkan di halaman ini dan, bilamana dipandang tepat, akan diberitahukan kepada anda melalui e-mail.</p>
            <p><strong>Pihak yang dapat dihubungi</strong></p>
            <p>Pertanyaan, komentar dan permintaan mengenai kebijakan hak-pribadi ini akan disambut baik dan harus dialamatkan ke <strong>digital@solopos.com</strong>.</p>
 
        </div>
    </div>                    
      <!-- Post Author-->
      <div class="profile-content-wrapper">
        <!-- Settings Option-->
        <div class="profile-settings-option"><a class="post-share" href="#" data-toggle="modal" data-target="#postShareModal"><i class="fa fa-share-alt"></i></a></div>
        <div class="container">
          <!-- User Meta Data-->
          <div class="user-meta-data d-flex">
            <!-- User Thumbnail-->
            <div class="user-thumbnail">
                  <img loading="lazy" src="{{ asset('images/icon.png') }}" alt="editor">
            </div>
            <!-- User Content-->
            <div class="user-content">
              <h6><a href="{{ url('/') }}" title="">Redaksi Solopos</a></h6>
            </div>
          </div>
        </div>
      </div>
@endsection