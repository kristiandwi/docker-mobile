@extends('layouts.app')
@section('content')
<div class="page-content-wrapper">
	<!-- Blok 1 Terkini Wrapper -->
    <div class="video-wrapper">
        <div class="mb-3" align="center">
            <a target="_blank" href="https://bob.kemenparekraf.go.id/" target="_blank">
            <img src="{{ asset('banner/bob-banner-mr.gif') }}">
            </a>
        </div>
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">GALERI VIDEO</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/wisata-joglosemar/video">Galeri Video</a>
          </div>
        </div>        
        <div class="container">
            @foreach($video as $item)
            @foreach(explode('yt:video:',$item['id']) as $ids)@endforeach            
            <div class="spos-v-card mb-5">
                <a class="video-icon" href="https://www.youtube.com/watch?v={{ $ids }}?utm_source=Solopos.com" title="{{$item['title']}}" target="_blank" style="background: #fb072e; width: 70px; border-radius: 17%;opacity: 0.8;">
                <i class="lni lni-play" style="background-color: #fb072e;width: 70px; border-radius: 17%;opacity: 0.8;"></i>
                </a>            
                <img class="card-img-top" src="https://i1.ytimg.com/vi/{{ $ids }}/mqdefault.jpg" alt="Thumbnail" style="object-fit: cover;" >

                <div class="post-content text-center" style="padding: 7px; background: linear-gradient(to bottom, rgba(255,0,0,0), #0c153b)">
                <h5 align="center">
                    <a class="post-title" href="https://www.youtube.com/embed/{{ $ids }}?rel=0&vq=hd1080&autoplay=1&amp;loop=1" title="{{$item['title']}}" target="_blank">{{$item['title']}}</a></h5>
                </div>
            </div>
            @endforeach
            <div class="text-center mt-3">
                <a class="btn btn-info btn-lg" href="https://www.youtube.com/channel/UCfdq2dv6zodZqqWpvc3U-MA" target="_blank">
                Video Lainnya...
                </a>
            </div>
          
        </div>
    </div>
</div>
@endsection