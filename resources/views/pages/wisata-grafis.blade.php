@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper">

      <div class="mb-3" align="center">
        <a target="_blank" href="https://bob.kemenparekraf.go.id/" target="_blank">
          <img src="{{ asset('banner/bob-banner-mr.gif') }}">
        </a>
  	  </div>

      <!-- Info Grfis Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">INFO GRAFIS</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/wisata-joglosemar/grafis">Indeks Grafis</a>
          </div>
        </div>
        <div class="container">  
          @php $g_loop = 1; @endphp
          @foreach($grafis as $item)                  
          <div class="card mb-3 content-box">
            <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
            <img class="card-img-top" src="{{ $item['images']['thumbnail'] }}" alt="{{ $item['title'] }}">
            </a>
            <div class="card-body">
            <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
            </div>
          </div>
          @php $g_loop++ @endphp
          @endforeach    

          <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Grafis">
                Info Grafis Lainnya...
              </a>
              <a href="https://m.solopos.com/wisata-joglosemar/foto" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Grafis">
                Arsip Info Grafis
              </a>
            </div>

        </div>
      </div>

      <div class="mt-3 mb-3" align="center">
        <a target="_blank" href="https://instagram.com/boborobudur" target="_blank">
          <img src="{{ asset('banner/bob-banner-mr.gif') }}">
        </a>
      </div>

      <div class="container">
        <div class="border-top"></div>
      </div>

    </div>
@endsection