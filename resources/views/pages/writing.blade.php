@extends('layouts.app')
@section('content')
@include('includes.ads.popup-banner')
    <div class="page-content-wrapper">
      @include('includes.ads.top-swipe')
      <!-- Headline Wrapper-->
      <div class="headline-wrapper">
        <div class="container">
          <!-- Hero Slides-->
          <div class="slides-wrapper">
            <div class="hero-slides owl-carousel">           
              @php $hl_loop = 1; @endphp
              @foreach($headline as $hl)
              @if($hl_loop <=5)           
              <div class="single-hero-slide">
                <div class="headline-image d-flex align-items-start">
                  <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">
                  <img src="{{ $hl['featured_image']['medium'] }}" loading="lazy" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="d-flex align-items-end">
                  <div class="container-fluid mb-3">
                    <div class="post-meta d-flex align-items-center">
                      <a href="">{{ $hl['catsname'] }}</a> <!--| <span style="margin-left: 5px;">{{ Helper::time_ago($hl['date']) }}</span> -->
                    </div>
                    @if($hl['is_premium'] == 'premium') 
                    <span class="espos-plus">+ PLUS</span>
                    @endif                  
                    <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ html_entity_decode($hl['title']) }}">{{ html_entity_decode($hl['title']) }}</a>
                  </div>
                </div>
              </div>
              @endif
              @php $hl_loop++ @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>

      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Writing Contest</h5><a class="btn btn-primary btn-sm" href="{{ url('/writing-contest') }}">Indeks</a>
          </div>
        </div>
        <div class="container">
          @php $b_loop = 1; @endphp
          @foreach ($headline as $item)
          @if($b_loop >5 &&  $b_loop <=20)          
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">
                  <img src="{{ $item['featured_image']['thumbnail'] }}" alt="" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                @if($item['is_premium'] == 'premium') 
									<span class="espos-plus">+ PLUS</span>
								@endif
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}">{{ html_entity_decode($item['title']) }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $item['catsname'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            </div>
            @endif
            @php $b_loop++ @endphp
            @endforeach
  
        </div>
      </div>
      <div class="iklan">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script> 
      </div>      

      <div class="container">
        <div class="border-top"></div>
      </div>

    </div>
@endsection