@extends('layouts.app')
@section('content')
    <div class="page-content-wrapper">

      <!-- Headline Wrapper-->
      <div class="headline-wrapper">
        <div class="container">
          <!-- Hero Slides-->
          <div class="slides-wrapper">
            <div class="hero-slides owl-carousel">           
              @php $hl_loop = 1; @endphp
              @foreach($headline as $hl)
              @if($hl_loop <=5)           
              <div class="single-hero-slide">
                <div class="headline-image d-flex align-items-start">
                  <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ $hl['title'] }}">
                  <img src="{{ $hl['featured_image']['medium'] }}" loading="lazy" alt="{{ $hl['title'] }}">
                  </a>
                </div>
                <div class="d-flex align-items-end">
                  <div class="container-fluid mb-3">
                    <div class="post-meta d-flex align-items-center">
                      <a href="">{{ $hl['catsname'] }}</a> <!--| <span style="margin-left: 5px;">{{ Helper::time_ago($hl['date']) }}</span> -->
                    </div>                  
                    <a class="post-title d-block" href="{{ url("/{$hl['slug']}-{$hl['id']}") }}" title="{{ $hl['title'] }}">{{ $hl['title'] }}</a>
                  </div>
                </div>
              </div>
              @endif
              @php $hl_loop++ @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>

      <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.youtube.com/c/RSJIHSolo" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-1.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
          window.googletag = window.googletag || {cmd: []};
          googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-1', [[336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1640666517438-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
          });
        </script>
        <!-- /54058497/RSJIH-MOBILE-1 -->
        <div id='div-gpt-ad-1640666517438-0' style='min-width: 300px; min-height: 250px;'>
          <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666517438-0'); });
          </script>
        </div>
  	  </div>

      <!-- Terkini Wrapper -->
      <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Artikel Bugar</h5><a class="btn btn-primary btn-sm" href="{{ url('/rsjihsolo/artikel') }}">Indeks</a>
          </div>
        </div>
        <div class="container">
          @php $b_loop = 1; @endphp
          @foreach ($headline as $item)
          @if($b_loop <=3)          
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  <img loading="lazy" src="{{ $item['featured_image']['thumbnail'] }}" alt="{{ $item['title'] }}" style="object-fit: cover; height: 100px; width: 100px;">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $item['catsname'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            </div>
            @endif
            @php $b_loop++ @endphp
            @endforeach
            {{-- <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Artikel Lainnya
              </a>
              <a href="{{ url('/rsjihsolo/artikel') }}" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Artikel
              </a>
            </div>--}}
        </div>
      </div>

      <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.instagram.com/rs.jihsolo/" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-2.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
          window.googletag = window.googletag || {cmd: []};
          googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-2', [[336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1640666650907-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
          });
        </script>
        <!-- /54058497/RSJIH-MOBILE-2 -->
        <div id='div-gpt-ad-1640666650907-0' style='min-width: 300px; min-height: 250px;'>
          <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666650907-0'); });
          </script>
        </div>
      </div>

 <!-- Prestasi Wrapper -->
 <div class="terkini-wrapper loadmore-frame">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Infografis Bugar</h5><a class="btn btn-primary btn-sm" href="{{ url('/rsjihsolo/grafis') }}">Indeks</a>
          </div>
        </div>
        <div class="container">
            @php $p_loop = 1; @endphp
            @foreach ($grafis as $item)
            @if($p_loop <=3)  
            <!-- Terkini Post-->
            <div class="terkini-post content-box">
              <div class="d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  <img loading="lazy" src="{{ $item['featured_image']['medium'] }}" alt="{{ $item['title'] }}" style="object-fit: cover; height: 100px; width: 100px;">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
                <div class="post-meta d-flex align-items-center">
                  <a href="">{{ $item['catsname'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
            </div>
            @endif
            @php $p_loop++ @endphp
            @endforeach
            {{-- <div class="text-center mt-3">
              <a href="javascript:void(0)" class="btn btn-primary load-more" title="Kumpulan Berita">
                Cek Infografis Lainnya
              </a>
              <a href="{{ url('/rsjihsolo/grafis') }}" class="btn btn-primary load-more-arsip" style="display: none;" title="Kumpulan Berita">
                Arsip Infografis
              </a>
            </div>                                --}}
        </div>
      </div>

      <!-- Galeri Foto -->
      <div class="video-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between">
            <h5 class="mb-0 pl-1 spos-title">GALERI FOTO BUGAR</h5><a class="btn btn-primary btn-sm" href="{{ url('/rsjihsolo/foto') }}">Indeks Foto</a>
          </div>
        </div>
        <div class="container">
          <div class="row spos-video-slides owl-carousel">
            @php $f_loop = 1; @endphp
            @foreach($foto as $item)
            @if($f_loop <=3) 
            <div class="col-md-4 mt-3">
              <div class="spos-v-card">
                <a class="video-icon" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}"><i class="lni lni-image"></i></a>
                <div class="post-thumbnail">
                  <img loading="lazy" src="{{ $item['featured_image']['medium'] }}" alt="{{ $item['title'] }}" style=" height: 220px;">
                </div>
                <div class="post-content" align="center">
                  <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  {{ $item['title'] }}
                  </a>
                </div>
              </div>
            </div>
            @endif
            @php $f_loop++ @endphp
            @endforeach              
          </div>
        </div>
      </div>


      <!-- Galeri Foto -->
      <div class="video-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between">
            <h5 class="mb-0 pl-1 spos-title">INFO PROMO</h5><a class="btn btn-primary btn-sm" href="{{ url('/rsjihsolo/foto') }}">Indeks</a>
          </div>
        </div>
        <div class="container">
          <div class="row spos-video-slides owl-carousel">
            @php $f_loop = 1; @endphp
            @foreach($promo as $item)
            @if($f_loop <=3) 
            <div class="col-md-4 mt-3">
              <div class="spos-v-card">
                <a class="video-icon" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  <i class="lni lni-tag"></i>
                </a>
                <div class="post-thumbnail">
                  <img loading="lazy" src="{{ $item['featured_image']['medium'] }}" alt="{{ $item['title'] }}" style=" height: 220px;">
                </div>
                <div class="post-content" align="center">
                  <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  {{ $item['title'] }}
                  </a>
                </div>
              </div>
            </div>
            @endif
            @php $f_loop++ @endphp
            @endforeach              
          </div>
        </div>
      </div>

      <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.rs-jih.co.id/rsjihsolo/medsos" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-3.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-3', [[300, 250], [336, 280], [300, 300]], 'div-gpt-ad-1640666720305-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
        </script>
        <!-- /54058497/RSJIH-MOBILE-3 -->
        <div id='div-gpt-ad-1640666720305-0' style='min-width: 300px; min-height: 250px;'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666720305-0'); });
        </script>
        </div>
    </div>

      <div class="container">
        <div class="border-top"></div>
      </div>

      <!--<style type="text/css">
      .spos-v-card .post-thumbnail::after {opacity: 0 !important; }
      .blog-description a {color: #b7212c !important; }
      .btn-primary, #uksw .bg-primary, #uksw .badge-primary {background: #ffc107 !important; border-color: #ffc107; color:#00437c;/*linear-gradient(to right, #00437c, #b7212c) !important;*/}
      .spos-title {color:#00437c; border-bottom: 1px solid #b7212c;}
      .single-hero-slide::after {background:#ffc107;/*background: linear-gradient(to right, #00437c, #b7212c);*/}
      .single-hero-slide .post-title {color: #00437c; line-height: 25px;}
      .single-hero-slide .post-meta a {color:#b7212c; }
      .single-hero-slide::after {background:#ffc107;/*background: linear-gradient(to right, #00437c, #b7212c);*/}
      </style>-->

    </div>
@endsection