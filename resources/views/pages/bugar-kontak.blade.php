@extends('layouts.app')
@section('content')
<div class="page-content-wrapper">
	  <!-- Blok 1 Terkini Wrapper -->
      <div class="terkini-wrapper">
        <div class="container">
			<img loading="lazy" style="max-width: 100%; margin-bottom: 30px;" src="{{ url('/images/bugar/gedung.jpg') }}">
			<h3>Kontak Kami</h3>
			<p>Rumah Sakit "JIH" Solo mengembangkan layanan prima dengan fokus pada kebutuhan pasien. Didukung oleh Dokter, Perawat, Paramedis dan Staf yang profesional dan ramah dalam melayani pasien. Serta didukung dengan peralatan medis modern dan terbaru, kami yakin Rumah Sakit "JIH" Solo akan selalu menjadi pilihan Anda dan Keluarga.</p>
				<div class="widget contact-info">
					<div class="contact-info-box">
						<div class="contact-info-box-content">
							<h4>Alamat</h4>
							<p>Jl. Adi Sucipto No. 118, Jajar, Kecamatan Laweyan, Surakarta, Jawa Tengah 57144</p>
						</div>
					</div>

					<div class="contact-info-box">
						<i class="fa fa-envelope fa-fw"></i> Email : <a href="mailto:infosolo@rs-jih.co.id" target="_blank">infosolo@rs-jih.co.id</a><br><br>
					</div>
						
					<div class="contact-info-box">
						<div class="contact-info-box-content">
							<h4>Nomor Telepon</h4>
							<p>
								<i class="fa fa-phone fa-fw"></i> Pusat Panggilan : <a href="tel:(0271) 746 9100" target="_blank">(0271) 746 9100</a>
							</p>
							<p >
								<i class="fa fa-ambulance fa-fw"></i> Gawat Darurat : <a href="tel:1-500-805" target="_blank"> 1-500-805</a>
							</p>
							<p >
								<i class="fa fa-whatsapp fa-fw"></i> Whatsapp : <a href="https://api.whatsapp.com/send?phone=+62811500805" target="_blank">+62811500805</a>
							</p>
							<br>
						</div>
					</div>

						<!--div class="contact-info-box">
							<div class="contact-info-box-content">
								<h4>Website</h4>
								<i class="fa fa-chrome fa-fw"></i> <a href="https://www.rs-jih.co.id/rsjihsolo" target="_blank">www.rs-jih.co.id/rsjihsolo </a><br><br>
							</div>
						</div-->

						<div class="contact-info-box">
							<div class="contact-info-box-content">
								<h4>Media Sosial</h4>
								<h4>
								<img loading="lazy" style="width: 32px; height: 32px;" src="{{ url('/images/bugar/instagram.png') }}"><a href="https://www.rs-jih.co.id/rsjihsolo" target="_blank"> @rs.jihsolo </a><br><br>
								<img loading="lazy" style="width: 32px; height: 32px;" src="{{ url('/images/bugar/tiktok-logo.png') }}"> <a href="https://www.tiktok.com/@rsjihsolo" target="_blank"> @rsjihsolo </a><br><br>
								<img loading="lazy" style="width: 32px; height: 32px;" src="{{ url('/images/bugar/facebook.png') }}"> <a href="https://www.facebook.com/rs.jihsoloofficial/" target="_blank"> Rumah Sakit JIH Solo  </a><br><br>
								<img loading="lazy" style="width: 32px; height: 32px;" src="{{ url('/images/bugar/youtube.png') }}"> <a href="https://www.youtube.com/c/RSJIHSolo" target="_blank"> RS JIH Solo </a><br><br>
								<img loading="lazy" style="width: 32px; height: 32px;" src="{{ url('/images/bugar/twitter.png') }}"> <a href="https://twitter.com/rsjihsolo?lang=en" target="_blank"> @rsJIHSolo</a><br><br>
								</h4>
								<br>
							</div>
						</div>

			</div><!-- Widget end -->

			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3955.225403030267!2d110.78766931477669!3d-7.55038249455471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a155fc3ffc6af%3A0xb8b4d86b4dbef654!2sRS%20JIH%20Solo!5e0!3m2!1sid!2sid!4v1633011235104!5m2!1sid!2sid" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

        </div>
      </div>
</div>
@endsection