@extends('layouts.app')
@section('content')
      <!-- Share Modal-->
      <div class="modal fade post-share-modal" id="postShareModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button class="close close" type="button" data-dismiss="modal" aria-label="Close"><i class="lni lni-close"></i></button>
              <!-- Heading-->
              <h6 class="mb-3 pl-2">Share this post</h6>
              <div class="social-share-btn d-flex align-items-center flex-wrap"><a class="btn-facebook" href="#"><i class="lni lni-facebook"></i></a><a class="btn-twitter" href="#"><i class="lni lni-twitter-original"></i></a><a class="btn-instagram" href="#"><i class="lni lni-instagram"></i></a><a class="btn-whatsapp" href="#"><i class="lni lni-whatsapp"></i></a><a class="btn-linkedin" href="#"><i class="lni lni-linkedin"></i></a><a class="btn-tumblr" href="#"><i class="lni lni-tumblr"></i></a><a class="btn-quora" href="#"><i class="lni lni-quora"></i></a></div>
            </div>
          </div>
        </div>
      </div>      

    <div class="page-content-wrapper">
      <!-- Scroll Indicator-->
      <div id="scrollIndicator"></div>

      <!-- Single Blog Thumbnail-->
      <div class="single-blog-thumbnail">
        <img loading="lazy" class="w-100 single-blog-image" src="{{ asset('images/solopos.jpg') }}" alt="Solopos Digital Media">
      </div>

      <!-- Single Blog Info-->
      <div class="single-blog-info">
          <div class="d-flex align-items-center">
            <!-- Post Content Wrap-->
            <div class="post-content-wrap">
              <h1 class="mt-2 mb-3">Kontak Kami</h1>
            </div>
          </div>
      </div>     
      <!-- Blog Description-->
      <div class="blog-description">
        <div class="container">
          <p>Griya SOLOPOS<br />
            Jl. Adisucipto 190 Solo Lt. 2<br />
            Telp. (0271) 724811<br />
            Fax. (0271) 724833 (REDAKSI)<br />
            WA. +62 817-724-811</p>
 
        </div>
      </div>          
      <!-- Post Author-->
      <div class="profile-content-wrapper">
        <!-- Settings Option-->
        <div class="profile-settings-option"><a class="post-share" href="#" data-toggle="modal" data-target="#postShareModal"><i class="fa fa-share-alt"></i></a></div>
        <div class="container">
          <!-- User Meta Data-->
          <div class="user-meta-data d-flex">
            <!-- User Thumbnail-->
            <div class="user-thumbnail">
                  <img loading="lazy" src="{{ asset('images/icon.png') }}" alt="editor">
            </div>
            <!-- User Content-->
            <div class="user-content">
              <h6><a href="{{ url('/') }}" title="">Redaksi Solopos</a></h6>
            </div>
          </div>
        </div>
      </div>
@endsection