@extends('layouts.app')
@section('content')
<div class="page-content-wrapper">
	<!-- Blok 1 Terkini Wrapper -->
    <div class="video-wrapper">
        <div class="mt-3 mb-3" align="center">
            <!--<a href="https://www.youtube.com/c/RSJIHSolo" target="_blank">
            <img src="{{ url('images/bugar/mobile-banner-1.gif') }}">
            </a>-->
            <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
            <script>
            window.googletag = window.googletag || {cmd: []};
            googletag.cmd.push(function() {
                googletag.defineSlot('/54058497/RSJIH-MOBILE-1', [[336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1640666517438-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
            </script>
            <!-- /54058497/RSJIH-MOBILE-1 -->
            <div id='div-gpt-ad-1640666517438-0' style='min-width: 300px; min-height: 250px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666517438-0'); });
            </script>
            </div>
        </div>
        
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">GALERI VIDEO</h5><a class="btn btn-primary btn-sm" href="{{ url('/rsjihsolo/video') }}">Galeri Video</a>
          </div>
        </div>        
        <div class="container">
            @foreach($video as $item)
            @foreach(explode('yt:video:',$item['id']) as $ids)@endforeach            
            <div class="spos-v-card mb-5">
                <a class="video-icon" href="https://www.youtube.com/watch?v={{ $ids }}?utm_source=Solopos.com" title="{{$item['title']}}" target="_blank" style="background: #fb072e; width: 70px; border-radius: 17%;opacity: 0.8;">
                <i class="lni lni-play" style="background-color: #fb072e;width: 70px; border-radius: 17%;opacity: 0.8;"></i>
                </a>            
                <img loading="lazy" class="card-img-top" src="https://i1.ytimg.com/vi/{{ $ids }}/mqdefault.jpg" alt="Thumbnail" style="object-fit: cover;" >

                <div class="post-content text-center" style="padding: 7px; background: linear-gradient(to bottom, rgba(255,0,0,0), #0c153b)">
                <h5 align="center">
                    <a class="post-title" href="https://www.youtube.com/embed/{{ $ids }}?rel=0&vq=hd1080&autoplay=1&amp;loop=1" title="{{$item['title']}}" target="_blank">{{$item['title']}}</a></h5>
                </div>
            </div>
            @endforeach
            <div class="text-center mt-3">
                <a class="btn btn-info btn-lg" href="https://www.youtube.com/c/RSJIHSolo" target="_blank">
                Video Lainnya...
                </a>
            </div>
          
        </div>
    </div>

    <div class="mt-3 mb-3" align="center">
        <!--<a href="https://www.rs-jih.co.id/rsjihsolo/medsos" target="_blank">
          <img src="{{ url('images/bugar/mobile-banner-3.gif') }}">
        </a>-->
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/RSJIH-MOBILE-3', [[300, 250], [336, 280], [300, 300]], 'div-gpt-ad-1640666720305-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
        </script>
        <!-- /54058497/RSJIH-MOBILE-3 -->
        <div id='div-gpt-ad-1640666720305-0' style='min-width: 300px; min-height: 250px;'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640666720305-0'); });
        </script>
        </div>
    </div>

    <div class="container">
        <div class="border-top"></div>
    </div> 
</div>
@endsection