@extends('layouts.app')
@section('content')
      <!-- Share Modal-->
      <div class="modal fade post-share-modal" id="postShareModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button class="close close" type="button" data-dismiss="modal" aria-label="Close"><i class="lni lni-close"></i></button>
              <!-- Heading-->
              <h6 class="mb-3 pl-2">Share this post</h6>
              <div class="social-share-btn d-flex align-items-center flex-wrap"><a class="btn-facebook" href="#"><i class="lni lni-facebook"></i></a><a class="btn-twitter" href="#"><i class="lni lni-twitter-original"></i></a><a class="btn-instagram" href="#"><i class="lni lni-instagram"></i></a><a class="btn-whatsapp" href="#"><i class="lni lni-whatsapp"></i></a><a class="btn-linkedin" href="#"><i class="lni lni-linkedin"></i></a><a class="btn-tumblr" href="#"><i class="lni lni-tumblr"></i></a><a class="btn-quora" href="#"><i class="lni lni-quora"></i></a></div>
            </div>
          </div>
        </div>
      </div>      

    <div class="page-content-wrapper">
      <!-- Scroll Indicator-->
      <div id="scrollIndicator"></div>

      <!-- Single Blog Thumbnail-->
      <div class="single-blog-thumbnail">
        <img loading="lazy" class="w-100 single-blog-image" src="{{ asset('images/solopos.jpg') }}" alt="Solopos Digital Media">
      </div>

      <!-- Single Blog Info-->
      <div class="single-blog-info">
          <div class="d-flex align-items-center">
            <!-- Post Content Wrap-->
            <div class="post-content-wrap">
              <h1 class="mt-2 mb-3">About Us</h1>
            </div>
          </div>
      </div>     
      <!-- Blog Description-->
      <div class="blog-description">
        <div class="container">
						
          <p><a href="https://www.solopos.com">Solopos.com</a> merupakan bagian dari Solopos Media Group (SMG). Website Solopos.com diluncurkan pada 19 September 2007 bertepatan dengan HUT Ke-10 Harian Solopos. <em>Solopos.com</em> tak hanya menghadirkan kabar seputar Soloraya namun informasi nasional dan global.</p>

          <p><a href="https://semarangpos.com" target="_blank">Semarangpos.com</a>, dan <a href="https://madiunpos.com" target="_blank">Madiunpos.com</a> merupakan bagian dari portal yang kami hadirkan untuk melayani netizen di seputar Jawa Tengah, Jawa Timur, dan DIY. Termasuk <a href="https://jeda.id" target="_blank">Jeda.id</a> yang memberikan informasi dari sudut pandang yang berbeda. Informasi yang khas dan mendalam juga bisa dinikmati pembaca melalui <a href="https://www.solopos.com/plus" target="_blank">Espos Plus</a>.</p>
          
          <p>Informasi kami kemas dalam gaya entertain, simple, unique dan inspiratif, sehingga bisa menjadi panduan dan inspirasi bagi para pembaca. Solopos.com juga menghadirkan video-video unik akun Youtube <a href="https://www.youtube.com/solopostv" target="_blank">SoloposTV</a>.</p>

          <div class="text-center about-us">
          <div class="mb-1">
            <hr data-content="PUBLISHER" class="hr-text">
          </div>
          <p>
            PT. Aksara Solopos <br>
            Verifikasi Dewan Pers: 777/DP-Verifikasi/K/VIII/2021
          </p>
          
          <div class="mb-3">
            <hr data-content="BOARD OF DIRECTOR" class="hr-text">
          </div>

            <p><strong>Presiden Direktur</strong><br />
            Arief Budisusilo</p>

            <p><strong>Direktur Bisnis &amp; Konten</strong><br />
            Suwarmin</p>

            <p><strong>Direktur Keuangan &amp; SDM</strong><br />
            Anissa Nurul Aini</p>

            <div class="mb-3">
              <hr data-content="EDITORIAL TEAM" class="hr-text">
            </div>
            <p><strong>Pemimpin Redaksi/Penanggung Jawab</strong><br />
            Rini Yustiningsih</p>

            <p><strong>Redaktur Pelaksana</strong><br />
            Danang Nur Ihsan, Syifaul Arifin, Ivan Indrakesuma</p>

            <p><strong>Manager Konten</strong><br />
            Abu Nadhif, Adib M. Asfar, Ahmad Mufid Aryono, Anik Sulistyawati, Astrid Prihatini Wisnu Dewi, Ayu Prawitasari, Burhan Aris Nugraha, Damar Sri Prakoso, Haryono Wahyudiyanto, Ichwan Prasetyo, Imam Yuda Saputra, Kaled Hasby Ashshidiqy, Moh. Khodiq Duhri, Oriza Vilosa, Rahmat Wibisono, R. Bambang Aris S., Rohmah Ermawati, Suharsih, Sri Sumi Handayani, Tri Wiharto</p>

            <p><strong>Reporter-Content Writer</strong><br />
            <strong>Kota Solo:</strong> Afifa Enggar Wulandari, Arif Fajar S., Bayu Jatmiko Adi, Bony Eko Wicaksono, Gigih Windar P., Ichsan Kholif Rahman, Kurniawan, Wahyu Prakoso;<strong>Boyolali:</strong> Nimatul Faizah, Nova Malinda; <strong>Klaten:</strong>Taufiq Sidik Prakoso, Wildan Farih Kurniawan; <strong>Karanganyar:</strong> Akhmad Ludiyanto, Indah Septiyaning W.; <strong>Sragen:</strong> Galih Aprilia Wibowo, Tri Rahayu; <strong>Sukoharjo:</strong> Magdalena Naviriana Putri, Tiara Surya Madani; <strong>Wonogiri:</strong> Luthfi Shobri Marzuqi, Muhammad Diky P.;<strong>Foto:</strong> Nicolous Irawan Ika Paksi</p>

            
            <p><strong>Kontributor</strong><br />
            <strong>Semarang:</strong> Adhik Kurniawan; <strong>Salatiga:</strong> Hawin Alaina; <strong>Madiun:</strong> Ronaa Nisa'us Sholikhah</p>

            <p><strong>Multimedia</strong><br />
            <strong>Manajer:</strong> Jafar Sodiq Assegaf; <br>
            <strong>Supervisor:</strong> Argo Suryo P, Andhi Susanto; <br>
            <strong>Staf:</strong> Abdillah Tsani, Andhika Wahyu Purnama, Candra Mantovani, Deasy Puspitaningarum, Erika Fitriani, Najmuddin Fadhil, Riza Noor Wahyudi</p>

            <p><strong>Platform</strong><br/>
            <strong>Manajer:</strong> Budi Cahyono; <br>
            <strong>Programmer:</strong> Yeyen Pamula, Eko Prasetyo; <br>
            <strong>Dev Opt:</strong> Kristian Dwi Nugroho</p>

            <p><strong>SEO</strong><br>
            <strong>Manajer:</strong> Ginanjar Saputra; <br>
            <strong>Staf:</strong> Nugroho Meidinata; <br />

            <p><strong>Media Sosial</strong><br>
            <strong>Manajer:</strong> Ahmad Baihaqi; <br>
            <strong>Staf:</strong> Anindya A.W.</p>

            <p><strong>Desain</strong><br />
            <strong>Manajer:</strong> Galih Ertanto<br />
            <strong>Supervisor:</strong> Handoko B.S, Rahmanto Iswahyudi; <br>
            <strong>Staf:</strong> Ali Zamroni, Aziz Nugroho, Muhammad Nur Wakhid, Eka Novitasari, K. Tri Chandra P., Wisnupaksa K., Irlani, Bagyo Surono, Mahfud Burhanuddin, Tosa Adi Wigana, Andi Sutaji</p>

            <p><strong>Sekretariat</strong><br />
            <strong>Manajer:</strong> Sri Handayani</p>

            <div class="mb-3">
              <hr data-content="IMS TEAM" class="hr-text">
            </div>
            <p>
            <strong>General Manajer:</strong> Yonantha Chandra Premana<br />
            <strong>Manajer Senior:</strong> Alvari Kunto Prabowo<br />
            <strong>Manajer IMS:</strong> Susi Ashari, Suyanto, Arif Hidayanto<br>
            <strong>Manajer Program:</strong> Hijriyah Al Wakhidah, Tika Sekar Arum<br>
            <strong>Manajer Promosi:</strong> Alvari Kunto Prabowo<br>
            <strong>Manajer Sirkulasi:</strong> Franky Simon<br> 
            <strong>Manajer EO:</strong> Dewi Lestari<br>
            <strong>Manajer Solopos Institute:</strong> Sholahuddin</p>

            <div class="mb-4">
              <hr data-content="HEAD OFFICE" class="hr-text">
            </div>						
            <p class="text-center">Griya Solopos Jl Adisucipto 190 Solo 57145<br />
            Telepon (0271) 724 811 Fax Redaksi (0271) 724 833 Fax Perusahaan (0271) 724850<br />
            Email: redaksi@solopos.co.id (redaksi); iklan@solopos.com (iklan) </p>

            <p><strong>Iklan Perwakilan Jakarta:</strong> Suyanto (087770984454) dan Rayendra (085742173017), <em>Wisma Bisnis Indonesia Lt. 5-8 Jl. K.H. Mas Mansyur No. 12A Karet Tengsin,Tanah Abang Jakarta Pusat 10220</em>, Telp (021) 57901023 ext 536 Faks (021) 57901024<br />
            <strong>Perwakilan Semarang:</strong> <em>Jl Sompok Baru No. 79 Semarang</em> Telp (024) 8442852;</p>
          </div> 
          <div class="mb-4">
            <hr class="dotted">
          </div>						
          <p>
            <strong>
              "Hak Cipta dilindungi Undang-undang. Dilarang mengutip sebagian atau seluruh isi pemberitaan Solopos.com tanpa izin dari penerbit. Pengutipan diperkenankan sepanjang hanya mencantumkan link judul dan lead berita dan tidak mengambil keseluruhan isi berita, serta diberikan link langsung ke berita dimaksud di alamat solopos.com. Silahkan kontak kami untuk kepentingan redistribusi berita."									
            </strong>
          </p>						
        </div>	
      </div>     
      <style>
      .about-us p {font-size:14px;}
      </style>      
      <!-- Post Author-->
      <div class="profile-content-wrapper">
        <!-- Settings Option-->
        <div class="profile-settings-option"><a class="post-share" href="#" data-toggle="modal" data-target="#postShareModal"><i class="fa fa-share-alt"></i></a></div>
        <div class="container">
          <!-- User Meta Data-->
          <div class="user-meta-data d-flex">
            <!-- User Thumbnail-->
            <div class="user-thumbnail">
                  <img loading="lazy" src="{{ asset('images/icon.png') }}" alt="editor">
            </div>
            <!-- User Content-->
            <div class="user-content">
              <h6><a href="{{ url('/') }}" title="">Redaksi Solopos</a></h6>
            </div>
          </div>
        </div>
      </div>
@endsection