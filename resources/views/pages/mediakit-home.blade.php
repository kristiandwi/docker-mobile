<html>
<head>
    <script type="text/javascript">
      if (screen.width > 640) {
        var pathname = window.location.pathname;
        window.location.href = "https://www.solopos.com"+pathname;
      }
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-MBVNS9P');</script>
    <!-- End Google Tag Manager -->
    <title>Ads Inventory Solopos.com Mobile Homepage</title>
    <meta content="Ads Inventory Solopos.com Mobile Homepage" itemprop="headline" />
    <meta name="title" content="Ads Inventory Solopos.com Mobile Homepage" />
    <meta name="description" content="Ads Inventory Solopos.com Mobile Homepage" itemprop="description" />
    <meta name="keywords" content="Ads Inventory Solopos.com Mobile Homepage" itemprop="keywords" />
    <meta name="news_keywords" content="Ads Inventory Solopos.com Mobile Homepage" itemprop="keywords" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta name="copyright" content="Solopos Media Group" /> 
    <meta name="googlebot-news" content="index,follow" />    
    <meta name="googlebot" content="index,follow,all" />
    <meta name="Bingbot" content="index,follow">
    <meta name="robots" content="index,follow,max-image-preview:large" />
    <meta name="google-site-verification" content="AA4UjbZywyFmhoSKLELl4RA451drENKllt5Sbq9uINw" />
    <meta name="yandex-verification" content="7966bb082003f8ae" />
    <meta name="msvalidate.01" content="F2320220951CEFB78E7527ECC232031C" />
    <meta name='dailymotion-domain-verification' content='dm0w2nbrkrxkno2q2' />
    <meta name="language" content="id" />
    <meta name="geo.country" content="id" />
    <meta http-equiv="content-language" content="In-Id" />
    <meta name="geo.placename" content="Indonesia" />
    <meta name="showus-verification" content="pub-2627" />

    <!-- Favicon-->
    <link rel="icon" href="{{ url('images/favicon.ico') }}">
  
    <!-- Stylesheet-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" />    
    <link rel="stylesheet" href="https://cdn.lineicons.com/2.0/LineIcons.css">
    <link rel="stylesheet" href="{{ url('style.css')}}?v={{time()}}">    
  </head>
<!-- Header Area-->
<div class="header-area" id="headerArea">
    <div class="container h-100 d-flex align-items-center justify-content-between">

      <div class="navbar--toggler" id="sposNavbarToggler">
        <a href="#">
          <i class="lni lni-grid-alt" style="font-size: 1.45rem; font-weight: bold; display: block; color: #00437c; margin-top: 2px; margin-left: 3px;"></i>
        </a>
      </div>
     
      <!-- Logo-->
      <div class="logo-wrapper">                       
        <a href="{{ url('/mediakit') }}"> 
          <img class="logo-image" src="{{ asset('images/logo.png') }}" alt="Solopos.com">
        </a>
      </div>

      <div class="spos-story">
        <div class="spos-icon">
          <a href="https://m.solopos.com/" title="Solopos Story">
            <img src="{{ asset('images/icon.png') }}">
          </a>
        </div>
      </div>                           

    </div>
  </div>
  <div class="page-content-wrapper">
      
    <!-- Headline Wrapper-->
    <div class="headline-wrapper">
      <div class="container">
        <!-- Hero Slides-->
        <div class="slides-wrapper">
          <div class="hero-slides owl-carousel">                    
            <div class="single-hero-slide">
              <div class="headline-image d-flex align-items-start">
                <a class="post-title d-block" href="{{ url("mediakit/single") }}" title="">
                <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                </a>
              </div>
              <div class="d-flex align-items-end">
                <div class="container-fluid mb-3">
                  <div class="post-meta d-flex align-items-center">
                    <a href="">Category</a> | <span style="margin-left: 5px;"> 1 jam yang lalu</span>
                  </div>                  
                  <a class="post-title d-block" href="{{ url("/mediakit/single") }}" title="">Lorem ipsum dolor sit amet</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Konten Premium Wrapper -->
    <div class="container">
        <div class="editorial-choice-news-wrapper mt-3">
          <div class="bg-shape1"><img src="{{ asset('images/edito.png') }}"></div>
          <div class="bg-shape2" style="background-image: url('{{ asset('images/edito2.png') }}')"></div>
          <div class="container" align="center">
            <img src="{{ asset('images/premium.png') }}" height="50%" width="50%" style="margin-bottom: 30px;">
          </div>
          <div class="container">
            <div class="editorial-choice-news-slide owl-carousel">
     
              <div class="single-editorial-slide d-flex">
                <a class="bookmark-post" href="#"><i class="lni lni-star"></i></a>
                <div class="post-thumbnail">
                  <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" style="object-fit: cover; height: 177px; width: 132px;" loading="lazy" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                </div>
                <div class="post-content">
                  <a class="post-catagory" href="{{ url("/premium") }}">Premium</a>
                  <a class="post-title d-block" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet" >Lorem ipsum dolor sit amet</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Ads Mobile Home 1 -->
      <div class="iklan mt-3">
        <div class="iklan" align="center">
                <a href="" data-toggle="modal" data-target="#ModalMobileHome1">
                    <img class="img-fluid" src="{{ url('images/iklan-img/mobile-home-1.png') }}" style="object-fit: cover; object-position: center;">
                </a>
            </div>	  		
      </div>

      <!-- Konten Terpopuler Wrapper -->
      <div class="mt-3">
        <!-- Item Slides Wrapper-->
         <div class="container">
           <div class="d-flex align-items-center justify-content-between mb-3 mt-3">
             <a class="btn btn-primary btn-sm" href="/arsip">Berita Terpopular</a>
           </div>
         </div>       
         <div class="item-wrapper mb-3">       
           <div class="container">
             <!-- Catagory Slides-->
             <div class="item-slides owl-carousel">                        
               <div class="item">
               <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet}">
                   <div class="item-thumbnail">
                     <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; width: 195px; height: 115px;" onerror="javascript:this.src='https://m.solopos.com/images/no-medium.jpg'">
                   </div> 
                   <div class="item-caption">
                     <span class="link-title">Lorem ipsum dolor sit amet</span>
                   </div>
                 </a>
               </div>                                                        
             </div>
           </div>
         </div>  
         </div>
    
    <!-- Konten Promosi Wrapper-->
      <div class="container mt-3">
        <div class="d-flex align-items-center justify-content-between mb-3">
        <div class="badge badge-danger">Promo & Events</div>
        </div>
        <!-- Default Carousel-->
        <div class="carousel slide promosi" id="carouselExampleIndicators" data-ride="carousel">
          <!-- Indicators-->
          <ol class="carousel-indicators">
            <li class="active" data-target="#carouselExampleIndicators" data-slide-to="0"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          </ol>
          <!-- Carousel Inner-->
          <div class="carousel-inner">		  
              <div class="carousel-item active">
                <a href="" data-toggle="modal" data-target="#ModalMobilePromo" title="Promo dan Event">
                  <img class="d-block w-100" src="{{ url('./banner/pisalin_oct.png') }}">
                  </a>
              </div>
              <div class="carousel-item">
                <a href="" data-toggle="modal" data-target="#ModalMobilePromo" title="Promo dan Event">
                  <img class="d-block w-100" src="{{ url('banner/ayam-silaturahmi.png') }}" alt="Promo dan Event">
                  </a>
              </div>	  
          </div>
        </div>         
    </div>

    <!-- Blok 1 Terkini Wrapper -->
    <div class="terkini-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between mb-3">
            <h5 class="mb-0 pl-1 spos-title">Berita Terkini</h5><a class="btn btn-primary btn-sm" href="/arsip">Indeks Berita</a>
          </div>
        </div>
        <div class="container">

            <div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                  <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                <div class="post-meta d-flex align-items-center">
                  <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 2 Jam yang lalu</a>
                </div>
              </div>
            </div>

            <div class="terkini-post d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                    <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 2 Jam yang lalu</a>
                  </div>
                </div>
              </div>

              <div class="terkini-post d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                    <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 2 Jam yang lalu</a>
                  </div>
                </div>
              </div>

              <div class="terkini-post d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/mediakit/single") }}" title="Lorem ipsum dolor sit amet">
                    <img src="{{ url('images/iklan-img/office-solopos.jpg') }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
                  </a>
                </div>
                <div class="post-content">
                  <a class="post-title" href="" data-toggle="modal" data-target="#ModalMobileAdvetorial" title="Lorem ipsum dolor sit amet">IKLAN ADVETORIAL</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href=""> Category</a>|<a href="#" style="padding-left:7px;"> 4 Jam yang lalu</a>
                  </div>
                </div>
              </div>

            <!-- Ads Mobile Home 2 -->
            <div class="iklan mt-3">
                <div class="iklan" align="center">
                    <a href="" data-toggle="modal" data-target="#ModalMobileHome2">
                        <img class="img-fluid" src="{{ url('images/iklan-img/mobile-home-2.png') }}" style="object-fit: cover; object-position: center;">
                    </a>
                </div>
            </div>

        <!-- Editors Choice Wrapper -->
        <div class="terkini-wrapper">
            <div class="container">
              <div class="d-flex align-items-center justify-content-between mb-3">
                <h5 class="mb-0 pl-1 spos-title">Fokus</h5><a class="btn btn-primary btn-sm" href="#">Indeks Berita</a>
              </div>
            </div>
            <div class="container">      
              <div class="card mb-3">
                <a href="{{ url("/mediakit/single") }}"  title="Ini 3 Langkah Darurat yang Bisa Selamatkan Nyawa Saat Keracunan Sianida">
                <img class="card-img-top" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Ini 3 Langkah Darurat yang Bisa Selamatkan Nyawa Saat Keracunan Sianida">
                </a>
                <div class="card-body">
                  <a class="post-title" href="{{ url("/mediakit/single") }}"  title="Ini 3 Langkah Darurat yang Bisa Selamatkan Nyawa Saat Keracunan Sianida">Ini 3 Langkah Darurat yang Bisa Selamatkan Nyawa Saat Keracunan Sianida</a>
                </div>
              </div>

              <div class="terkini-post d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/mediakit/single") }}"  title="Pemkab Klaten Kumpulkan Pelaku Usaha Wisata Hingga Rumah Makan, Ada Apa Ya?">
                    <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Ilustrasi pengaturan jarak di tempat makan untuk menghindari penyebaran COvid-19. (Freepik) " style="object-fit: cover; height: 100px; width: 100px;">
                  </a>
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/mediakit/single") }}"  title="Pemkab Klaten Kumpulkan Pelaku Usaha Wisata Hingga Rumah Makan, Ada Apa Ya?">Pemkab Klaten Kumpulkan Pelaku Usaha Wisata Hingga Rumah Makan, Ada Apa Ya?</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="">soloraya</a>|<a href="#" style="padding-left:7px;">11 jam lalu</a>
                  </div>
                </div>
              </div>

              <div class="terkini-post d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/mediakit/single") }}"  title="Jateng Larang Salat Id Berjemaah di Zona Merah & Oranye">
                    <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Gubernur Jateng, Ganjar Pranowo, seusai mengisi acara FGD di Gedung Rektorat Baru Kampus 3 UIN Semarang, Minggu (4/4/2021). (Semarangpos.com-Humas Pemprov Jateng)" style="object-fit: cover; height: 100px; width: 100px;">
                  </a>
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/mediakit/single") }}"  title="Jateng Larang Salat Id Berjemaah di Zona Merah & Oranye">Jateng Larang Salat Id Berjemaah di Zona Merah & Oranye</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="">jateng</a>|<a href="#" style="padding-left:7px;">12 jam lalu</a>
                  </div>
                </div>
              </div>                                                  

              <div class="terkini-post d-flex">
                <div class="post-thumbnail">
                  <a href="{{ url("/mediakit/single") }}"  title="Covid-19 Grobogan, Sehari Ada 22 Pasien Sembuh Dan 20 Kasus Baru">
                    <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Bupati Grobogan Sri Sumarni mengingatkan masih tingginya kasus Covid-19 di Grobogan. Masyarakat diminta tidak abai dan tetap patuhi protokol kesehatan. (Solopos.com/Arif Fajar Setiadi)" style="object-fit: cover; height: 100px; width: 100px;">
                  </a>
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/mediakit/single") }}"  title="Covid-19 Grobogan, Sehari Ada 22 Pasien Sembuh Dan 20 Kasus Baru">Covid-19 Grobogan, Sehari Ada 22 Pasien Sembuh Dan 20 Kasus Baru</a>
                  <div class="post-meta d-flex align-items-center">
                    <a href="">jateng</a>|<a href="#" style="padding-left:7px;">18 jam lalu</a>
                  </div>
                </div>
              </div>                                   
            </div>
          </div>
          

          <!-- Ads Mobile Home 3 -->
          <div class="iklan">
            <div class="iklan" align="center">
              <a href="" data-toggle="modal" data-target="#ModalMobileHome3">
                <img class="img-fluid" src="{{ url('images/iklan-img/mobile-home-3.png') }}" style="object-fit: cover; object-position: center;">
              </a>
            </div>      
          </div>
  
          <!-- Blok 2 Terkini Wrapper-->
          <div class="terkini-wrapper">
            <div class="container">
                       <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Ibu Katolik dan Ayah Buddha, Ini Cerita Dian Sastro Pilih Memeluk Islam">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Dian Sastrowardoyo (Instagram/@therealdisastr)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Ibu Katolik dan Ayah Buddha, Ini Cerita Dian Sastro Pilih Memeluk Islam">Ibu Katolik dan Ayah Buddha, Ini Cerita Dian Sastro Pilih Memeluk Islam</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">entertainment</a>|<a href="#" style="padding-left:7px;">26 menit lalu</a></div>
                  </div>
                </div>
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Koalisi DMFI Apresiasi Wali Kota Salatiga yang Larang Peredaran Daging Anjing">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Wali Kota Salatiga, Yuliyanto, mengisi Lokakarya Keluarga Berencana di Aula Kantor Pemerintah Kecamatan Argomulyo, Senin (5/4/2021). (Semarangpos.com-Humas Pemkot Salatiga)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Koalisi DMFI Apresiasi Wali Kota Salatiga yang Larang Peredaran Daging Anjing">Koalisi DMFI Apresiasi Wali Kota Salatiga yang Larang Peredaran Daging Anjing</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">jateng</a>|<a href="#" style="padding-left:7px;">44 menit lalu</a></div>
                  </div>
                </div>
                       <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Beda Tipis, Pilih Toyota Raize atau Daihatsu Rocky?">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Toyota Raize. (Toyota.com)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Beda Tipis, Pilih Toyota Raize atau Daihatsu Rocky?">Beda Tipis, Pilih Toyota Raize atau Daihatsu Rocky?</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">otomotif</a>|<a href="#" style="padding-left:7px;">52 menit lalu</a></div>
                  </div>
                </div>
            </div>
          </div>
  
          <!-- Opini Wrapper-->
          <div class="opini-wrapper mb-3">
            <div class="bg-shapes">
              <div class="shape1"></div>
              <div class="shape2"></div>
              <div class="shape3"></div>
              <div class="shape4"></div>
              <div class="shape5"></div>
            </div>
            <h6 class="mb-3 catagory-title">Kolom</h6>
            <div class="container">
              <!-- Catagory Slides-->
              <div class="opini-slides owl-carousel">
                                              
                <div class="card catagory-card">
                  <a class="post-title d-block" href="{{ url("/mediakit/single") }}" title="Sinergi Mencegah Perkawinan Anak">
                    <img src="https://images.solopos.com/2021/04/Retno-Winarni-2-555x370.jpg" alt="Retno Winarni (Istimewa/Dokumen pribadi)." style="object-fit: cover; height: 150px; width: 134px;">
                    <h6>Retno Winarni</h6>
                  </a>
                </div>
                            
                <div class="card catagory-card">
                  <a class="post-title d-block" href="{{ url("/mediakit/single") }}" title="Bisnis Monyet Tanaman Hias">
                    <img src="https://images.solopos.com/2021/04/Nadia-Aliya-Azki-555x370.jpg" alt="Nadia Aliya Azki (Istimewa/Dokumen pribadi)" style="object-fit: cover; height: 150px; width: 134px;">
                    <h6>Nadia Aliya Azki</h6>
                  </a>
                </div>
                            
                <div class="card catagory-card">
                  <a class="post-title d-block" href="{{ url("/mediakit/single") }}" title="(Andai) Demokrasi Tanpa Oligarki">
                    <img src="https://images.solopos.com/2021/04/Siti-Farida-2-555x370.jpg" alt="Siti Farida (Istimewa/Dokumen pribadi)." style="object-fit: cover; height: 150px; width: 134px;">
                    <h6>Siti Farida</h6>
                  </a>
                </div>
                            
                <div class="card catagory-card">
                  <a class="post-title d-block" href="{{ url("/mediakit/single") }}" title="Perlawanan Perempuan">
                    <img src="https://images.solopos.com/2021/04/Syifaul-Arifin-3-555x370.jpg" alt="Syifaul Arifin (Istimewa/Dokumen pribadi)." style="object-fit: cover; height: 150px; width: 134px;">
                    <h6>Syifaul Arifin </h6>
                  </a>
                </div>
                            
                <div class="card catagory-card">
                  <a class="post-title d-block" href="{{ url("/mediakit/single") }}" title="Singkong Goreng dan Segelas Wine">
                    <img src="https://images.solopos.com/2021/04/Maria-Y.-Benyamin-3-554x370.jpg" alt="Maria Y. Benyamin (Istimewa/Dokumen pribadi)." style="object-fit: cover; height: 150px; width: 134px;">
                    <h6>Maria Y. Benyamin</h6>
                  </a>
                </div>
                </div>
            </div>
          </div>
  
          <!-- Ads Mobile Home 4-->
          <div class="iklan">
            <a href="" data-toggle="modal" data-target="#ModalMobileHome4">
                   <img class="img-fluid" src="{{ url('images/iklan-img/mobile-home-4.png') }}" style="object-fit: cover; object-position: center;">
                </a>
          </div>
  
          <!-- Blok 3 Terkini Wrapper-->
          <div class="terkini-wrapper">
            <div class="container">  
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Pemkab Wonogiri Kembali Terima Opini WTP dari BPK">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Penyerahan Laporan opini WTP dilakukan Kepala Perwakilan (Kalan) BPK Jawa Tengah Ayub Amali secara langsung kepada Bupati Wonogiri Joko Sutopo (kanan) di Auditorium BPK Jawa Tengah, Senin (3/5/2021) di Semarang. (Istimewa)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Pemkab Wonogiri Kembali Terima Opini WTP dari BPK">Pemkab Wonogiri Kembali Terima Opini WTP dari BPK</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">soloraya</a>|<a href="#" style="padding-left:7px;">1 jam lalu</a></div>
                  </div>
                </div>
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Pengumuman! Wali Kota Salatiga Larang Peredaran Daging Anjing">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Ilustrasi penolakan konsumsi daging anjing (Solopos)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Pengumuman! Wali Kota Salatiga Larang Peredaran Daging Anjing">Pengumuman! Wali Kota Salatiga Larang Peredaran Daging Anjing</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">jateng</a>|<a href="#" style="padding-left:7px;">1 jam lalu</a></div>
                  </div>
                </div>
                  
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Polres Sragen Salurkan 1.124 Paket Zakat Fitrah untuk Warga">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Penyerahan paket zakat fitrah dari Polres Sragen kepada warga di halaman Mapolres Sragen, Selasa (4/5/2021). (Solopos-Moh. Khodiq Duhri)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Polres Sragen Salurkan 1.124 Paket Zakat Fitrah untuk Warga">Polres Sragen Salurkan 1.124 Paket Zakat Fitrah untuk Warga</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">soloraya</a>|<a href="#" style="padding-left:7px;">1 jam lalu</a></div>
                  </div>
                </div>
            </div>
          </div>
  
          <!-- Spos V Card Wrapper-->
          <div class="info-grafis-wrapper mt-3 mb-3">
            <div class="container">
              <div class="d-flex align-items-center justify-content-between">
                <h5 class="mb-0 pl-1 spos-title">#Espospedia</h5>
              </div>
            </div>
            <div class="container">
              <div class="row">
                                   
                <div class="col-6 col-md-4">
                  <div class="spos-v-card mt-3"><a class="bookmark-post" href="#"><i class="lni lni-bookmark"></i></a>
                    <div class="post-thumbnail">
                      <img src="https://images.solopos.com/2021/05/040521_PERSISTIMNAS_HEADER-560x281.jpg" alt="Infografis Persis (Solopos/Whisnupaksa)" style="object-fit: cover; height: 210px; width: 188px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Persis Solo Rasa Timnas Indonesia">#infoGrafis #esposPedia</a>
                    </div>
                  </div>
                </div>
                 
                <div class="col-6 col-md-4">
                  <div class="spos-v-card mt-3"><a class="bookmark-post" href="#"><i class="lni lni-bookmark"></i></a>
                    <div class="post-thumbnail">
                      <img src="https://images.solopos.com/2021/04/280421_TIKTOK_HEADER-560x281.jpg" alt="Infografis Tiktok (Solopos/Whisnupaksa)" style="object-fit: cover; height: 210px; width: 188px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Tiktok, dari Hiburan jadi Cuan">#infoGrafis #esposPedia</a>
                    </div>
                  </div>
                </div>
                 
                <div class="col-6 col-md-4">
                  <div class="spos-v-card mt-3"><a class="bookmark-post" href="#"><i class="lni lni-bookmark"></i></a>
                    <div class="post-thumbnail">
                      <img src="https://images.solopos.com/2021/03/ESPOSPEDIA-TB-CILIK-560x280.jpg" alt="Infografis TB Covid (Khoirul Tri Candra P/Solopos)" style="object-fit: cover; height: 210px; width: 188px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Gejala TB dan Covid 19">#infoGrafis #esposPedia</a>
                    </div>
                  </div>
                </div>
                 
                <div class="col-6 col-md-4">
                  <div class="spos-v-card mt-3"><a class="bookmark-post" href="#"><i class="lni lni-bookmark"></i></a>
                    <div class="post-thumbnail">
                      <img src="https://images.solopos.com/2021/04/270421_SEX_HEADER-560x281.jpg" alt="Infografis Doyan Seks (Solopos/Whisnupaksa)" style="object-fit: cover; height: 210px; width: 188px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Negara yang Warganya Doyan Seks">#infoGrafis #esposPedia</a>
                    </div>
                  </div>
                </div>
                          </div>
            </div>
          </div>
  
          <!-- Ads Mobile Home 5 -->
          <div class="iklan">
            <div class="iklan" align="center">
                <a href="" data-toggle="modal" data-target="#ModalMobileHome5">
                    <img class="img-fluid" src="{{ url('images/iklan-img/mobile-home-5.png') }}" style="object-fit: cover; object-position: center;">
                 </a>
            </div>
          </div>
  
          <!-- Blok 4 Terkini Wrapper-->
          <div class="terkini-wrapper">
            <div class="container">
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Rindu Lebaran di Kampung Halaman, Warga Boyolali Nekat Mudik Pakai Bajaj">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Bajaj yang digunakan Didik untuk mudik dari Jakarta menuju Cepogo, Boyolali. (Solopos.com/Imam Yuda Saputra)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Rindu Lebaran di Kampung Halaman, Warga Boyolali Nekat Mudik Pakai Bajaj">Rindu Lebaran di Kampung Halaman, Warga Boyolali Nekat Mudik Pakai Bajaj</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">jateng</a>|<a href="#" style="padding-left:7px;">2 jam lalu</a></div>
                  </div>
                </div>
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Karakter Nagini Karya J.K. Rowling Terinsipirasi Mitologi Jawa">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Karakter Nagini dalam Film Fantastic Beast 2 (Okezone.com)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Karakter Nagini Karya J.K. Rowling Terinsipirasi Mitologi Jawa">Karakter Nagini Karya J.K. Rowling Terinsipirasi Mitologi Jawa</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">entertainment</a>|<a href="#" style="padding-left:7px;">2 jam lalu</a></div>
                  </div>
                </div>
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Kasus Positif Covid-19 Indonesia Meningkat, Kemenkes Khawatir">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Kasus positif Covid-19. (Detik.com-Grandyos Zafna)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Kasus Positif Covid-19 Indonesia Meningkat, Kemenkes Khawatir">Kasus Positif Covid-19 Indonesia Meningkat, Kemenkes Khawatir</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">news</a>|<a href="#" style="padding-left:7px;">3 jam lalu</a></div>
                  </div>
                </div>                                               
            </div>
          </div>
  
          <!-- Berita Video Wrapper-->
          <div class="video-wrapper">
            <div class="container">
              <div class="d-flex align-items-center justify-content-between">
                <h5 class="mb-0 pl-1 spos-title">Berita Video</h5>
              </div>
            </div>
            <div class="container">
              <div class="row spos-video-slides owl-carousel">
                            <div class="col-md-4 mt-3">
                  <div class="spos-v-card">
                    <a class="video-icon" href="#"><i class="lni lni-play"></i></a>
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Bus Mira Jurusan Jogja-Surabaya Nyungsep Parit 2 Meter di Ngawi" style="height: 220px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Bus Mira Jurusan Jogja-Surabaya Nyungsep Parit 2 Meter di Ngawi">Bus Mira Jurusan Jogja-Surabaya Nyungsep Parit 2 Meter di Ngawi</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 mt-3">
                  <div class="spos-v-card">
                    <a class="video-icon" href="#"><i class="lni lni-play"></i></a>
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Butuh 7 Jam Ambil Foto 49.000 Megapiksel di Hajar Aswad" style="height: 220px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Butuh 7 Jam Ambil Foto 49.000 Megapiksel di Hajar Aswad">Butuh 7 Jam Ambil Foto 49.000 Megapiksel di Hajar Aswad</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 mt-3">
                  <div class="spos-v-card">
                    <a class="video-icon" href="#"><i class="lni lni-play"></i></a>
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Jelang Lebaran, Polresta Solo Musnahkan Miras Hasil Operasi Pekat" style="height: 220px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Jelang Lebaran, Polresta Solo Musnahkan Miras Hasil Operasi Pekat">Jelang Lebaran, Polresta Solo Musnahkan Miras Hasil Operasi Pekat</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 mt-3">
                  <div class="spos-v-card">
                    <a class="video-icon" href="#"><i class="lni lni-play"></i></a>
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Ibu Katolik dan Ayah Buddha, Ini Cerita Dian Sastro Pilih Memeluk Islam" style="height: 220px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Ibu Katolik dan Ayah Buddha, Ini Cerita Dian Sastro Pilih Memeluk Islam">Ibu Katolik dan Ayah Buddha, Ini Cerita Dian Sastro Pilih Memeluk Islam</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
          <!-- Ads Mobile Home 6 -->
          <div class="iklan">
            <div class="iklan" align="center">
                <a href="" data-toggle="modal" data-target="#ModalMobileHome6">
                    <img class="img-fluid" src="{{ url('images/iklan-img/mobile-home-6.png') }}" style="object-fit: cover; object-position: center;">
                    </a>
            </div>
          </div>
  
          <!-- Blok 5 Terkini Wrapper-->
          <div class="terkini-wrapper">
            <div class="container">                         
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Mengakses Permodalan Perbankan Tak Sesulit yang Dibayangkan">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Webinar UMKM Virtual Expo 2021 hasil kerja sama Bank Indonesia Kantor Perwakilan Solo dengan Solopos  masuk pada sesi ke-3, Selasa (4/5/2021)." style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Mengakses Permodalan Perbankan Tak Sesulit yang Dibayangkan">Mengakses Permodalan Perbankan Tak Sesulit yang Dibayangkan</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">bisnis</a>|<a href="#" style="padding-left:7px;">5 jam lalu</a></div>
                  </div>
                </div>             
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Larangan Mudik Dimulai, Desa/Kelurahan Di Sukoharjo Diminta Buka Rumah Isolasi">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Ilustrasi mudik. (Solopos/Whisupaksa Kridhangkara)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Larangan Mudik Dimulai, Desa/Kelurahan Di Sukoharjo Diminta Buka Rumah Isolasi">Larangan Mudik Dimulai, Desa/Kelurahan Di Sukoharjo Diminta Buka Rumah Isolasi</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">soloraya</a>|<a href="#" style="padding-left:7px;">6 jam lalu</a></div>
                  </div>
                </div>              
                <!-- Terkini Post-->
                <div class="terkini-post d-flex">
                  <div class="post-thumbnail">
                    <a href="{{ url("/mediakit/single") }}" title="Niat Cuci Tangan, Kakek-Kakek di Tegal Malah Hanyut di Irigasi">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Tim SAR gabungan tengah mengevakuasi jenazah balita berusia 4 tahun yang hanyut di saluran irigasi di Wonosobo, Sabtu (21/12/2019). (Semarangpos.com-Humas Basarnas Jateng)" style="object-fit: cover; height: 100px; width: 100px;">
                    </a>
                  </div>
                  <div class="post-content">
                    <a class="post-title" href="{{ url("/mediakit/single") }}" title="Niat Cuci Tangan, Kakek-Kakek di Tegal Malah Hanyut di Irigasi">Niat Cuci Tangan, Kakek-Kakek di Tegal Malah Hanyut di Irigasi</a>
                    <div class="post-meta d-flex align-items-center">
                      <a href="">jateng</a>|<a href="#" style="padding-left:7px;">6 jam lalu</a></div>
                  </div>
                </div>                
            </div>
          </div>
  
          <!-- Tabs News Wrapper-->
          <div class="tabs-news-wrapper bg-gray">
            <div class="container">
              <nav>
                <!-- Nav Tabs-->
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-jateng-tab" href="#nav-jateng" data-toggle="tab" role="tab" aria-controls="nav-jateng" aria-selected="true">Jateng</a>
                  <a class="nav-item nav-link" id="nav-jatim-tab" href="#nav-jatim" data-toggle="tab" role="tab" aria-controls="nav-jatim" aria-selected="false">Jatim</a>
                  <a class="nav-item nav-link" id="nav-jogja-tab" href="#nav-jogja" data-toggle="tab" role="tab" aria-controls="nav-jogja" aria-selected="false">Jogja</a>
                </div>
              </nav>
              <!-- Tabs Content-->
              <div class="tab-content" id="nav-tabContent">
  
                <!-- Single Tab Pane-->
                <div class="tab-pane fade show active" id="nav-jateng" role="tabpanel" aria-labelledby="nav-jateng-tab">
                                                     
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Wali Kota Salatiga, Yuliyanto, mengisi Lokakarya Keluarga Berencana di Aula Kantor Pemerintah Kecamatan Argomulyo, Senin (5/4/2021). (Semarangpos.com-Humas Pemkot Salatiga)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Koalisi DMFI Apresiasi Wali Kota Salatiga yang Larang Peredaran Daging Anjing">Koalisi DMFI Apresiasi Wali Kota Salatiga yang Larang Peredaran Daging Anjing</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">44 menit lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Ilustrasi penolakan konsumsi daging anjing (Solopos)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Pengumuman! Wali Kota Salatiga Larang Peredaran Daging Anjing">Pengumuman! Wali Kota Salatiga Larang Peredaran Daging Anjing</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">1 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Pohon Asam (Instagram/@fennieekawati)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Ada Kisah Pohon Asam di Balik Nama Kota Semarang">Ada Kisah Pohon Asam di Balik Nama Kota Semarang</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">2 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Bajaj yang digunakan Didik untuk mudik dari Jakarta menuju Cepogo, Boyolali. (Solopos.com/Imam Yuda Saputra)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Rindu Lebaran di Kampung Halaman, Warga Boyolali Nekat Mudik Pakai Bajaj">Rindu Lebaran di Kampung Halaman, Warga Boyolali Nekat Mudik Pakai Bajaj</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">2 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Petugas tengah menghentikan kendaraan yang melintas di exit tol Pejagan, Brebes, Selasa (4/5/2021). (Humas Pemprov Jateng)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Hari ini Bakal Jadi Puncak Arus Mudik, Ganjar Minta Penyekatan Diperketat">Hari ini Bakal Jadi Puncak Arus Mudik, Ganjar Minta Penyekatan Diperketat</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">3 jam lalu</a>
                      </div>
                    </div>
                  </div>
                              </div>
  
                <!-- Single Tab Pane-->
                <div class="tab-pane fade" id="nav-jatim" role="tabpanel" aria-labelledby="nav-jatim-tab">
                                                     
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Bus Mira terguling dan masuk parit di Ngawi. (Beritajatim.com)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Bus Mira Jurusan Jogja-Surabaya Nyungsep Parit 2 Meter di Ngawi">Bus Mira Jurusan Jogja-Surabaya Nyungsep Parit 2 Meter di Ngawi</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">4 menit lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Calon penumpang KA melakukan tes GeNose C19 di Stasiun Madiun, Selasa (4/5/2021). (Solopos.com/Abdul Jalil)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Ada Lima KA Beroperasi Selama Larangan Mudik, Khusus Untuk Kalangan Ini">Ada Lima KA Beroperasi Selama Larangan Mudik, Khusus Untuk Kalangan Ini</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">4 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Polisi melakukan olah TKP (Detik.com-dok. Istimewa)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Perawat Cantik Dibakar di Malang, Polisi Buru Pelaku">Perawat Cantik Dibakar di Malang, Polisi Buru Pelaku</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">14 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Tangkapan layar pria yang menghujat pengunjung mal yang mengenakan masker." style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Viral! Video Pria Membodohkan Pengunjung Mal Bermasker">Viral! Video Pria Membodohkan Pengunjung Mal Bermasker</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">17 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Perawat cantik bernama Eva Sofiana dibakar orang tak dikenal. (Instagram/@evasofiana2510)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Miris! Perawat Cantik di Malang Dibakar Pria Tak Dikenal">Miris! Perawat Cantik di Malang Dibakar Pria Tak Dikenal</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">21 jam lalu</a>
                      </div>
                    </div>
                  </div>
                              </div>
  
                <!-- Single Tab Pane-->
                <div class="tab-pane fade" id="nav-jogja" role="tabpanel" aria-labelledby="nav-jogja-tab">
                                                     
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Ketua Gugus Tugas Percepatan Penanganan COVID-19 Kulon Progo Fajar Gegana. (Antara)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Tok! Pemkab Kulonprogo Larangan Salat Idulfitri Berjemaah di Lapangan">Tok! Pemkab Kulonprogo Larangan Salat Idulfitri Berjemaah di Lapangan</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">2 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Foto Nani Aprilliani Nurjaman, 25, memakai daster di tahanan viral di media sosial. (istimewa/detik.com)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="{{ url("/mediakit/single") }}" title="Ini Pelajaran dan Fakta dari Kasus Takjil Beracun Sianida di Bantul">Ini Pelajaran dan Fakta dari Kasus Takjil Beracun Sianida di Bantul</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">3 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Harian Jogja/ Jumali: Nani Apriliani Nurjaman, 25, alias Tika tertunduk menyesali tindakan bodohnya, di Aula Wirapratama, Polres Bantul, Senin (3/5/2021). Tersangka asal Dusun Sukaasih, Desa Buniwangi, Kecamatan Palasah, Kabupaten Majalengka, Jawa Barat itu terancam hukuman mati dalam tragedi satai beracun.  " style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="#" title="Kasus Sate Beracun di Bantul: Aiptu Tomi dan Nani Apriliani Sudah Nikah Siri">Kasus Sate Beracun di Bantul: Aiptu Tomi dan Nani Apriliani Sudah Nikah Siri</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">22 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Harian Jogja/ Jumali: Nani Apriliani Nurjaman, 25, alias Tika tertunduk menyesali tindakan bodohnya, di Aula Wirapratama, Polres Bantul, Senin (3/5/2021). Tersangka asal Dusun Sukaasih, Desa Buniwangi, Kecamatan Palasah, Kabupaten Majalengka, Jawa Barat itu terancam hukuman mati dalam tragedi satai beracun.  " style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="#" title="Memburu Pembisik Tragedi Takjil Beracun Bantul">Memburu Pembisik Tragedi Takjil Beracun Bantul</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">23 jam lalu</a>
                      </div>
                    </div>
                  </div>
                               
                  <!-- Single News Post-->
                  <div class="single-news-post d-flex align-items-center">
                    <div class="post-thumbnail">
                      <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Nani Apriliani, 25, pelaku pengirim satai beracun yang menewaskan Naba Faiz Prasetya, 9, saat dihadirkan di hadapan awak media di Mapolres Bantul, Senin (3/5/2021).(Jumali/Harian Jogja)" style="object-fit: cover; height: 70px; width: 70px;">
                    </div>
                    <div class="post-content">
                      <a class="post-title" href="#" title="Ini Dia Aiptu Tomi, Anggota Polresta Yogyakarta yang Jadi Target Sate Beracun">Ini Dia Aiptu Tomi, Anggota Polresta Yogyakarta yang Jadi Target Sate Beracun</a>
                      <div class="post-meta d-flex align-items-center">
                        <a href="#">1 hari lalu</a>
                      </div>
                    </div>
                  </div>
                              </div>
              </div>
            </div>
          </div>
        </div>

        </div>
      </div>

  </div>

  <div class="container">
    <div class="border-top"></div>
</div>

<!-- Footer Nav-->
<div class="footer-nav-area" id="footerNav">
  <div class="spos-footer-nav h-100">
    <ul class="h-100 d-flex align-items-center justify-content-between">
      <li>
          <a href="/videos">
              <img src="{{ asset('images/icon_live.png') }}" width="32px" style="margin-top:10px;">
          </a>
      </li>
      <li>
          <a href="/terpopuler">
              <img src="{{ asset('images/icon_trending.png') }}"><!--i class="lni lni-bolt-alt"></i--><span>TRENDING</span>
          </a>
      </li>
      <li>
          <a href="/premium">
              <img src="{{ asset('images/icon_premium.png') }}"><!--i class="lni lni-star-filled"></i--><span>PREMIUM</span>
          </a>
      </li>
      <li>
          <a href="/soloraya">
              <img src="{{ asset('images/icon_soloraya.png') }}" width="32px"><!--i class="lni lni-shortcode"></i--><span>SOLORAYA</span> 
          </a>
      </li>          
      <li>
          <a href="/arsip">
              <img src="{{ asset('images/icon_index.png') }}" width="32px"><!--i class="lni lni-archive"></i--><span>INDEKS</span>
          </a>
      </li>
    </ul>
  </div>
</div>

<div id="footer">
  <div class="footer" data-component="mobile:footer" data-component-name="mobile:footer">
    <div class="footer__nav-wrapper">
      <div class="footer__logo">
        <img class="footer-logo-image" style="filter: grayscale(100%);" src="{{ asset('images/logo-solopos.png') }}" alt="Solopos.com">
      </div>
      
      <ul class="footer--nav-list js-footer-nav-list" data-component-name="mobile:footer:nav-list">
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/about-us" title="Tentang Kami">Tentang Kami</a>
        </li>   
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/copyright" title="Copyright">Copyright</a>
        </li>                     
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/kontak" title="Kontak">Kontak</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/mediakit" title="Media Kit">Media Kit</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/about-us" title="Redaksi">Redaksi</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/code-of-conduct" title="Pedoman Media Siber">Pedoman Media Siber</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/loker" title="Karir">Karir</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/page/privacy-policy" title="Privacy Policy">Privacy Policy</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/cekfakta" title="Metode Cek Fakta">Cek Fakta</a>
        </li>
        <li class="footer--nav-list__item">
          <a class="footer--nav-list__link" href="/info-iklan" title="Info Iklan">Info Iklan</a>
        </li>
      </ul>

      <div class="footer__copyright">
        <p> Copyright © 2021 <a href="https://www.solopos.com" class="footer__copyright-link">solopos.com</a><br>Media Informasi & Inspirasi. All Rights Reserved </p>
      </div>
    </div>
  </div>
</div>

@include('includes.navbar-section')

  <!-- Modal Mobile Home 1-->
  <div class="modal fade" id="ModalMobileHome1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Home 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan (Dibawah Widget Espos Premium, Diatas Berita Terpopuler)<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Home 2-->
  <div class="modal fade" id="ModalMobileHome2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Home 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan (Dibawah Berita Terkini)<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Home 3-->
  <div class="modal fade" id="ModalMobileHome3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Home 3 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan (Diantara Feed Berita Menu FOKUS, Setelah Feed Berita Ke-4 )<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Home 4-->
  <div class="modal fade" id="ModalMobileHome4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Home 4 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan (Dibawah KOLOM)<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Home 5-->
  <div class="modal fade" id="ModalMobileHome5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Home 5 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan (Dibawah Espospedia/Infografis)<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Home 6-->
  <div class="modal fade" id="ModalMobileHome6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Home 6 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan (Dibawah Berita Video, Setelah Feed Berita Pertama)<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Artikel 1-->
  <div class="modal fade" id="ModalMobileArtikel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Artikel 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single/Konten (Kecuali Premium) Didalam artikel Setelah Paragraf Ke-2<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Artikel 2-->
  <div class="modal fade" id="ModalMobileArtikel2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Artikel 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single/Konten (Kecuali Premium) Didalam artikel Setelah Paragraf Ke-7<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Artikel 3-->
  <div class="modal fade" id="ModalMobileArtikel3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Artikel 3 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single/Konten Diakhir artikel/Setelah Paragrah Terakhir<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  
  <!-- Modal Mobile Under Artikel-->
  <div class="modal fade" id="ModalMobileUnderArtikel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Under Artikel Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single/Konten (Kecuali Premium) Dibawah Artikel/Konten<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Single 1-->
  <div class="modal fade" id="ModalMobileSingle1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Single 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single ( Diatas Bagian Berita Terpopuler )<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Single 2-->
  <div class="modal fade" id="ModalMobileSingle2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Single 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single ( Diatas Bagian Berita Terkini )<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Single 3-->
  <div class="modal fade" id="ModalMobileSingle3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Single 3  Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Single ( Bagian Berita Terkini setelah  Berita Ke-7 )<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Single 4-->
  <div class="modal fade" id="ModalMobileSingle4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Single 4 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan (Dibawah Berita Terpopuler)<br>
          <b>Rekomendasi Ukuran : </b>320 x 50 pixel, 320 x 100 pixel, 300 x 300 pixel, 300 x 250 pixel, 336 x 280 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Widget-->
  <div class="modal fade" id="ModalMobileWidget" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Widget Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Dibawah Konten / Artikel (Dalam Bentuk feed/list berita)
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Promo -->
  <div class="modal fade" id="ModalMobilePromo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Mobile Promo & Event Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Home/Depan & Artikel, Banner Slide<br>
          <b>Rekomendasi Ukuran : </b>340 x 100 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Mobile Advetorial-->
  <div class="modal fade" id="ModalMobileAdvetorial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Advetorial</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Iklan berita yang disisipkan selalu tampil di halaman awal (HOMEPAGE) selama rentang waktu tertentu.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

<!-- All JavaScript Files-->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="{{ asset('js/waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/jquery.animatedheadline.min.js') }}"></script>
<script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/default/date-clock.js') }}"></script>
<script src="{{ asset('js/default/dark-mode-switch.js') }}"></script>
<script src="{{ asset('js/default/active.js') }}"></script>

</body>
</html>