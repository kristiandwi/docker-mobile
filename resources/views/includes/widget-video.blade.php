      <!-- Solopos TV-->
      <div class="video-wrapper">
        <div class="container">
          <div class="d-flex align-items-center justify-content-between">
            <h5 class="mb-0 pl-1 spos-title">Berita Video</h5><a class="btn btn-primary btn-sm" href="https://m.solopos.com/videos">View All</a>
          </div>
        </div>
        <div class="container">
          <div class="row spos-video-slides owl-carousel">
            @php $no=1; @endphp
            @foreach($video as $vid)
            @if($no <= 5 )
            <div class="col-md-4 mt-3">
              <div class="spos-v-card">
                <a class="video-icon" href="{{ url("/{$vid['slug']}-{$vid['id']}") }}?utm_source=video_mobile" title="{{ $vid['title'] }}"><i class="lni lni-play"></i></a>
                <div class="post-thumbnail">
                  <img loading="lazy" src="{{ $vid['images']['thumbnail'] }}" alt="{{ $vid['title'] }}" style=" height: 220px;">
                </div>
                <div class="post-content">
                  <a class="post-title" href="{{ url("/{$vid['slug']}-{$vid['id']}") }}?utm_source=video_mobile" title="{{ $vid['title'] }}">{{ $vid['title'] }}</a>
                </div>
              </div>
            </div>
            @endif
            @php $no++; @endphp
            @endforeach
          </div>
        </div>
      </div> 