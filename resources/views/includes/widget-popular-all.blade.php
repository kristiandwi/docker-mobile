<div class="mt-5">
    <!-- Item Slides Wrapper-->
    <div class="container">
    <div class="d-flex align-items-center justify-content-between mb-3">
        <h5 class="mb-0 pl-1 spos-title">Berita Terpopular</h5><a class="btn btn-primary btn-sm" href="{{ url('/') }}/terpopuler">Indeks Berita</a>
    </div>
    </div>
    <div class="item-wrapper mb-3">
    <div class="container">
        <!-- Catagory Slides-->
        <div id="populars" class="item-slides owl-carousel">
        
        </div>
    </div>
    </div>
</div>
@push('custom-scripts')
<script>
    function timeSince(date) {
    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = seconds / 31536000;

    if (interval > 1) {
    return Math.floor(interval) + " tahun yang lalu";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
    return Math.floor(interval) + " bulan yang lalu";
    }
    interval = seconds / 86400;
    if (interval > 1) {
    return Math.floor(interval) + " hari yang lalu";
    }
    interval = seconds / 3600;
    if (interval > 1) {
    return Math.floor(interval) + " jam yang lalu";
    }
    interval = seconds / 60;
    if (interval > 1) {
    return Math.floor(interval) + " menit yang lalu";
    }
    return Math.floor(seconds) + " detik yang lalu";
}

$(document).ready(function() {
        $.ajax({
        type: "GET",
        url: "https://tf.solopos.com/api/v1/stats/popular/all/",
        dataType: "JSON",
        success: function(data) {
            var populars = $("#populars");

            $.each(data.data, function(i, item) {
                if(i < 15) {
                    populars.append("<div class=\"item\"><a href=\"" + item['post_slug'] + "-" + item['post_id'] + "?utm_source=bacajuga_desktop\" title=\"" + item['post_title'] + "\"><div class=\"item-thumbnail\"><img src=\"" + item['post_thumb'] +"\" alt=\"" + item['post_title'] + "\" loading=\"lazy\" style=\"object-fit: cover; width: 195px; height: 115px;\"></div><div class=\"item-caption\"><span class=\"link-title\">" + item['post_title'] + "</span></div></a></div>");
                }
            });
        }
    });
});
</script>
@endpush

{{-- <div class="mt-5">
    <!-- Item Slides Wrapper-->
     <div class="container">
       <div class="d-flex align-items-center justify-content-between mb-3 mt-3">
         <a class="btn btn-primary btn-sm" href="/arsip">Berita Terpopular</a>
       </div>
     </div>       
     <div class="item-wrapper mb-3">       
       <div class="container">
         <!-- Catagory Slides-->
         <div class="item-slides owl-carousel">   
           @php $no = 1; @endphp
           @foreach ($popular as $item)
           @if($no <=12)                      
           <div class="item">
             <a href="{{ url("/{$item['post_slug']}-{$item['post_id']}") }}" title="{{ html_entity_decode($item['post_title']) }}">
               <div class="item-thumbnail">
                 <img src="{{ $item['post_thumb'] }}" loading="lazy" alt="{{ html_entity_decode($item['post_title']) }}" style="object-fit: cover; width: 195px; height: 115px;">
               </div> 
               <div class="item-caption">
                 <span class="link-title">{{ html_entity_decode($item['post_title']) }}</span>
               </div>
             </a>
           </div> 
           @endif
           @php $no++ @endphp
           @endforeach                                                          
         </div>
       </div>
     </div>  
</div> --}}