    @include('includes.ads.top-swipe')
    <!-- Header Area-->
    <div id="sticky1-sticky-wrapper" class="header-area is-ts-sticky">
        <div class="container h-100 d-flex align-items-center justify-content-between">

          <div class="navbar--toggler" id="sposNavbarToggler">
            <a href="#">
              <i class="lni lni-grid-alt" style="font-size: 1.45rem; font-weight: bold; display: block; color: #00437c; margin-top: 2px; margin-left: 3px;"></i>
            </a>
          </div>

          @if(!empty($is_bob) AND $is_bob == 'yes')
          @include('includes.tematik.header-bob')

		      @elseif(!empty($is_uksw) AND $is_uksw == 'yes')
          @include('includes.tematik.header-uksw')

          @elseif(!empty($is_bugar) AND $is_bugar == 'yes')
          @include('includes.tematik.header-bugar')

          @elseif(!empty($is_ubah) AND $is_ubah == 'yes')
          @include('includes.tematik.header-ubahlaku')

          @else
          <!-- Logo-->
          <div class="logo-wrapper">
            @if(!empty($if_regional))
            <a href="{{ url('/') }}/jateng/{{ $regional_name }}">
              <img loading="lazy" class="logo-image" src="{{ asset('images/logo-') }}{{ $regional_name }}.png" alt="Logo Regional">
            </a>
            {{-- @elseif(!empty($premium_content) AND $premium_content == 'premium')
            <a href="{{ url('/plus') }}">
              <img loading="lazy" style="width: auto; height:27px;" src="{{ asset('images/plus.png') }}" alt="Espos Plus">
            </a> --}}
            @elseif (isset($category) AND $category == 'sekolah')
            <a href="{{ url('/sekolah') }}">
                <img loading="lazy" style="width: auto; height:27px;" src="{{ asset('images/logo-sekolah-solopos.png') }}" alt="Sekolah">
              </a>
            @else
            <a href="{{ url('/') }}">
              <img loading="lazy" class="logo-image" src="{{ asset('images/logo.png') }}" alt="Solopos.com">
            </a>
            @endif
          </div>
          @endif

          <div class="navbar--toggler" id="userNavbarToggler">
            <a href="#">
              @if(Cookie::get('is_login') == 'true')
              <i class="fa fa-user-plus" style="font-size: 1.75rem; display: block; color: #00437c; margin-right: 3px;"></i>
              @else
              <i class="fa fa-user-circle-o" style="font-size: 1.75rem; display: block; color: #00437c; margin-right: 3px;"></i>
              @endif
            </a>
          </div>
          
          {{-- <div class="spos-story">
            <div class="spos-icon">
              <a href="https://m.solopos.com/story" title="Solopos Story">
                <img loading="lazy" src="{{ asset('images/icon.png') }}">
              </a>
            </div>
          </div> --}}

        </div>
      </div>
