@if( date('Y-m-d H:i:s') >= '2022-03-28 09:00:01' && date('Y-m-d H:i:s') <= '2022-04-10 23:59:59')
    <!-- Item Slides Wrapper-->
    <div class="container">
    <a href="https://m.solopos.com/tag/ekspedisi-kuliner-solo" title="Ekspedisi Kuliner Solo" target="_blank">
        <img loading="lazy" src="https://cdn.solopos.com/banner/widget/widget-kuliner-solo.jpg" alt="Ekspedisi Kuliner Solo" style="display: inline-block; width: 100%;">
    </a>
    </div>
    <div class="item-wrapper mt-3">       
      <div class="container">
        <!-- Catagory Slides-->
        <div class="item-slides owl-carousel">   
          @php $no = 1; @endphp
          @foreach ($widget2 as $item)
          @if($no <=10)                      
          <div class="item">
          <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">
              <div class="item-thumbnail">
                <img loading="lazy" src="{{ $item['featured_image']['thumbnail'] }}" loading="lazy" alt="{{ html_entity_decode($item['title']) }}" style="object-fit: cover; width: 195px; height: 115px;">
              </div> 
              <div class="item-caption">
                <span class="link-title" style="color: rgb(143 3 57);">{{ html_entity_decode($item['title']) }}</span>
              </div>
            </a>
          </div> 
          @endif
          @php $no++ @endphp
          @endforeach                                                          
        </div>
      </div>
    </div>
    {{--
    <div class="container">
      <a href="https://m.solopos.com/tag/ekspedisi-pendidikan-2022" target="_blank">
        <img src="https://cdn.solopos.com/banner/widget/Ekspedisi_Pendidikan.png">
      </a>
    </div> --}}
    @endif 