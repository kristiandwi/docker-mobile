  <div class="iklan mt-3 mb-3" align="center">
    @if( date('Y-m-d H:i:s') >= '2021-09-24 00:00:01' && date('Y-m-d H:i:s') <= '2021-09-30 23:59:59') 
    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
      googletag.defineSlot('/54058497/mitsubishi-300x250', [300, 250], 'div-gpt-ad-1632396452017-0').addService(googletag.pubads());
      googletag.pubads().enableSingleRequest();
      googletag.enableServices();
    });
    </script>
    <!-- /54058497/mitsubishi-300x250 -->
    <div id='div-gpt-ad-1632396452017-0' style='min-width: 300px; min-height: 250px;'>
      <script>
      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1632396452017-0'); });
      </script>
    </div>
    @else 
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
      <!-- Iklan Responsif -->
      <ins class="adsbygoogle"
          style="display:block"
          data-ad-client="ca-pub-4969077794908710"
          data-ad-slot="2921244965"
          data-ad-format="auto"
          data-full-width-responsive="true"></ins>
      <script>
          (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
    @endif            
  </div>