        @if( date('Y-m-d H:i:s') >= '2022-04-08 00:00:01' && date('Y-m-d H:i:s') <= '2022-06-23 23:59:59') 
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/Mobile-Under-Artikel', [[320, 90], [320, 100], [320, 50], [336, 280], [300, 300], [300, 250]], 'div-gpt-ad-1644561969280-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
        </script>
        <!-- /54058497/Mobile-Under-Artikel -->
        <div id='div-gpt-ad-1644561969280-0' style='min-width: 300px; min-height: 50px;'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1644561969280-0'); });
        </script>
        </div><br>
        
        @elseif ( date('Y-m-d H:i:s') >= '2022-03-28 09:00:01' && date('Y-m-d H:i:s') <= '2022-04-03 23:59:59')
			<a href="https://m.solopos.com/tag/ekspedisi-kuliner-solo" target="_blank"><img loading="lazy" src="https://cdn.solopos.com/banner/widget/MR_Eksepedisi_Kuliner_Solo.jpg"></a>
            
        @elseif( date('Y-m-d H:i:s') >= '2022-01-08 19:00:01' && date('Y-m-d H:i:s') <= '2022-10-05 23:59:59') 
        <iframe width="360" height="202" data-src="https://www.youtube.com/embed/8TAUUsNSWak?rel=0&amp;autoplay=1&mute=1" title="Presiden & Tokoh: Solopos Harus Jadi Panduan Informasi Terpercaya | 25 Tahun Solopos Media Group" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
        <script>
            function init() {
            var vidDefer = document.getElementsByTagName('iframe');
            for (var i=0; i<vidDefer.length; i++) {
            if(vidDefer[i].getAttribute('data-src')) {
            vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
            } } }
            window.onload = init;		
        </script>        

        @else
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-ad-format="auto"
            data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
		@endif

        @if( date('Y-m-d H:i:s') >= '2021-12-17 00:00:01' && date('Y-m-d H:i:s') <= '2021-12-23 23:59:59')
        <br>
        <!-- datautama 25/11 - 09/12 -->
        <a href="https://datautama.net.id/"><img loading="lazy" src="{{ ('./banner/datautama.jpg') }}"></a>
        @endif

        @if( date('Y-m-d H:i:s') >= '2021-12-23 19:00:01' && date('Y-m-d H:i:s') <= '2022-07-07 23:59:59') 
                <video width="100%" autoplay loop controls muted>
				<source src="https://cdn.solopos.com/iklan/DSC2022/Dsc22_Short.mp4" type="video/mp4">
				Your browser does not support the video.
				</video>
        @endif 