<div class="iklan mt-3" align="center">
  @if( date('Y-m-d H:i:s') >= '2022-09-19 00:00:01' && date('Y-m-d H:i:s') <= '2022-09-25 23:59:59')
    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <script>
      window.googletag = window.googletag || {cmd: []};
      googletag.cmd.push(function() {
        googletag.defineSlot('/54058497/Mobile-Home-3', [[336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1637827601448-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>
    <!-- /54058497/Mobile-Home-3 -->
    <div id='div-gpt-ad-1637827601448-0' style='min-width: 300px; min-height: 250px;'>
      <script>
        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1637827601448-0'); });
      </script>
    </div>
    {{-- <a href="https://m.solopos.com/tts-spesial-hut-solopos-berhadiah-total-rp8-juta-netizen-bisa-ikutan-1157129" target="_blank">
      <img src="{{ url('banner/tts-jumbo.jpg') }}">
    </a> --}}
    @elseif ( date('Y-m-d H:i:s') >= '2022-06-22 00:00:01' && date('Y-m-d H:i:s') <= '2022-07-21 23:59:59')
        <a href="https://m.solopos.com/tag/sbmptn-2022" target="_blank" title="SBMPTN 2022"><img loading="lazy" src="https://cdn.solopos.com/banner/info-mahasiswa-baru-336x280.gif?v={{time()}}"  alt="SBMPTN 2022"></a>
    @else
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
    <!-- Iklan Responsif -->
    <ins class="adsbygoogle"
        style="display:block"
        data-ad-client="ca-pub-4969077794908710"
        data-ad-slot="2921244965"
        data-ad-format="auto"
        data-full-width-responsive="true"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
    @endif          
  </div>