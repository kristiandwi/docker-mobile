@if( date('Y-m-d H:i:s') >= '2022-03-16 22:00:01' && date('Y-m-d H:i:s') <= '2022-03-17 23:59:59')
<div class="single-hero-slide">
  <div class="headline-image d-flex align-items-start">
    <a class="post-title d-block" href="https://m.solopos.com/ringkasan-laporan-penyelenggaraan-pemerintahan-daerah-klaten-2021-1274582" title="Ringkasan Laporan Penyelenggaraan Pemerintahan Daerah Klaten 2021">
    <img src="https://images.solopos.com/2022/03/Cover-RLPPD-Klaten-2021.png" loading="lazy">
    </a>
  </div>
  <div class="d-flex align-items-end">
    <div class="container-fluid mb-3">
      <div class="post-meta d-flex align-items-center">
        <a href="">Klaten</a> | <span style="margin-left: 5px;">17 Maret 2022</span>
      </div>                  
      <a class="post-title d-block" href="https://m.solopos.com/ringkasan-laporan-penyelenggaraan-pemerintahan-daerah-klaten-2021-1274582" title="Ringkasan Laporan Penyelenggaraan Pemerintahan Daerah Klaten 2021">Ringkasan Laporan Penyelenggaraan Pemerintahan Daerah Klaten 2021</a>
    </div>
  </div>
</div>
@endif