<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('.hidden').slideDown(1500);
        $(".btn1").click(function(){
            $(".swipe-up-ad").slideUp();
        });
    });
</script>
<style>
.hidden {
    display: none;
}
/* swipe-up-ad */
.swipe-up-ad {
    height: 300px;
    width: 100%;
    margin: 0px auto;
    position: relative;
}

.swipe-up-ad p {
    margin: 5px 0;
    color: white;
    font-size: 8px;
    line-height: 18px;
    text-align: center;
    font-family: Arial, Helvetica, sans-serif;
}

.swipe-up-ad .inner-swipe-up-ad {
    clip: rect(auto, auto, auto, auto);
    height: 100%;
    position: absolute;
    width: 100%;
}

.swipe-up-ad .img-swipe-up-ad {
    height: 300px;
    margin: 0 auto 0 auto;
    position: fixed;
    top: 50px;
    -webkit-transform: translateZ(0px);
    transform: translateZ(0px);
    width: 100%;
    text-align: center;
}
.img-swipe-up-ad img {
    overflow-y: hidden;
    border: medium none;
    height: auto;
    width: 100%!important;
}
.swipe-up-ad-dfp {
    height: 420px;
    width: 100%;
    margin: 0px auto;
    position: relative;
}

.swipe-up-ad-dfp p {
    margin: 5px 0;
    color: white;
    font-size: 8px;
    line-height: 18px;
    text-align: center;
    font-family: Arial, Helvetica, sans-serif;
}

.swipe-up-ad-dfp .inner-swipe-up-ad-dfp {
    clip: rect(auto, auto, auto, auto);
    height: 100%;
    position: absolute;
    width: 100%;
}

.swipe-up-ad-dfp .img-swipe-up-ad-dfp {
    height: 420px;
    margin: 0 auto 0 auto;
    position: fixed;
    top: 50px;
    -webkit-transform: translateZ(0px);
    transform: translateZ(0px);
    width: 100%;
    text-align: center;
}
.img-swipe-up-ad-dfp img {
    overflow-y: hidden;
    border: medium none;
    height: auto;
    width: 100%!important;
}
/* end swipe-up-ad */
.adsPushdown__container{top:0;width:100%;position:relative;z-index:0;background:#212121;height:480px;overflow:hidden}
.adsPushdown__container_dfp{top:0;width:100%;position:relative;z-index:0;background:#212121;height:420px;overflow:hidden}
.adsPushdown__big{width:360px;height:480px;left:0;right:0;margin:0 auto}
.adsPushdown__big_dfp{width:336;height:420;left:0;right:0;margin:0 auto}
.adsPushdown__swipe{position:relative;top:0;width:100%;height:20px;overflow:hidden;color:#fff;line-height:2;font-size:10px;font-weight:400;text-align:center;letter-spacing:.6px;background:#00437d;text-transform:uppercase}
.adsPushdown__inner{position:fixed;top:0;left:0;right:0;margin:0 auto;width:360px;height:480px}
.adsPushdown__inner_dfp{position:fixed;top:0;left:0;right:0;margin:0 auto;width:336px;height:420px}
.adsPushdown__inner:before{content:"Advertisement";position:absolute;z-index:-1;left:0;top:0;width:100%;height:100%;font:400 11px/1.4 Open sans,sans-serif;color:#c8ccce;display:flex;justify-content:center;align-items:center;background-color:#212121}
.adsPushdown__inner_dfp:before{content:"Advertisement";position:absolute;z-index:-1;left:0;top:0;width:100%;height:100%;font:400 11px/1.4 Open sans,sans-serif;color:#c8ccce;display:flex;justify-content:center;align-items:center;background-color:#212121}
.adsPushdown__inner.top{transform:translateY(50px)}
.adsPushdown__close{right:5px;z-index:99;width:48px;height:22px;overflow:hidden;position:absolute;top:5px;background:#00437d;text-transform:uppercase;color:#fff;font-size:11px;font-weight:700;letter-spacing:.5px;padding:3px 5px;cursor:pointer}
.adsPushdown~header{background:#fcfcfc}
.adsPushdown~header .header2_detail_inject{position:relative;margin-top:0}
.adsPushdown~.wrapper-description{z-index:1;margin-top:0;padding-top:60px}
.adsPushdown~#div-gpt-ad-mr2,.adsPushdown~#div-gpt-ad-oop1,.adsPushdown~.bodyWrap,.adsPushdown~.footer,.adsPushdown~.row .col-sm-12 .wrapper-list-news,.adsPushdown~.wrapper-description{position:relative;background:#fcfcfc}
</style>

{{-- @if( date('Y-m-d H:i:s') >= '2022-06-07 00:00:01' && date('Y-m-d H:i:s') <= '2022-06-17 23:59:59')
    <div class="swipe-up-ad-dfp hidden">    
        <div class="inner-swipe-up-ad-dfp">
            <div class="img-swipe-up-ad-dfp">
                <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
                <script>
                window.googletag = window.googletag || {cmd: []};
                googletag.cmd.push(function() {
                    googletag.defineSlot('/54058497/Mobile-Top-Swipe-Banner', [[336, 420], [336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1639723800334-0').addService(googletag.pubads());
                    googletag.pubads().enableSingleRequest();
                    googletag.enableServices();
                });
                </script>
                <!-- /54058497/Mobile-Top-Swipe-Banner -->
                <div id='div-gpt-ad-1639723800334-0' style='min-width: 300px; min-height: 250px;'>
                <script>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1639723800334-0'); });
                </script>
                </div>
            </div>
        </div>
    </div> --}}
    {{-- @endif --}}

{{-- @if( date('Y-m-d H:i:s') >= '2022-04-21 07:00:01' && date('Y-m-d H:i:s') <= '2022-04-22 06:59:59')
<div class="container pl-0 pr-0" style="background: url(https://cdn.solopos.com/iklan/live-mobile.png);background-size: 100% 100%;min-height:360px;position:relative;">
    <a href="https://www.youtube.com/watch?v=ZjYKWIVo3tw" target="_blank" title="Live Solopos">
        <div style="width:100%;height:155px;position:absolute;left:0;top:0;z-index:2;">
            <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
            <script>
            window.googletag = window.googletag || {cmd: []};
            googletag.cmd.push(function() {
                googletag.defineSlot('/54058497/Mobile-LIVE', [360, 155], 'div-gpt-ad-1650350780904-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
            </script>
            <!-- /54058497/Mobile-LIVE -->
            <div id='div-gpt-ad-1650350780904-0' style='min-width: 360px; min-height: 155px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1650350780904-0'); });
            </script>
            </div>
        </div>
    </a>
    <div style="width:336px;height:192px;position:absolute;left:0;right:0;margin:auto;bottom:10px;z-index:3;">
        <div class="streaming-mobile">                            
            <!-- 1. The <iframe> (video player) will replace this <div> tag. -->
            <div class="iframe-container">
              <div id="player"></div>
            </div>
            <script>
              // 2. This code loads the IFrame Player API code asynchronously.
              var tag = document.createElement('script');
        
              tag.src = "https://www.youtube.com/iframe_api";
              var firstScriptTag = document.getElementsByTagName('script')[0];
              firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        
              // 3. This function creates an <iframe> (and YouTube player)
              //    after the API code downloads.
              var player;
              function onYouTubeIframeAPIReady() {
                player = new YT.Player('player', {
                  width: '100%',
                  videoId: 'ZjYKWIVo3tw',
                  playerVars: { 'autoplay': 1, 'playsinline': 1 },
                  events: {
                    'onReady': onPlayerReady
                  }
                });
              }
        
              // 4. The API will call this function when the video player is ready.
              function onPlayerReady(event) {
                 event.target.mute();
                event.target.playVideo();
              }
            </script>
        
            <style>
            /* Make the youtube video responsive */
              .iframe-container{
                position: relative;
                width: 100%;
                padding-bottom: 56.25%;
                height: 0;
              }
              .iframe-container iframe{
                position: absolute;
                top:0;
                left: 0;
                width: 100%;
                height: 100%;
              }
            </style>
        </div>        
         <iframe width="336" height="192" src="https://www.youtube.com/embed/ZjYKWIVo3tw?&autoplay=1&mute=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
    </div>
</div>    --}}
{{-- @elseif ( date('Y-m-d H:i:s') >= '2022-04-29 22:00:01' && date('Y-m-d H:i:s') <= '2022-05-06 06:59:59')
    <div class="swipe-up-ad hidden">    
        <div class="inner-swipe-up-ad">
            <div class="img-swipe-up-ad">
                <a href="https://www.solopos.com/emagz/lebaran-asyik-soloraya" target="_blank" title="Lebaran Asyik Soloraya 2022"><img loading="lazy" src="https://cdn.solopos.com/banner/lebaran-mobile.png" alt="lebaran soloraya 2022"></a>
        </div>
    </div>
@else
<div class="swipe-up-ad hidden">    
    <div class="inner-swipe-up-ad">
        <div class="img-swipe-up-ad">
            <a href="https://id.solopos.com/login" target="_blank"><img loading="lazy" src="https://cdn.solopos.com/banner/mobile-swipe-banner.jpg" title="Espos Plus"></a>
        </div>
    </div>
</div>    
@endif --}}
{{--@if(Cookie::get('is_login') == null && Cookie::get('is_membership') == null || Cookie::get('is_membership') == 'free')
<div class="adsPushdown">
    <div class="adsPushdown__container">
        <div class="adsPushdown__big">
            <div class="adsPushdown__inner">
                <a href="https://id.solopos.com/login" target="_blank"><img loading="lazy" src="https://cdn.solopos.com/banner/mobile-swipe-banner.jpg" title="Espos Plus" style="width: 360px;height:480px;"></a>
            </div>
        </div>
        <div class="adsPushdown__close" role="button">Tutup</div>
    </div>
    <div class="adsPushdown__swipe">Scroll untuk melanjutkan membaca</div>
</div>
<script>
    var closePushdown=document.querySelector(".adsPushdown__close"),pushdownBanner=document.querySelector(".adsPushdown"),pushdownBannerInner=document.querySelector(".adsPushdown__inner"),adsDownload=document.querySelector(".wrapper__ads-download");null!=closePushdown&&closePushdown.addEventListener("click",function(){pushdownBanner.style.display="none"});    
</script>--}}
@if(date('Y-m-d H:i:s') >= '2022-10-01 00:00:01' && date('Y-m-d H:i:s') <= '2022-10-24 23:59:59' && Cookie::get('is_login') == null && Cookie::get('is_membership') == null || Cookie::get('is_membership') == 'free')
<div class="adsPushdown">
    <div class="adsPushdown__container_dfp">
        <div class="adsPushdown__big_dfp">
            <div class="adsPushdown__inner_dfp">
            <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
                <script>
                window.googletag = window.googletag || {cmd: []};
                googletag.cmd.push(function() {
                    googletag.defineSlot('/54058497/Mobile-Top-Swipe-Banner', [[336, 420], [336, 280], [300, 250], [300, 300]], 'div-gpt-ad-1639723800334-0').addService(googletag.pubads());
                    googletag.pubads().enableSingleRequest();
                    googletag.enableServices();
                });
                </script>
                <!-- /54058497/Mobile-Top-Swipe-Banner -->
                <div id='div-gpt-ad-1639723800334-0' style='min-width: 300px; min-height: 250px;'>
                <script>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1639723800334-0'); });
                </script>
                </div>
            </div>
        </div>
        <div class="adsPushdown__close" role="button">Tutup</div>
    </div>
    <div class="adsPushdown__swipe">Scroll untuk melanjutkan membaca</div>
</div>
<script>
    var closePushdown=document.querySelector(".adsPushdown__close"),pushdownBanner=document.querySelector(".adsPushdown"),pushdownBannerInner=document.querySelector(".adsPushdown__inner"),adsDownload=document.querySelector(".wrapper__ads-download");null!=closePushdown&&closePushdown.addEventListener("click",function(){pushdownBanner.style.display="none"});    
</script>
@elseif(Cookie::get('is_login') == null && Cookie::get('is_membership') == null || Cookie::get('is_membership') == 'free')
<div class="adsPushdown">
    <div class="adsPushdown__container">
        <div class="adsPushdown__big">
            <div class="adsPushdown__inner">
                <a href="https://id.solopos.com/login" target="_blank"><img loading="lazy" src="https://cdn.solopos.com/banner/mobile-swipe-banner.jpg" title="Espos Plus" style="width: 360px;height:480px;"></a>
            </div>
        </div>
        <div class="adsPushdown__close" role="button">Tutup</div>
    </div>
    <div class="adsPushdown__swipe">Scroll untuk melanjutkan membaca</div>
</div>
<script>
    var closePushdown=document.querySelector(".adsPushdown__close"),pushdownBanner=document.querySelector(".adsPushdown"),pushdownBannerInner=document.querySelector(".adsPushdown__inner"),adsDownload=document.querySelector(".wrapper__ads-download");null!=closePushdown&&closePushdown.addEventListener("click",function(){pushdownBanner.style.display="none"});    
</script>
@endif