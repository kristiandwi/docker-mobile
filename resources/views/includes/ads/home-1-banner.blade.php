<!-- Mobile Home 1 Banner -->
<div class="iklan mt-3">
    @if( date('Y-m-d H:i:s') >= '2022-09-14 00:00:01' && date('Y-m-d H:i:s') <= '2022-09-25 23:59:59')
    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <script>
      window.googletag = window.googletag || {cmd: []};
      googletag.cmd.push(function() {
        googletag.defineSlot('/54058497/Mobile-Home-1', [[300, 250], [336, 280], [300, 300]], 'div-gpt-ad-1637758157784-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>
    <!-- /54058497/Mobile-Home-1 -->
    <div id='div-gpt-ad-1637758157784-0' style='min-width: 300px; min-height: 250px;'>
      <script>
        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1637758157784-0'); });
      </script>
    </div>
    @elseif ( date('Y-m-d H:i:s') >= '2022-04-29 22:00:01' && date('Y-m-d H:i:s') <= '2022-09-01 23:59:59')
        <a href="https://m.solopos.com/tag/ekspedisi-umkm-2022" target="_blank" title="Ekspedisi UMKM 2022"><img loading="lazy" src="https://cdn.solopos.com/banner/ekspedisi-umkm-2022-336.jpg?v={{time()}}" alt="Ekspedisi UMKM 2022" onerror="javascript:this.src='https://cdn.solopos.com/banner/ekspedisi-umkm-2022-300.jpg?v={{time()}}'"></a>
    @else
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
      <!-- Iklan Responsif -->
      <ins class="adsbygoogle"
          style="display:block"
          data-ad-client="ca-pub-4969077794908710"
          data-ad-slot="2921244965"
          data-ad-format="auto"
          data-full-width-responsive="true"></ins>
      <script>
          (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
    @endif      
  </div>