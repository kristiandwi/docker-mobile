<!-- Ads Top Editor's Choice -->
<div class="iklan mt-3">
  @if( date('Y-m-d H:i:s') >= '2022-08-24 00:00:01' && date('Y-m-d H:i:s') <= '2022-09-23 23:59:59')
  <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
  <script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
      googletag.defineSlot('/54058497/Mobile-Home-2', [[300, 250], [336, 280], [300, 300]], 'div-gpt-ad-1637827289231-0').addService(googletag.pubads());
      googletag.pubads().enableSingleRequest();
      googletag.enableServices();
    });
  </script>
  <!-- /54058497/Mobile-Home-2 -->
  <div id='div-gpt-ad-1637827289231-0' style='min-width: 300px; min-height: 250px;'>
    <script>
      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1637827289231-0'); });
    </script>
  </div>
  @elseif ( date('Y-m-d H:i:s') >= '2022-04-29 22:00:01' && date('Y-m-d H:i:s') <= '2022-09-15 23:59:59')
  <a href="https://wa.me/6285974110958" target="_blank" title="Commuter RUN 2022"><img src="https://cdn.solopos.com/banner/KAI300x300.jpg?v={{time()}}" width="300px" height="300px" alt="Commuter RUN 2022"></a>
  @else
  <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
  <!-- Iklan Responsif -->
  <ins class="adsbygoogle"
      style="display:block"
      data-ad-client="ca-pub-4969077794908710"
      data-ad-slot="2921244965"
      data-ad-format="auto"
      data-full-width-responsive="true"></ins>
  <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
  </script> 
  @endif        
</div>