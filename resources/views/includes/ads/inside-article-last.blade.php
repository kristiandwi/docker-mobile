<div class="iklan mt-3 mb-3" align="center">
  @if( date('Y-m-d H:i:s') >= '2021-11-24 00:00:01' && date('Y-m-d H:i:s') <= '2021-11-26 23:59:59')
  <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
  <script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
      googletag.defineSlot('/54058497/Mobile-Artikel-LastParagraf', [[300, 250], [336, 280], [300, 300]], 'div-gpt-ad-1637827200208-0').addService(googletag.pubads());
      googletag.pubads().enableSingleRequest();
      googletag.enableServices();
    });
  </script>
  <!-- /54058497/Mobile-Artikel-LastParagraf -->
  <div id='div-gpt-ad-1637827200208-0' style='min-width: 300px; min-height: 250px;'>
    <script>
      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1637827200208-0'); });
    </script>
  </div>

  @else
    <!-- PubMatic ad tag (Javascript) : solopos.com_mobile_320x50 | TADEX_MobileWeb-PT_Aksara_Solopos- | 320 x 50 Mobile -->
    <script type="text/javascript">
      var pubId=157566;
      var siteId=841453;
      var kadId=3732073;
      var kadwidth=320;
      var kadheight=50;
      var kadschain="SUPPLYCHAIN_GOES_HERE";
      var kadUsPrivacy=""; <!-- Insert user CCPA consent string here for CCPA compliant inventory -->
      var kadtype=1;
      var kadGdpr="0"; <!-- set to 1 if inventory is GDPR compliant -->
      var kadGdprConsent=""; <!-- Insert user GDPR consent string here for GDPR compliant inventory -->
      var kadexpdir = '1,2,3,4,5';
      var kadbattr = '8,9,10,11,14';
      var kadifb = 'Dc';
      var kadpageurl= "%%PATTERN:url%%";
    </script>
    <script type="text/javascript" src="https://ads.pubmatic.com/AdServer/js/showad.js"></script><br><br>

    <!-- PubMatic ad tag (Javascript) : solopos.com_Mobile_300x600 | TADEX_MobileWeb-PT_Aksara_Solopos- | 300 x 600 Filmstrip -->
    <script type="text/javascript">
      var pubId=157566;
      var siteId=841453;
      var kadId=4434919;
      var kadwidth=300;
      var kadheight=600;
      var kadschain="SUPPLYCHAIN_GOES_HERE";
      var kadUsPrivacy=""; <!-- Insert user CCPA consent string here for CCPA compliant inventory -->
      var kadtype=1;
      var kGeoLatitude= "";
      var kGeoLongitude= "";
      var kadloc= "";
      var kadGdpr="0"; <!-- set to 1 if inventory is GDPR compliant -->
      var kadGdprConsent=""; <!-- Insert user GDPR consent string here for GDPR compliant inventory -->
      var kadexpdir = '1,2,3,4,5';
      var kadbattr = '8,9,10,11,14';
      var kadifb = 'Dc';
      var kadpageurl= "%%PATTERN:url%%";
    </script>
    <script type="text/javascript" src="https://ads.pubmatic.com/AdServer/js/showad.js"></script>    

    <!-- PubMatic ad tag (Javascript) : solopos.com_desktop_320x250 | TADEX_Web-PT_Aksara_Solopos- | 300 x 250 Sidekick -->
    {{-- <script type="text/javascript">
      var pubId=157566;
      var siteId=841452;
      var kadId=3732071;
      var kadwidth=300;
      var kadheight=250;
      var kadschain="SUPPLYCHAIN_GOES_HERE";
      var kadUsPrivacy=""; <!-- Insert user CCPA consent string here for CCPA compliant inventory -->
      var kadtype=1;
      var kadGdpr="0"; <!-- set to 1 if inventory is GDPR compliant -->
      var kadGdprConsent=""; <!-- Insert user GDPR consent string here for GDPR compliant inventory -->
      var kadexpdir = '1,2,3,4,5';
      var kadbattr = '8,9,10,11,14';
      var kadifb = 'Dc';
      var kadpageurl= "%%PATTERN:url%%";
    </script>
    <script type="text/javascript" src="https://ads.pubmatic.com/AdServer/js/showad.js"></script>  --}}
  @endif         
</div>