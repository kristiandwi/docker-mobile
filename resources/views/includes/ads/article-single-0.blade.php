<div class="iklan mt-3 mb-3" align="center">
  @if( date('Y-m-d H:i:s') >= '2022-03-01 00:00:01' && date('Y-m-d H:i:s') <= '2022-08-17 23:59:59')
  <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
  <script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
      googletag.defineSlot('/54058497/Mobile-Single-0', [[320, 100], [300, 300], [320, 50], [300, 250], [336, 280]], 'div-gpt-ad-1651074268032-0').addService(googletag.pubads());
      googletag.pubads().enableSingleRequest();
      googletag.enableServices();
    });
  </script>
  <!-- /54058497/Mobile-Single-0 -->
  <div id='div-gpt-ad-1651074268032-0' style='min-width: 300px; min-height: 50px;'>
    <script>
      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1651074268032-0'); });
    </script>
  </div>

  @else
  <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2449643854165381" crossorigin="anonymous"></script>
  <!-- Responsive -->
  <ins class="adsbygoogle"
      style="display:block"
      data-ad-client="ca-pub-2449643854165381"
      data-ad-slot="3798913759"
      data-ad-format="auto"
      data-full-width-responsive="true"></ins>
  <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
  </script>
  @endif         
</div>