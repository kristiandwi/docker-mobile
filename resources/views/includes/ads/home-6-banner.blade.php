<div class="iklan mt-3" align="center">
  @if( date('Y-m-d H:i:s') >= '2022-03-01 00:00:01' && date('Y-m-d H:i:s') <= '2022-06-23 23:59:59')
    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <script>
      window.googletag = window.googletag || {cmd: []};
      googletag.cmd.push(function() {
        googletag.defineSlot('/54058497/Mobile-Home-6', [[336, 280], [300, 250], [300, 300], [320, 50]], 'div-gpt-ad-1648710161144-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>
    <!-- /54058497/Mobile-Home-6 -->
    <div id='div-gpt-ad-1648710161144-0' style='min-width: 300px; min-height: 50px;'>
      <script>
        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1648710161144-0'); });
      </script>
    </div>
    @else
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4969077794908710" crossorigin="anonymous"></script>
    <!-- Iklan Responsif -->
    <ins class="adsbygoogle"
        style="display:block"
        data-ad-client="ca-pub-4969077794908710"
        data-ad-slot="2921244965"
        data-ad-format="auto"
        data-full-width-responsive="true"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
    @endif          
  </div>