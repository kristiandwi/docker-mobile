@if( date('Y-m-d H:i:s') >= '2021-11-17 00:00:01' && date('Y-m-d H:i:s') <= '2021-12-11 23:59:59')

<style>
  .fixfloatingads {
    width: 336px;
    display: block;
    left: 0;
    right: 0;
    margin: 0 auto 0 auto;
    position: fixed;
    z-index: 998;
  }
  .closeads {
    padding: 1px 3px;
    font-size: 10px;
    background: rgba(0,0,0,0.5);
    float: right;
    color: #fff;
    text-transform: uppercase;
    font-weight: bold;
    cursor: pointer;
    position: absolute;
    top: 0;
    right: 0;
    z-index: 999;
  }
  .clickarea {
    width: 100%;
    min-height: 280px;
    cursor: pointer;
    position: absolute;
    top: 40px;
    right: 0;
    z-index: 999;
  }
  </style>

<div class="fixfloatingads" id="fixfloatingads" style="margin-top: 50px;">
<span class="closeads" onclick="closeads();">[ X ] CLOSE </span>

  <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
  <script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
      googletag.defineSlot('/54058497/PopUpBanner336x280', [336, 280], 'div-gpt-ad-1633410395809-0').addService(googletag.pubads());
      googletag.pubads().enableSingleRequest();
      googletag.enableServices();
    });
  </script>
  <!-- /54058497/PopUpBanner336x280 -->
  <div id='div-gpt-ad-1633410395809-0' style='min-width: 336px; min-height: 280px;'>
    <script>
      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1633410395809-0'); });
    </script>
  </div>

</div>
<script>
   function closeads()
    {
        var element = document.getElementById("fixfloatingads");
        element.parentNode.removeChild(element);
    }
    var delay = 15000;
    setTimeout(closeads, delay);        
</script>

@endif