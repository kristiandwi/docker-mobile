    <?php //include (TEMPLATEPATH . '/ads/ads-footer.php'); ?>

    @if(!empty($is_bob) AND $is_bob == 'yes')

      @include('includes.tematik.footer-bob')

	  @elseif(!empty($is_uksw) AND $is_uksw == 'yes')

      @include('includes.tematik.footer-uksw')

    @elseif(!empty($is_bugar) AND $is_bugar == 'yes')

      @include('includes.tematik.footer-bugar')

    @elseif(!empty($is_ubah) AND $is_ubah == 'yes')

      @include('includes.tematik.footer-ubahlaku')

    @else

    <div class="container">
        <div class="border-top"></div>
    </div>

    <!-- Footer Nav-->
    <div class="footer-nav-area" id="footerNav">
      <div class="spos-footer-nav h-100">
        <ul class="h-100 d-flex align-items-center justify-content-between">
          <li>
          	<a href="https://interaktif.solopos.com/" target="_blank">
          		{{-- <img src="{{ url('images/logo-interaktif.png') }}" width="32px" style="margin-top:10px;"> --}}
              <img loading="lazy" src="{{ url('images/logo-interaktif.png') }}" width="24px" style="margin-top:7px;margin-bottom:5px;"> <span>INTERAKTIF</span>
          	</a>
          </li>
          <li>
          	<a href="/terpopuler">
          		<img loading="lazy" src="{{ asset('images/icon_trending.png') }}"><!--i class="lni lni-bolt-alt"></i--><span>TRENDING</span>
          	</a>
          </li>
          <li>
          	<a href="/plus">
          		<img loading="lazy" src="{{ asset('images/icon_plus.png') }}" width="42px" style="padding-top:3px;padding-bottom:4px;"><!--i class="lni lni-star-filled"></i--><span>+PLUS</span>
          	</a>
          </li>
          <li>
          	<a href="/soloraya">
          		<img loading="lazy" src="{{ asset('images/icon_soloraya.png') }}" width="32px"><!--i class="lni lni-shortcode"></i--><span>SOLORAYA</span>
          	</a>
          </li>
          <li>
          	<a href="/arsip">
          		<img loading="lazy" src="{{ asset('images/icon_index.png') }}" width="32px"><!--i class="lni lni-archive"></i--><span>INDEKS</span>
          	</a>
          </li>
        </ul>
      </div>
    </div>

    @endif

    <div id="footer">
      <div class="footer">
        <div class="container">
          <div class="footer__logo">
            <img loading="lazy" class="footer-logo-image" style="filter: saturate(10%);" src="{{ asset('images/logo-solopos.png') }}" alt="Solopos.com">
          </div>
          <h2 class="footer-title">Jaringan</h2>
          <ul class="footer--nav-list">
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://www.bisnis.com" title="Bisnis.com" target="_blank" rel="noopener noreferrer">
                <span class="icon i-bisnis"></span>
                <span class="label">Bisnis.com</span>
              </a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://www.harianjogja.com" title="Harianjogja.com" target="_blank" rel="noopener noreferrer">
                <span class="icon i-harianjogja"></span>
                <span class="label">Harianjogja.com</span>
              </a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://bisnismuda.id" title="Bisnismuda.id" target="_blank" rel="noopener noreferrer">
                <span class="icon i-bisnis-muda"></span>
                <span class="label">Bisnismuda.id</span>
              </a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://www.soloposfm.com" title="Soloposfm.com" target="_blank" rel="noopener noreferrer">
                <span class="icon i-soloposfm"></span>
                <span class="label">Soloposfm.com</span>

              </a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://www.semarangpos.com" title="Semarangpos.com" target="_blank" rel="noopener noreferrer">
                <span class="icon i-semarangpos"></span>
                <span class="label">Semarangpos.com</span>

              </a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://www.ibukotakita.com" title="Ibukotakita.com" target="_blank" rel="noopener noreferrer">
                <span class="icon i-ibukotakita"></span>
                <span class="label">Ibukotakita.com</span>

              </a>
            </li>
            <!-- <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://www.jeda.id" title="Jeda.id" target="_blank" rel="noopener noreferrer">
                <span class="icon i-jeda"></span>
                <span class="label">Jeda.id</span>

              </a>
            </li> -->

            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://www.madiunpos.com" title="Madiunpos.com" target="_blank" rel="noopener noreferrer">
                <span class="icon i-madiunpos"></span>
                <span class="label">Madiunpos.com</span>

              </a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://www.pisalin.com" title="Toko Solopos" target="_blank" rel="noopener noreferrer">
                <span class="icon i-pisalin"></span>
                <span class="label">Pisalin.com</span>

              </a>
            </li>
            <!-- <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://www.solografika.co.id" title="Solografika.co.id" target="_blank" rel="noopener noreferrer">
                <span class="icon i-solo-grafika"></span>
                <span class="label">Solografika.co.id</span>

              </a>
            </li> -->
          </ul>
          <h2 class="footer-title">Informasi</h2>
          <ul class="footer--nav-list">
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/page/about-us" title="Tentang Kami">Tentang Kami</a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/page/copyright" title="Copyright">Copyright</a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/page/kontak" title="Kontak">Kontak</a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/mediakit" title="Media Kit">Media Kit</a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/page/about-us" title="Redaksi">Redaksi</a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/page/code-of-conduct" title="Pedoman Media Siber">Pedoman Media Siber</a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/loker" title="Karir">Karir</a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/page/privacy-policy" title="Privacy Policy">Privacy Policy</a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/cekfakta" title="Metode Cek Fakta">Cek Fakta</a>
            </li>
            <li class="footer--nav-list__item">
              <a class="footer--nav-list__link" href="https://m.solopos.com/info-iklan" title="Info Iklan">Info Iklan</a>
            </li>
          </ul>
        </div>
        <div class="footer__copyright">
          <p> Copyright &copy; 2007-<?php echo date("Y");?> <a href="https://www.solopos.com" class="footer__copyright-link">solopos.com</a><br>Media Informasi & Inspirasi. All Rights Reserved </p>
        </div>
      </div>
    </div>

    <?php //include (TEMPLATEPATH . '/navbar-section.php'); ?>
    @include('includes.navbar-section')
    @include('includes.navbar-user-section')

    <!-- All JavaScript Files-->
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0/jquery.sticky.min.js"></script>
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <script src="{{ asset('js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.animatedheadline.min.js') }}"></script>
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/default/date-clock.js') }}"></script>
    <script src="{{ asset('js/default/dark-mode-switch.js') }}"></script>
    <script src="{{ asset('js/default/active.js') }}?v={{time()}}"></script>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe loading="lazy" src="https://www.googletagmanager.com/ns.html?id=GTM-M9K2TV3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    @stack('custom-scripts')
