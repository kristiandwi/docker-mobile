<!-- Sidenav Black Overlay-->
<div class="sidenav-black-overlay"></div>
<!-- Side Nav Wrapper-->
<div class="sidenav-wrapper" id="sidenavUserWrapper">
  <div class="container">
    <!-- Settings Wrapper-->
    <div class="settings-wrapper">
        @if(Cookie::get('is_login') == 'true')
        <div class="alert alert-primary alert-dismissable mb-30" style="padding: 5px 10px;">
            <h6 style="margin:0 0 10px 0;font-size:13px;font-weight:400; display:block;">
              <a href="https://id.solopos.com/profile">{{ Helper::greetings() }}, <strong>{{ Cookie::get('is_name') }} !</strong></a>
            </h6>
            <div class="user-meta-data d-flex">
              <div class="user-content" style="padding-left:0;">
                <p style="font-size:12px; line-height:15px;padding-top:10px;color:#555;font-weight:200;">Selamat datang di ekosistem SoloposID. Silakan kelola akun Anda melalui Dashboard SoloposID.</p>
              </div>
              <div class="align-items-center" style="padding-left:5px;">
                <img loading="lazy" src="https://id.solopos.com/images/logo.png" style="width:120px;" alt="Solopos.id">
                <div style="text-align:center;">
                  <button class="btn btn-success btn-sm" style="padding:.25rem 1rem;"><a href="https://id.solopos.com" style="color:#fff !important;">Account</a></button>
                </div>
              </div>
            </div>
          </div>        
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-user"></i><span>Update Profile</span></div>
                    <div class="data-content"><a href="https://id.solopos.com/profile">Update<i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div>         
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-lock"></i><span>Edit Password</span></div>
                    <div class="data-content"><a href="https://id.solopos.com/change_password">Change<i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div> 
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-users"></i><span>Langganan, Dapat Mobil</span></div>
                    <div class="data-content"><a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}">Member<i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div>          
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-bell"></i><span>Notifications</span></div>
                    <div class="data-content">
                        <div class="toggle-button-cover">
                            <div class="button r">
                                <input class="checkbox" type="checkbox" checked>
                                <div class="knobs"></div>
                                <div class="layer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-unlock"></i><span>Logout dari Solopos ID</span></div>
                    <div class="data-content"><a href="https://id.solopos.com/profile?ref={{ request()->fullUrl() }}">Logout<i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div>         
        @else
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-sign-in"></i><span>Punya akun? Silahkan login</span></div>
                    <div class="data-content"><a class="pl-4" href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}">Login<i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div>
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-address-card"></i><span>Daftar sekarang...</span></div>
                    <div class="data-content"><a class="pl-4" href="https://id.solopos.com/register">dapat mobil!<i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div>
        @endif        
        <div class="card mb-1">
            <div class="card-body">
                <!-- Single Settings-->
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-support"></i><span>Support - FaQ</span></div>
                    <div class="data-content"><a class="pl-4" href="{{ url('/page/help') }}">help<i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div>        
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-shield"></i><span>Privacy Policy</span></div>
                    <div class="data-content"><a class="pl-4" href="{{ url('/page/privacy-policy') }}"><i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div>
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-address-book"></i><span>Tentang Kami</span></div>
                    <div class="data-content"><a class="pl-4" href="{{ url('/page/about-us') }}"><i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div>  
        <div class="card mb-1">
            <div class="card-body">
                <div class="single-settings d-flex align-items-center justify-content-between">
                    <div class="title"><i class="fa fa-phone"></i><span>Kontak Kami</span></div>
                    <div class="data-content"><a class="pl-4" href="{{ url('/page/kontak') }}"><i class="lni lni-chevron-right"></i></a></div>
                </div>
            </div>
        </div>              
        <div class="card">
            <div class="card-body">
                <!-- Single Settings-->
                <div class="single-settings d-flex align-items-center justify-content-between">
                <div class="title"><i class="fa fa-moon-o"></i><span>Night Mode</span></div>
                <div class="data-content">
                    <div class="toggle-button-cover">
                    <div class="button r">
                        <input class="checkbox" id="darkSwitch" type="checkbox">
                        <div class="knobs"></div>
                        <div class="layer"></div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>         
    </div>
  </div>      
  <!-- Go Back Button-->
  <div class="go-home-btn" id="goHomeBtnUser"><i class="lni lni-arrow-left"></i></div>
</div>  