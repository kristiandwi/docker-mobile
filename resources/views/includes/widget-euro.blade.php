		<!-- Widget Euro -->
		<a href="https://m.solopos.com/tag/euro-2020" style="display: inline-block; width: 100%;">
		<img loading="lazy" src="https://www.solopos.com/assets/ads/euro2020_atas.png" class="visible animated" width="100%"></a>
		<div class="euro-widget">
		  <ul>
			@php $eu_loop = 1; @endphp
			@foreach ($euro as $item)
			@if($eu_loop <=10) 
            <!-- Terkini Post-->
            <li>
			<div class="terkini-post d-flex">
              <div class="post-thumbnail">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                  <img src="{{ $item['images']['thumbnail'] }}" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;">
                </a>
              </div>
              <div class="post-content">
                <a class="post-title" href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a>
				<div class="post-meta d-flex align-items-center">
                  <a href="">{{ $item['category'] }}</a>|<a href="#" style="padding-left:7px;">{{ Helper::time_ago($item['date']) }}</a>
                </div>
              </div>
            </div>
			</li>
            @endif
            @php $eu_loop++ @endphp
            @endforeach 
		  </ul>                                   
        </div>
		<div class="mb-3"></div>
		<div class="euro-copy">
              <a href="https://m.solopos.com/tag/euro-2020">
                <img loading="lazy" src="https://www.solopos.com/assets/ads/euro2020_bawah.png" class="visible animated" width="100%">
              </a>
        </div>


      <style type="text/css">
    	.euro-widget ul {	
		  list-style: none; 
		  margin: 0; 
		  padding: 0; 
		  max-height: 325px; 
		  overflow-y: scroll; 
		  overflow-x: hidden; }
		.euro-widget ul li { 
		  list-style: none; 
		  display: block; 
		  color: blue; 
		  font-weight: bold; 
		  font-family: arial;
		  padding: 5px;
		  line-height: 17px; }
		.euro-copy {
		  border-top: none; }  
      </style>