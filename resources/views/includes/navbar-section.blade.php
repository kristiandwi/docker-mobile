    <!-- Sidenav Black Overlay-->
    <div class="sidenav-black-overlay"></div>
    <!-- Side Nav Wrapper-->
    <div class="sidenav-wrapper" id="sidenavWrapper">
      <!-- Time - Weather-->
      <div class="time-date-weather-wrapper text-center pt-5">
        <div class="search-post-wrapper">
          <div class="container">
            {{-- <h4 class="mb-3 pl-2">Apa yang ingin anda baca?</h4> --}}
            <div class="search-page-form">
              <!-- Search via Voice-->
              <a class="search-via-voice" href="#"><i class="lni lni-mic"></i></a>
              <form action="{{ route('search') }}" method="post">
                  {{ csrf_field() }}
                  <input type="search" class="form-control" name="s" placeholder="Search" value="">
                  <button type="submit"><i class="fa fa-search"></i></button>
              </form>
            </div>
          </div>
        </div>        
        {{-- <div class="time-date">
          <div id="dashboardDate"></div>
          <div class="running-time d-flex justify-content-center">
            <div id="hours"></div><span>:</span>
            <div id="min"></div><span>:</span>
            <div id="sec"></div>
          </div>
        </div>       --}}
      </div>
      <!-- Sidenav Nav-->
      <ul class="sidenav-nav">
        <li>
          <a href="{{ url('/news') }}">
            <i class="lni lni-spellcheck"></i>News
          </a>
        </li>
        <li>
          <a href="{{ url('/bisnis') }}">
            <i class="lni lni-briefcase"></i>Ekbis
          </a>
        </li>        
        <li>
          <a href="{{ url('/soloraya') }}">
            <i class="lni lni-shortcode"></i>Soloraya
          </a>
        </li>   
        <li>
          <a href="{{ url('/sport') }}">
            <i class="lni lni-bi-cycle"></i>Sport
          </a>
        </li> 
        <li>
          <a href="{{ url('/lifestyle') }}">
            <i class="lni lni-island"></i>Lifestyle
          </a>
        </li>  
        <li>
          <a href="{{ url('/jateng') }}">
            <i class="lni lni-map-marker"></i>Jateng
          </a>
        </li>
        <li>
          <a href="{{ url('/jatim') }}">
            <i class="lni lni-map-marker"></i>Jatim
          </a>
        </li>    
        <li>
          <a href="{{ url('/jogja') }}">
            <i class="lni lni-map-marker"></i>Jogja
          </a>
        </li>            
        <li>
          <a href="{{ url('/otomotif') }}">
            <i class="lni lni-car-alt"></i>Otomotif
          </a>
        </li> 
        <li>
          <a href="{{ url('/teknologi') }}">
            <i class="lni lni-cogs"></i>Teknologi
          </a>
        </li>          
        <li>
          <a href="{{ url('/entertainment') }}">
            <i class="lni lni-display-alt"></i>Entertainment
          </a>
        </li>
        <li>
          <a href="{{ url('/plus') }}">
            <i class="lni lni-star"></i>Espos Plus<span class="red-circle ml-2 flashing-effect"></span>
          </a>
        </li>        
        <li>
          <a href="{{ url('/espospedia') }}">
            <i class="lni lni-pallet"></i>Espospedia
          </a>
        </li> 
  
        <li>
          <a href="{{ url('/foto') }}">
            <i class="lni lni-camera"></i>Foto
          </a>
        </li>                                       
   
      </ul>
      <div class="container">
        <div class="border-top pb-3" style="position:relative">
            <div style="position:absolute;top:-9px;font-size:10px;background:#dcebfd;padding-right:5px;padding-left:10px;">Part of Solopos.com</div>
        </div>
      </div> 
      <ul class="sidenav-nav">
        <li>
          <a href="{{ url('/sekolah') }}">
            <i class="lni lni-graduation"></i>Sekolah <span class="ml-2 badge badge-danger">new</span>
          </a>
        </li>
        <li>
          <a href="{{ url('/videos') }}">
            <i class="lni lni-video"></i>Video
          </a>
        </li> 
        <li>
          <a href="{{ url('/jatim') }}">
            <i class="lni lni-list"></i>Madiunpos
          </a>
        </li> 
        <li>
          <a href="{{ url('/uksw') }}">
            <i class="lni lni-graduation"></i>UKSW
          </a>
        </li>           
        <li>
          <a href="{{ url('/jateng/semarang') }}">
            <i class="lni lni-list"></i>Semarangpos
          </a>
        </li> 
        <li>
          <a href="{{ url('/rsjihsolo') }}">
            <i class="lni lni-sprout"></i>Bugar 
            {{-- <span class="ml-2 badge badge-warning">12+</span> --}}
          </a>
        </li>         
      </ul>

      {{-- <div class="container">
        <!-- Settings Wrapper-->
        <div class="settings-wrapper">
          <div class="card settings-card">
            <div class="card-body">
              <!-- Single Settings-->
              <div class="single-settings d-flex align-items-center justify-content-between">
                <div class="title"><i class="lni lni-night"></i><span>Night Mode</span></div>
                <div class="data-content">
                  <div class="toggle-button-cover">
                    <div class="button r">
                      <input class="checkbox" id="darkSwitch" type="checkbox">
                      <div class="knobs"></div>
                      <div class="layer"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card settings-card">
            <div class="card-body">
              <!-- Single Settings-->
              <div class="single-settings d-flex align-items-center justify-content-between">
                <div class="title"><i class="lni lni-alarm"></i><span>Notifications</span></div>
                <div class="data-content">
                  <div class="toggle-button-cover">
                    <div class="button r">
                      <input class="checkbox" type="checkbox" checked>
                      <div class="knobs"></div>
                      <div class="layer"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>          
          <div class="card settings-card">
            <div class="card-body">
              <!-- Single Settings-->
              <div class="single-settings d-flex align-items-center justify-content-between">
                <div class="title"><i class="lni lni-question-circle"></i><span>Support</span></div>
                <div class="data-content"><a class="pl-4" href="{{ url('/page/kontak') }}"><i class="lni lni-chevron-right"></i></a></div>
              </div>
            </div>
          </div>
          <div class="card settings-card">
            <div class="card-body">
              <!-- Single Settings-->
              <div class="single-settings d-flex align-items-center justify-content-between">
                <div class="title"><i class="lni lni-protection"></i><span>Privacy</span></div>
                <div class="data-content"><a class="pl-4" href="{{ url('/page/privacy-policy') }}"><i class="lni lni-chevron-right"></i></a></div>
              </div>
            </div>
          </div>
          <div class="card settings-card">
            <div class="card-body">
              <!-- Single Settings-->
              <div class="single-settings d-flex align-items-center justify-content-between">
                <div class="title"><i class="lni lni-lock"></i><span>Password<span>Updated 15 days ago</span></span></div>
                <div class="data-content"><a href="#">Change<i class="lni lni-chevron-right"></i></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>       --}}
      <!-- Go Back Button-->
      <div class="go-home-btn" id="goHomeBtn"><i class="lni lni-arrow-left"></i></div>
    </div>  