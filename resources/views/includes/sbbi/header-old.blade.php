<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>SBBI - Solopos.com</title>
		
		<link rel="icon" href="https://cms.solopos.com/elements/themes/desktop/sbbi/img/faviconsbbi.png">
		<!-- CSS FILES -->
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/uikit@latest/dist/css/uikit.min.css">
		<link rel="stylesheet" type="text/css" href="https://cms.solopos.com/elements/themes/desktop/sbbi/css/style.css">
	</head>
	<body>
		<!-- TOP -->
		<div class="top-wrap uk-position-relative uk-light uk-background-secondary">
			
			<!-- NAV -->
			<div class="nav" data-uk-sticky="cls-active: uk-background-secondary uk-box-shadow-medium; top: 100vh; animation: uk-animation-slide-top">
				<div class="uk-container">
					<nav class="uk-navbar uk-navbar-container uk-navbar-transparent" data-uk-navbar>
						<div class="uk-navbar-left">
							<div class="uk-navbar-item uk-padding-remove-horizontal">
								<a class="uk-logo" title="Logo" href="https://m.solopos.com/sbbi"><img src="https://cdn.solopos.com/sbbi/2022/logo.png" alt="Logo"></a>
							</div>
						</div>
						<div class="uk-navbar-right">
							<ul class="uk-navbar-nav uk-visible@s">
								<li class="uk-active uk-visible@m"><a href="{{ url('/') }}/sbbi" data-uk-icon="home"></a></li>
								<li><a href="{{ url('/') }}/sbbi/tentang-sbbi-award-2022">Tentang SBBI</a></li>
								<li><a href="{{ url('/') }}/sbbi/tentang-imab">Tentang IMAB</a></li>
								<li><a href="{{ url('/') }}/sbbi/event">Event</a></li>
								<li><a href="{{ url('/') }}/sbbi#news">Informasi</a></li>
								<li><a href="{{ url('/') }}/sbbi/pemenang-2021">Pemenang</a></li>
								<!--li><a href="https://www.solopos.com/sbbi/partner">Partner</a></li-->
							</ul>
							<a class="uk-navbar-toggle uk-navbar-item uk-hidden@s" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav"></a>
						</div>
					</nav>
				</div>
			</div>
			<!-- /NAV -->