      <!-- Item Slides Wrapper-->
      <div class="container">
          <!--img src="{{ asset('banner/energi-widget-atas.gif') }}"-->
            <a href="./tag/Ekspedisi-Energi-2021"><img src="{{ ('./banner/energi-widget-atas.gif') }}"></a>
      </div>       
      <div class="item-wrapper mt-3 mb-3 ">       
        <div class="container">
          <!-- Catagory Slides-->
          <div class="item-slides owl-carousel">   
            @php $no = 1; @endphp
            @foreach ($widget as $item)
            @if($no <=10)                      
            <div class="item">
            <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
                <div class="item-thumbnail">
                  <img loading="lazy" src="{{ $item['images']['thumbnail'] }}" loading="lazy" alt="{{ $item['title'] }}" style="object-fit: cover; width: 195px; height: 115px;">
                </div> 
                <div class="item-caption">
                  <span class="link-title">{{ $item['title'] }}</span>
                </div>
              </a>
            </div> 
            @endif
            @php $no++ @endphp
            @endforeach                                                          
          </div>
        </div>
      </div> 
      <div class="container">
        <a href="./tag/Ekspedisi-Energi-2021"><img src="{{ ('./banner/energi-widget-bawah.gif') }}"></a>
      </div> 