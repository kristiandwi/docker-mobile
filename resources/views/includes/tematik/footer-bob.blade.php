    <div class="footer-nav-area" id="footerNav">
      <div class="spos-footer-nav h-100">
        <ul class="h-100 d-flex align-items-center justify-content-between">
          <li><a href="/wisata-joglosemar/artikel"><i class="lni lni-library"></i><span>BERITA</span></a></li>
          <li><a href="/wisata-joglosemar/grafis"><i class="lni lni-crown"></i><span>GRAFIS</span></a></li>
          <li><a href="/wisata-joglosemar/foto"><i class="lni lni-image"></i><span>GALERI FOTO</span> </a></li> 
          <li><a href="/wisata-joglosemar/video"><i class="lni lni-video"></i><span>GALERI VIDEO</span> </a></li>       
          <li><a href="/wisata-joglosemar/kontak"><i class="lni lni-question-circle"></i><span>KONTAK KAMI</span></a></li>
        </ul>
      </div>
    </div>