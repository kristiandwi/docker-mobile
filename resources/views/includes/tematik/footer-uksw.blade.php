    <div class="footer-nav-area" id="footerNav" style="background: #ffc107;">
      <div class="spos-footer-nav h-100">
        <ul class="h-100 d-flex align-items-center justify-content-between">
          <li><a href="/uksw/berita"><i class="lni lni-library"></i><span>BERITA</span></a></li>
          <li><a href="/uksw/prestasi"><i class="lni lni-crown"></i><span>PRESTASI</span></a></li>
          <li><a href="/uksw/foto"><i class="lni lni-image"></i><span>GALERI FOTO</span> </a></li> 
          <li><a href="/uksw/video"><i class="lni lni-video"></i><span>GALERI VIDEO</span> </a></li> 		  
          <li><a href="/uksw/tanya-jawab"><i class="lni lni-question-circle"></i><span>TANYA JAWAB</span></a></li>
        </ul>
      </div>
    </div>