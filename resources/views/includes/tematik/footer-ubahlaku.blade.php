    <div class="footer-nav-area" id="footerNav">
      <div class="spos-footer-nav h-100">
        <ul class="h-100 d-flex align-items-center justify-content-between">
          <li><a href="/ubahlaku/news"><i class="lni lni-library"></i><span>BERITA</span></a></li>
          <li><a href="/ubahlaku/cek-fakta"><i class="lni lni-crown"></i><span>CEK FAKTA</span></a></li>
          <li><a href="/ubahlaku/info-grafis"><i class="lni lni-image"></i><span>INFOGRAFIS</span> </a></li> 
          <li><a href="/ubahlaku/video"><i class="lni lni-video"></i><span>GALERI VIDEO</span> </a></li> 		  
          <li><a href="/ubahlaku/faq"><i class="lni lni-question-circle"></i><span>F.A.Q</span></a></li>
        </ul>
      </div>
    </div>