@if( date('Y-m-d H:i:s') >= '2022-05-02 12:00:01' && date('Y-m-d H:i:s') <= '2022-09-27 23:59:59')
<div class="terkini-post d-flex">
  <div class="post-thumbnail">
    <a href="https://m.solopos.com/sewa-mobil-listrik-bisa-sampai-ke-danau-toba-loh-1426136" title="Sewa Mobil Listrik Bisa Sampai ke Danau Toba Loh!">
      <img src="https://images.solopos.com/2022/09/sewa-mobil-listrik-TRAC-danau-toba-100x100.jpg" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
    </a>
  </div>
  <div class="post-content">
    <a class="post-title" href="https://m.solopos.com/sewa-mobil-listrik-bisa-sampai-ke-danau-toba-loh-1426136" title="Sewa Mobil Listrik Bisa Sampai ke Danau Toba Loh!">Sewa Mobil Listrik Bisa Sampai ke Danau Toba Loh!</a>
    <div class="post-meta d-flex align-items-center">
      <a href="">Otomotif</a>|<a href="#" style="padding-left:7px;"> 19 September 2022</a>
    </div>
  </div>
</div>
@endif

@if( date('Y-m-d H:i:s') >= '2022-04-17 16:00:00' && date('Y-m-d H:i:s') <= '2022-09-28 23:59:59')
<div class="terkini-post d-flex">
  <div class="post-thumbnail">
    <a href="https://m.solopos.com/tips-sewa-bus-pariwisata-biar-liburan-gak-gagal-total-1426180" title="Tips Sewa Bus Pariwisata, Biar Liburan Gak Gagal Total">
      <img src="https://images.solopos.com/2022/09/sewa-bus-pariwisata-TRAC-100x100.jpg" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
    </a>
  </div>
  <div class="post-content">
    <a class="post-title" href="https://m.solopos.com/tips-sewa-bus-pariwisata-biar-liburan-gak-gagal-total-1426180" title="Tips Sewa Bus Pariwisata, Biar Liburan Gak Gagal Total">Tips Sewa Bus Pariwisata, Biar Liburan Gak Gagal Total</a>
    <div class="post-meta d-flex align-items-center">
      <a href="">Otomotif</a>|<a href="#" style="padding-left:7px;"> 20 September 2022</a>
    </div>
  </div>
</div>
@endif

@if( date('Y-m-d') == '2022-04-04' or date('Y-m-d') == '2022-04-10' or date('Y-m-d') == '2022-04-17' or date('Y-m-d') == '2022-04-25')
<div class="terkini-post d-flex">
  <div class="post-thumbnail">
    <a href="https://m.solopos.com/dynamix-hadirkan-3-pilihan-produk-semen-sesuai-kebutuhan-1287758" title="Dynamix Hadirkan 3 Pilihan Produk Semen Sesuai Kebutuhan">
      <img src="https://images.solopos.com/2022/04/bc-dynamix-100x100.jpg" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
    </a>
  </div>
  <div class="post-content">
    <a class="post-title" href="https://m.solopos.com/dynamix-hadirkan-3-pilihan-produk-semen-sesuai-kebutuhan-1287758" title="Dynamix Hadirkan 3 Pilihan Produk Semen Sesuai Kebutuhan">Dynamix Hadirkan 3 Pilihan Produk Semen Sesuai Kebutuhan</a>
    <div class="post-meta d-flex align-items-center">
      <a href="">Bisnis</a>|<a href="#" style="padding-left:7px;"> 03 April 2022</a>
    </div>
  </div>
</div>
@endif

@if( date('Y-m-d') <= '2022-04-11')
<div class="terkini-post d-flex">
  <div class="post-thumbnail">
    <a href="https://m.solopos.com/sewa-bus-kini-bisa-online-lebih-mudah-dan-banyak-pilihan-1288106" title="Sewa Bus Kini Bisa Online, Lebih Mudah dan Banyak Pilihan!">
      <img src="https://images.solopos.com/2022/04/bus-100x100.jpg" loading="lazy" style="object-fit: cover; height: 100px; width: 100px;" onerror="javascript:this.src='https://m.solopos.com/images/no-thumb.jpg'">
    </a>
  </div>
  <div class="post-content">
    <a class="post-title" href="https://m.solopos.com/sewa-bus-kini-bisa-online-lebih-mudah-dan-banyak-pilihan-1288106" title="Sewa Bus Kini Bisa Online, Lebih Mudah dan Banyak Pilihan!">Sewa Bus Kini Bisa Online, Lebih Mudah dan Banyak Pilihan!</a>
    <div class="post-meta d-flex align-items-center">
      <a href="">Otomotif</a>|<a href="#" style="padding-left:7px;"> 04 April 2022</a>
    </div>
  </div>
</div>
@endif