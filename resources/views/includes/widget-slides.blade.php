@if( date('Y-m-d H:i:s') >= '2022-06-20 00:00:01' && date('Y-m-d H:i:s') <= '2022-07-09 23:59:59')
    <!-- Item Slides Wrapper-->
    <div class="container">
    <a href="https://m.solopos.com/tag/ekspedisi-energi-2022" title="Ekspedisi Energi 2022" target="_blank">
        <img loading="lazy" src="https://cdn.solopos.com/banner/widget/Ekspedisi-Energi-2022-Widget.gif" alt="Ekspedisi Energi 2022" style="display: inline-block; width: 100%; align: center;">
    </a>
    </div>
    <div class="item-wrapper mt-3 mb-3">
      <div class="container">
          <div id="widgetads" class="item-slides owl-carousel">
          
          </div>
      </div>
    </div>

    @push('custom-scripts')
  <script>
    function timeSince(date) {
    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = seconds / 31536000;

    if (interval > 1) {
    return Math.floor(interval) + " tahun yang lalu";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
    return Math.floor(interval) + " bulan yang lalu";
    }
    interval = seconds / 86400;
    if (interval > 1) {
    return Math.floor(interval) + " hari yang lalu";
    }
    interval = seconds / 3600;
    if (interval > 1) {
    return Math.floor(interval) + " jam yang lalu";
    }
    interval = seconds / 60;
    if (interval > 1) {
    return Math.floor(interval) + " menit yang lalu";
    }
    return Math.floor(seconds) + " detik yang lalu";
}

$(document).ready(function() {
        $.ajax({
        type: "GET",
        url: "https://api.solopos.com/api/breaking/tag/posts?tags=792516",
        dataType: "JSON",
        success: function(data) {
            var widgetads = $("#widgetads");

            $.each(data, function(i, item) {
                if(i < 10) {
                  widgetads.append("<div class=\"item\"><a href=\"" + item['slug'] + "-" + item['id'] + "?utm_source=widget\" title=\"" + item['title'] + "\"><div class=\"item-thumbnail\"><img src=\"" + item['featured_image']['thumbnail'] +"\" alt=\"" + item['title'] + "\" loading=\"lazy\" style=\"object-fit: cover; width: 195px; height: 115px;\"></div><div class=\"item-caption\"><span class=\"link-title\">" + item['title'] + "</span></div></a></div>");
                }
            });
        }
    });
});
</script>
@endpush
{{--
<div class="item-wrapper mt-3">       
  <div class="container">
    <!-- Catagory Slides-->
    <div class="item-slides owl-carousel">   
      @php $no = 1; @endphp
      @foreach ($widget2 as $item)
      @if($no <=10)                      
      <div class="item">
      <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ html_entity_decode($item['title']) }}">
          <div class="item-thumbnail">
            <img src="{{ $item['featured_image']['thumbnail'] }}" loading="lazy" alt="{{ html_entity_decode($item['title']) }}" style="object-fit: cover; width: 195px; height: 115px;">
          </div> 
          <div class="item-caption">
            <span class="link-title" style="color: rgb(143 3 57);">{{ html_entity_decode($item['title']) }}</span>
          </div>
        </a>
      </div> 
      @endif
      @php $no++ @endphp
      @endforeach                                                          
    </div>
  </div>
</div>--}}
@endif 