      <!-- Item Slides Wrapper-->
    <div class="container">
      <a href="https://www.sologreatsale.com" title="Solo Great Sale" target="_blank">
        <img loading="lazy" src="{{ ('./banner/solo-great-sale.png') }}" alt="Solo Great Sale">
      </a>
    </div>       
    <div class="item-wrapper mt-3 mb-3 ">       
      <div class="container">
        <!-- Catagory Slides-->
        <div class="item-slides owl-carousel">   
          @php $no = 1; @endphp
          @foreach ($widget as $item)
          @if($no <=10)                      
          <div class="item">
          <a href="{{ url("/{$item['slug']}-{$item['id']}") }}" title="{{ $item['title'] }}">
              <div class="item-thumbnail">
                <img src="{{ $item['images']['thumbnail'] }}" loading="lazy" alt="{{ $item['title'] }}" style="object-fit: cover; width: 195px; height: 115px;">
              </div> 
              <div class="item-caption">
                <span class="link-title" style="color: rgb(143 3 57);">{{ $item['title'] }}</span>
              </div>
            </a>
          </div> 
          @endif
          @php $no++ @endphp
          @endforeach                                                          
        </div>
      </div>
    </div> 
    <div class="container">
      <a href="https://www.sologreatsale.com" title="Solo Great Sale" target="_blank">
        <img loading="lazy" src="{{ ('./banner/solo-great-sale.png') }}" alt="Solo Great Sale">
      </a>
    </div> 