<div style="padding: 5px 10px;">
    <div class="wrapper_widget">

        <div class="widget_header">
            <div>
              {{-- <i class="fa fa-fort-awesome"></i> --}}
              {{-- <img src="https://www.svgrepo.com/show/56174/story.svg" alt="Solopos Stories"> --}}
            </div>
            <div class="widget_header-text">
                {{-- Solopos Stories --}}
            </div>
        </div>

        <ul id="storiesList"></ul>
        {{-- @php $stories_loop = 1; @endphp
        @foreach ($stories as $item)
        @if($stories_loop <=8)   
        @if ($stories_loop==1)      
        <div class="widget_thumbnail">
            <img src="{{ $item['featured_image']['medium'] }}" alt="{{ $item['title'] }}">
            <div class="widget_thumbnail-text">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}?utm_source=widget_solopos.com">
                    <div class="widget_list-news-title">{{ $item['title'] }}</div>
                </a>
            </div>
        </div>
        @else
        <div class="widget_list-news">
            <div class="widget_list-news-title">
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}?utm_source=widget_solopos.com">
                    {{ $item['title'] }}
                </a>
            </div>
        </div>
        @endif

        @endif
        @php $stories_loop++ @endphp
        @endforeach  --}}
        <div class="widget_button">
            <a href="https://www.solopos.com/tag/solopos-stories" class="btn btn-widget" target="_blank">Lihat Selengkapnya</a>
        </div>

    </div>
  </div>  
  
  <style>
    .wrapper_widget{
      background-image:url(https://cdn.solopos.com/misc/solopos-stories.png);background-size:cover;background-repeat:no-repeat;
      /* background: linear-gradient(to bottom, rgb(0 67 125) 0%,rgba(32, 173, 255, 1) 100%); */
      border-radius: 10px 10px 0 0;}
    .widget_header{height:60px;display:flex;justify-content:center;padding-top:15px;padding-bottom: 20px;}
    .widget_header i {font-size: 19px;color: #fff;padding-top: 3px;}
    .widget_header-text{margin-left:10px;font-size:18px;font-weight:500;color:#fff;text-transform: uppercase;}
    .widget_thumbnail img{width:100%}
    .widget_thumbnail{position:relative;margin-top:3px;margin-bottom:12px}
    .widget_list-news{margin-left:15px;margin-right:15px; border-bottom: 1px solid #c0d0df}
    .widget_border-line{border-top:1px solid #66c8de;margin-bottom:8px}
    .widget_list-news-title {font-size:13px;font-weight:500;color:#fff;padding:11px 0;margin:0;line-height: 18px !important;}
    .widget_list-news-title a{color:#00437d}
    .widget_list-news-datetime{font-size:10px;font-weight:400;color:#fff;font-style:italic;padding-bottom:10px;margin:0}
    .widget_list-news-datetime.white{color:#fff}
    .widget_button{position: absolute;max-width: calc(100% - 20px);width: 100%;}
    .btn-widget{background:#00437d;border-radius:0 0 10px 10px;width:100%;font-size:12px;color:#fff;padding:15px 0}
    .btn-widget:hover{color:#104471;background:#fff;border:1px solid #104471}
    .widget_thumbnail-text{position:absolute;bottom:0;padding:10% 15px 0;background: linear-gradient(181deg, #0f76bc00, #00437d);}
    a .widget_list-news-title:hover{color:#104471}
    a .widget_list-news-title.white:hover{color:#ffce21}        
  </style>
<script>
    $(document).ready(function() {
        $.ajax({ //create an ajax request related post
        type: "GET",
        url: "https://api.solopos.com/api/wp/v2/posts?tags=782886&per_page=8", //72325
        dataType: "JSON",
        success: function(data) {
            var storiesLists = $("#storiesList");
            $.each(data, function(i, item) {
                if(i == 0) {
                    storiesLists.append("<div class=\"widget_thumbnail\"><img loading=\"lazy\" src=\"" + data[i]['one_call']['featured_list']['media_details']['sizes']['medium']['source_url'] +"\" alt=\"" + data[i]['title']['rendered'] + "\" ><div class=\"widget_thumbnail-text\"><a href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"" + data[i]['title']['rendered'] + "\"><div class=\"widget_list-news-title\">" + data[i]['title']['rendered'] + "</div></a></div></div>");
                } else {
                    storiesLists.append("<div class=\"widget_list-news\"><div class=\"widget_list-news-title\"><a href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "\" title=\"\">"+ data[i]['title']['rendered'] + "</a></div></div>");
                }
            });
            }
        });
    });
</script>