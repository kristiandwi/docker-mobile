      <!-- Promo Wrapper-->
      <div class="container mt-5">
          <div class="d-flex align-items-center justify-content-between mb-3">
          <div class="badge badge-danger">Promo & Events</div>
          </div>
          <!-- Default Carousel-->
          <div class="carousel slide promosi" id="carouselExampleIndicators" data-ride="carousel">
            <!-- Indicators-->
            <ol class="carousel-indicators">
              <li class="active" data-target="#carouselExampleIndicators" data-slide-to="0"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              @if( date('Y-m-d H:i:s') >= '2022-06-20 15:00:01' && date('Y-m-d H:i:s') <= '2022-06-26 20:59:59')
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              @endif
			  <!--li data-target="#carouselExampleIndicators" data-slide-to="3"></li-->
            </ol>
            <!-- Carousel Inner-->
            <div class="carousel-inner">
			    <!-- <div class="carousel-item " style='align: center;'>
                    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
                    <script>
                    window.googletag = window.googletag || {cmd: []};
                    googletag.cmd.push(function() {
                        googletag.defineSlot('/54058497/Mobile-Promosi-Widget', [340, 125], 'div-gpt-ad-1637985939917-0').addService(googletag.pubads());
                        googletag.pubads().enableSingleRequest();
                        googletag.enableServices();
                    });
                    </script>-->
                    <!-- /54058497/Mobile-Promosi-Widget -->
                     <!--<div id='div-gpt-ad-1637985939917-0' style='height: 125px; width: 100%;'>
                        <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1637985939917-0'); });
                        </script>
                    </div>
                </div>	-->	
                @if( date('Y-m-d H:i:s') >= '2022-06-20 15:00:01' && date('Y-m-d H:i:s') <= '2022-06-26 20:59:59')
                <div class="carousel-item active">
                    <a href="https://bit.ly/SoloKeren2" title="Solo Keren #2" target="_blank">
                    <img loading="lazy" class="d-block w-100" src="https://cdn.solopos.com/banner/solokeren-340.jpg?v={{time()}}">
                    </a>
                </div>
                @endif
                @if( date('Y-m-d H:i:s') >= '2022-06-20 15:00:01' && date('Y-m-d H:i:s') <= '2022-06-26 20:59:59')
                <div class="carousel-item">
                @else
                <div class="carousel-item active">
                @endif
                    <a href="https://www.instagram.com/reel/CfgoN0Rp8do/?igshid=YmMyMTA2M2Y=" title="Pisalin" target="_blank">
                    <img loading="lazy" class="d-block w-100" src="https://cdn.solopos.com/banner/pisalin/pisalin-340.jpg?v={{time()}}">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="https://ayampanggangsolo.com " target="_blank" title="Promo dan Event">
                    <img loading="lazy" class="d-block w-100" src="{{ url('banner/ayam-silaturahmi.png') }}" alt="Promo dan Event">
                    </a>
                </div>	  
            </div>
          </div>         
      </div>