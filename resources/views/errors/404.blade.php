<!DOCTYPE html>
<html lang="id-ID">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta name="googlebot" content="index,follow,all" />
        <meta name="Bingbot" content="index, follow">
        <meta name="copyright" content="Solopos Digital Media" /> 
        <meta name="robots" content="index,follow,noodp,noydir" />
        <meta name="google-site-verification" content="AA4UjbZywyFmhoSKLELl4RA451drENKllt5Sbq9uINw" />
        <meta name="yandex-verification" content="7966bb082003f8ae" />
        <meta name="msvalidate.01" content="F2320220951CEFB78E7527ECC232031C" />
        <meta name='dailymotion-domain-verification' content='dm0w2nbrkrxkno2q2' />
        <meta property="fb:pages" content="395076396062" />
        <meta name="language" content="id" />
        <meta name="geo.country" content="id" />
        <meta http-equiv="content-language" content="In-Id" />
        <meta name="geo.placename" content="Indonesia" />
        <meta name="showus-verification" content="pub-2627" />
        <meta property="dable:item_id" content="2015111202028">
        <meta property="article:section" content="{{ $header['category'] ?? '' }}">   

        <title>404 Halaman Tidak Ditemukan - Solopos.com - Media Informasi dan Inspirasi</title> 
                
        <!-- Favicon-->
        <link rel="icon" href="{{ asset('images/favicon.ico') }}">
        <!-- Stylesheet-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" />    
        <link rel="stylesheet" href="https://cdn.lineicons.com/2.0/LineIcons.css">
        <link rel="stylesheet" href="{{ asset('style.css')}}">
  
    </head>
    <body>
        @include('includes.header-area')

        <div class="page-content-wrapper d-flex align-items-center justify-content-center">
            <div class="container">
                <!-- Error Content-->
                <div class="error-content text-center"><img class="mb-3" src="{{ asset('images/404.png') }}" alt="404">
                <h3 class="mb-3">404 !</h3>
                <p class="mb-4">Ooops ! Kelihatannya halaman yang anda tuju tidak ditemukan, silahkan mencari pada form pencarian atau klik tombol di bawah ini.</p><a class="btn btn-primary" href="{{ url('/') }}">Ke Beranda</a>
                </div>
            </div>
        </div>
        
        @include('includes.footer')
    </body>
</html>