<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class UbahlakuController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $slug = $request->segment(1);
        $subSlug = $request->segment(2);

        // Redirect when on desktop device
        // if(Helper::desktop_detect() && !empty($subSlug)) {
        //     return redirect()->away(Config::get('app.desktop_url').'/'.$slug.'/'.$subSlug);
        // } else {
        //     return redirect()->away(Config::get('app.desktop_url').'/'.$slug);
        // }

        $xmlPath = Config::get('xmldata.topic');
        $xmlPathBreak = Config::get('xmldata.breaking');
        
        $headline = Helper::read_xml($xmlPath, 'perilaku-baru');
        $berita = Helper::read_xml($xmlPath, 'perilaku-baru');
        $prestasi = Helper::read_xml($xmlPath, 'perilaku-baru');
        $foto = Helper::read_xml($xmlPath, 'perilaku-baru');
        $cekfakta = Helper::read_xml($xmlPath, 'cek-fakta-corona');
        

        $xmlObject = simplexml_load_file('https://www.youtube.com/feeds/videos.xml?playlist_id=PLMNxf31imllG4UzyrB8vSakTZ980IJV8G');
        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);
        //dd($phpArray);
        $video = $phpArray['entry']; 
        

        
        //dd($yid);

        $is_ubah = 'yes';

        $view = 'pages.ubahlaku';
        $title = 'Ubah Laku #SetopPenularanCovid19';

        if($slug == 'ubahlaku') {
            if ($subSlug == 'galeri') {
                $title = 'Foto';
                $view = 'pages.ubahlaku-galeri';
            }
            if( $subSlug == 'news' ) {
                $title = 'Berita';
                $view = 'pages.ubahlaku-berita';
            }
            if( $subSlug == 'info-grafis' ) {
                $title = 'Info Grafis';
                $view = 'pages.ubahlaku-infografis';
            }
            if( $subSlug == 'cek-fakta' ) {
                $title = 'Cek Fakta';
                $view = 'pages.ubahlaku-cekfakta';
            }
            if( $subSlug == 'video' ) {
                $title = 'Video';
                $view = 'pages.ubahlaku-video';
            }
            if( $subSlug == 'data' ) {
                $title = 'Data Covid-19';
                $view = 'pages.ubahlaku-data';
            }
            if( $subSlug == 'faq' ) {
                $title = 'Tanya Jawab ';
                $view = 'pages.ubahlaku-faq';
            }             
        } else {
            abort(404);
        }

        $header = array(
            'title' => $title,
            'description' => 'Menyajikan berita terpopuler hari ini, berita trending Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'link'  => 'https://m.solopos.com/ubahlaku',
            'canonical'  => 'https://www.solopos.com/ubahlaku',
            'category' => 'Ubah Laku',
            'is_premium' => '',
            'focusKeyword' => 'Perilaku Baru',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',             
        );

        return view($view, ['berita' => $berita, 'headline' => $headline, 'prestasi' => $prestasi, 'foto' => $foto, 'video' => $video, 'cekfakta' => $cekfakta, 'is_ubah' => $is_ubah, 'header' => $header]);
        
    }
}
