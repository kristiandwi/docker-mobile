<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class PageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $pages = array('sbbi');
        $subpages = array('tentang-sbbi-award-2021','event','arsip','pemenang-2020','pemenang-2021','kilas-balik','kontak','partner','ruang-lingkup-penghargaan','tentang','tentang-sda-2021','tentang-imab','tentang-sbbi-award-2022');

        $page = $request->segment(1);
        $subPage = $request->segment(2);

        $res = Http::get('https://api.solopos.com/api/wp/v2/pages?slug='.$subPage);
        $data = $res->json();
        $pageId = $data[0]['id'];
        
        //dd($pageId);
        $subRes = Http::get('https://api.solopos.com/api/wp/v2/pages/'.$pageId);
        $data = $subRes->json();
        // dd($subData);

        $datasbbi = Http::get('https://api.solopos.com/api/breaking/tag/posts?tags=775088'); // tag sbbi 24076 ,775088 SBBI 2022
        $sbbi = $datasbbi->json();
        
        if (empty($data['one_call']['featured_list'])):
            $file_img = 'https://m.solopos.com/images/solopos.jpg';
        else:
            $file_img = $data['one_call']['featured_list']['source_url'];
        endif;

        //dd($file_img);
        // $img_headers = @get_headers($file_img);
        // if($img_headers[0] == 'HTTP/1.1 404 Not Found') {
        //     $image = 'https://www.solopos.com/images/solopos.jpg';
        // }
        // else {
            $image = $file_img;
        // }
        
        if (empty($data['one_call']['post_author']['avatar_url'])):
            $avatar_url = 'https://m.solopos.com/images/solopos.jpg';
        else:
            $avatar_url = $data['one_call']['post_author']['avatar_url'];
        endif;
                    
        
        // $avatar_headers = @get_headers($avatar_url);
        // if($avatar_headers[0] == 'HTTP/1.1 404 Not Found') {
        //     $avatar = 'https://images.solopos.com/2021/02/avatar-100x100.png';
        // }
        // else {
            $avatar = $avatar_url;
        // }

        $content = [
            'id' => $data['id'],
            'date' => $data['date'],
            'title' => $data['title']['rendered'],
            'summary' => $data['content']['summary'] ?? '',
            'content' => $data['content']['rendered'],
            'slug' => $data['slug'],
            'image' => $image,
            'caption' => $data['one_call']['featured_list']['caption'] ?? 'Solopos Digital Media - Panduan Informasi dan Inspirasi',                
            //'image' => 'https://dev.solopos.com/images/solopos.jpg',
            //'caption' => 'Solopos Digital Media',
            // 'category' => $cat_list,
            // 'category_parent' => $cat_list,
            // 'category_child' => $cat_list,
            // 'tag' => $tag_name,
            'author' => $data['one_call']['postmeta']['solopos'][0] ?? 'Redaksi Solopos.com',
            'editor' => $data['one_call']['post_author']['display_name'],
            'avatar' => $avatar,
            // 'editor_url' => '',
        ];
        //dd($content['content']);
        if(in_array($page, $pages)) {
            $title = 'SBBI 2022 - Solo Best Brand Index 2022';
            $view = 'pages.page-sbbi-home';
        } else {
            $title = 'Tentang Kami';
            $view = 'pages.page';            
        }

        if(in_array($subPage, $subpages)) {
            $title = 'Tentang Kami';
            $view = 'pages.page-sbbi-read';
        }        
        // dd($view);

        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url').'/'.$slug.'/'.$subPage);
        // }
        $xmlPath = Config::get('xmldata.breaking');
        $story = Helper::read_xml($xmlPath, 'breaking-story');
                
        // $view = 'pages.page';
        $title = 'Halaman';

        if($page == 'page') {
            if ($subPage == 'about-us') {
                $title = 'Tentang Kami';
                $view = 'pages.page-about';
            }
            if( $subPage == 'kontak' ) {
                $title = 'Kontak Kami';
                $view = 'pages.page-kontak';
            }
            if( $subPage == 'copyright' ) {
                $title = 'Hak Cipta - Copyright';
                $view = 'pages.page-copyright';
            }
            if( $subPage == 'kode-etik' ) {
                $title = 'Kode Etik Jurnalistik';
                $view = 'pages.page-kode-etik';
            }
            if( $subPage == 'privacy-policy' ) {
                $title = 'Privacy Policy';
                $view = 'pages.page-privacy';
            }
            if( $subPage == 'code-of-conduct' ) {
                $title = 'Pedoman Media Siber';
                $view = 'pages.page-pms';
            }            
            if( $subPage == 'help' ) {
                $title = 'Solopos Media Group Care';
                $view = 'pages.page-help';
            }            
        }
        // dd($mysubpage);

        $header = array(
            'title' => $title,
            'description' => 'Portal berita yang menyajikan informasi terhangat baik peristiwa politik, entertainment dan lain lain',
            'category' => 'Pages',
            'is_premium' => '',
            'focusKeyword' => $title,
            'link'  => 'https://m.solopos.com',
            'canonical'  => 'https://www.solopos.com',
            'image' => 'https://m.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
        );

        return view($view, ['data' => $data, 'content' => $content,'page' => $page, 'subpage' => $subPage, 'sbbi' => $sbbi, 'story' => $story, 'header' => $header]);
        
    }
}
