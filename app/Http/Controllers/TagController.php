<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class TagController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $slug)
    {
        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url').'/tag/'.$slug);
        // }

        $amp = $request->segment(3); // AMP slug on 2nd segment URL
        
        $xmlPath = Config::get('xmldata.breaking');
        $res = Http::get('https://api.solopos.com/api/wp/v2/tags?slug='.$slug);

        $tag = $res->json();
        
        if(empty($tag[0])) {
            abort(404);
        }

        $tagId = $tag[0]['id'];
        $tagName = ucwords($tag[0]['name']);
        //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/posts?tags='.$tagId.'&per_page=50');
        $data = Http::get('https://api.solopos.com/api/breaking/tag/posts?tags='.$tagId);
        //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/search?search='.$tagName.'&per_page=50&_embed');
        $video = Helper::read_xml($xmlPath, 'breaking-videos');

        $tags = $data->json();
        //dd($tags);
        // foreach($tags as $e){
        //     $tagList[] = $e['_embedded']['self'][0];
        // }    
        //dd($tagList);
        $header = array(
            'title' => 'Berita '.$tagName.' terbaru, Berita '.$tagName.' hari ini, Arsip Berita ' .$tagName,
            'name' => $tagName,
            'category' => 'Tags',
            'is_premium' => '',
            'description' => 'Kumpulan Berita '.$tagName.' terbaru, Berita '.$tagName.' terkini hari ini, Info '.$tagName.' terbaru',
            'focusKeyword' => 'Berita '.$tagName.' Terbaru Hari ini',
            'link'  => 'https://m.solopos.com/tag/'.$slug,
            'canonical'  => 'https://www.solopos.com/'.$slug,
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita '.$tagName.', Info '.$tagName.', Arsip ' .$tagName,
            'news_keyword' => 'Berita '.$tagName.', Info '.$tagName.', Arsip ' .$tagName,            
        );

        //return $tags;

        $view = 'pages.tag';

        if(!empty($amp)) {
            $view = 'pages.amp-tag';
        }

        return view($view, ['breaking' => $tags, 'video' => $video, 'header' => $header]);
    }
}
