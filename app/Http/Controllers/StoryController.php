<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class StoryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url').'/cekfakta');
        // }
        
        $xmlPath = Config::get('xmldata.breaking');
        
        $data = Helper::read_xml($xmlPath, 'breaking-story');
        //dd($data);
        return view('pages.stories', ['data' => $data]);
    }
}
