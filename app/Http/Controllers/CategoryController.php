<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class CategoryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $list_cat = array('news', 'soloraya', 'lifestyle', 'jatim', 'otomotif', 'entertainment', 'bisnis', 'sport', 'jateng', 'jogja', 'teknologi', 'videos', 'writing-contest', 'loker', 'kolom','cekfakta','jagad-jawa','foto','videos','espospedia','pojokbisnis', 'sekolah' );
        $cat = $request->segment(1);
        $amp = $request->segment(2); // AMP slug on 2nd segment URL

        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url').'/'.$cat);
        // }

        if(!in_array($cat, $list_cat)) {
            // redirect()->action([HomeController::class, 'index']);
            // if(!empty($amp)) {
            //     // redirect('https://m.solopos.com/'.$cat.'/amp');
            //     // redirect()->action(ReadController::class, [$cat]);
            //     redirect()->action([HomeController::class, 'index']);
            // } else {
                // redirect()->action([HomeController::class, 'index']);
                abort(404);
            // }
        }

        $xmlPath = Config::get('xmldata.breaking');
        $xmlPathTopic = Config::get('xmldata.topic');

        $headline = Helper::read_xml($xmlPath, 'breaking-headline');
        $breaking = Helper::read_xml($xmlPath, 'breaking-all');
        $breakingcat = Helper::read_xml($xmlPath, 'breaking-'.$cat);
        $premium = Helper::read_xml($xmlPath, 'breaking-premium');
        // $popular = Helper::read_xml($xmlPath, 'breaking-popular');
        $res = Http::get('https://tf.solopos.com/api/v1/stats/popular/all');
        $data = $res->json();
        $popular = $data['data'];
        $editorchoice = Helper::read_xml($xmlPath, 'breaking-editor-choice');
        $video = Helper::read_xml($xmlPath, 'breaking-videos');
        $wisata = Helper::read_xml($xmlPathTopic, 'wisata-joglosemar');
		$uksw = Helper::read_xml($xmlPathTopic, 'uksw');
        $view = 'pages.category';

        if($cat == 'videos') {
            $view = 'pages.video';
        }
        if($cat == 'foto') {
            $view = 'pages.foto';
        }
        if($cat == 'espospedia') {
            $view = 'pages.espospedia';
        }
        $catTitle = ucwords($cat);
        //$catMeta = $cat;
        $header = array(
            'title' => 'Berita ' .$catTitle. ' terbaru, Berita ' .$catTitle. ' hari ini, Info ' .$catTitle.' terkini',
            'category' => $catTitle,
            'focusKeyword' => 'Berita '.$catTitle.' Terbaru Hari ini',
            'is_premium' => '',
            'description' => 'Berita ' .$catTitle. ' terbaru, Berita ' .$catTitle. ' hari ini, Info ' .$catTitle.' terkini',
            'link'  => 'https://m.solopos.com/'.$cat,
            'canonical'  => 'https://www.solopos.com/'.$cat,
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita ' .$catTitle. ' terbaru, Berita ' .$catTitle. ' hari ini, Info ' .$catTitle.' terkini',
            'news_keyword' => 'Berita ' .$catTitle. ' terbaru, Berita ' .$catTitle. ' hari ini, Info ' .$catTitle.' terkini',
        );

        if(!empty($amp)) {
            $view = 'pages.amp-category';
        }
        $cat2 = lcfirst($cat);
        return view($view, ['category' => $cat2, 'headline' => $headline, 'breaking' => $breaking, 'breakingcat' => $breakingcat, 'premium' => $premium, 'popular' => $popular, 'editorchoice' => $editorchoice, 'video' => $video, 'wisata' => $wisata, 'uksw' => $uksw, 'header' => $header]);
    }
}
