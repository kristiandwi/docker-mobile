<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class EditorController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $xmlPath = Config::get('xmldata.breaking');
        $author = $request->segment(2);
        $amp = $request->segment(3); // AMP slug on 2nd segment URL

        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url').'/author/'.$author);
        // }

        //dd($author);
        $res = Http::get('https://api.solopos.com/api/wp/v2/users?slug='.$author);

        $user = $res->json();
        //dd($user);
        if(empty($user[0])) {
            abort(404);
        }

        $userId = $user[0]['id'];
        $userName = $user[0]['name'];
        $userSlug = $user[0]['slug'];
        $userDesc = $user[0]['description'];
        $userUrl = $user[0]['url'];
        $userAvatar = $user[0]['avatar_urls'][96];
        //dd($userAvatar);
        //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/posts?tags='.$tagId.'&per_page=50');
        $data = Http::get('https://api.solopos.com/api/breaking/author/posts?author='.$userId);
        //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/search?search='.$tagName.'&per_page=50&_embed');
        $video = Helper::read_xml($xmlPath, 'breaking-videos');

        $users = $data->json();
        //dd($users);
        // foreach($tags as $e){
        //     $tagList[] = $e['_embedded']['self'][0];
        // }    
        //dd($tagList);
        if($userDesc == ''):
            $deskripsi = 'Jurnalis di Solopos Group. Menulis konten di Solopos Group yaitu Harian Umum Solopos, Koran Solo, Solopos.com.';
        else:
            $deskripsi = $userDesc;
        endif;
        $userName = ucwords($userName);
        $header = array(
            'title' => 'Arsip Berita '.$userName.' terbaru, Berita '.$userName.' hari ini',
            'description' => $deskripsi,
            'category' => 'Author Pages',
            'is_premium' => '',
            'focusKeyword' => 'Arsip Berita '.$userName,
            'link'  => 'https://m.solopos.com/author/'.$userSlug,
            'canonical'  => 'https://www.solopos.com/'.$userSlug,
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => $userName,
            'author' => $userName,
            'keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'name' => $userName,
            'avatar' => $userAvatar,
            'website' => $userUrl,
        );

        $view = 'pages.penulis';

        if(!empty($amp)) {
            $view = 'pages.amp-penulis';
        }        
        //return $tags;

        return view($view, ['breaking' => $users, 'author' => $author, 'video' => $video, 'header' => $header]);
    }
}
