<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class UkswController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $slug = $request->segment(1);
        $subPage = $request->segment(2);

        // Redirect when on desktop device
        // if(Helper::desktop_detect() && !empty($subPage)) {
        //     return redirect()->away(Config::get('app.desktop_url').'/'.$slug.'/'.$subPage);
        // } else {
        //     return redirect()->away(Config::get('app.desktop_url').'/'.$slug);
        // }

        $xmlPath = Config::get('xmldata.topic');
        
        $headline = Helper::read_xml($xmlPath, 'uksw');
        $berita = Helper::read_xml($xmlPath, 'uksw');
        $prestasi = Helper::read_xml($xmlPath, 'prestasi-uksw');
        $foto = Helper::read_xml($xmlPath, 'foto-uksw-salatiga');
        $tanyajawab = Helper::read_xml($xmlPath, 'uksw');

        $xmlObject = simplexml_load_file('https://www.youtube.com/feeds/videos.xml?channel_id=UCSNKgXlbQ7x0dMQ0UsPi3gw');
        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);
        //dd($phpArray);
        $video = $phpArray['entry']; 
        //dd($yid);

        $is_uksw = 'yes';

        $view = 'pages.uksw';
        $title = 'UKSW Salatiga';

        if($slug == 'uksw') {
            if ($subPage == 'foto') {
                $title = 'Foto UKSW Salatiga';
                $view = 'pages.uksw-foto';
            }
            if( $subPage == 'berita' ) {
                $title = 'Berita UKSW Salatiga';
                $view = 'pages.uksw-berita';
            }
            if( $subPage == 'prestasi' ) {
                $title = 'Prestasi UKSW Salatiga';
                $view = 'pages.uksw-prestasi';
            }
            if( $subPage == 'video' ) {
                $title = 'Video UKSW Salatiga';
                $view = 'pages.uksw-video';
            }
            if( $subPage == 'tanya-jawab' ) {
                $title = 'Tanya Jawab UKSW Salatiga';
                $view = 'pages.uksw-tanya-jawab';
            }
            if( $subPage == 'tanya-jawab' ) {
                $title = 'Tanya Jawab Tentang UKSW Salatiga';
                $view = 'pages.uksw-faq';
            }             
        } else {
            abort(404);
        }

        $header = array(
            'title' => $title,
            'description' => 'Menyajikan berita terpopuler hari ini, berita trending Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'link'  => 'https://m.solopos.com/uksw',
            'canonical'  => 'https://www.solopos.com/uksw',
            'category' => 'UKSW Salatiga',
            'is_premium' => '',
            'focusKeyword' => 'UKSW Salatiga',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',             
        );

        return view($view, ['berita' => $berita, 'headline' => $headline, 'prestasi' => $prestasi, 'foto' => $foto, 'video' => $video, 'tanya-jawab' => $tanyajawab, 'is_uksw' => $is_uksw, 'header' => $header]);
        
    }
}
