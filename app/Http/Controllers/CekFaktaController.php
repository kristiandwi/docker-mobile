<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class CekFaktaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url').'/cekfakta');
        // }
        $item = Http::get('https://api.solopos.com/api/breaking/posts?category=670832');
        $breaking = $item->json();

        $xmlPath = Config::get('xmldata.breaking');
        
        $cekfakta = $breaking; //Helper::read_xml($xmlPath, 'breaking-cekfakta');

        $header = [
            'title' => 'Cek Fakta',
            'description' => 'Menyajikan berita terpopuler hari ini, berita trending Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'link'  => 'https://m.solopos.com/cekfakta',
            'canonical'  => 'https://www.solopos.com/cekfakta',
            'category' => 'Cek Fakta',
            'is_premium' => '',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',             
        ];

        return view('pages.cekfakta', ['cekfakta' => $cekfakta, 'header' => $header]);
    }
}
