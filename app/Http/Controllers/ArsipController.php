<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;

class ArsipController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url').'/arsip');
        // }

        // $uri = $request->thn;
        //dd($uri);
        $xmlPath = Config::get('xmldata.breaking');
        $now = Carbon::now();
        // $y = $request->thn;
        // if(empty($y)) {
        //     $thn = $now->year;
        //     $bln = $now->month;
        //     $tgl = $now->day;
        // }else{
        //     $thn = $request->segment(2);
        //     $bln = $request->segment(3);
        //     $tgl = $request->segment(4);            
        // }   
        //dd($y);
        
        $tgl = $request->tgl ?? $now->day;
        $bln = $request->bln ?? $now->month;
        $thn = $request->thn ?? $now->year;

        // $tagId = $tag[0]['id'];
        // $tagName = $tag[0]['name'];
        //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/posts?tags='.$tagId.'&per_page=50');
        //$data = Http::get('https://cmsx.solopos.com/api/breaking/tag/posts?tags='.$tagId);
        //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/search?search='.$tagName.'&per_page=50&_embed');
        // $res = Http::get('https://api.solopos.com/api/breaking/arsip/posts?year='.$thn.'&month='.$bln.'&day='.$tgl);
  
        $video = Helper::read_xml($xmlPath, 'breaking-videos');

        // $data = $res->json();
        // dd($data);
        // if(empty($data)) {
        //     //abort('404');
        //     return view('errors.empty');
        // }

        // dd($data);

        // $catid = $data[0]['categories'][0] ?? 'News';
        //dd($catid);
        //dd($data);
        // foreach($tags as $e){
        //     $tagList[] = $e['_embedded']['self'][0];
        // }    
        //dd($tagList);
        $header = array(
            'title' => 'Index Berita Berita Hari ini, Berita Terbaru dan Terkini',
            'category' => 'Index Berita',
            'is_premium' => '',
            'focusKeyword' => 'Index Berita Solopos',
            'description' => 'Kumpulan kabar berita Solopos.com situs portal terlengkap yang menyajikan informasi terhangat baik peristiwa politik, entertainment dan lain lain',
            'link'  => 'https://m.solopos.com/arsip',
            'canonical'  => 'https://www.solopos.com/arsip',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',            
        );

        //return $tags;

        return view('pages.arsip', [/*'breaking' => $data,*/ 'thn'=>$thn,'bln'=>$bln,'tgl'=>$tgl, 'video' => $video, 'header' => $header]);
    } 
}
