<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use App\Helpers\Helper;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url'));
        // }

        $amp = $request->segment(1); // AMP slug on 2nd segment URL

        $xmlPath = Config::get('xmldata.breaking');
        
        $headline = Helper::read_xml($xmlPath, 'breaking-headline');
        $breaking = Helper::read_xml($xmlPath, 'breaking-all');
        $premium = Helper::read_xml($xmlPath, 'breaking-premium');
        // $popular = Helper::read_xml($xmlPath, 'breaking-popular');
        // $res = Http::get('https://tf.solopos.com/api/v1/stats/popular/all');
        // $data = $res->json();
        // $popular = $data['data'];
        $editorchoice = Helper::read_xml($xmlPath, 'breaking-editor-choice');
        $kolom = Helper::read_xml($xmlPath, 'breaking-kolom');
        $espospedia = Helper::read_xml($xmlPath, 'breaking-espospedia');
        $jateng = Helper::read_xml($xmlPath, 'breaking-jateng');
        $jatim = Helper::read_xml($xmlPath, 'breaking-jatim');
        $jogja = Helper::read_xml($xmlPath, 'breaking-jogja');
        $video = Helper::read_xml($xmlPath, 'breaking-videos');
        
        $xmlPath2 = Config::get('xmldata.topic');
        $euro = Helper::read_xml($xmlPath2, 'euro-2020'); 
        $widget = Helper::read_xml($xmlPath2, 'Honda-Motor-Jateng');
        $datawidget = Http::get('https://api.solopos.com/api/breaking/tag/posts?tags=781384');
        $widget2 = $datawidget->json();

        $dataStories = Http::get('https://api.solopos.com/api/breaking/tag/posts?tags=782886');
        $widgetStories = $dataStories->json();
        // dd($widget2);
        $view = 'pages.home';

        $header = [
            'title' => 'Berita Hari ini, Berita Terbaru dan Terkini',
            'description' => 'Portal berita yang menyajikan informasi terhangat baik peristiwa politik, entertainment dan lain lain',
            'link'  => 'https://m.solopos.com',
            'canonical'  => 'https://www.solopos.com',
            'category' => 'Front Page',
            'is_premium' => '',
            'focusKeyword' => 'Berita Hari Ini',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',            
        ];

        if(!empty($amp)) {
            $view = 'pages.amp-home';
        }

        return view($view, ['headline' => $headline, 'breaking' => $breaking, 'premium' => $premium, /*'popular' => $popular,*/ 'editorchoice' => $editorchoice, 'kolom' => $kolom, 'espospedia' => $espospedia, 'jateng' => $jateng, 'jatim' => $jatim, 'jogja' => $jogja, 'video' => $video, 'euro' => $euro, 'widget' => $widget, 'widget2' => $widget2, 'stories' => $widgetStories, 'header' => $header]);
    }

    // public function parseXML($path, $filename) {
    //     $xmlString = $path.$filename.'.xml';
    //     $xmlObject = simplexml_load_file($xmlString);

    //     $json = json_encode($xmlObject);
    //     $phpArray = json_decode($json, true);

    //     // dd($phpArray);
    //     $data = $phpArray['post'];

    //     return $data;
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postid = explode('-', $id);
        $xmlPath = Config::get('xmldata.posts');
        $xmlString = $xmlPath.end($postid).'.xml';
        $xmlObject = simplexml_load_file($xmlString);

        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);

        // dd($phpArray);
        $data = $phpArray['posts'];

        return view('pages.read', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
