<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ReporterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Redirect when on mobile device
        // if(Helper::mobile_detect()) {
        //     return redirect()->away(Config::get('app.mobile_url').'/author/'.$request->segment(2));
        // }


        $xmlPath = Config::get('xmldata.breaking');
        $author = $request->segment(2);
        $author_data = str_replace('_', ' ', $author);
        $amp = $request->segment(3); // AMP slug on 2nd segment URL
        //dd($author_data);
        $res = Http::get('https://api.solopos.com/api/data/user?fullname='.$author_data);
        // $res = Http::get('https://api.solopos.com/api/wp/v2/users?search='.$author_data);
        //dd($res);
        $user = $res->json();
        //dd($user);
        // if(empty($user[0])) {
        //     abort(404);
        // }

        // dd($user);
        if(empty($user)):
            $userName = $author_data;
            $userSlug = $author;
            $userDesc = 'Kumpulan artikel '.ucwords($author_data).' yang tayang di solopos.com';
            $userUrl = '';
            $userAvatar = 'https://images.solopos.com/2021/02/avatar-100x100.png';

            $data = Http::get('https://api.solopos.com/api/breaking/penulis/other/source?nama='.$author_data);
        else:
            $userId = $user['id'];
            $userName = $user['name'];
            $userSlug = $user['slug'];
            $userDesc = $user['description'];
            $userUrl = $user['url'];
            $userAvatar = $user['avatar_urls'];
            //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/posts?tags='.$tagId.'&per_page=50');
            $data = Http::get('https://api.solopos.com/api/breaking/penulis/author?author_id='.$userId);
            //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/search?search='.$tagName.'&per_page=50&_embed');
        endif;
        // dd($userId);
        $video = Helper::read_xml($xmlPath, 'breaking-videos');

        $users = $data->json();
        //dd($users);
        // foreach($tags as $e){
        //     $tagList[] = $e['_embedded']['self'][0];
        // }
        //dd($tagList);
        if($userDesc == ''):
            $deskripsi = 'Jurnalis di Solopos Group. Menulis konten di Solopos Group yaitu Harian Umum Solopos, Koran Solo, Solopos.com.';
        else:
            $deskripsi = $userDesc;
        endif;
        $userName = ucwords($userName);
        $header = array(
            'title' => 'Arsip Berita '.$userName.' terbaru, Berita '.$userName.' hari ini',
            'description' => $deskripsi,
            'category' => 'Author Pages',
            'is_premium' => '',
            'focusKeyword' => 'Arsip Berita '.$userName,
            'link'  => 'https://www.solopos.com/penulis/'.$userSlug,
            'image' => $userAvatar,
            'editor' => $userName,
            'author' => $userName,
            'keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'name' => $userName,
            'avatar' => $userAvatar,
            'website' => $userUrl,
        );

        $view = 'pages.penulis';
        
        if(!empty($amp)) {
            $view = 'pages.amp-penulis';
        }
        //return $tags;

        return view($view, ['breaking' => $users, 'author' => $author, 'video' => $video, 'header' => $header]);
    }
}
