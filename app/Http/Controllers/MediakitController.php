<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;


class MediakitController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $slug = $request->segment(1);
        $subSlug = $request->segment(2);

        $view = 'pages.mediakit-home';
        
        if($slug == 'mediakit') {
            if( $subSlug == 'home' ) {
                $view = 'pages.mediakit-home';
            }
            if( $subSlug == 'single' ) {
                $view = 'pages.mediakit-single';
            }
        } else {
            abort(404);
        }

        $header = array(
            'description' => 'Menyajikan berita terpopuler hari ini, berita trending Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'link'  => 'https://m.solopos.com/mediakit',
            'canonical'  => 'https://www.solopos.com/mediakit',
            'category' => 'Mediakit',
            'is_premium' => '',
            'focusKeyword' => 'Mediakit',
            'image' => 'https://m.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya', 
        );

        return view($view, ['header' => $header]);
        
    }
}
