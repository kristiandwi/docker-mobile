<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;


class WritingController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $slug = $request->segment(1);
        $subSlug = $request->segment(2);

        // Redirect when on mobile device
        // if(Helper::mobile_detect() && !empty($subSlug)) {
        //     return redirect()->away(Config::get('app.mobile_url').'/'.$slug.'/'.$subSlug);
        // } else {
        //     return redirect()->away(Config::get('app.mobile_url').'/'.$slug);
        // }

        $xmlPath = Config::get('xmldata.topic');
        $xmlPathBreak = Config::get('xmldata.breaking');
        
        //$headline = Http::get('https://api.solopos.com/api/breaking/posts?category=750705');
        $berita = Http::get('https://api.solopos.com/api/breaking/posts?category=734344');
        $popular = Helper::read_xml($xmlPathBreak, 'breaking-popular');
        $lifestyle = Helper::read_xml($xmlPathBreak, 'breaking-lifestyle');
        $story = Helper::read_xml($xmlPathBreak, 'breaking-story');
        $news = Helper::read_xml($xmlPathBreak, 'breaking-news');
        $kolom = Helper::read_xml($xmlPathBreak, 'breaking-kolom');
        //dd($yid);
        $headline = $berita->json();
   

        $view = 'pages.writing';
        $title = 'Writing Contest - Solopos.com';


        $header = array(
            'title' => $title,
            'description' => 'Menyajikan berita terpopuler hari ini, berita trending Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'link'  => 'https://m.solopos.com/writing-contest',
            'canonical'  => 'https://www.solopos.com/writing-contest',
            'category' => 'Writing Contest',
            'is_premium' => '',
            'focusKeyword' => 'Writing Contest',
            'image' => 'https://m.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya', 
        );

        return view($view, ['berita' => $berita, 'headline' => $headline, 'popular' => $popular, 'lifestyle' => $lifestyle,  'story' => $story, 'news' => $news, 'kolom' => $kolom, 'header' => $header]);
        
    }
}
