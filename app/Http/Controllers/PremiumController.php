<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class PremiumController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $premium_content = 'premium'; //$request->segment(1);

        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url').'/'.$premium_content);
        // }

        $xmlPath = Config::get('xmldata.breaking');
        
        $premium = Helper::read_xml($xmlPath, 'breaking-premium');

        $header = array(
            'title' => 'Espos Plus',
            'category' => 'Espos Plus',
            'is_premium' => '',
            'focusKeyword' => 'Berita Premium',
            'description' => 'Espos Premium adalah artikel yang menyajikan informasi dari fakta dan peristiwa yang diolah serta dipaparkan dengan cara kreatif dan lebih visual.',
            'link'  => 'https://m.solopos.com/plus',
            'canonical'  => 'https://www.solopos.com/plus',
            'image' => 'https://www.solopos.com/images/background-espos-plus.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Espos Premium, Berita Premium, Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Espos Premium, Berita Premium, Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',               
        );

        return view('pages.premium', ['premium' => $premium, 'premium_content' => $premium_content, 'header' => $header]);
    }
}
