<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class EspospediaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Redirect when on desktop device
        // if(Helper::desktop_detect()) {
        //     return redirect()->away(Config::get('app.desktop_url').'/espospedia');
        // }

        $xmlPath = Config::get('xmldata.breaking');
        
        $espospedia = Helper::read_xml($xmlPath, 'breaking-espospedia');

        $header = [
            'title' => 'Arsip Espospedia, Info Grafis',
            'description' => 'Espospedia adalah info grafis yang menyajikan informasi, fakta dan peristiwa yang diolah serta dipaparkan secara kreatif dan visual.',
            'link'  => 'https://m.solopos.com/espospedia',
            'canonical'  => 'https://www.solopos.com/espospedia',
            'category' => 'Espospedia',
            'is_premium' => '',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Espospedia, Info Grafis, Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Espospedia, Info Grafis, Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',             
        ];

        return view('pages.espospedia', ['espospedia' => $espospedia, 'header' => $header]);
    }
}
