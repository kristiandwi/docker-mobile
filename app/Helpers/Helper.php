<?php

namespace App\Helpers;
use Illuminate\Support\Carbon;
use Jenssegers\Agent\Agent;

class Helper {
    // Parsing XML data
    public static function read_xml($path, $filename) {
        $xmlString = $path.$filename.'.xml';
        $xmlObject = simplexml_load_file($xmlString);

        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);

        // dd($phpArray);
        $data = $phpArray['post'];

        return $data;
    }

    public static function time_ago($date) {
        return Carbon::parse($date)->diffForhumans();
    }

    public static function ampify($html='') {

        # Replace img, audio, and video elements with amp custom elements
        $html = str_ireplace(
            ['<img', 'loading="lazy"', '<video','/video>','<audio','/audio>'],
            ['<amp-img', 'layout="responsive"', '<amp-video','/amp-video>','<amp-audio','/amp-audio>'],
            $html
        );

        # Add closing tags to amp-img custom element
        $html = preg_replace('/<amp-img(.*?)>/', '<amp-img$1></amp-img>',$html);
        $html = preg_replace('/<iframe/', '<amp-iframe',$html);
        $html = preg_replace('</iframe>', '</amp-iframe>',$html);
        $html = preg_replace('/onclick="return vz.expand(this)"/', '', $html);
        $html = preg_replace('/cms.solopos.com/', 'm.solopos.com', $html);
        $html = preg_replace('/www.solopos.com/', 'm.solopos.com', $html);
        $html = preg_replace('/alt=""/','alt="Images Solopos.com"',$html);
        $html = preg_replace('/alt="(.*?)"/','alt="Solopos Digital Media"',$html);
        $html = preg_replace('/onclick="(.*?)"/','',$html);
        $html = preg_replace('/aria-describedby="(.*?)"/','',$html);
        $html = preg_replace('/alt=""/','alt="Images Solopos.com"',$html);

        # Whitelist of HTML tags allowed by AMP
        $html = strip_tags($html,'<h1><h2><h3><h4><h5><h6><a><p><ul><ol><li><blockquote><q><cite><ins><del><strong><em><code><pre><svg><table><thead><tbody><tfoot><th><tr><td><dl><dt><dd><article><section><header><footer><aside><figure><time><abbr><div><span><hr><small><br><amp-img><amp-audio><amp-video><amp-ad><amp-anim><amp-carousel><amp-fit-rext><amp-image-lightbox><amp-instagram><amp-lightbox><amp-twitter><amp-youtube>');

        return $html;

    }

    public static function konten($html='') {
        # Add closing tags to amp-img custom element
        $html = preg_replace('/cms.solopos.com/', 'm.solopos.com', $html);
        $html = preg_replace('/www.solopos.com/', 'm.solopos.com', $html);
        //$html = preg_replace('/target="_blank"/','',$html);

        return $html;
    }

    public static function desktop_detect() {
        // $agent = new Agent();

        // // redirect when agent is desktop/tablet device
        // if($agent->isDesktop()) {
        //     return true;
        // }
        return 0;
    }

    public static function greetings() {
        date_default_timezone_set('Asia/Jakarta');
        //Here we define out main variables
        $welcome_string="Halo...!";
        $numeric_date=date("G");

        //Start conditionals based on military time
        if($numeric_date>=0&&$numeric_date<=3)
        $welcome_string="Selamat Dini Hari";
        else if($numeric_date>=4&&$numeric_date<=10)
        $welcome_string="Selamat Pagi";
        else if($numeric_date>=11&&$numeric_date<=14)
        $welcome_string="Selamat Siang";
        else if($numeric_date>=15&&$numeric_date<=18)
        $welcome_string="Selamat Sore";
        else if($numeric_date>=19&&$numeric_date<=23)
        $welcome_string="Selamat Malam";

        return $welcome_string;
    }

    // 2nd param type [full, thumbnail, thumb]
    public static function getImage($image, $type)
    {
        if($type == 'full') {
            $img = str_replace('https://images.solopos.com', 'https://imgproxy.solopos.com/@static', $image);
        } elseif($type == 'thumbnail') {
            $img = str_replace('https://images.solopos.com', 'https://imgproxy.solopos.com/@static/_sm', $image);
        } elseif($type == 'thumb') {
            $img = str_replace('https://images.solopos.com', 'https://imgproxy.solopos.com/@static/_thumb', $image);
        } else {
            $img = str_replace('https://images.solopos.com', 'https://imgproxy.solopos.com/@static', $image);
        }
        return $img;
    }
}
